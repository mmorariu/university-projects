function Lab02_Exercise01()

clc;
clear all;

% spamList = countwords('spam\train');
% hamList = countwords('ham\train');
% save('spamList.mat', 'spamList');
% save('hamList.mat', 'hamList');

load spamList.mat
load hamList.mat
nSpamWords = 20;

[words, counts] = wordtable(spamList, hamList);
nSpamEmails = length(ls('spam\train'));
nHamEmails = length(ls('ham\train'));
nTotalWords = length(words);
counts(:, 1) = counts(:, 1);
counts(:, 2) = counts(:, 2);
counts(:, 3) = counts(:, 1) / nSpamEmails - counts(:, 2) / nHamEmails;
counts(:, 4) = [1:nTotalWords];

counts = sortrows(counts, 3);
counts = flipdim(counts, 1);

% sortedSpams = flipdim(sortedSpams, 1);
% sortedHams = sortrows(counts, 4);
% sortedHams = flipdim(sortedHams, 1);

sSpamWords = words(counts(1:nSpamWords, 4));
cSpamWords = cellstr(sSpamWords');

for i = 1:nSpamWords
    display([sSpamWords(i), '-> ', num2str(counts(i, 1) / nSpamEmails), '%']);
end

spamTestDir = [pwd, '\ham\test\'];
spamTestCollection = ls(spamTestDir);

for i = 3:length(spamTestCollection)
    spamTestFile = spamTestCollection(i, :);
    featVec = presentre([spamTestDir, spamTestFile], cSpamWords);
    prob_C1 = nSpamEmails / (nSpamEmails + nHamEmails);
    prob_C2 = nHamEmails / (nSpamEmails + nHamEmails);
    prob_C1_x = p(featVec, 1, counts, nSpamEmails, nHamEmails) * prob_C1 / (p(featVec, 1, counts, nSpamEmails, nHamEmails) * prob_C1 + p(featVec, 2, counts, nSpamEmails, nHamEmails) * prob_C2);
    display([spamTestCollection(i, :), '-> ', num2str(prob_C1_x * 100), '% chance of being spam.']);
end

end

function [prob_x_Ck] = p(x, k, counts, nSpamEmails, nHamEmails)

nFeatVec = length(x);
prob_x_Ck = 1;

if (k == 1)
    nTotalMails = nSpamEmails;
    prob_Ck = nSpamEmails / (nSpamEmails + nHamEmails);
else
    nTotalMails = nHamEmails;
    prob_Ck = nHamEmails / (nSpamEmails + nHamEmails);
end

for i = 1:nFeatVec
    miu_IK = (1 + counts(i, k)) / (2 + nTotalMails);
%     miu_IK = (counts(i, k) / (nSpamEmails + nHamEmails)) / prob_Ck;
    prob_x_Ck = prob_x_Ck * (miu_IK^x(i)) * (1 - miu_IK)^(1 - x(i));
end

end