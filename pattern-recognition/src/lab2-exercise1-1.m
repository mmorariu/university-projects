function Lab02_Exercise01_1()

clc;
clear all;
close all;

% spamList = countwords('spam\train');
% hamList = countwords('ham\train');
% save('spamList.mat', 'spamList');
% save('hamList.mat', 'hamList');

load spamList.mat
load hamList.mat
nSpamWords = 20;
threshold = 0.70;

[words, counts] = wordtable(spamList, hamList);
nSpamEmails = length(ls('spam\train')) - 2;
nHamEmails = length(ls('ham\train')) - 2;
nTotalWords = length(words);
counts(:, 3) = counts(:, 1) / nSpamEmails - counts(:, 2) / nHamEmails;
counts(:, 4) = [1:nTotalWords];

counts = sortrows(counts, 3);
counts = flipdim(counts, 1);

% sortedSpams = flipdim(sortedSpams, 1);
% sortedHams = sortrows(counts, 4);
% sortedHams = flipdim(sortedHams, 1);

sSpamWords = words(counts(1:nSpamWords, 4));
cSpamWords = cellstr(sSpamWords');

% for i = 1:nSpamWords
%     display([sSpamWords(i), '-> ', num2str(counts(i, 1) / nSpamEmails), '%']);
% end

testDir = [pwd, '\ham\test\'];
testCollection = ls(testDir);
nTestingMails = length(testCollection) - 2;

if (strcmp(testDir, [pwd, '\spam\test\']) == 1)
    spamChoice = true;
else
    spamChoice = false;
end

prob_C1 = 0.78;
prob_C2 = 0.22;

% prob_C1 = nSpamEmails / (nSpamEmails + nHamEmails);
% prob_C2 = nHamEmails / (nSpamEmails + nHamEmails);

nIdentifiedSpam = 0;

for i = 3:length(testCollection)
    spamTestFile = testCollection(i, :);
    featVec = presentre([testDir, spamTestFile], cSpamWords);
    prob_C1_x = p(featVec, 1, counts, nSpamEmails, nHamEmails) * prob_C1 / (p(featVec, 1, counts, nSpamEmails, nHamEmails) * prob_C1 + p(featVec, 2, counts, nSpamEmails, nHamEmails) * prob_C2);
    
    display([testCollection(i, :), '-> ', num2str(prob_C1_x * 100), '% chance of being spam.']);
    
    if (prob_C1_x > threshold)
        nIdentifiedSpam = nIdentifiedSpam + 1;
    end
end

if (spamChoice == true)   
    display(['Total misclassification rate: ', num2str((nTestingMails - nIdentifiedSpam) / nTestingMails * 100), '%.']);
else
    display(['Total misclassification rate: ', num2str(nIdentifiedSpam / nTestingMails * 100), '%.']);
end

thresholdVector = linspace(0, 1, 20);
figure(1);
hold all;
xlabel('False Spam');
ylabel('True Spam');

for i = 1:length(thresholdVector)
    threshold = thresholdVector(i);
    nIdentifiedSpam = 0;

    for i = 3:length(testCollection)
        spamTestFile = testCollection(i, :);
        featVec = presentre([testDir, spamTestFile], cSpamWords);
        prob_C1_x = p(featVec, 1, counts, nSpamEmails, nHamEmails) * prob_C1 / (p(featVec, 1, counts, nSpamEmails, nHamEmails) * prob_C1 + p(featVec, 2, counts, nSpamEmails, nHamEmails) * prob_C2);
        
        if (prob_C1_x > threshold)
            nIdentifiedSpam = nIdentifiedSpam + 1;
        end
    end
    
    if (spamChoice == true)
        X(i) = (nTestingMails - nIdentifiedSpam) / nTestingMails;
        Y(i) = nIdentifiedSpam / nTestingMails;
    else
        X(i) = nIdentifiedSpam / nTestingMails;
        Y(i) = (nTestingMails - nIdentifiedSpam) / nTestingMails;
    end
    
    plot(X, Y);
end

grid on;

end

function [prob_x_Ck] = p(x, k, counts, nSpamEmails, nHamEmails)

nFeatVec = length(x);
prob_x_Ck = 1;
const = nSpamEmails / 10;

if (k == 1)
    nTotalMails = nSpamEmails;
    prob_Ck = nSpamEmails / (nSpamEmails + nHamEmails);
else
    nTotalMails = nHamEmails;
    prob_Ck = nHamEmails / (nSpamEmails + nHamEmails);
end

for i = 1:nFeatVec
%     miu_IK = sum(x) / nTotalMails;
    miu_IK = (const + counts(i, k)) / (2 * const + nTotalMails);
%     miu_IK = (counts(i, k) / (nSpamEmails + nHamEmails)) / prob_Ck;
    prob_x_Ck = prob_x_Ck * (miu_IK^x(i)) * (1 - miu_IK)^(1 - x(i));
end

end

function [probCorrect, probWrong] = testDirectory(testDir, nSpamWords, spamList, hamList, threshold)

end