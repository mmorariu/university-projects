function MOG = init_mog(X, C)

N = length(X);
lenTrainA = N/2;
lenTrainB = lenTrainA;

trainA = X(1:lenTrainA, :);
trainB = X(lenTrainB+1:N, :);
trainA = sortrows(trainA, 2);
trainB = sortrows(trainB, 2);

nClustersA = C/2;
nClustersB = C/2;

for i = 1:nClustersA
    meanCluster = mean(trainA(lenTrainA/nClustersA*(i-1) + 1:lenTrainA/nClustersA*i, :));
    covCluster = cov(trainA(lenTrainA/nClustersA*(i-1) + 1:lenTrainA/nClustersA*i, :));
    MOG{i} = struct('PI', {1/C}, 'MU', meanCluster, 'SIGMA', covCluster);
end

for i = 1:nClustersB
    meanCluster = mean(trainB(lenTrainB/nClustersB*(i-1) + 1:lenTrainB/nClustersB*i, :));
    covCluster = cov(trainB(lenTrainB/nClustersB*(i-1) + 1:lenTrainB/nClustersB*i, :));
    MOG{nClustersB + i} = struct('PI', {1/C}, 'MU', meanCluster, 'SIGMA', covCluster);
end
end


