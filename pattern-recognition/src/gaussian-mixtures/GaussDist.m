function GaussDist()

clc;
clear all;
load banana.mat;

figure(1);
hold all;
grid on;

% nCross = 50;

% A = mixData(A);
% B = mixData(B);
[trainA, testA] = splitData(A);
[trainB, testB] = splitData(B);

muA = mean(trainA);
muB = mean(trainB);
sigmaA = cov(trainA);
sigmaB = cov(trainB);

% auxMeanA = zeros(nCross, 2);
% auxCovA = zeros(nCross, 2, 2);
% auxMeanB = zeros(nCross, 2);
% auxCovB = zeros(nCross, 2, 2);
% 
% for i = 1:nCross
%     A = mixData(A);
%     B = mixData(B);
%     [trainA, testA] = splitData(A);
%     [trainB, testB] = splitData(B);
%     
%     auxMeanA(i, :) = mean(trainA);
%     auxCovA(i, :, :) = cov(trainA);
%     auxMeanB(i, :) = mean(trainB);
%     auxCovB(i, :, :) = cov(trainB);
% end

% muA = mean(auxMeanA);
% sigmaA = [mean(auxCovA(:, 1, 1)) mean(auxCovA(:, 1, 2)); mean(auxCovA(:, 2, 1)) mean(auxCovA(:, 2, 2))];
% muB = mean(auxMeanB);
% sigmaB = [mean(auxCovB(:, 1, 1)) mean(auxCovB(:, 1, 2)); mean(auxCovB(:, 2, 1)) mean(auxCovB(:, 2, 2))];

lenTrainA = length(trainA);
lenTrainB = length(trainB);
lenTrain = lenTrainA + lenTrainB;

pA = lenTrainA / lenTrain;
pB = lenTrainB / lenTrain;

pXAA = mvnpdf(testA, muA, sigmaA);
pXAB = mvnpdf(testA, muB, sigmaB);
pXBA = mvnpdf(testB, muA, sigmaA);
pXBB = mvnpdf(testB, muB, sigmaB);
pAX = pXAA * pA./(pXAA * pA + pXAB * pB);
pBX = pXBB * pB./(pXBA * pA + pXBB * pB);

correctA = sum(pAX > 0.5);
correctB = sum(pBX > 0.5);
wrongA = length(pAX) - correctA;
wrongB = length(pBX) - correctB;

plot(A(:, 1), A(:, 2), 'o', 'MarkerFaceColor', [.03; .44; .63], 'MarkerEdgeColor', [.03; .44; .63]);
plot(B(:, 1), B(:, 2), 'o', 'MarkerFaceColor', [0; .65; 0], 'MarkerEdgeColor', [0; .65; 0]);
plot(muA(1), muA(2), 'o', 'MarkerFaceColor', [.7; 0; 0], 'MarkerEdgeColor', [.7; 0; 0]);
plot(muB(1), muB(2), 'o', 'MarkerFaceColor', [0; 0; 0], 'MarkerEdgeColor', [0; 0; 0]);
legend('Class A', 'Class B', 'Mean for class A', 'Mean for class B');

confussion = [correctA wrongA; wrongB correctB]

end

function [trainData, testData] = splitData(data)
N = length(data);
coeff = 0.5;
trainData = data(1:coeff*N, :);
testData = data(coeff*N+1:N, :);
end

function sorted = mixData(data)
N = length(data);
perm = randperm(N);
sorted = zeros(N, 2);

for i = 1:N
    sorted(i, :) = data(perm(i), :);
end
end