function [nrErrors] = testPhase( testData, meanA, meanB, sigmaA, sigmaB )

p_Ca = 0.5;
p_Cb = 0.5; % probabilies of the 2 classes are equal 50%
count = 0;

for i=1 : length(testData)
    probA_Ca = mvnpdf(testData(i,:),meanA,sigmaA);
    probA_Cb =  mvnpdf(testData(i,:),meanB,sigmaB);
    probA = (probA_Ca * p_Ca) / (probA_Ca * p_Ca + probA_Cb + p_Cb);
    probB = (probA_Cb * p_Cb) / (probA_Ca * p_Ca + probA_Cb + p_Cb);
    if(probA < probB)
       plot(testData(i,1),testData(i,2), '*');
        count = count + 1;
    end
end
nrErrors = count;
end
