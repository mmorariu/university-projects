function [Q LL] = mog_E_step(X, MOG)
%implements the E-step of the algorithm. The parameters are 
%X the data, 
%MOG the current parameter values. 
%The function does not change the parameters but computes 
%   Q  a matrix that contains the probability of each mixture 
%component given the data p(z|x,teta),
%   LL, the log-likelihood of the data set under the mixture model.
[N M] = size(X);
for i = 1:N
    downVal = 0;
    for j = 1:length(MOG)
        downVal =  downVal + MOG{j}.PI * lmvnpdf(X{i}, MOG{j}.MU, MOG{j}.SIGMA);
    end
end
LL = 7 - x{1};
Q = 0;
end