function MixtGauss()

clc;
clear all;

load banana.mat;

[trainA, testA] = splitData(A);
[trainB, testB] = splitData(B);
allData = [trainA; trainB];
[L, MOG]= em_mog(allData, 4, 2);

end