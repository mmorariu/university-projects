function LogProb()

clc;
clear all;
format short;
logsumexp(-1000, -1001)

end

function R = logsumexpN(V)
N = length(V);

if (N < 2)
    R = V(1);
else
    R = logsumexp(V(1), V(2));
    
    for i = 3:N
        R = R + logsumexp(R, V(i));
    end
end
end

function C = logsumexp(A, B)
nRows = size(A, 1);
nCols = size(A, 2);

C = zeros(nRows, nCols);

for i = 1:nRows
    for j = 1:nCols
        logA = A(i, j);
        logB = B(i, j);
        
        if (logA > logB)
            logAB = logA + log(1 + exp(logB - logA));
        else
            logAB = logB + log(1 + exp(logA - logB));
        end
        
        C(i, j) = logAB;
    end
end
end
