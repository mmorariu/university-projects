function Bayes()

clc;
clear all;
close all;

% spamList = countwords('spam\train');
% hamList = countwords('ham\train');
% save('spamList.mat', 'spamList');
% save('hamList.mat', 'hamList');

load spamList.mat
load hamList.mat
nSpamWords = 20;
threshold = 0.89;

[words, counts] = wordtable(spamList, hamList);
nSpamEmails = length(ls('spam\train')) - 2;
nHamEmails = length(ls('ham\train')) - 2;
nTotalWords = length(words);

counts(:, 3) = counts(:, 1) / nSpamEmails - counts(:, 2) / nHamEmails;
counts(:, 4) = [1:nTotalWords];
counts = sortrows(counts, 3);
counts = flipdim(counts, 1);

sSpamWords = words(counts(1:nSpamWords, 4));
cSpamWords = cellstr(sSpamWords');

% [probCorrectS, probWrongS] = testDirectory([pwd, '\spam\test\'], counts, nSpamEmails, nHamEmails, cSpamWords, threshold)
% [probCorrectH, probWrongH] = testDirectory([pwd, '\ham\test\'], counts, nSpamEmails, nHamEmails, cSpamWords, threshold)
    
ROC(10, counts, nSpamEmails, nHamEmails, cSpamWords, threshold);

end

function [prob_x_Ck] = p(x, k, counts, nSpamEmails, nHamEmails)

nFeatVec = length(x);
prob_x_Ck = 1;
const = nSpamEmails / 10;

if (k == 1)
    nTotalMails = nSpamEmails;
    prob_Ck = nSpamEmails / (nSpamEmails + nHamEmails);
else
    nTotalMails = nHamEmails;
    prob_Ck = nHamEmails / (nSpamEmails + nHamEmails);
end

for i = 1:nFeatVec
    miu_IK = (const + counts(i, k)) / (2 * const + nTotalMails);
    prob_x_Ck = prob_x_Ck * (miu_IK^x(i)) * (1 - miu_IK)^(1 - x(i));
end

end

function [probCorrect, probWrong] = testDirectory(testDir, counts, nSpamEmails, nHamEmails, cSpamWords, threshold)

if (strcmp(testDir, [pwd, '\spam\test\']) == 1)
    spamChoice = true;
else
    spamChoice = false;
end

testCollection = ls(testDir);
nTestingMails = length(testCollection) - 2;

prob_C1 = 0.786;
prob_C2 = 0.214;
nIdentifiedSpam = 0;

for i = 3:length(testCollection)
    spamTestFile = testCollection(i, :);
    featVec = presentre([testDir, spamTestFile], cSpamWords);
    prob_C1_x = p(featVec, 1, counts, nSpamEmails, nHamEmails) * prob_C1 / (p(featVec, 1, counts, nSpamEmails, nHamEmails) * prob_C1 + p(featVec, 2, counts, nSpamEmails, nHamEmails) * prob_C2);
    
    display([testCollection(i, :), '-> ', num2str(prob_C1_x * 100), '% chance of being spam.']);
    
    if (prob_C1_x > threshold)
        nIdentifiedSpam = nIdentifiedSpam + 1;
    end
end

if (spamChoice == true)   
    probWrong = (nTestingMails - nIdentifiedSpam) / nTestingMails;
    probCorrect = 1 - probWrong;
else
    probWrong = nIdentifiedSpam / nTestingMails;
    probCorrect = 1 - probWrong;
end

end

function ROC(nThreshold, counts, nSpamEmails, nHamEmails, cSpamWords, threshold)

thresholdVector = linspace(0, 1, nThreshold);

figure(1);
hold all;
xlabel('Ham e-mails identified as spam');
ylabel('Spam e-mails identified as spam');

X = zeros(nThreshold, 1);
Y = zeros(nThreshold, 1);

for i = 1:nThreshold
    threshold = thresholdVector(i);
    [probCorrectS, probWrongS] = testDirectory([pwd, '\spam\test\'], counts, nSpamEmails, nHamEmails, cSpamWords, threshold);
    [probCorrectH, probWrongH] = testDirectory([pwd, '\ham\test\'], counts, nSpamEmails, nHamEmails, cSpamWords, threshold);
    
    X(i) = probWrongH;
    Y(i) = probCorrectS;
end

format short;
plot(X, Y, 'LineWidth', 2, 'Color', 'k', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'r');

for i = 1:nThreshold
    text(X(i) - 0.02, Y(i), sprintf('%.2f', thresholdVector(i)), 'BackgroundColor', 'r', 'Color', 'w');
end

axis tight;
grid on;

end