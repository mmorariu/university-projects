 load data.mat
 clc
 
 % 1
 
 lenV = length(v);
 display(['1. The order of vector v is ', num2str(lenV), '.']);
 
 % 2a
 
 start1 = tic;
 norm = 0;
 
 for i = 1:lenV
     norm = norm + v(i)^2;
 end
 
 norm = sqrt(norm);
 display(['2a. |v| = ', num2str(norm)]);
 time1 = toc(start1);
 
 % 2b
 
 start2 = tic;
 display(['2b. |v| = ', num2str(sqrt(v'*v))]);
 time2 = toc(start2);
 
 % 3
 
 display(['3. It took ', num2str(time1), ' seconds to compute 2a and ', num2str(time2), ' seconds to compute 2b.']);
 
 % 4
 
 scalar = 10;
 countS = sum(v2 > scalar);
 count0 = sum(v2 > 0);
 count1 = sum(v2 > 1);
 count2 = sum(v2 > 2);
 count3 = sum(v2 > 3);
 count4 = sum(v2 > 4);
 count5 = sum(v2 > 5);
 sum4 = 0;
 
 for i = 1:lenV     
     if (v(i) > 4)
         sum4 = sum4 + v(i);
     end
 end
 
 display(['4. There are ', num2str(countS), ' elements larger than ', num2str(scalar), '.']);
 display(['   There are ', num2str(count0), ' elements larger than 0.']);
 display(['   There are ', num2str(count1), ' elements larger than 1.']);
 display(['   There are ', num2str(count2), ' elements larger than 2.']);
 display(['   There are ', num2str(count3), ' elements larger than 3.']);
 display(['   There are ', num2str(count4), ' elements larger than 4.']);
 display(['   There are ', num2str(count5), ' elements larger than 5.']);
 
 % 5
 
 display(['5. The sum of all elements larger than 4 is ', num2str(sum4), '.']);
 
 % 6
 
 lenL = length(l);
 e = zeros(lenL, 5);

 for i = 1:lenL
     e(i, l(i)) = 1;
 end
 
 display('6. The first 5 labels are: ');
 e(1:6, :)
 
 % 7
 
 r = zeros(lenL, 1);
 
 for i = 1:lenL
     r(i) = find(e(i, :));
 end
 
 if (isequal(l, r) == 1)
     display('7. The new "l" vector is identical to the old one.');
 else
     display('7. The new "l" vector is not identical to the old one.');
 end
