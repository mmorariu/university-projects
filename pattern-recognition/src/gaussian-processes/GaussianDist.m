clc;
clear all;
close all;

mu = [0 0];
sigma = [1 0; 0 3];
sigma_inv = inv(sigma);

% 1

figure(1);

[X1, X2] = meshgrid(linspace(-3, 3, 50)', linspace(-5, 5, 50));
X = [X1(:) X2(:)];
lenX1 = size(X1, 2);
minX1 = min(X1(1, :));
maxX1 = max(X1(1, :));

p = mvnpdf(X, mu, sigma);
R = reshape(p, [50, 50]);
subplot(2, 1, 1);
contour(X1, X2, R);
xlabel('X1');
ylabel('X2');
grid on;

% 2

subplot(2, 1, 2);
plot(X1(1, :), normpdf(X1(1, :), mu(1), sigma(1, 1)));
xlabel('X1');
ylabel('P(X1)');
grid on;

% 3

figure(2);

for i = -3:3
    mu_cond = mu(1) - 1/sigma_inv(1, 1) * sigma_inv(1, 2) * (i - mu(2));
    var_cond = 1/sigma_inv(1, 1);
    p_X1_X2 = normpdf(X1(1, :), mu_cond, var_cond);
    
    subplot(2, 4, i + 4);
    plot(X1(1, :), p_X1_X2);
    axis tight;
    grid on;
end

% 4

figure(3);

[X1, X2] = meshgrid(linspace(-3, 3, 50)', linspace(-3, 3, 50));
X = [X1(:) X2(:)];
mu = [0 0];
sigma = [1 .7; .7 1];
sigma_inv = inv(sigma);

p = mvnpdf(X, mu, sigma);
R = reshape(p, [50, 50]);
subplot(2, 1, 1);
% surf(R, 'EdgeAlpha', .2);
plot3(X1(:), X2(:), p, '*', 'Color', 'b');
xlabel('X1');
ylabel('X2');
zlabel('P(X1, X2)');
grid on;

% 5

subplot(2, 1, 2);
plot(X1(1, :), normpdf(X1(1, :), mu(1), sigma(1, 1)));
xlabel('X1');
ylabel('P(X1)');
grid on;

% 6

figure(4);
X1 = linspace(-3, 3, 50);

% for i = -3:3
%     p_X1_X2 = zeros(lenX1, 1);
%     mu_cond = mu(1) - 1/sigma_inv(1, 1) * sigma_inv(1, 2) * (i - mu(2));
%     var_cond = 1/sigma_inv(1, 1);
%     
%     for j = 1:lenX1
%         p_X1_X2(j) = normpdf(X1(1, j), mu_cond, var_cond);
%     end
%     
%     subplot(2, 4, i + 4);
%     plot(X1(1, :), p_X1_X2);
%     axis tight;
%     grid on;
% end

for i = -3:3
    y = [];
    meanT = 0.51 * 7 * i;
    variance = 0.51;
    axesX = [(meanT - 3 * variance):0.05:(meanT + 3 * variance)];
    
    for j = 1:size(axesX, 2)
        y = [y normpdf(axesX(j), meanT, variance)];
    end
    
    subplot(2, 4, i + 4);
    plot(axesX, y);
    axis tight;
    grid on;
end
