function GaussProc

clc;
clear all;
close all;
load chirps.mat;

[trainData, testData] = splitData(chirps);

figure(1);
hold all;

X = trainData(:, 1);
t = trainData(:, 2);

log_p_t_X = computePerformance(X, t)

obj1 = plot(trainData(:, 1), trainData(:, 2), 'o', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'k');
obj2 = plot(testData(:, 1), testData(:, 2), 'o', 'MarkerFacecolor', [0 0.65 0], 'MarkerEdgeColor', 'k');%[0 0.65 0]);
legend([obj1 obj2], 'Train data', 'Test data');

end

function [trainData, testData] = splitData(data)
N = size(data, 1);
coeff = 0.75;
trainData = data(1:floor(coeff*N), :);
testData = data(floor(coeff*N)+1:N, :);
end

function y = kernel(x, x_)
l = 1;
y = exp(-1/(2*l) * (x - x_)'*(x - x_));
end

function K = buildKernel(X)
N = size(X, 1);

for i = 1:N
    for j = 1:N
        K(i, j) = kernel(X(i), X(j));
    end
end
end

function log_p_t_X = computePerformance(X, t, sigma)

sigma = 0.5;
K = buildKernel(X);
N = size(X, 1);

I = eye(N);
CN = K + sigma^2*I;
CN_inv = inv(CN);
L = chol(K + sigma^2*I, 'lower');
alpha = L' \ (L \ t);

x_star_vec = linspace(12, 22, 100);
lenXS = size(x_star_vec, 2);
f_star_vec = zeros(lenXS, 1);
var_star_vec = zeros(lenXS, 1);

for i = 1:lenXS

    x_star = x_star_vec(i);
    k_star = zeros(N, 1);

    for j = 1:N
        k_star(j) = kernel(X(j), x_star);
    end
    
    f_star_vec(i) = k_star'*alpha;
    v = L \ k_star;
    var_star_vec(i) = kernel(x_star, x_star) + sigma^2 - v'*v;
    
end

detL = 0;

for i = 1:N
    detL = detL + log(L(i, i));
end

fill(x_star_vec, min(f_star_vec + 3 * sqrt(var_star_vec), f_star_vec - 3 * sqrt(var_star_vec)), 'w', 'EdgeColor', 'w', 'FaceAlpha', .9);
fill(x_star_vec, max(f_star_vec + 3 * sqrt(var_star_vec), f_star_vec - 3 * sqrt(var_star_vec)), 'y', 'EdgeColor', 'w', 'FaceAlpha', .5);
plot(x_star_vec, f_star_vec, 'Color', 'b', 'LineWidth', 1.5);

log_p_t_X = -1/2 * t' * alpha - detL - N/2 * log(2*pi);

end

function storeVec = addData(storeVec, data)
N = size(storeVec, 1);
storeVec(N+1, :) = data;
end