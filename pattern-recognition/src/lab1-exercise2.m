 clc;
 load data
 close all;

 % 1
 
 x = linspace(-5, 5, 100);
 figure(1);
 
 subplot(1, 2, 1);
 hold all;

 miu = 0;
 sigma = 1;
 plot(x, 1/sqrt(2*pi*sigma^2) * exp(-(x - miu).^2 / (2*sigma^2)), 'LineWidth', 2, 'Color', 'b');
 
 miu = 0;
 sigma = 2;
 plot(x, 1/sqrt(2*pi*sigma^2) * exp(-(x - miu).^2 / (2*sigma^2)), 'LineWidth', 2, 'Color', 'r');
 
 miu = 1;
 sigma = 3;
 plot(x, 1/sqrt(2*pi*sigma^2) * exp(-(x - miu).^2 / (2*sigma^2)), 'LineWidth', 2, 'Color', 'g');
 
 legend('miu = 0, sigma = 1', 'miu = 0, sigma = 2', 'miu = 1, sigma = 3'); 
 grid on;
 
 subplot(1, 2, 2);
 hold all;
 
 miu = 0;
 sigma = 1;
 plot(x, normpdf(x, miu, sigma), 'LineWidth', 2, 'Color', 'b');
 
 miu = 0;
 sigma = 2;
 plot(x, normpdf(x, miu, sigma), 'LineWidth', 2, 'Color', 'r');
 
 miu = 1;
 sigma = 3;
 plot(x, normpdf(x, miu, sigma), 'LineWidth', 2, 'Color', 'g');
 
 legend('miu = 0, sigma = 1', 'miu = 0, sigma = 2', 'miu = 1, sigma = 3'); 
 grid on;
 
 % 2
 
 figure(2);
 hold all;
 
 lenV2 = length(v2);
 [N, C] = hist(v2, 20);
 unit = (max(v2) - min(v2)) / 20;
 area = unit * sum(N);
 bar(C, N / area);
 grid on;
 

 MEAN = mean(v2);
 VAR = sqrt(cov(v2));
 plot(x, normpdf(x, MEAN, VAR), 'LineWidth', 2, 'Color', 'r');
 
 % 3

 figure(3);
 hold all;

 mu = [0 0];
 sigma = [1 0; 0 1];
 [X1, X2] = meshgrid(linspace(-5, 5, 50)', linspace(-5, 5, 50)');
 X = [X1(:) X2(:)];
 p = mvnpdf(X, mu, sigma);
 R = reshape(p, 50, 50);
 surf(X1, X2, R, 'EdgeAlpha', .2);
 grid on;
 
 % 4
 
 figure(4);
 hold all;
 contour(R);
 grid on;
 
 figure(5);
 hold all;
 
 sigma = [2 -1; -1 2];
 [X1, X2] = meshgrid(linspace(-5, 5, 60)', linspace(-5, 5, 60)');
 X = [X1(:) X2(:)];
 p = mvnpdf(X, mu, sigma);
 R = reshape(p, 60, 60);
 
 subplot(1, 3, 1);
 contour(R);
 grid on;
 
 sigma = [4 3; 3 4];
 [X1, X2] = meshgrid(linspace(-5, 5, 60)', linspace(-5, 5, 60)');
 X = [X1(:) X2(:)];
 p = mvnpdf(X, mu, sigma);
 R = reshape(p, 60, 60);
 
 subplot(1, 3, 2);
 contour(R);
 grid on;
 
 sigma = [12 2; 2 12];
 [X1, X2] = meshgrid(linspace(-5, 5, 60)', linspace(-5, 5, 60)');
 X = [X1(:) X2(:)];
 p = mvnpdf(X, mu, sigma);
 R = reshape(p, 60, 60);
 
 subplot(1, 3, 3);
 contour(R);
 grid on;
 