 clc;
 close all;
 load data
 
 lenA = length(a);
 lenB = length(b);
 meanA = sum(a) / lenA;
 meanB = sum(b) / lenB;
 varA = sqrt(sum((a - meanA).^2) / lenA);
 varB = sqrt(sum((b - meanB).^2) / lenB);
 
 % 1 & 3
 
 figure(1);
 
 subplot(1, 2, 1);
 hold all;
 [N, C] = hist(a);
 minA = min(a);
 maxA = max(a);
 unit = (maxA - minA) / 10;
 area = unit * sum(N);
 
 bar(C, N / area); 
 h = findobj(gca, 'Type', 'patch');
 set(h, 'FaceColor', 'r', 'EdgeColor', 'w');
 
 x = linspace(minA, maxA, 100);
 plot(x, normpdf(x, meanA, varA), 'LineWidth', 2, 'Color', 'k');
 grid on;
 
 subplot(1, 2, 2);
 hold all;
 [N, C] = hist(b);
 minB = min(b);
 maxB = max(b);
 unit = (max(b) - min(b)) / 10;
 area = unit * sum(N);
 
 bar(C, N / area);
 h = findobj(gca, 'Type', 'patch');
 set(h, 'FaceColor', 'b', 'EdgeColor', 'w');
 
 x = linspace(minB, maxB, 100);
 plot(x, normpdf(x, meanB, varB), 'LineWidth', 2, 'Color', 'k');
 grid on;
 
 % 2
 
 display(['2. The ML-estimators for the a-vector are (', num2str(meanA), ', ', num2str(varA), ').']);
 display(['   The ML-estimators for the b-vector are (', num2str(meanB), ', ', num2str(varB), ').']);
 
 % 4
 
 pXCaX0 = 1 / (2*pi*varA^2) * exp( -meanA^2 / (2*varA^2) );
 display(['4. The probability that a new datapoint, located at x = 0, belongs to class Ca, is ', num2str(pXCaX0), '.']);
 
 % 5
 
 figure(2);
 x = linspace(-20, 20, 1000);
 plot(x, normpdf(x, meanA, varA), 'LineWidth', 2);
 grid on;