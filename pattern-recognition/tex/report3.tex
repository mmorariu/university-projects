\documentclass [11pt] {article}

\usepackage {amsmath}
\usepackage {anysize}
\usepackage {caption}
\usepackage {color}
\usepackage {courier}
\usepackage {graphicx}
\usepackage {listings}
\usepackage {subfigure}

\newcommand{\dsum} {\displaystyle\sum}

\lstset{
         basicstyle = \footnotesize\ttfamily,
         numberstyle = \tiny, 
         numbersep = 5pt,     
         tabsize = 2,         
         extendedchars = true,
         breaklines = true,
         keywordstyle = \color{red},
    		 frame = b,
         stringstyle = \color{white}\ttfamily,
         showspaces = false,
         showtabs = false,
         xleftmargin = 17pt,
         framexleftmargin = 17pt,
         framexrightmargin = 5pt,
         framexbottommargin = 4pt,
         showstringspaces = false 
}

\lstloadlanguages{ MatLab }
\DeclareCaptionFont {white}{\color{white}}
\DeclareCaptionFormat {listing}{\colorbox[cmyk]{0.43, 0.35, 0.35,0.01}{\parbox{\textwidth}{\hspace{15pt}#1#2#3}}}
\captionsetup [lstlisting]{format = listing, labelfont = white, textfont = white, singlelinecheck = false, margin = 0pt, font = {bf,footnotesize}}

\title {Pedestrian Classification}
\author {Vlad Lep \\
				 Mihai Adrian Morariu \\
				 Andrei Oghin\u{a}
				}

\begin {document}

\maketitle

\

\begin {abstract}

\

In this report, we describe methods that can be used for pedestrian recognition within still images. We perform dimensionality reduction of the data sets, when needed, by means of Principal Component Analysis. We train two classifiers using different types of features extracted from the images. Finally, we evaluate, compare and discuss the experimental results.
\end {abstract} 

\

\section {Training}

\

In the first exercise, we trained two classifiers using three different types of features for a two-class classification problem - that is, detecting whether a pedestrian is displayed in an image or not. The two classifiers were \emph {Linear SVM } (linSVM) and \emph {Gaussian Mixture Model} (GMM). The three feature sets were \emph {Gray-Level Intensity} (GLI), \emph {Histogram of Oriented Graidients} (HOG) and \emph {Local Receptive Fields} (LRF).

\

\subsection {Linear SVM}

\

For the {\bf linear SVM} classifier, we wanted to determine a function $y(\mathbf{x}) = \mathbf{w}^T \mathbf{x} + b$ such as $y(\mathbf{x_n}) > threshold$ (where \emph {threshold} was a predefined constant) for the points $(\mathbf{x_n}, t_n)$ for which $t_n = 1$ and $y(\mathbf{x_n}) < threshold$ for the points $(\mathbf{x_n}, t_n)$ for which $t_n = -1$. Basically, we needed to determine the vector $\mathbf{w}$ (which had the same size as our data) and the scalar $b$. To do this, we created the vector $\mathbf{X}$ which consisted of both pedestrian training data and non-pedestrian training data and a vector $\mathbf{Y}$ containing the corresponding target values for $\mathbf{X}$.

\

We then computed the values of $\mathbf{w}$ and $b$ with the aid of the \emph {primal\_svm} command:
\begin {center}
	[w, b] = primal\_svm(1, Y, .5);
\end {center}

Finally, we tested an image $\mathbf{x}$ within the test set by computing $y(\mathbf{x})$ and checking whether $y(\mathbf{x}) \geq threshold$ or $y(\mathbf{x}) < threshold$. In the first case, we classified $\mathbf{x}$ as being a pedestrian image, whereas in the second case we classified it as being a non-pedestrian image.

\

\subsection {Gaussian - Mixture Model}

\

For the {\bf Gaussian-Mixture Model}, we first applied {\bf PCA} to reduce the dimensionality of our data from $N$ dimensions to $D < N$ dimensions. In the next section, we will give a mathematical justification for applying the algorithm.

\

Each class of our training data was modeled by a Gaussian mixture comprising $C$ components. The {\bf EM algorithm} was used for determining the means, the covariance matrices and the mixing coefficients for every mixture component. We also used the {\bf K-Means algorithm} to avoid initializing the EM algorithm parameters randomly and to reduce the number of subsequent iterations.

\

\subsection {Principal Component Analysis}

\
	
For some of the features, the amount of training data available was insufficient to train a classifier. This is because the more dimensions there exist, the more training data is required for the classifier to perform correctly. Therefore PCA was needed to reduce the dimensions for some of the features. In this section, we shall explain how we decided when PCA should be used or not. We define:

\begin {itemize}
	\item $\mathbf{N}$ - the number of dimensions of the data (e.g. N = 1250 for the gray scale features)
	\item $\mathbf{T}$ - the size of the training data for each class (e.g. T = 1500 for the gray scale features)
\end {itemize}

The sum of the dimensions of the parameters to be learned for each algorithm was required to be smaller or equal to the number of training data. Otherwise the system had an indefinite solution.

\

\subsubsection {PCA for SVM}

\

For the SVM algorithm, we were required to determine the parameters $\mathbf{w}$ and $b$, where

\

\begin {itemize}
	\centering
	\item dim({\bf w}) = $N$
	\item dim(b) = 1
\end {itemize}

Therefore,

\begin {equation}
	\label {eq:1}
	\mbox{dim(SVM)} = \mbox{dim({\bf w})} + \mbox{dim(b)} = N + 1
\end {equation}

\

Equation \eqref{eq:1} gives the total number of free parameters to train for the SVM algorithm. The algorithm used all the training data to obtain a solution, not only one specific set for each class. Therefore, in this case, $T$ was the sum of the sizes of training data for the two classes (pedestrian and non-pedestrian) and had to obey the inequation

\begin {align}
	T \geq \mbox{dim(SVM)}
\end {align}

\begin {itemize}
	\item In the case of GLI images, $T = 3000$ and $N = 1250$, so $\mbox{dim(SVM)} = 1251$ and $T > \mbox{dim(SVM)}$. Therefore, PCA was not required for that SVM task.
	\item In the case of HOG images, $T = 3000$ and $N = 320$, so $\mbox{dim(SVM)} = 321$ and $T > \mbox{dim(SVM)}$. Therefore, PCA was not required for that SVM task.
	\item In the case of LRF images, $T = 3000$ and $N = 1152$, so $\mbox{dim(SVM)} = 1153$ and $T > \mbox{dim(SVM)}$. Therefore, PCA was not required for that SVM task.
\end {itemize}

\

\subsubsection {PCA for GMM}

\

The parameters that were required to be trained for this algorithm were $\mathbf{\pi}$ (the mixing coefficient of each component), $\mathbf{\mu}$ (the mean of each component) and $\mathbf{\Sigma}$ (the covariances of each component). We shall denote:

\

\begin {itemize}
	\item {\bf C} - the number of Gaussian mixture components
	\item dim({\bf x}) - the length of the vector {\bf x}
	\item dim(Gauss) - the number of parameters to train for the current Gaussian component
	\item $\mbox{dim(MIX}_A$) - the number of parameters to train for the Gaussian mixture representing \emph {class A}, comprising $C$ components
\end {itemize}

\

In our case, this values were represented by

\

\begin {itemize}
	\item dim($\mathbf{\mu}$) = $N$
	\item dim($\mathbf{\pi}$) = 1
\end {itemize}

\

\begin {itemize}
	\item dim(Gauss) = dim($\mathbf{\pi}$) + dim($\mathbf{\mu}$) + dim($\mathbf{\Sigma}$)
	\item dim($\mathbf{\Sigma}$) = $\dfrac{N(N+1)}{2}$ (because the covariance matrix is symmetric and we therefore did not need to learn all $N^2$ values, but only the values above / below the diagonal and the ones lying on the diagonal)
\end {itemize}

\

$\Rightarrow \mbox{dim(MIX}_{\mbox{PED}}) = \dsum_{i = 1}^C \mbox{dim(Gauss}_i) = \dsum_{i = 1}^C \mbox{dim($\mathbf{\pi}$}_i) + \dsum_{i = 1}^C \mbox{dim($\mathbf{\mu}$}_i) + \dsum_{i = 1}^C \mbox{dim($\mathbf{\Sigma}$}_i)$

\

We kept in mind that $\dsum_{i = 1}^C \mathbf{\pi}_i = 1$ for each of the Gaussian mixtures, so we could infer that we did not need to count the last component's $\pi_C$ because

\begin {align}
	\mathbf{\pi}_C = 1 - \dsum_{i = 1}^{C - 1} \mathbf{\pi}_i
\end {align}

\

Therefore, $\dsum_{i = 1}^C \mbox{dim}(\mathbf{\pi}_i) = C - 1$ if $C > 1$ and we could infer that

\

\begin {align}
	\Rightarrow \mbox{dim(MIX}_{\mbox{PED}}) &= \dsum_{i = 1}^C \mbox{dim(Gauss}_i) = C - 1 + C N + \dfrac{C N(N+1)}{2} \\
	\label {eq:2}
																					 &= C - 1 + \dfrac{C N (N+3)}{2}
\end {align}

\

Equation \eqref{eq:2} gives the total number of free parameters that were needed to be trained for the Gaussian mixture corresponding to the pedestrian class. However, to estimate this number, we needed to have at least as many training data for that class as there were free parameters:

\

\begin {align}
	T \geq \mbox{dim(MIX}_{\mbox{PED}})
\end {align}

\

We applied the algorithm to each of the two classes. For each class, only the corresponding training data was used to model the mixture components over that class. For example, for the pedestrian class and the GLI features ($T = 1500, N = 1250$), we had:

\

\begin {align}
	\mbox{dim(MIX}_{\mbox{PED}}) &= C - 1 + \dfrac{C N (N+3)}{2} \\
															 &= 783126 C - 1
\end {align}
where $C$ denoted the number of components for each class. Since $T = 1500$ and $C \geq 1 \Rightarrow T < 783126 C - 1$.   Therefore, PCA was needed to reduce the number of dimensions.

\

Next, we tried to determine the maximum number of dimensions to be used, in order to store as few data as possible and also have a good accuracy for the classifier. Since

\begin {align}
	T + 1 - C \geq \dfrac{C N(N+3)}{2}
\end {align}
and $T = 1500$, it yielded that

\begin {align}
	\dfrac{3002}{C} > (N + 1)(N + 2)
\end {align}

\

We took into consideration different values of $C$ and tried to determine the maximum value for $N$, as described in the table from below.

\begin {table}[!h]
	\centering
	\label {table:1}
		\begin {tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
			\hline
			$\mathbf{C}$ & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 12 & 13 & 15 & 17 & 20 & \dots & 100 & 500 \\
			\hline
			$\mathbf{N_{MAX}}$ & 53 & 37 & 30 & 25 & 23 & 20 & 19 & 17 & 16 & 15 & 14 & 13 & 12 & 11 & 10 & \dots & 4 & 1 \\
			\hline
		\end {tabular}
		\caption {Correlation between $C$ and the maximum value of $N$}
\end {table}

\

\begin {itemize}
	\item In the case of GLI images, $T = 1500$ and $N = 1250$, so $\mbox{dim(MIX}_{\mbox{PED}}) = 783126 C - 1 > T$ since $C \geq 1$. Therefore, PCA was required for that GMM task.
	\item In the case of HOG images, $T = 1500$ and $N = 320$, so $\mbox{dim(MIX}_{\mbox{PED}}) = 51681 C - 1 > T$ since $C \geq 1$. Therefore, PCA was required for that GMM task.
	\item In the case of LRF images, $T = 1500$ and $N = 1152$, so $\mbox{dim(MIX}_{\mbox{PED}}) = 665281 C - 1 > T$ since $C \geq 1$. Therefore, PCA was required for that GMM task.
\end {itemize}

\

\subsection {Applying PCA}

\

Below is a small sample from the data used for training.

\begin {figure}[!h]
	\centering
	\label {fig:1}
	\includegraphics [scale = .4] {pedtrain_original.jpg}
	\caption {Sample from the original training data}
\end {figure}

\

After applying the PCA algorithm, we generated the images from the first and the last 10 components on which data was projected. These are presented in fig.(2).

\begin {figure}[!h]
	\label {fig:2}
	\centering
	\subfigure {\includegraphics [scale = 0.4] {first_10_principal_components.jpg}} 
	\subfigure {\includegraphics [scale = 0.4] {last_10_principal_components.jpg}} 
	\caption {Images of the first 10 (left) and last 10 (right) components}
\end {figure}

\

It can be easily observed that the first 10 components carry significant information, in some pictures these representations being sufficient to visually spot pedestrians. The reverse applies for the last 10 components, where the representations seem to be useless, because the amount of relevant information they have associated with is very small.

\

For the intensity features, we generated 10 images obtained after projecting them onto the corresponding linear subspace, from top 20 and top 100 PCA dimensions. The results are presented in fig.(3).

\

\begin {figure}[!h]
	\label {fig:3}
	\centering
	\subfigure {\includegraphics [scale = 0.4] {pedtrain_20_dimensions.jpg}} 
	\subfigure {\includegraphics [scale = 0.4] {pedtrain_100_dimensions.jpg}} 
	\caption {Data projected onto the top 20 (left) and top 100 (right) dimensions}
\end {figure}

\

The projected images are obviously less clear than the original image. However, as expected, the resemblance is beyond dispute even when using only 20 dimensions, which confirms the efficiency of the Principal Component Analysis. While there is a noticeable improvement in image quality when increasing the number of components from 20 to 100, this improvement is small. Moreover, the image reassembles the original one quite well when using just 20 components. This confirms the fact that the first components are the most informative.

\

Each image from our dataset is encoded by three feature sets: GLI, HOG and LRF. For each of these, we have plotted the percentage of explained variance with respect to the number of PCA components. These can be seen in fig.(4) and fig.(5).

\

\begin {figure} [!h]
	\label {fig:4}
  \begin {center}
        \subfigure {\includegraphics [scale = 0.5] {ROC_HOG.png}} 
  			\subfigure {\includegraphics [scale = 0.5] {ROC_LRF.png}}
  \end {center}
  \caption {Explained variance for - HOG (left), LRF (right)}
\end {figure}

\begin {figure} [!h]
	\label {fig:5}
  \begin {center}
        \subfigure {\includegraphics [scale = 0.5] {ROC_GRAY.png}} 
  \end {center}
  \caption {Explained variance for GLI}
\end {figure}


We can observe from these images that the first dimensions of the GLI dataset contain most of the information, the slope being very abrupt. Thus, for GLI, we predicted that we can reduce the number of dimensions significantly without much information loss. For the HOG feature, more dimensions contain information as suggested by the smoother slope. For the LRF we have an even smoother slope. Therefore, for these two features, reducing the dimensions to a very small number will result in a lot of information loss. 

\

We determined that 99\% precision is achieved for GLI with 405 dimensions, for HOG with 795 dimensions and for LRF with 251 dimensions. We can see that, for LFR, the relative decrease in number of dimensions is significantly smaller than for GLI and HOG. Given the curves profiles, GLI seems to benefit the most from using PCA, achieving 99\% precision by using less than a third of its original number of dimensions.

\

\section {Evaluation}

\

\subsection {SVM}

\

We ran the program for the SVM algorithm, setting the threshold to 0, on a test set comprising 500 images of each category. Below we display, on the first two rows, the number of correctly classified images and the accuracy rate for each category. On the last line, we display the total number of correctly identified images within both test sets, along with the overall accuracy rate of the classifier.

\

\begin {table}[!h]
	\label {tabel:2}
	\centering
		\begin {tabular}{|c|c|c|c|}
			\hline
			& GLI & HOG & LRF \\
			\hline
			Pedestrian & 346 (69.2\%) & 340 (68\%) & 347 (69.4\%) \\
			\hline
			Non-pedestrian & 283 (56.6\%) & 405 (81\%) & 411 (82.2\%) \\
			\hline
			Overall accuracy rate & 629 (62.9\%) & 745 (74.5\%) & 758 (75.8\%) \\
			\hline
		\end {tabular}
		\caption {Accuracy rate after applying the linSVM algorithm}
\end {table}

\subsection {Mixtures of Gaussians}

\

We also ran the program for the GMM algorithm on a test set comprising 500 images of each category. For each of the feature sets, we varied the number of dimensions and the number of mixture components to see in which case we would get better results. Eventually, we chose 37 dimensions and 2 mixture components, as described in Table (1).

\

\begin {table}[!h]
	\label {tabel:3}
	\centering
		\begin {tabular}{|c|c|c|c|}
			\hline
			& GLI & HOG & LRF \\
			\hline
			Pedestrian & 228 (45.6\%) & 270 (54\%) & 347 (69.4\%) \\
			\hline
			Non-pedestrian & 317 (63.4\%) & 251 (50.2\%) & 411 (82.2\%) \\
			\hline
			Overall accuracy rate & 545 (54.5\%) & 521 (52.1\%) & 758 (75.8\%) \\
			\hline
		\end {tabular}
		\caption {Accuracy rate after applying the GMM algorithm}
\end {table}

\subsection {ROC Curves}

\

Next, we evaluated the classifiers from the previous exercise using ROC curves, by varying the decision thresholds used on the test data set. 

\

We performed the first set of experiments on the SVM classifier. The resulting ROC curves can be observed in fig.(6), corresponding to each feature set. To vary the threshold, we first computed the minimum and maximum values of $y(\mathbf{x})$. We varied our threshold linearly between those two extreme points.

\begin {figure}[!h]
	\centering
	\label {fig:6}
	\includegraphics [scale = .6] {SVM_hog_lrf_grey.png}
	\caption {ROC for SVM applied to different fetures (Green = GLI, Red = HOG, Blue = LRF)}
\end {figure}

\

From fig.(6) we can deduce that SVM applied to the HOG and LRF features produces similar ROC curves, with the LRF appearing to have a slightly better profile. However, the decrease in performance is obvious for the GLI feature set. 

\

For the GMM it was a challenge to compute the ROC curves since the probabilities for each input image were very small. We shall describe how we obtained the solution for the GLI feature, the method being the same for all of the features. 

\

We calculated the minimum and maximum values returned by the \emph{pdf} function applied to the data and stored them in the \emph {extremeVal.mat} file. For GLI, we obtained the following values:

\begin {align}
	\mbox{minValue} &= 1.8124 \times 10^{-104} \nonumber \\
	\mbox{maxValue} &= 8.9317 \times 10^{-096} \nonumber
\end {align}

\

The problem we faced was the several order of magnitude difference between {\bf minValue} and {\bf maxValue}, {\bf minValue} being $10^8$ times smaller than {\bf maxValue}. To compensate for this, we transposed the probabilities in the log-log domain and varied the threshold for these new values.

\

We changed the original "if" statement from:

\begin{lstlisting} [caption = Original "if" statement ]
	if (probPed - probNonPed > threshold)
\end{lstlisting}
to
\begin{lstlisting} [caption = Modified "if" statement ]
	if (log10(probPed) - log10(probNonPed) > threshold)
\end{lstlisting}
and varied the threshold :

\begin{lstlisting} [caption = Varying the threshold]
threshold = [-8:0.5:8] 
\end{lstlisting}

\

The values of the threshold are selected so as to cover linearly the difference in the magnitude orders of {\bf minValue} and {\bf maxValue}. For example, for LRF, the values returned by the {\bf pdf} function vary between  $\left[5.5596 \times 10^{-36}; 8.5664 \time 10^{-24}\right]$, sothe threshold was varied between = [-12:1:12]. For HOG, the values returned by the {\bf pdf} function vary between  $\left[11.8684, 2.1071 \times 10^9\right]$ and we therefore set threshold = [-9:0.6:9].

\

Below we have the ROC curves for the GMM algorithm on the three types of features.

\

\begin {figure}[!h]
	\centering
	\label {fig:7}
 	\includegraphics [scale = .4] {ROC_HOG_MIX_log.png}
	\caption {ROC for HOG features with Gaussian Mix}
\end {figure}

\begin {figure}[!h]
	\centering
	\label {fig:20}
 	\includegraphics [scale = .2] {ROC_Grey_MIX_log.jpg}
	\caption {ROC for GLI features with Gaussian Mix}
\end {figure}

\begin {figure}[!h]
	\centering
	\label {fig:21}
 	\includegraphics [scale = .4] {Mix_LRF_ROC_log.png}
	\caption {ROC for LRF features with Gaussian Mix}
\end {figure}


\

\section {Incremental Bootstrapping}

\

For the linSVM algorithm and HOG, LRF features, we applied an incremental bootstrapping technique by adding additional non-pedestrian training samples to the training data. We checked to see how incremental bootstrapping would improve the performance of our classifier. First, we ran the original program on both sets of features, yielding the overall accuracy rate as described in Table (2). Again, note that the test set comprised 500 pedestrian and 500 non-pedestrian images. 

\

After selecting 250 random non-pedestrian samples from the incremental bootstrap set and re-training the classifier, we noticed a very small improvement of the accuracy rate. The number of correctly classified non-pedestrian HOG images increased from 405 to 420 - 430 and the number of correctly classified non-pedestrian HOG images decreased from 340 to 325 - 335, yielding an overall accuracy rate of around ~75.5\%, compared to the original ~74.5\%. However, we noticed that by having only garbage images in the bootstrap data, we bias the classifier so it can classify non-pedestrians more accurately.

\

For the LRF images, the number of correctly classified pedestrian samples decreased from 347 to 332 and the number of correctly classified non-pedestrian samples increased from 411 to 437, yielding an overall accuracy rate of 76.5\%, compared to the original's 74.5\%.

\

We then selected the 250 "most difficult" non-pedestrian samples from the incremental bootstrap set. To do this, we kept in mind that the linSVM classifier would classify points having $y(x_n) < 0$  as belonging to the non-pedestrian class, whereas points having $y(x_n) = 0$ were lying on the decision boundary. Therefore, in order to select the samples that were "closest" to the pedestrian ones, we determined the points $x_n$ for which $y(x_n) > 0$ and selected the first 250 ones having the largest values of $y(x_n)$ (so that the points $x_n$ are as far as possible from the decision boundary). We then re-trained the original classifier on the enlarged dataset and re-evaluated the performance. 

\

After running the program, we noticed that the number of correctly classified HOG pedestrian samples decreased from 340 to 329 and the number of correctly classified HOG non-pedestrian samples increased from 405 to 436, yielding an overall accuracy rate of 76.5\%, compared to the original's 74.5\%. For LRF, the number of correctly classified pedestrian samples decreased from 347 to 334 and the number of correctly classified non-pedestrian samples increased from 411 to 433, yielding an overall accuracy rate of 76.7\%, compared to the original's 74.5%.

\

\begin {table}[!h]
	\label {tabel:4}
	\centering
		\begin {tabular}{|c|c|c|}
			\hline
			& HOG & LRF \\
			\hline
			Original & 745 (74.5\%) & 758 (75.8\%) \\
			\hline
			250 - Random & 755 (75.5\%) & 765 (76.5\%) \\
			\hline
			250 - Most difficult & 765 (76.5\%) & 767 (76.7\%) \\
			\hline
		\end {tabular}
		\caption {Accuracy rate after applying the incremental bootstrapping technique}
\end {table}

For this experiment, we used set the threshold to 0. By varying this value, we generated the ROC curve from below and noticed the performance gain by applying the two bootstrapping techniques.

\

\begin {figure} [!h]
	\label {fig:8}
  \begin {center}
        \subfigure {\includegraphics [scale = 0.5] {boost_HOG_random.png}} 
  			\subfigure {\includegraphics [scale = 0.5] {boot_hog_mostProbable.png}}
  \end {center}
  \caption {HOG after adding random (left) and "most difficult" (right) garbage data (Red = Boosted algorithm, Green = Basic algorithm)}
\end {figure}

\begin {figure} [!h]
	\label {fig:9}
  \begin {center}
        \subfigure {\includegraphics [scale = 0.5] {boot_lrf_random.png}} 
  			\subfigure {\includegraphics [scale = 0.5] {Boostrap_mostProbable_LRF.png}}
  \end {center}
  \caption {LRF after adding random (left) and "most difficult" (right) garbage data (Red = Boosted algorithm, Green = Basic algorithm)}
\end {figure}

\

As shown in the table and ROC, bootstrapping improves the classifier. By having only non-pedestrian bootstrap data, we bias our classifier towards the non-pedestrian class, improving its corresponding accuracy rate. However, by varying the threshold, we noticed that we could obtain good results for both classes. We noticed that bootstrapping worked well for cases (random and "most difficult" data), improving our overall classification rate by 1 - 2\%. 

\

\section {Multi - Feature Classification}

\

In the last exercise we used a multi-feature approach. We obtained the final classification decision for an image by taking into account both HOG / SVM and LRF / SVM classifiers. We took the classification decision by computing the average of the output of both classifiers or selecting the maximum values of those outputs. When using the classifiers, we noticed that taking the max of the outputs reduced the error of the pedestrian class, whereas taking the min of the outputs reduced the error of the non-pedestrian class.

\

We wanted to find out what would the best possible classifier (which yielded the lowest error) by combining these two features. We created the "ideal-fake" classifier that used the max of the outputs when testing the pedestrian class and the min of the outputs when testing the non-pedestrian class. Obviously this cannot not be done in real life since we do not not know which class the points belong to. We emphasize that we computed this just out of curiosity to see what would be the best classifier that could be obtained by combining these two features. Using other ways of combining the outputs (other than max or average), better results might be obtained but they should be worse than the "ideal-fake" classifier.

\

We used this approach on basic versions of the classifiers but also on the boosted classifiers. Fig. (12) describes the performances obtained for the basic version of the SVM algorithm and the performances obtained on the boosted classifiers.

\

The below table shows the overall precision that we obtained during classification when setting the threshold to 0, for the six means of combining the three output selections and the two algorithms.

\

\begin {table}[!h]
	\centering
	\label {tabel:5}
		\begin {tabular}{|c|c|c|c|}
			\hline
			Threshold = 0 & Mean output & Max output & Min / Max output \\
			\hline
			Basic & 78.8\% & 81.1\% & 86.1\% \\
			\hline
			Boosted & 83\% & 82.5\% & 93.4\% \\
			\hline
		\end {tabular}
\end {table}

\

From the table and from the ROC we could conlcude that mixing the features significantly increased the precision of the individual classifier that used a single feature. Also, by using the boosted algorithms we noticed a significant error reduction.

\

When using the basic algorithms we noticed that selecting the max of the outputs increased the precision slightly more, but when using the boosted algorithms the precision increase remained be the same.

\

An interested point to notice is that by combining the outputs of HOG / SVM and LRF / SVM differently (other than max or average), we could obtain a classification precision up to 93.4\% as suggested by the "ideal-fake" classifier in the table and Fig.(12).

\

\begin {figure} [!h]
	\label {fig:9}
  \begin {center}
        \subfigure {\includegraphics [scale = 0.5] {Feature_combine_basic_alg_compare.png}} 
  			\subfigure {\includegraphics [scale = 0.5] {Feature_combine_boost_alg_compare.png}}
  \end {center}
  \caption {Applying basic (left) and boosted (right) classifiers for HOG and LRF (Red = Max the output, Green = Mean of the output, Blue = Ideal output}
\end {figure}

\newpage

\section {Conclusions}

\

In this project, we used a linear SVM classifier and a Gaussian Mixture Model to perform pedestrian recognition on images. The training and testing data consisted in three different high-dimensional feature sets. We used these to train the classifiers and to evaluate their performance, after applying Principal Component Analysis when necessary. We provided the mathematical justifications for the different settings we used, and compared the results obtained based on these settings.

\

To this end, we significantly improved the performance of the SVM-based approach by using incremental bootstrapping, thus reinforcing training on obvious mistakes. Finally, we combined the different features and evaluated the classification metrics based on this new input space and increased our precision significantly.

\

\end {document}

