\documentclass [11pt] {article}

\usepackage {amsmath}
\usepackage {anysize}
\usepackage {caption}
\usepackage {color}
\usepackage {courier}
\usepackage {graphicx}
\usepackage {listings}
\usepackage {subfigure}

\newcommand{\dsum} {\displaystyle\sum}

\lstset{
         basicstyle = \footnotesize\ttfamily,
         numberstyle = \tiny, 
         numbersep = 5pt,     
         tabsize = 2,         
         extendedchars = true,
         breaklines = true,
         keywordstyle = \color{red},
    		 frame = b,
         stringstyle = \color{white}\ttfamily,
         showspaces = false,
         showtabs = false,
         xleftmargin = 17pt,
         framexleftmargin = 17pt,
         framexrightmargin = 5pt,
         framexbottommargin = 4pt,
         showstringspaces = false 
}

\lstloadlanguages{ MatLab }
\DeclareCaptionFont {white}{\color{white}}
\DeclareCaptionFormat {listing}{\colorbox[cmyk]{0.43, 0.35, 0.35,0.01}{\parbox{\textwidth}{\hspace{15pt}#1#2#3}}}
\captionsetup [lstlisting]{format = listing, labelfont = white, textfont = white, singlelinecheck = false, margin = 0pt, font = {bf,footnotesize}}

\title {Gaussian Processes}
\author {Vlad Lep \\
				 Mihai Adrian Morariu \\
				 Andrei Oghin\u{a}
				}

\begin {document}

\maketitle

\begin {abstract}

\

In this report, we describe the methods that can be used to compute the conditional and marginal distribution of some dimensions, given others, for a multivariate Gaussian distribution. We then apply the Gaussian Process algorithm on a training data set to perform regression and we aim to predict the target value of new input data. Finally, we evaluate, compare and discuss the experimental results.
\end {abstract} 

\section {The Gaussian Distribution}

\

\subsection {Contour}

\

The first exercise involved plotting, using \emph {contour}, the distribution of a 2-dimensional random variable $X = \left[ X_1, X_2 \right]$ with zero mean and the covariance matrix

\begin {align}
	\mathbf{\Sigma} = 
	\begin {bmatrix}
		1 & 0 \\
		0 & 3
	\end {bmatrix}
\end {align}

To do this, we kept in mind that, to properly represent the data that is normally distributed, one would need to take into account the x-values ranging between $\left[ \mu - 3 \sigma, \mu + 3 \sigma \right]$, where $\mu$ denotes the mean and $\sigma$ denotes the standard deviation. 
\begin {figure}[!h]
	\centering
	\includegraphics [scale = .4] {Variance.jpg}
	\caption {Plot of the normal distribution}
\end {figure}

In our case, the standard deviation was 1; so, on the X axis, we plotted the data ranging between $\left[ -3, 3 \right]$.
With the same reasoning, the data chosen on the Y axis ranged between $\left[ -5, 5 \right]$. After computing the value of the multivariate normal distribution function for these points, we generated the top-image from below using the \emph {contour} function.
\begin {figure}[!htp]
	\centering
	\includegraphics [scale = .45] {Ex1_1.jpg}
	\caption {The contour of the multivariate normal distribution (top) and the marginal distribution over $X_1$ (bottom)}
	\label {fig:1}
\end {figure}

\label {sec:1}
\subsection {Marginal distribution over $X_1$}

\

Next, we computed the marginal distribution over $X_1$. To do this, we took into account that $X_1$ was a normally distributed random variable with zero mean and unit variance. The image that we plotted is displayed in figure \eqref{fig:1}, bottom.

\

\label {sec:2}
\subsection {Conditional distribution over $X_1$}

\

We then wanted to compute the distribution over $X_1$ for $X_2 \in \left\{ -3, -2, \dots, 2, 3 \right\}$. To do so, for each $i \in \left\{ -3, -2, \dots, 2, 3 \right\}$, we needed to compute the probability $p \left( X_1 | X_2 = i \right)$, which is given by the formula
\begin {align}
	p \left( X_1 | X_2 = i \right) = \mathcal{N} \left( X_1; \mu_1 - \Lambda_{11}^{-1} \Lambda_{12} (i - \mu_2), \Lambda_{11}^{-1} \right)
\end {align}
where $\Lambda = \Sigma^{-1}$ and the means of $X_1$ and $X_2$ are $\mu_1$ and $\mu_2$, respectively. We obtained the plot of the conditional probability as shown in figure \eqref{fig:2}.

\begin {align}
\end {align}
\begin {figure}[!h]
	\centering
	\includegraphics [scale = .48] {Ex1_3.jpg}
	\caption {The conditional probability of $X_1$}
	\label {fig:2}
\end {figure}

\subsection {Plot3}

\

Next, we considered the covariance matrix 
\begin {align}
	\mathbf{\Sigma} = 
	\begin {bmatrix}
		1 & 0.7 \\
		0.7 & 1
	\end {bmatrix}
\end {align}
and wanted to plot the distribution using \emph {plot3}. To do this, since $\mbox{cov} \left( X_1, X_1 \right) = 1$, again we took into account the values $i$ for $X_1$ and $j$ for $X_2$ ranging in the discretized $\left[ -3, 3 \right]$ interval. For each two values, we computed $p \left( X_1 = i, X_2 = j \right)$. The result is displayed at the top of figure \eqref {fig:3}.
\begin {figure}[!h]
	\centering
	\includegraphics [scale = .22] {Ex1_4.jpg}
	\caption {The joint probability (top) and the marginal probability over $X_1$ (bottom)}
	\label {fig:3}
\end {figure}

\subsection {Marginal distribution over $X_1$}

\

The marginal distribution over $X_1$ was computed in basically the same way as in subsection \eqref {sec:1}, yielding the plot from the bottom of figure \eqref {fig:3}.

\

\subsection {Conditional distribution over $X_1$}

\

The conditional distribution over $X_1$ was computed in basically the same way as in subsection \eqref {sec:2}, yielding the plots from below.
\begin {figure}[!h]
	\centering
	\includegraphics [scale = .5] {Ex1_6.jpg}
	\caption {The conditional distribution over $X_1$}
	\label {fig:4}
\end {figure}

The covariance matrix, in the previous exercise, had zero non-diagonal values. This implied that $X_1$ and $X_2$ were independent, and so the distribution over $X_1$ didn't change if $X_2$ was observed. In the second case, the covariance matrix had non-zero values for its non-diagonal elements. Therefore, this time $X_1$ and $X_2$ co-varied, and observing $X_2$ affected the distribution of $X_1$. This is graphically confirmed in figures \eqref{fig:2} and \eqref{fig:4}.

\

\section {Gaussian Processes}

\

In second exercise, we performed nonlinear regression on a training data set $\left( x_n, t_n \right)_{n = \overline{1, N}}$ using the Gaussian Processes algorithm. Our aim was to predict, for a new input data $x_{N+1}$, the value of the target $t_{N+1}$. To do this, we applied the GP algorithm in order to evaluate the predictive distribution
\begin {align}
	p \left( t_{N+1} | t_1, \dots, t_N, x_1, \dots, x_N, x_{N+1} \right)
\end {align}
and chose a value for $t_{N+1}$ such as this probability be as large as possible. We obtained the regression plotted in the figure from below
\begin {figure}[t]
	\centering
	\includegraphics [scale = .3] {GP.jpg}
	\caption {Regression with the GP algorithm}
\end {figure}
where the red points denote the training data and the green points denote the test data.

\

\subsection {Performance on the test set}

\

We ran the program on a set comprising 15 data points. Out of these, 10 points were used for training and the remaining 5 points for testing. Below we provide a table with the original target values for the test data set and the predicted values after applying the GP algorithm:

\begin {align}
	\begin {tabular}{|c|c|c|c|c|c|}
		\hline
		Input test values ($\mathbf{x^*}$) & 17.1 & 14.7 & 16 & 17.1 & 20 \\ \hline
		Original target values & 82 & 69.7 & 71.6 & 80.6 & 88.6 \\ \hline
		Predicted target values ($t^*$) & 78.3617 & 70.4758 & 76.4447 & 78.3617 & 72.3993 \\
		\hline
	\end {tabular}
\end {align}

\

We could notice that the target values for some points are fairly well predicted, whereas for others (say, $\mathbf{x^*} = 20$), the difference between the actual and the predicted values is relatively large. We ran the program again on a set comprising 8 training data points and 7 test points and got the following results:

\begin {align}
	\begin {tabular}{|c|c|c|c|c|c|c|c|}
		\hline
		Input test values ($x^*$) & 18.4 & 15 & 17.1 & 14.7 & 16 & 17.1 & 20 \\ \hline
		Original target values & 84.3 & 79.6 & 82 & 69.7 & 71.6 & 80.6 & 88.6 \\ \hline
		Predicted target values ($t^*$) & 53.5226 & 69.2927 & 76.5344 & 67.0661 & 77.0830 & 76.5344 & 72.8361 \\
		\hline
	\end {tabular}
\end {align}

\

\begin {figure} [!h]
  \begin {center}
        \subfigure {\includegraphics [scale = 0.3] {5_Test_Set.jpg}} 
  			\subfigure {\includegraphics [scale = 0.3] {7_Test_Set.jpg}}
  \end {center}
  \caption {Applying the algorithm on a test set comprising 5 points (left) and 7 points (right)}
\end {figure}

\subsection {Dependence on the training set}

\

Obviously, the number of training data points had a strong influence over the performance of our classifier. Also, $\sigma$ denotes the amount of noise that we expected to have on our training data. Therefore, if $\sigma$ is 0, the estimation would pass exactly through the points. We pointed out the influence of $\sigma$ with four images where $\sigma$ is 0, 0.1, 0.5 and 1 (with $l$ set to 1) in the image from below.

\

\begin {figure} [!h]
  \begin {center}
        \subfigure {\includegraphics [scale = 0.3] {sigma0.png}} 
  			\subfigure {\includegraphics [scale = 0.3] {sigma-01.png}}
  \end {center}
  \caption {GP with $\sigma$ set to 0 (left) and to 0.1 (right)}
\end {figure}
\begin {figure} [!h]
  \begin {center}
        \subfigure {\includegraphics [scale = 0.3] {sigma-05.png}} 
  			\subfigure {\includegraphics [scale = 0.3] {sigma-1.png}}
  \end {center}
  \caption {GP with $\sigma$ set to 0.5 (left) and to 1 (right)}
\end {figure}

\

We observe that, for $\sigma = 0$, the best approximation for the training data will try to go through the all of the training points and thus overfit the training data. If $\sigma$ is 0.1, the best approximation will go very close to the points. Choosing $\sigma = 0.5$ makes the algorithm to consider a larger domain for the noise. This seems to be the case and we obtained better classification on the testing data with this value. In the other extreme, where $\sigma$ is 1, we noticed a large difference between the real target values and the prdicted ones. This will give worse results on the testing phase.

\subsection {Influence of the kernel function's parameter}

\


The parameter $l$ of the kernel function influenced the performance of our classifier. The kernel function was given by the formula
\begin {align}
	k \left( \mathbf{x}, \mathbf{x_n} \right) = \exp -\dfrac{1}{2 l} \left( \mathbf{x} - \mathbf{x_n} \right)^T \left( \mathbf{x} - \mathbf{x_n} \right)
\end {align}
 
We will try to explain on an example what happens when setting $l$ to different values and then explain what consequences these had on our classifier.
\begin {figure}[!h]
	\centering
	\includegraphics [scale = .5] {influence_of_l.jpg}
\end {figure}
In the image, we computed the kernel function around a fix point $x = 1$ and set $l$ to 1, 3 and 6. As we can see on the image, by doing so we increased the probability of observations that were further from the mean. 

\

Since each training point had an associated kernel function, $l$ influenced the probability of the surrounding points. If $l$ had been set to be larger, points that were farther from the training would still have had a significantly large probability. If $l$ had been small, only farther points would have had very small probability, near to 0.

\

Overall, for our classifier, had we set $l$ to be small, then the covariance (the yellow strip around the predicted values) around our training points would have been small; had $l$ increased, the covariance would have increased as well. A consequence of this is that for large values of $l$ the obtained classifier was smoother, not so influenced by the training point, while for small $l$, the classifier tried to fit the training point better. 

\subsection {Variance around the predictions}

\

For testing inputs between 12 and 22 we obtained a variance vector that looked as follows:
\begin {align}
 \mbox{var\_star\_vec(v*)} = [&1.0243, 0.9987, 0.9716, \dots, 0.2923, 0.2921, 0.2921, \nonumber \\
													    &0.2922, 0.2924, \dots, 0.9234, 0.9526, 0.9805] 
\end {align}

\

As shown by this vector, the variance was large when $x$ was near 12 and 22 (the margins) and decreased for $x$ near 16.
We can justify this by the lack of training points with the values near 12 or 22. Having no training data in that region caused our estimator to have lower confidence about its predictions. Therefore the form of the function, in that region, looked differently and the variance around the prediction was larger. 

\

As a conclusion we can state that the variance of the prediction is directly affected by the variance and density of the training data. Wherever there was more training data in a region, we had a lower variance and viceversa.

\subsection {Conclusions}

\

In this project, we plotted the multivariate Gaussian distribution for two random variables. We computed marginal  and conditional probabilities. We saw the impact of co-varying random variables on the conditional probability of one with respect to the other. 

\

In the end, we performed regression by implementing the Gaussian Processes algorithm. We tried to predict the corresponding target values of input test data by computing the means and variances of Gaussian distributions. 

%
%[23:02:29] Vlad Lep: function y = kernel(x, x_)
%[23:02:40] Vlad Lep: function y = kernel(x, x_)
%l = 3;
%y = exp(-1/(2*l) * (x - x_)'*(x - x_));
%end


%To apply the Gaussian process models, we needed to take into account the noise of the observed target values, which were given by
%\begin {align}
%	t_n = y_n + \epsilon_n\ \ \ \forall n \in \left\{ 1, \dots, N \right\}
%\end {align}
%where $y_n = y(x_n)$ and $\epsilon_n$ represented a random noise variable whose value was chosen independently for each observation $n$. We considered that the noise processes were normally distributed, so
%\begin {align}
%	p \left( t_n | y_n \right) = \mathcal{N} \left( t_n | y_n, \sigma^2 \right)\ \ \ \forall n \in \left\{ 1, \dots, N \right\}
%\end {align}
%
%\
%
%Therefore,
%\begin {align}
%	p \left( \mathbf{t_N} | \mathbf{y_N} \right) = \mathcal{N} \left( \mathbf{t_N} | \mathbf{y_N}, \sigma^2 \mathbf{I_N} \right)
%\end {align}
%where $\mathbf{t_N} = \left[ t_1, \dots, t_N \right]^T$, $\mathbf{y_N} = \left[ y_1, \dots, y_N \right]^T$ and $\mathbf{I_N}$ denotes the $N \times N$ identity matrix. From the definition of a Gaussian process, $\mathbf{y_N}$ was normally distributed with zero mean and covariance given by the Gram matrix $\mathbf{K}$, so that
%\begin {align}
%	p \left( \mathbf{y_N} \right) = \mathcal{N} \left( \mathbf{y_N} | 0, \mathbf{K} \right)
%\end {align}
%
%\
%
%The marginal distribution of $\mathbf{t_N}$ was then given by
%\begin {align}
%	p \left( \mathbf{t_N} \right) = \int p \left( \mathbf{t_N} | \mathbf{y_N} \right)\,d\mathbf{y} = \mathcal{N} \left( \mathbf{t_N} | 0, \mathbf{C_N} \right)
%\end {align}
%where \mathbf{C_N} was given by
%\begin {align}
%	\mathbf{C_N} = 
%\mathbf{t_N} | 0, \mathbf{C_N} \right)


\end {document}

