\documentclass [11pt] {article}

\usepackage {amsmath}
\usepackage {anysize}
\usepackage {fourier}
\usepackage {graphicx}
\usepackage {html, makeidx}

\title {Naive Bayes Spam Detector}
\author {Vlad Lep \\
				 Mihai Adrian Morariu \\
				 Andrei Oghin\u{a}
				}

\begin {document}

\maketitle

\

\begin {abstract}

\

In this report, we are presenting the Naive Bayes technique that we have used for discriminating between spam and non-spam email messages. The filtering process consists of two phases. In the first phase, we perform feature selection by determining which words present in the e-mails are most discriminative for the given classes. In the second phase, the knowledge from the first phase is used to determine whether a particular message (or many) is spam or not. Finally, we adjust the threshold used for deciding whether a message is spam or not so as to minimize the misclassification error and maximize the probability of correct identification.
\end {abstract} 

\section {Preliminaries}

\

For the classification process, we  have four sets of e-mails at our disposal, namely \emph {spam training e-mails}, \emph {spam testing e-mails}, \emph {ham training e-mails} and \emph {ham testing e-mails}. The training e-mails are used to create a set of words (typically, a string array) that we consider likely to appear in spam messages. We then use Bayes' rule to compute the probability that a specific e-mail message (or many) is spam or not, given the fact that some such words appear in the message(s) or not.

\

\section {Feature Selection}


\

\subsection {Approach}

\

The first step of the process is the feature selection. The computer is given two sets of e-mails, called \emph {training sets}. One such set, called \emph {spam training set}, consists of messages that are spams. The other set, called \emph {ham training set}, consists of legimitate messages.

\

We start by creating a data structure called \emph {spamList} that retains the words that are present in the spam training folder along with their frequencies. We then create another data structure called \emph {hamList} that retains the words that are present in the ham training folder along with their frequencies. This is done using the commands

\begin {center}
	\begin {minipage}[c]{8cm}
		spamList = countwords('spam\textbackslash train'); \\
		hamList = countwords('ham\textbackslash train');
	\end {minipage}
\end {center}

Using the {\bf wordtable} function and passing {\bf spamList} and {\bf hamList} as input parameters, we then create two data structures \emph {words} and \emph {counts} using the command

\begin {center}
	\begin {minipage}[c]{10cm}
		[words, counts] = wordtable(spamList, hamList);
	\end {minipage}
\end {center}


The {\bf words} cell array stores all the \emph {nTotalWords} words that are present within the spam and ham training sets. The $\mbox{nTotalWords} \times 2$ {\bf counts} matrix stores the frequency of each word within each of the spam and ham categories. On the first column, we store the frequency of each word within the spam training e-mails and on the second column the frequency within the ham training e-mails.

\

We then store the total number of spam training e-mails in the \emph {nSpamEmails} variable, the total number of ham training e-mails in \emph {nHamEmails} variable and the total number of words in \emph {nTotalWords} variable.  This is done using the commands

\

\begin {center}
	\begin {minipage}[c]{8cm}
		nSpamEmails = length(ls('spam\textbackslash train')) - 2; \\
		nHamEmails = length(ls('ham\textbackslash train')) - 2; \\
		nTotalWords = length(words);
	\end {minipage}
\end {center}

\

We need these values for computing the frequency rate of each of the words within each of the categories. Note that, when using the {\bf ls} command in MatLab, the output returned by the system consists of all the files in the current directory, but also the "." and ".." root directories. For this reason, we need to substract two from the value returned by the {\bf length} command.

\

Our goal is now identifying words that are representative to spam e-mails, based on the information that we have from the training data. We select the words that appear most frequently in spam training e-mails and least frequently in ham training e-mails with the command

\begin {center}
	\begin {minipage}[c]{12cm}
		counts(:, 3) = counts(:, 1) / nSpamEmails - counts(:, 2) / nHamEmails;
	\end {minipage}
\end {center}

This command creates a third column in the {\bf counts} matrix that stores, for each of the words in the vocabulary, the difference between the word's probability of appearance in spam training e-mails and the probability of appearance in ham training e-mails. We want to select those words for which this difference is as large as possible, so basically we need to sort the {\bf counts} matrix in descending order with respect to the third column. But the sorting the matrix does not imply sorting the {\bf words} vector, so we somehow need to create a link between this vector and the entires in the matrix. To do this, we create a fourth column in the {\bf counts} matrix that stores, for each of the {\bf nTotalWords} words in our vocabulary, its corresponding index $i \in \left\{ 1 \dots \mbox{nTotalWords} \right\}$ within the {\bf words} vector. Basically, this column contains all the numbers $\left\{ 1 \dots N \right\}$ in ascending order.

\begin {center}
	\begin {minipage}[c]{12cm}
		counts(:, 3) = counts(:, 1) / nSpamEmails - counts(:, 2) / nHamEmails; \\
		counts(:, 4) = [1:nTotalWords];
	\end {minipage}
\end {center}

We want to select only those words that are most representative for the spam category, so we need to sort the {\bf counts} matrix in descending order with respect to the third column.

\begin {center}
	\begin {minipage}[c]{12cm}
		counts = sortrows(counts, 3); \\
		counts = flipdim(counts, 1); \\ 

		sSpamWords = words(counts(1:nSpamWords, 4)); \\
		cSpamWords = cellstr(sSpamWords');
	\end {minipage}
\end {center}

\

Finally, we store the first \emph {nSpamWords} (a predefined constant) in the \emph {sSpamWords} string array and convert it to the \emph {cSpamWords} cell array. We do this because later on we will need the information from {\bf sSpamWords} under the form of a cell array.

\

\subsection {Results}

\

In our program, we took into account the first 20 words as mentioned below. We chose this number by running the program on the same training data set multiple times and trying to determine which number maximizes the probability of correct classification. We were aware that large values of the threshold would cause the problem of overfitting, whereas small values would make the program underfit. After several trial and error approaches, $\mbox{nSpamWords} = 20$ turned out to be a reasonable value for our threshold variable. We provide, below, the list of the words that the program selected, along with their frequency rates within each categories and the differences between these frequency rates, the criteria for selection.

\begin {align}
	\begin {tabular} {|c|c|c|c|}
			\hline
			{\bf Word}  & {\bf Frequency rate in spam e-mails} & {\bf Frequency rate in ham e-mails} & {\bf Difference} \\
			\hline
			\multicolumn {3} {c}{ } \\ 
			\hline
			HTTP        & 0.6813                               & 0.0929                               & 0.5884 \\
    	COM         & 0.4286															 & 0.1814 															& 0.2472 \\
    	SG          & 0.2198 															 & 0.0044 															& 0.2154 \\
    	MEDS        & 0.1648 															 & 0 																		& 0.1648 \\
    	BEST        & 0.2143 															 & 0.0575 															& 0.1568 \\
    	CN          & 0.1319 															 & 0																		& 0.1319 \\
    	WWW         & 0.1593 															 & 0.0664 															& 0.0930 \\
    	YOUR        & 0.4835 															 & 0.3938 															& 0.0897 \\
    	WATCHES     & 0.0824 															 & 0 																		& 0.0824 \\
    	PILLS       & 0.0824 															 & 0 																		& 0.0824 \\
    	MR          & 0.0934 															 & 0.0133 															& 0.0801 \\
    	NO          & 0.2253 															 & 0.1460 															& 0.0793 \\
    	CHEAP       & 0.0769 															 & 0 																		& 0.0769 \\
    	LIFE        & 0.0879 															 & 0.0133 															& 0.0746 \\
    	SAVE        & 0.0824 															 & 0.0088																& 0.0736 \\
    	THOUSAND    & 0.0714 															 & 0 																		& 0.0714 \\
    	HUNDRED     & 0.0714 															 & 0																		& 0.0714 \\
    	FUNDS       & 0.0714 															 & 0.0044																& 0.0670 \\
    	PURCHASE    & 0.0769 															 & 0.0133																& 0.0636 \\
    	RECEIVE     & 0.0989 															 & 0.0354																& 0.0635 \\
    	\hline
		\end {tabular}
	\nonumber
\end {align}

\newpage

\section {Building the Classifier}

\

\subsection {Approach}

\


Classifying a particular e-mail as being spam or ham first involves determining a binary vector \emph {featVec}, called \emph {the feature vector}, associated to the message. For each of the spam words in the {\bf cSpamWords} array, we associate a cell $i$ in {\bf featVec} in such a way that
\begin {align}
	\mbox{featVec(i) = }
	\begin {cases}
		1,\ \ \ \mbox{if the word is contained in the message} \nonumber \\
		0,\ \ \ \mbox{otherwise}
	\end {cases}
\end {align}

This is done by using the {\bf presentre} command that receives as input parameters the path of the file and the cell array containing the spam words.

\begin {center}
	\begin {minipage}[c]{12cm}
		featVec = presentre([testDir, spamTestFile], cSpamWords);
	\end {minipage}
\end {center}

Next, we proceed to computing the probability that the e-mail is spam with the aid of Bayes' rule. 

\begin {align}
		\label {Bayes}
		p \left( \mbox{Spam | featVec} \right) = \dfrac{p \left( \mbox{featVec | Spam} \right) p \left( \mbox{Spam} \right)}{p \left( \mbox{featVec | Spam} \right) p \left( \mbox{Spam} \right) + p \left( \mbox{featVec | Ham} \right) p \left( \mbox{Ham} \right)}
\end {align}

According to \htmladdnormallink {the statistics provided by Symantec in January 2011}{http://www.symantec.com/about/news/release/article.jsp?prid=20110125_01}
, spam messages make up 78.6\% of the worldwide e-mails, whereas only 21.4\% of the message are legitimate. For our computation, this means that
\begin {align}
	&p \left( \mbox{Spam} \right) = 0.786 \nonumber \\
	&p \left( \mbox{Ham} \right) = 0.214 \nonumber
\end {align}
which are predefined constants in our program, denoted by \emph {prob\_C1} and \emph {prob\_C2}. The only values that are left to be computed are $p \left( \mbox{featVec | Spam} \right)$ and $p \left( \mbox{featVec | Ham} \right)$. At this point, we make the naive assumption that the selected spam words appear independently of each other within an e-mail. Mathematically, this would be expressed as 

\begin {align}
	p \left( \mbox{featVec} | C_k \right) &= \prod_i {\mu_{ik}}^{\mbox{featVec(i)}} \left( 1 -  \mu_{ik} \right)^{1 - \mbox{featVec(i)}} \nonumber
\end {align}
where $C_1$ corresponds to the spam class, $C_2$ to the ham class and $\mu_{ik} = p \left( \mbox{featVec(i)} | C_k \right)$ to the frequency rate of the i-th spam word from {\bf cSpamWords} within category $C_k$.

\

The value $p \left( \mbox{featVec} | C_k \right)$ is worked out by a separate function in the program, denoted as \emph {p}, by first computing $\mu_{ik}$ and then computing the product from above.

\begin {center}
	\begin {minipage}[c]{12cm}
		for i = 1:nFeatVec \\
    \mbox{\ \ \ }miu\_IK = (const + counts(i, k)) / (2 * const + nTotalMails); \\
    \mbox{\ \ \ }prob\_x\_Ck = prob\_x\_Ck * (miu\_IK\^x(i)) * (1 - miu\_IK)\^(1 - x(i)); \\
		end
	\end {minipage}
\end {center}

$\mu_{ik}$ is computed by dividing the number of occurences of the i-th word within $C_k$, {\bf counts(i, k)}, to the total number of e-mails of the category, \emph {nTotalMails}. Because it is not realistic to have probabilities of 0 or 1 for a feature, we use Laplace smoothing to regularize this formula. By adding a constant to the numerator and two times that constant to the denominator we change the extreme probabilities. For example, to compute the frequency rate of the i-th word of {\bf cSpamWords} within the spam category $C_1$, we compute $\dfrac{\mbox{const} + \mbox{counts}(i, 1)}{2 \times \mbox{const} + \mbox{nSpamEmails}}$.

\

Laplace smoothing does not change equal probabilities (50\%) and has smaller influences as probabilities for an event and its complementary become closer, towards 50\%. Basically the constant describes how many time we assume we have seen that feature before. It is important how we choose this constant not. If we choose a big constant, comparing with the number of email, it will have a big influence on the probabilities. It will smooth them too much, making them become very similar, having values around 50\%. If the constant is too small, the 0 probabilities will not change very much and although they will not make the product 0, they will decrease it significantly. 

\

Example: for the number {\bf nSpamEmails} of spam e-mails, the constant will be

\begin {itemize}
	\item \textbf{big}: $\mbox{const } = 100  \times \mbox{{ nSpamEmail}} \Rightarrow \mu = \dfrac{100 * \mbox{nSpamEmail} * \mbox{count(i,l)}}{200 * \mbox{nSpamEmails} + \mbox{nSpamEmails}} \rightarrow $ 0.5.
 
	\item \textbf{small}: $\mbox{const} = 10^{-6}  \times \mbox{{ nSpamEmail}} \Rightarrow \mu = \dfrac{ 10^{-6}* \mbox{nSpamEmail} * \mbox{count(i,l)}}{2 *10^{-6} * \mbox{nSpamEmails} + \mbox{nSpamEmails}} \Rightarrow $ \texttt{will not influence enough}
%	\item big: $\mbox{const + counts(i, k)} = 100 \times \mbox{{\bf nSpamEmail}} \Rightarrow \mu = \mbox{const} + \dfrac{\mbox{count}}{2}
% \mbox{const} + \dfrac{\mu}{2}$
%	\item small: $\mbox{const} = 10^{-6} \times \mbox{{\bf nSpamEmail}} \Rightarrow \mu = \mbox{const} + \dfrac{\mbox{count}}{2} \mbox{const} + \mu - $ will not influence enough.
\end {itemize}

\

Based on these, we have chosen the constant to be 10\% of the total number of spam e-mails.

\

We then have all the required information for computing the probability that the e-mail is spam, $p \left( \mbox{Spam | featVec} \right)$, using the Bayes' rule \eqref {Bayes}. We classify the message as being spam if this value is greater than a predefined variable named \emph {threshold}, initially set to 0.7. Otherwise, we classify it as being ham.

\

\subsection {Results}

\

With the {\bf threshold} variable initially set to 0.7, we ran the program on the spam training data set. The program correctly classified 81.48\% of the messages as being spam and wrongly classified 18.52\% of them as being ham. We then tested on the ham training data set. The program correctly classified 84.03\% of the messages as being ham and wrongly classified 15.97\% of them as being spam.

\newpage

\section {Evaluation}

\

\subsection {Approach}

\


In the evaluation part, our goal is to analyze how well the program performed on the testing set. That is, if we analyze the spam testing set, we wish to know the frequency rate \emph {pCorrectS} of the e-mails correctly identified as being spam and the frequency rate \emph {pWrongS} of the e-mails wrongly identified as being ham. If we analyze the ham testing set, we wish to know the frequency rate \emph {pCorrectH} of the e-mails correctly identified as being ham and the frequency \emph {pWrongH} of the e-mails wrongly identified as being spam. This information will help us adjust the value of the threshold so as to improve the frequency rate of correct identification and to reduce the frequency rate of misclassification.

\


For each of the testing data set, we compute the probability $p \left( \mbox{wrong} | C_k \right)$of wrongly classified e-mails. This probability is obtained by dividing the number of wrongly classified e-mails of each class by the total number of testing e-mails. The probability of correct classification is then 1 - $p \left( \mbox{wrong} | C_k \right)$. For example, if we choose to test the spam set, the probability of misclassification is obtained by dividing the number of e-mails wrongly identified as being ham to the total number of testing e-mails.

\

\begin {center}
	\begin {minipage}[c]{12cm}
		if (spamChoice == true)
			\mbox{\ \ \ }probWrong = (nTestingMails - nIdentifiedSpam) / nTestingMails; \\
			\mbox{\ \ \ }probCorrect = 1 - probWrong; \\
		else \\
			\mbox{\ \ \ }probWrong = nIdentifiedSpam / nTestingMails; \\
			\mbox{\ \ \ }probCorrect = 1 - probWrong; \\
		end
	\end {minipage}
\end {center}

\

With this information, we can plot the Receiver Operating Characteristics (ROC) curve. This curve is useful for determining an optimal value of the threshold that maximizes the probability of correctly identified spam e-mails and minimizes the probability of wrongly identified ham e-mails. This involves choosing, for the {\bf thershold} variable, values that lie in the interval $[0, 1]$. For each of these variables, we run the classification test again for both spam and ham sets and determine the values of {\bf pCorrectS} and {\bf pWrongH}. We then plot these values on the X and Y axis, accordingly, obtaning the curve from below.

%\begin {figure} [htp]
%\begin {center}
%	\includegraphics [scale = .45] {ROC.jpg} \nonumber
%\end {center}
%\end {figure}

\begin{figure} 
	\hspace {-2.4 cm}
		\includegraphics [scale = .45] {ROC.jpg}
\end{figure}


\

\subsection {Results}

\

We have used the Receiver Operating Characteristics curve to determine a value for the {\bf threshold} variable that maximizes the probability of correctly classifying spam e-mails and minimizes the probability of wrongly classifying ham e-mails. Since it is more important to NOT classify legitimate e-mails as being spam, because these might contain valuable information for the user, we set the threshold variable to 0.89. For this value, the probability for correctly classified ham e-mails raised to 95.14\% and the probability for wrongly classified ham e-mails dropped to 4.86\%. At the same time, the probability for correctly classified spam e-mails dropped to 68.52\% and the probability for wrongly classified spam e-mails raised to 31.48\%.

\

\section {Conclusions}

\

For an efficient spam filter, it is always important that we have enough training data in order to select the words that are most representative for spam e-mails and to avoid overfitting. Moreover, one must make a tradeoff between better identification of spam e-mails or the decrease of the probability of misclassifying ham e-mails. It is more harmful for the user if he / she does not receive a legitimate e-mail that might contain valuable information than in the case he / she receives a spam e-mail. Therefore, it is important that proper values for {\bf nSpamWords} and {\bf threshold} are chosen, otherwise the probability of misclassification may increase for each of the categories.

\end {document}