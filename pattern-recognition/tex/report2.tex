\documentclass [11pt] {article}

\usepackage {amsmath}
\usepackage {anysize}
\usepackage {caption}
\usepackage {color}
\usepackage {courier}
\usepackage {graphicx}
\usepackage {listings}
\usepackage {subfigure}

\lstset{
         basicstyle = \footnotesize\ttfamily,
         numberstyle = \tiny, 
         numbersep = 5pt,     
         tabsize = 2,         
         extendedchars = true,
         breaklines = true,
         keywordstyle = \color{red},
    		 frame = b,
         stringstyle = \color{white}\ttfamily,
         showspaces = false,
         showtabs = false,
         xleftmargin = 17pt,
         framexleftmargin = 17pt,
         framexrightmargin = 5pt,
         framexbottommargin = 4pt,
         showstringspaces = false 
}

\lstloadlanguages{ MatLab }
\DeclareCaptionFont {white}{\color{white}}
\DeclareCaptionFormat {listing}{\colorbox[cmyk]{0.43, 0.35, 0.35,0.01}{\parbox{\textwidth}{\hspace{15pt}#1#2#3}}}
\captionsetup [lstlisting]{format = listing, labelfont = white, textfont = white, singlelinecheck = false, margin = 0pt, font = {bf,footnotesize}}

\title {Expectation Maximization}
\author {Vlad Lep \\
				 Mihai Adrian Morariu \\
				 Andrei Oghin\u{a}
				}

\begin {document}

\maketitle

\

\begin {abstract}

\

In this report, we are presenting the probabilistic classifier that we have built for discriminating between two classes A and B that some given data points belong to. In the first task, we were asked to split the data in training and testing sets, training some parameters and use Bayes' rule to compute the posterior probability $p(C_k | x)$. In the second task, we were asked to implement the EM algorithm to discriminate between the two classes. Finally, we were asked to correct numerical underflows which appear in the computation of logarithms of sums of probabilities, by implementing the \emph {logsumexp} function.
\end {abstract} 

\

\section {Gaussian Distribution}

\

\subsection {Approach}

\

The first task involved computing the posterior probability $p(C_k | x),\ k = \overline{1, 2}$, for a given set of data points. We started by loading, from a separate file, the data belonging to each of the two classes.

\

After plotting the data, we observed the two closely facing banana shaped data set. Visual observation suggested that having a model with two components would yield a relatively high missclasification rate, due to the points situated in the top and bottom extremities (the banana tails). Four components would probably provide a better model for the data, given these shapes (two components for each half of each of the two bananas). Increasing the number of components might provide further small improvements, but our assumption was that these improvements would not justify the increase in model complexity and the higher risk of overfitting. These assumptions were to be tested in the experiments described in the following sections.

\

Next, we split the data from each of the classes into a training set and a testing set, by using the \emph {splitData} function:

\begin {center}
	$\left[\mbox{trainA, testA}\right]$ = splitData(A); \\
	$\left[\mbox{trainB, testB}\right]$ = splitData(B);
\end {center}

This function gets the data set stored in the \emph {data} input argument and returns two 2-dimensional vectors, \emph {trainData} and \emph {testData}. The {\bf trainData} vector contains the first \emph {coeff} $\times N$ elements of {\bf data}, where $N$ is the length of {\bf data} and {\bf coeff} is a predefined constant between 0 and 1. The {\bf testData} vector contains the rest of the elements from {\bf data}. For example, if {\bf coeff} = 0.75 and {\bf data} has a length of 1000, the training set will comprise the first 750 elements and the testing set will comprise the rest of 250 elements.

\

Having splitted the data of each class into training sets \emph {trainA}, \emph {trainB} and testing sets \emph {testA}, \emph {testB}, we computed the prior probabilities $pA = p(C_A)$ and $pB = p(C_B)$ by dividing the number of elements in training set A to the total number of training elements and the number of elements in training set B to the total number of training elements, respectively. In the next step, we computed the means $\mu_A$, $\mu_B$ and covariances $\Sigma_A$, $\Sigma_B$ for the points of each of the training sets. We needed these parameters for modelling the class-conditional probability with a multi-variate normal distribution. These could be either computed with the formulas obtained from maximizing the log-likelihood probability; that is

\[ \mu_A = \dfrac{1}{N_A} \sum_{\mathbf{x}_n \in A} \mathbf{x}_n\ \ \ \ \ \mu_B = \dfrac{1}{N_B} \sum_{\mathbf{x}_n \in B} \mathbf{x}_n \]
\[ \Sigma_A = \dfrac{1}{N_A} \sum_{\mathbf{x}_n \in A} \left( \mathbf{x}_n - \mu_A \right) \left( \mathbf{x}_n - \mu_A \right)^T\ \ \ \ \ \Sigma_B = \dfrac{1}{N_B} \sum_{\mathbf{x}_n \in B} \left( \mathbf{x}_n - \mu_B \right) \left( \mathbf{x}_n - \mu_B \right)^T \]

\

We then used the testing sets to check the performance of the classifier. We first computed the class-conditional probabilities $p(\emph{xTestA} | C_A)$ and $p(\emph{xTestA} | C_B)$ with the commands

\begin {center}
	pXAA = mvnpdf(testA, muA, sigmaA); \\
	pXAB = mvnpdf(testA, muB, sigmaB);
\end {center}
and then used Bayes' rule to compute the probability that each of the testing sets belongs to its corresponding classes. For example, to compute the probability of observing class A, given the data points in testing set A, we used the command

\begin {center}
	pAX = pXAA * pA./(pXAA * pA + pXAB * pB);
\end {center}

The same reasoning applies to the points contained in class B. Applying this algorithm to each of the testing sets results in the possibility of determining how many points of each of the testing sets belong to their corresponding class. That is, the points from the {\bf pAX} vector that have a classification probability $> 0.5$ were classified as belonging to class A and the points from the {\bf pAX} vector that have a classification probability of $\leq 0.5$ were classified as belonging to class B. The same reasoning applies to the points contained in the {\bf pBX} vector.

\subsection {Results}

\

We ran the program on three testing sets. Out of a total of 1000 data points for each class, the first testing set comprised 150 data points, the second one comprised 250 data points and the third one comprised 500 data points. In each case, the remaining data points were used for training. For each of the testing sets, we computed the confussion matrices $C_1$, $C_2$ and $C_3$, respectively, to get an image of how well the classifier works.

\

In the first case, the program correctly classified 136 points from {\bf testA} as belonging to class A (which yields an accuracy of 90.6\%) and 135 points from {\bf testB} as belonging to class B (which yields an accuracy of 90\%), leading to the following confussion matrix:

\begin {center}
	$C_1 = $
	$\begin {bmatrix}
		136 & 14 \\
		15 & 135
	\end {bmatrix}$
\end {center}

\

In the second case, the program correctly classified 226 points from {\bf testA} as belonging to class A (which yields an accuracy of 90.4\%) and 224 points from {\bf testB} as belonging to class B (which yields an accuracy of 89.6\%), leading to the following confussion matrix:

\begin {center}
	$C_2 = $
	$\begin {bmatrix}
		226 & 24 \\
		26 & 224
	\end {bmatrix}$
\end {center}

\

Finally, the program correctly classified 446 points from {\bf testA} as belonging to class A (which yields an accuracy of 89.2\%) and 447 points from {\bf testB} as belonging to class B (which yields an accuracy of 89.4\%), leading to the following confussion matrix:

\begin {center}
	$C_3 = $
	$\begin {bmatrix}
		446 & 54 \\
		53 & 447
	\end {bmatrix}$
\end {center}

\

\begin {figure}[!h]
	\centering
	\caption {Misclassified points are highlighted as colored stars}
	\includegraphics [scale = .45]{ex1_error_2.jpg}
\end {figure}

\

\section {Mixture of Gaussians}

\

In this exercise, we implemented the required primitives for the EM algorithm and tested our implementation using the provided dataset and framework.


\subsection {Approach}

\

The second task involved implementing the EM algorithm to discriminate between the two classes A and B, using mixtures of Gaussians. For our task, we were required to create a function for each of the three steps of the EM algorithm: \emph {initialization}, \emph {E-step},  \emph {M-step} and a testing function.

\

\subsubsection {Initialization}

\

We tested our algorithm on two types of initializations: a "dummy" one and a "smart" one. For the "dummy" version we initialised each of the means of the Gaussians with a slightly varied mean of the data set, and the variance with the variance of the data set. We also tried to initialize all the Gaussians in the same point, but obviously this made our algorithm block at the same point.

\

Our "smart" version split each of the two classes into C/2 subsets, where C was the number of Gaussians, each subset comprising the data points of a mixture component. We wanted to choose the means and the covariances so as to reduce the number of iterations in the subsequent steps of the EM algorithm. To do this, we sorted out the points, based on their Y value, and split the data points into C/2 groups, each of them describing a mixture component. The initial value for the mixture component was chosen to be the mean of those data points and their covariance. We did the same for the other class B. The initial probability of observing the components, was $\dfrac{1}{\mbox{Total number of mixture components}}$, so, in our case, $\dfrac{1}{C}$. We stored the results in the \emph {MOG} data structure, which was needed for the next steps of the algorithm.

\begin{figure}[htp]
  \begin{center}
    \subfigure[Fig 1. Smart Initialization]{\includegraphics[scale=0.35]{MixtureComponents.jpg}}
    \subfigure[Fig 2. Dummy Initialization]{\includegraphics[scale=0.48]{mplr-3-bad-init-1.png}} \\
  \end{center}
\end{figure}

\

\subsubsection {E-step}

\

In the E-step, we computed the responsabilitites $\gamma$ for each data point of each class, with respect to the current parameter values. These reflected the posterior probabilities of each mixture component with respect to the input data, and were used to adjust the parameters of the distributions during the M-step. 

\

In order to do so, we iterated through the training data, and, for each Gaussian, we computed the probability of that point for that particular Gaussian. We obtained, after this step, an $N \times C$ matrix \emph {Q}, where $N$ was the total number of training points and $C$ was the number of Gaussians. 

\[ \gamma_k(\mathbf{x}_i) = \dfrac{\pi_k \mathcal{N}(\mathbf{x}_i; \mu_k, \Sigma_k)}{\sum_j{\pi_j \mathcal{N}(\mathbf{x}_i; \mu_j,\Sigma_j) }} \]
In this formula, $\gamma_k(\mathbf{x}_i)$ denotes the responsability of the Gaussian k for the point $\mathbf{x}_i$ and this value was stored in $Q(i,j)$. \\

\

We also computed, during this step, the log-likelihood of the data for the current mixture model, so that a test for convergence could be performed. This was done by determining "the moment" when the change of the log-likelihood function fell bellow a certain threshold.

\[ \log \left( \mathbf{x}_1, \dots, \mathbf{x}_N \right) = \sum_{i = 1}^N \log \sum_{k = 1}^N \pi_k \mathcal{N} \left( \mathbf{x}_i, \mu_k, \Sigma_k \right)\]

\begin{lstlisting}[caption = The code for computing $\gamma_k(\mathbf{x}_i)$:]
C = length(MOG);
Q = zeros(N,C);

for i = 1:N
    downVal = 0;
    
    for j = 1:C
        downVal = downVal + MOG{j}.PI * exp(lmvnpdf(X(i, :), MOG{j}.MU, MOG{j}.SIGMA));
    end
    
    for j=1:C
        Q(i, j) = MOG{j}.PI * exp(lmvnpdf(X(i, :), MOG{j}.MU, MOG{j}.SIGMA)) / downVal;
        
    end
end
\end{lstlisting}

\

\begin{lstlisting}[caption = The code for computing $\log(\mathbf{x}_1 \dots \mathbf{x}_N)$:]
LL = 0;

for i = 1:N
	innerSum = 0;
		
	for j = 1:C
		innerSum = innerSum + MOG{j}.PI * exp(lmvnpdf(X(i, :), MOG{j}.MU, MOG{j}.SIGMA));
	end
	
	LL = LL + log(innerSum);
end
\end{lstlisting}


\

We used the {\bf lmvnpdf} instead of {\bf mvnpdf}, as we noticed some precision problems that this function had for small values of the covariance.

\

\subsubsection {M-step}

\

In this step, we used the newly computed responsabilitites to perform new estimations on the means, covariances and mixing coeficients for each mixture component, based on the previous values of these parameters.

\[ \mu_k = \dfrac{\sum_{i = 1}^N \gamma_k(\mathbf{x}_i) \mathbf{x}_i }{\sum_{i = 1}^N \gamma_k(\mathbf{x}_i)}\ \ \ \ \pi_k = \dfrac{ \sum_{i = 1}^N \gamma_k(\mathbf{x}_i) }{N} \]
\[ \Sigma_k = \dfrac{\sum_{i = 1}^N \gamma_k(\mathbf{x}_i) \mathbf{x}_i \mathbf{x}_i^T}{\sum_{i = 1}^N \gamma_k(\mathbf{x}_i)} - \mu_k \mu_k^T \]

\

It was important to avoid singularities. Otherwise, we were at risk of losing a mixture component, since it could have become useless once it had collapsed on a single data point. This was done by computing the sensitivity of $ \Sigma $, and skipping the parameter update step if the value was above a certain threshold. We implemented this test at the end of the M-step.


\subsection {Results}

\

We analized the algorithm in terms of prediction, accuracy and speed, by varying different aspects:

\begin {itemize}
	\item initialization
	\item number of components
	\item epsilon value
	\item percent of training data and test data
\end {itemize}

\ 

The training sets were sampled randomly from the training data of each class. From the distribution of the points we assumed that two Gaussian components for each class were able to describe the data with good preformance, so we used this setting when comparing aspects. 

\

\textbf{Initialization}

\

First we checked our algorithm with our "dummy" initialization, taking half of the total data points as training data and the other half as test data. The chosen number of components was two.

\

Convergence was obtained after 156 iterations.. We obtained an error in classification of 6\% for class A and of 4.5\% for class B. We repeated this experiment several times, but at one run we obtained a bad result. The algorithm converged but the found solution was not the global optimum. This made sense since we poorly initialized the parameters. After 96 iterations,  the algorithm stopped after reaching a local minimum. The figures from below show the Gaussian distributions at the initialization and at the end.

\begin{figure}[htp]
  \begin{center}
        \subfigure[Fig. Dummy Initialization ]{\includegraphics[scale=0.48]{mplr-3-bad-init-1.png}} 
  			\subfigure[Fig. Final result with dummny init. that converged correctly]{\includegraphics[scale=0.48]{bad-init-done-156-iterations.png}}
 				\subfigure[Fig. Final result with dummny init. that did not converged correctly] {\includegraphics[scale=0.49]{bad-init-done-96-iterations.png}}
  \end{center}

\end{figure}

\

When running with "smart" initializations, the program needed only 50 iterations on average to converge and the error in classification was 3.4\% for A and 1.8\% for B. This was our baseline and we used it to compare with the other runs:

\begin {itemize}
	\item 50\% training data
	\item 4 Gaussian Components
	\item given epsilon:  1e-5
	\item smart initialization
\end {itemize}
\

As noticed, the smart initialization improves the speed of our algorithm significantly, and also yields better results. 
\begin{figure}[htp]
  \begin{center}
        \subfigure[Fig. Smart Initialization]{\includegraphics[scale=0.35]{MixtureComponents.jpg}} 
        \subfigure[Fig. Results obtained with the smart Initialization]{\includegraphics[scale=0.43]{smart-50-2-gaus-end.png}}\\
   \end{center}
\end{figure}

\

\textbf{Number of components}

\

We then modified the number of components to see how this could affect our performance. As pointed out by the first exercise, having one Gaussian component for each class is insufficient. We tried to use six Gaussian components and noticed that the number of interations needed to converge was slightly lower than 40, but it was computationally more expensive. The performance increased as well: we got an error of 2.2\% for A and 1.2\% for B. Still the increase in performance was balanced by the increase in complexity.

\begin{figure}[htp]
  \begin{center}
        \subfigure[6 components at the beginning]{\includegraphics[scale=0.3]{3-gauss-inceput.jpg}}
        \subfigure[Results obtained with 6 components]{\includegraphics[scale=0.3]{3-gauss-end.jpg}} \\
   \end{center}	
\end{figure}

\begin{figure}[htp]
  \begin{center}
        \subfigure[8 components at the beginning]{\includegraphics[scale=0.37]{8-gauss-begin.png}} 
        \subfigure[Results obtained with 8 components]{\includegraphics[scale=0.49]{8-gauss-end.png}} \\
   \end{center}	
\end{figure}

\

In the end, we ran the program with eight Gaussian components and noticed the over-fitting problem. The number of iterations increased rapidly to 144 and the computation time and the error rate dropped to 2.4\% A and 1.0\% for B. We noticed that increasing the number of components up to a certain point would increase the overall performance and this was justified by considering four Gaussian components instead of two. The error decreased from an average of 10\% to 2.6\%. From our point of view, increasing the number of components to six would have only been justified in some other situations, where the error rates were esential. Otherwise, the increase in complexity would have made this modification unjustified.

\

\textbf {Influence of epsilon}

\

We also researched the influence of the epsilon parameter from the $em\_log$ function. Basically, the value of epsilon determined when the algorithm would stop. We noticed that taking epsilon = 0.00003 (3 times larger) would decrease the error of misclassification for A - from 3.4\% to 3.2\%, and for B - from 1.6\% to 1.4\%. The number of steps also decreased with two units. This is unsignificant but when setting epsilon to be thirty times larger than the default value, we noticed a major decrease in the number of steps until converge was reached - 20 instead of 50. Surprisingly, the error rate also decreased for A to 2.5\% and remained the same for B, 1.6\% . This was explained by over-fitting, by trying to describe very accurately the training data. This modification of epsilon seemed in this context to have had much more contribution than increasing the number of Gaussian components.

\

\textbf {Influence of training data}

\

Increasing the amount of training data from 50\% of the available data to 75\% gave a lower error for the base algorithm. The error dropped to 2.8\% for A and 0.8\% for B. This is always the case, since having more training data improves the prediction. However, by increasing the training data, less testing data became available.

\

\subsection {Log-Probabilities}

\

The third and final task involved correcting numerical underflows which arrise in the computation of logarithms of sums of probabilities $\log \left( p(a) + p(b) \right)$, when $\log \left( p(a) \right)$ and $\log \left( p(b) \right)$ are known. The problem that arrises in this case is that, if we tried to compute

\[ \log \left( p(a) + p(b) \right) = \log \left( \exp \left( \log \left( p(a) \right) \right) + \exp \left( \log \left( p(b) \right) \right) \right) \]
and $\log \left( p(a) \right)$ or $\log \left( p(b) \right)$ were very small (for example, -1000 and -1001), MatLab would approximate $\exp \left( \log \left( p(a) \right) \right)$ and $\exp \left( \log \left( p(b) \right) \right)$ as 0, leading to a wrong computation of our value \mbox{($\log(0) = -\infty$)}, because the correct value is -999.6867. 

\

By implementing the \emph {logsumexp} function, we computed a more accurate value of $\log \left( p(a) + p(b) \right)$ in terms of $\log \left( p(a) \right)$ and $\log \left( p(b) \right)$ using the formula

\begin {align}
	\log \left( p(a) + p(b) \right) &= \log \left( p(a) \right) + \log \left[ 1 + \exp \left( \log \left( p(a) \right) - \log \left( p(b) \right) \right)\right] \\
																	&= \log \left( p(b) \right) + \log \left[ 1 + \exp \left( \log \left( p(b) \right) - \log \left( p(a) \right) \right)\right]
\end {align}
These two equivalent formulas provided us with a safer way to compute the value of the logarithm since, if the difference between $\log \left( p(a) \right)$ and $\log \left( p(b) \right)$ was small enough, the exponential function would return the precise value; otherwise it would return 0 and the returned value for $\log \left( p(a) + p(b) \right)$ would be $\log \left( p(a) \right)$ or $\log \left( p(b) \right)$. So we took the largest value out of the $\log$ and computed our value using either formula (1) or formula (2). That is, is $\log \left( p(a) \right) > \log \left( p(b) \right)$, we used formula (1); otherwise, we used formula (2).

\

We then improved the {\bf logexpsum} function so that it could also take two matrices $A$ and $B$ as input arguments. We computed the {\bf logexpsum} for each of the elements $a_{ij}$ and $b_{ij}$ and stored the result in $c_{ij}$.

\

\begin {lstlisting}[caption = Computing the {\bf logexpsum} function for two matrices]
function C = logsumexp(A, B)

nRows = size(A, 1);
nCols = size(A, 2);

C = zeros(nRows, nCols);

for i = 1:nRows
    for j = 1:nCols
        logA = A(i, j);
        logB = B(i, j);
        
        if (logA > logB)
            logAB = logA + log(1 + exp(logB - logA));
        else
            logAB = logB + log(1 + exp(logA - logB));
        end
        
        C(i, j) = logAB;
    end
end
end
\end{lstlisting}

\newpage

\subsection {Results}

\

The new function correctly computed the value $\log \left( p(a) + p(b) \right)$ for $\log \left( p(a) \right) = -1000$ and $\log \left( p(b) \right) = -1001$, yielding the correct result of -999.6867. Also, the same function could be used to compute, recursively, the logarithm of a sum of multiple probabilities, since
\[ \log \left( p(a) + p(b) + p(c) \right) = \log \left( p(c) \right) + \log \left[ 1 + \exp \left( \log \left( p(a) + p(b) \right) - \log \left( p(c) \right) \right)\right] \]
and $\log \left( p(a) + p(b) \right)$ can be computed with the algorithm described above.

\

\section {Conclusions}

\

In this report, we described how to build a probabilistic classifier and estimate its parameters. We showed how the class conditional distributions can be modelled using a mixture of Gaussians, and how to use the EM algorithm to optimise the log-likelihood function. We discussed and experimented with various ways of initialising the parameters, and defended our choice of number of components to use based on the classification accuracy, model complexity and overfitting risk. Moreover, we implemented the method and explained the importance of avoiding singularities. Finally, we presented and tested a solution for computing the logarithm of sums of probabilities, while avoiding numerical underflows in the case of very small values.

\

\newpage

\section {Appendix}

\

We enumerate here the attached files and what their purpose is: 

\

\begin {itemize}
	\item {\bf GaussDist.m} - exercise 1
	\item {\bf LogProb.m} - exercise 3
	\item {\bf exercise2\_main.m} - main function of exercise 2
	\item {\bf init\_mog.m} - smart init
	\item {\bf init\_mog\_old.m} - dummy init
	\item {\bf mog\_E\_step.m}  - E-step with {\bf lmvnpdf}
	\item {\bf mog\_E\_step\_old.m} - E-step with {\bf mvnpdf}
	\item {\bf mog\_M\_step.m} - M-step of algorithm
	\item {\bf splitDataRandom.m} - splits data randomly
	\item {\bf testPhase2} - test phase of exercise 2
\end {itemize}

\

\end {document}

