import java.io.*;
import java.lang.*;
import java.util.*;

/** This class defines the functionality of the predator. */
public class Predator
  extends Agent
{
  int xp, oldxp;		// x pray
  int yp, oldyp;		// y pray
  int xprd, oldxprd;	// x other predator
  int yprd, oldyprd;	// y other predator
  int reward;
  float alpha = 0.3f;	// learning rate
  float gamma = 0.3f;	// discount factor
  boolean bEvent;		// flag, if bEvent then Q is updated in event handler
  
  int nEpisode = 0;

  int dimQ = 16;
  int nDirs = 5;

  // Q-values. could be smaller ([6][6][15][15][5] I think)
  float[][][][][] Q = new float[17][17][17][17][5];

  // iterators
  int ixp, iyp, ixprd, iyprd, iMoveDirection;

  // actions
  int nMoveDirection, oldnMoveDirection;

  public Predator() {
  	int iMoveDirection;

  	// class constructor, init Q-values wuth 0 here
	for (ixp = 0; ixp < dimQ+1; ++ixp) {
	  	for (iyp = 0; iyp < dimQ+1; ++iyp) {
		  	for (ixprd = 0; ixprd < dimQ+1; ++ixprd) {
			  	for (iyprd = 0; iyprd < dimQ+1; ++iyprd) {
			  		for (iMoveDirection = 0; iMoveDirection < nDirs; ++iMoveDirection) {
			  			Q[ixp][iyp][ixprd][iyprd][iMoveDirection] = 0;
			  		}
			  	}
			}
		}
	}

	bEvent = true;
  }

  /** This method initialize the predator by sending the initialization message
      to the server. */
  public void initialize()
    throws IOException
  {
    g_socket.send( "(init predator)" );
  }

  /** This message determines a new movement command. Currently it only moves
      random. This can be improved.. */
  public String determineMovementCommand()
  {
    switch( this.nMoveDirection )
    {
      case 0:  return( "(move north)" );
      case 1:  return( "(move south)" );
      case 2:  return( "(move east)"  );
      case 3:  return( "(move west)"  );
      default: return( "(move none)"  );
    }
  }

  /** This method processes the visual information. It receives a message
      containing the information of the preys and the predators that are
      currently  in the visible range of the predator. */
  public void processVisualInformation( String strMessage ) 
  {
    int i = 0, x = 0, y = 0, nPray = 0, maxQMoveDir = 0, iMoveDirection = 0, reward = 0;
    float maxQ = 0;
    String strName = "";
    StringTokenizer tok = new StringTokenizer( strMessage.substring(5), ") (");
    
    while( tok.hasMoreTokens( ) )
    {
      if( i == 0 ) strName = tok.nextToken();                // 1st = name
      if( i == 1 ) x = Integer.parseInt( tok.nextToken() );  // 2nd = x coord
      if( i == 2 ) y = Integer.parseInt( tok.nextToken() );  // 3rd = y coord
      if( i == 2 )
      {	
      	if (strName.equals("prey")) {
      		xp = x;
      		yp = y;
      	}
      	
      	if (strName.equals("predator")) {
      		xprd = x;
      		yprd = y;
      	}

        //System.out.println( strName + " seen at (" + x + ", " + y + ")" );

      // TODO: do something nice with this information!
      }
      i = (i+1)%3;
    }

	if(Math.abs(xp) > 3 || Math.abs(yp) > 3) {
		// use hard-coded policy if predator is far away from the prey
		if (Math.abs(xp) > Math.abs(yp)) {
			// move on the X axis
			this.nMoveDirection = (xp < 0) ? 3 : 2;
		}
		else {
			// move on the Y axis
			this.nMoveDirection = (yp < 0) ? 1 : 0;
		}
	}
	else {
		// Q-learning when close

		maxQ = -9999;
		maxQMoveDir = -1; 

		// policy evaluation - get the action with the maximum Q-value for this state
		for (iMoveDirection = 0; iMoveDirection < nDirs; ++iMoveDirection) {
			if (Q[nC(xp)][nC(yp)][nC(xprd)][nC(yprd)][iMoveDirection] > maxQ) {
				maxQ = Q[nC(xp)][nC(yp)][nC(xprd)][nC(yprd)][iMoveDirection];
				maxQMoveDir = iMoveDirection;
			}
		}

		if (maxQMoveDir > -1) {
			// we have a preferred action (direction to move)

			if (nEpisode > 3000 || Math.random()*10 < 7) {
				// during training/exploration, use epsilon-greedy with epsilon = 30%
				nMoveDirection = maxQMoveDir;	
			}
			else {
				// during exploatation use greedy policy
				nMoveDirection = (int)(Math.random()*nDirs);
			}
		}
		else {
			// we don't have a prefered direction to move
			nMoveDirection = (int)(Math.random()*nDirs);
		}

		if (!bEvent) {
			// compute reward (yes, if-else would be more efficient here)
			if (Math.abs(xp) + Math.abs(yp) < Math.abs(oldxp) + Math.abs(oldyp)) {
				reward = 1;
			}
			if (Math.abs(xp) + Math.abs(yp) > Math.abs(oldxp) + Math.abs(oldyp)) {
				reward = -1;
			}
			if (Math.abs(xp) + Math.abs(yp) == Math.abs(oldxp) + Math.abs(oldyp)) {
				reward = 0;
			}

			if (Math.abs(oldxp) < 4 && Math.abs(oldyp) < 4) {
		    	// update Q
		    	updateQ(oldxp, oldyp, oldxprd, oldyprd, xp, yp, xprd, yprd, oldnMoveDirection, reward);
		    }
		}
		
		bEvent = false;
	}
	
   // save the old values for updating Q at next iteration
    oldxp = xp;
    oldyp = yp;
    oldxprd = xprd;
    oldyprd = yprd;

    oldnMoveDirection = nMoveDirection;
  }

  /** This method is called after a communication message has arrived from
      another predator. */
  public void processCommunicationInformation( String strMessage) 
  {
    // TODO: exercise 3 to improve capture times
  }
  
  /** This method is called and can be used to send a message to all other
       predators. Note that this only will have effect when communication is
      turned on in the server. */
  public String determineCommunicationCommand() 
  { 
    // TODO: exercise 3 to improve capture times
    return "" ; 
  }

  /**
   * This method is called when an episode is ended and can be used to
   * reset some variables.
   */
  public void episodeEnded( )
  {
     // this method is called when an episode has ended and can be used to
     // reinitialize some variables
     //System.out.println( "EPISODE ENDED\n" );

     reward = 1000;
     
     if (Math.abs(xp) < 4 && Math.abs(yp) < 4) {
		updateQ(xp, yp, xprd, yprd, 1000, 0, 0, 0, nMoveDirection, reward);
	 }

     nEpisode++;
     
     bEvent = true;
  }

  /**
   * This method is called when this predator is involved in a
   * collision.
   */
  public void collisionOccured( )
  {
     // this method is called when a collision occured and can be used to
     // reinitialize some variables
     //System.out.println( "COLLISION OCCURED\n" );

     //System.out.println(" pray seen at (" + xp + ", " + yp + ")\n" );
     //System.out.println(" predator seen at (" + xprd + ", " + yprd + ")\n" );

	 reward = -50;
	 
     updateQ(xp, yp, xprd, yprd, 1000, 0, 0, 0, nMoveDirection, reward);

	 bEvent = true;
  }

  /**
   * This method is called when this predator is involved in a
   * penalization.
   */
  public void penalizeOccured( )
  {
    // this method is called when a predator is penalized and can be used to
    // reinitialize some variables
    //System.out.println( "PENALIZED\n" );

	 reward = -50;

     updateQ(xp, yp, xprd, yprd, 1000, 0, 0, 0, nMoveDirection, reward);

     bEvent = true;
  }
  
    /**
   * This method is called when this predator is involved in a
   * capture of a prey.
   */
  public void preyCaught( )
  {
    System.out.println( "PREY CAUGHT\n" );

	// reward is given in episodeEnded instead
    
    // bEvent = true;

    // Q[xp][yp][xprd][yprd][nMoveDirection] = 100;
    
    // updateQ();
  }


  private void updateQ(int oldxp, int oldyp, int oldxprd, int oldyprd, int xp, int yp, int xprd, int yprd, int oldnMoveDirection, int reward) {
	int iMoveDirection;
	float maxQNextState;  

	// get max value for the state we transitioned in
	maxQNextState = 0;

	// when xp > 100 we update the Q value for an end state (event), so we don't know/care about the current state
	if (xp < 100) {
	maxQNextState = -9999;
	for (iMoveDirection = 0; iMoveDirection < nDirs; ++iMoveDirection) {
		if (Q[nC(xp)][nC(yp)][nC(xprd)][nC(yprd)][iMoveDirection] > maxQNextState) {
			maxQNextState = Q[nC(xp)][nC(yp)][nC(xprd)][nC(yprd)][iMoveDirection];
		}
	}
	}

	// compute Q for the state from which we transitioned
	Q[nC(oldxp)][nC(oldyp)][nC(oldxprd)][nC(oldyprd)][oldnMoveDirection] = (1 - alpha) * Q[nC(oldxp)][nC(oldyp)][nC(oldxprd)][nC(oldyprd)][oldnMoveDirection] + 
			  																alpha * ( reward +  gamma * maxQNextState);

  }

  private int nC(int coord) {
  	// translate coordinates
  	return coord + dimQ/2;
  }
 
 
  public static void main( String[] args )
  {
    Predator predator = new Predator();
    if( args.length == 2 )
      predator.connect( args[0], Integer.parseInt( args[1] ) );
    else
      predator.connect();
  }
}
  
