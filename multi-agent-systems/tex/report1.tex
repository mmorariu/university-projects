\documentclass [11pt] {article}

%\usepackage {anysize}
%\usepackage {fourier}
\usepackage{algorithmic}
\usepackage{algorithm}

\title {Assignment 1: Independent Agents and Multiagent Coordination}
\author{Mathias Breuss (6408192), Mihai Morariu (10232710), Andrei Oghina (6424171)}

\begin {document}
\maketitle
	
\begin {abstract}
In this report, we present our approach of solving the rational independent agents and multiagent coordination tasks, in the context of the Pursuit Domain paradigm. We perform experiments using the developed strategies, based on optimal decision making, social conventions and roles. We then analyze, compare, evaluate and test the significance of the results, present possible future work and formulate conclusions. 
\end {abstract}
	
\section {Introduction}
The Pursuit Domain consists of a map (also known as world) of $N \times M$ cells, a number of predators (denoted by blue circles) and a number of preys (denoted by orange triangles), which are initially positioned randomly on the map and can move orthogonally on adjacent cells in a fully observable environment. We say that a prey is captured if a specific criterion is fulfilled, such as having one predator occupying the prey's cell. When a prey is captured, it is removed from the board. When all preys are captured, the current episode ends and a new one begins. This involves placing all preys and predators at random positions on the map. Collisions between predators are penalized by repositioning the involved predators at random positions. The domain reports the capture times for each episode.
	
\section {Rational Independent Agents}
\label{sec:rationalags}
In this section, the problem setting of catching the prey and the methods used to achieve the goal are described.
\subsection {Problem Setting}
In the exercise, the criterion for capturing a prey is satisfied once one predator occupies the prey's corresponding cell. There are two preys and two predators. While the environment is fully observable, a constraint specifies that only the sensory information about the preys are to be used. The initial strategy for the predators is to move randomly on the map. The goal is to improve this strategy and to minimize the average capture time.
	
\subsection {Methods}
In this exercise, each predator is a reflex agent with a reactive policy:
\begin{equation}
\pi(\Theta_t) = a_t
\end{equation}
Since observing the other predator is forbidden, coordination is not possible. Thus, the observation of each agent at time $t$, $\Theta_t$, consists only in the relative positions of the one or two preys that are present on the map. 
\\\\
Given the goal of each predator to catch a prey and since the predators can only move orthogonally, the utility of each state $s_t$, $U(s_t)$, is inversely proportional with the Manhattan distance between the predator and the closest prey. The optimal decision making consists in maximizing the expected utility $U(s_{t+1})$, so minimizing the Manhattan distance between the predator and the prey. Thus, the optimal policy of the predator consists in simply moving towards the closest prey. The available action set is $A = \{"stand\ still", "move\ north", "move\ south", "move\ east", "move\ west"\}$. Once the closest prey is determined based on the observations, $a_t \in A$ is selected so that the predator moves in the direction of the prey.  
\\\\
Since the predators can only move orthogonally, the Manhattan distance is used to determine the closest prey. Given that both preys are present on the map at positions $(xp_1, yp_1)$ and $(xp_2, yp_2)$ relative to the current predator, the first prey is chosen if the following condition stands:
\begin{equation}
\mid xp_1 \mid + \mid yp_1 \mid < \mid xp_2 \mid + \mid yp_2 \mid 
\end{equation}
\\
Having selected the target prey, the next step is to determine the action $a_t \in A$. The prey is known to stand still with a probability of 20\% and to move in one of the directions north, east, south, west with a probability of 80\%. This means that there is a probability of 60\% that the distance to the predator does not increase. This is because, for example, if the relative distance of the prey from the predator is $\left(2, 2 \right)$, the distance does not increase if the prey stands still or moves west or south.
\\\\
Since the policy consists of minimizing the Manhattan distance between the predator and the prey, moving towards the prey is always better than standing still. After discarding the \emph{"stand still"} action, we are left with four actions, each corresponding to one of the four degrees of freedom of the predator.

\begin{algorithm}
\caption{Selecting the target prey and moving towards it}
\label{alg1}
\begin{algorithmic}
\IF {$\mbox{nPreys} < 2$ \OR $|\mbox{xp}_1| + |\mbox{yp}_1| < |\mbox{xp}_2| + |\mbox{yp}_2|$}
	\IF {$|\mbox{xp}_1| > |\mbox{yp}_1|$}
		\STATE MoveDirection $\gets (\mbox{xp}_1 < 0\ ?\ \mbox{"move\ west"} : \mbox{"move\ east"})$
	\ELSE
		\STATE MoveDirection $\gets (\mbox{yp}_1 < 0\ ?\ \mbox{"move\ south"} : \mbox{"move\ north"})$
	\ENDIF
\ELSE
	\IF {$|\mbox{xp}_2| > |\mbox{yp}_2|$}
		\STATE MoveDirection $\gets (\mbox{xp}_2 < 0\ ?\ \mbox{"move\ west"} : \mbox{"move\ east"})$
	\ELSE
		\STATE MoveDirection $\gets (\mbox{yp}_2 < 0\ ?\ \mbox{"move\ south"} : \mbox{"move\ north"})$
	\ENDIF
\ENDIF 
\end{algorithmic}
\end{algorithm}
\
From the remaining set of actions, at least two can be discarded, given the adopted policy (moving away from the prey increases the Manhattan distance). If the predator and the prey are on the same axis, then a third action can be discarded, leaving only one action available (moving towards the prey on the one axis that the predator and prey share). However, in all other cases, two actions remain in the set: moving towards the prey on the $x$ axis, and moving towards the prey on the $y$ axis, respectively. In order to maximize the probability of effectively approaching the prey, we select in this case the axis on which the distance to the prey is largest. Otherwise, there is a risk of wasting cycles by chasing the prey on only one axis. If it changes direction, we might still be far away on the other axis.  

\section {Multiagent Coordination}
\label{sec:coordination}
In this section, the problem setting of catching the prey and the methods used to achieve the goal are described, using social conventions and roles to coordinate the predators.
\subsection {Problem Setting}
In the second exercise there are four predators and two preys, and the criterion for capturing a prey is satisfied once the four predators completely surround the prey. The predators are not allowed to communicate. However, the environment is fully observable, and there is no more restriction in regard to seeing the other predators. Based on their sensory information, the predators have to coordinate themselves in attacking the preys and to avoid colliding with each other. The goal is to develop a strategy that minimizes the average capture time.

\subsection {Methods}
Similar to the rational independent agent, each predator is a reflex agent with a reactive policy. However, this time the observation of each agent at time $t$, $\Theta_t$, contains, besides the relative positions of the one or two preys that are present on the map, the relative positions of all the other predators. This information implies full observability and makes the current state of the world common knowledge among the agents, social conventions and roles are used to achieve coordination and avoid collisions. 
\\\\
Since all predators are needed in order to surround a prey and consequently catch it, the first step is to agree on the prey that is to be chased by the group. This is done using a social convention. Recall that in the previous exercise we used the Manhattan distance to determine the closest prey. This time we sum all these distances over all predators, to determine the closest prey to the group. Therefore, in order to determine which one of the $k$ preys to chase, each agent has to compute the sum of the relative distances to the preys $(xp_{k}, yp_{k})$ and to the predators $(xprd_{i}, yprd_{i})$, including himself:

\begin{equation}
k = \mbox{argmin}_{k} \left( \sum_{i=\overline{1, 4}} \mid xp_{k} - xprd(i) \mid + \mid yp_{k} - yprd(i) \mid \right), k \in \left\{1, 2\right\}
\end{equation}
\\
Once the predators agreed on a prey, they devise a plan by assigning a role for each agent. For this, an ordering is needed, which is the second social convention used. The first step is for each agent to add himself in its set of observed agents at position $(0, 0)$. Then, each agent orders the agents in its set based on their positions, in such a way that, for every agent at $(xprd_{i}, yprd_{i})$, we have $xprd_{i} < xprd_{i+1}$, or $xprd_{i} = xprd_{i+1}$ and $yprd_{i} < yprd_{i+1}$. This ensures a strict ordering, crucial for determining roles. 
\\\\
The next step is to assign roles to predators, in order. Each agent does this by himself, and because we have a common ordering, the resulting plan is the same for all agents. The role assignment phase goes as follows: the leftmost predator (the first one from the ordering) takes the role "left", meaning he has to surrender the prey from the left. The rightmost predator (the last one from the ordering) takes the role "right". The uppermost predator from the middle takes the role "top", and the last predator (the downiest from the middle) takes the role "down". This role assignment minimizes the distances from the predators to their target cells. Note that this approach is better than going the other way around and assigning roles based on which agent is closest to each target cell, because, in that case, agents that are far away may not get the target cells that are closest to them.
\
Once the roles are assigned, each agent can easily compute the target cell for all the agents. Then, based on these target cells and following the policy used at the previous exercise (moving on the axis on which the distance to the prey is largest), each agent can compute the next move of the other agents.

\begin{algorithm}
\caption{ Coordination with social conventions and roles }
\label{alg2}
\begin{algorithmic}
\STATE $(\mbox{xprd[], yprd[]}) \gets \mbox{SortAscXY(xprd[], yprd[])}$
\STATE $\mbox{roles[]} \gets \mbox{AssignRoles(xprd[], yprd[])}$
\STATE $\mbox{TargetCells[]} \gets \mbox{SetTargets(roles[], xprd[], yprd[])}$
\STATE $\mbox{nextxprd[]} \gets []$ 
\STATE $\mbox{nextyprd[]} \gets []$
\FOR{$i = 1..\mbox{nPredators}$}
	\STATE $\mbox{BlockedCells} \gets \mbox{GetBlockedCells(xprd[i], yprd[i], nextxprd, nextyprd)}$
	\STATE $\mbox{MoveDirection[i]} \gets \mbox{GetDirection(TargetCells[i], BlockedCells)}$
	\STATE $\mbox{(nextxprd[i], nextyprd[i])} \gets \mbox{GetNextPosition(xprd[i], yprd[i], MoveDirection[i])}$
\ENDFOR
\end{algorithmic}
\end{algorithm}
\
In order to avoid collisions, each agent computes the next move of all the agents, using the same ordering used for assigning roles. Once the move for an agent has been computed, the expected position is saved. For each agent that we compute the next move, we compare its expected position with the previously saved expected positions (of the other agents that we already treated). If a collision is detected, the agent tries to move on the other axis, and if this is not possible either, it simply stands still for one cycle. 
\
Once each agent formulates the entire plan consisting in the roles, target cells, and next moves of all the agents, which is consistent across agents due to the social conventions used, each agent simply performs its own action $a_t \in A$. The agent easily identifies himself in the ordered list, knowing that his relative position is (0, 0).
\
In order to prevent deadlocks where three predators surround the prey forcing it to always move in the same direction and thus making it hard for the forth predator to reach his position, a third social convention is used. If the agents detect they are chasing a prey for too long (a threshold for the number of cycles is used), during plan formulation, they replace the actions of two predators with the \emph{"stand still"} action, making them to stop for one cycle. This is very efficient in breaking the deadlock, allowing the prey to move in another direction and thus making it easier to the forth predator to come in formation.

\section {Results}
In our evaluation we report on various statistical measures like the observed Minimum, First and Third Quartile, Median, Mean, Max and Standard Deviation, where the Mean and the Standard Deviation \( \sigma \) are most relevant to us.
\\
\\
Further we do significance testing where appropriate to determine whether the difference in population means is significant. Due to the non-normal distribution of our population we choose a paired Wilcoxon signed-rank test, rather then a t-Test.

\subsection{Rational Independent Agents}
The results for the setup described in section \ref{sec:rationalags} can be found below.
\\
\\
\\
\begin{tabular}{|c|c|c|c|c|c|c|c|}
%% row 1
\hline
&Min.
&1st Qu.
&Median
&\textbf{Mean}
%% row 1 col 5
&3rd Qu.
&Max.
&\(\sigma\)
\\
\hline
\hline
%% row 2
Random
&21.0
&125.5 
&249.0
% row 2 col 5
&304.3
&388.0
&2009.0
&275.7179
\\
\hline
% row 3
Rational 1
&5.00
&9.50
&12.00
% row 3 col 5
&14.85
&17.00
&66.00
&8.992473\\
\hline
\end{tabular}
\\
\\
\\
Considering the Mean of the catching time we see that "Rational 1" outperforms the "Random" predators.
The significance test reveals that this improvement is significant with p-value \( < \) 2.2e-16.


\subsection{Multiagent Coordination}
The results for the setup described in section \ref{sec:coordination} can be found below.
\\
\\
\begin{tabular}{|c|c|c|c|c|c|c|c|}
% row 1
\hline
&Min.
&1st Qu.
&Median
&\textbf{Mean}
% row 1 col 5
&3rd Qu.
&Max.
&\(\sigma\)
\\
\hline
\hline
% row 2
Rational 2
&9.00
&19.00
&24.00
&24.92
&29.00
&75.00
&9.10671
\\
\hline
\end{tabular}

\section {Future Work}

Various heuristics may be employed to further improve the results. One idea is to order the agents not based on their position, but based on their
distance to the prey, with the most distant predator first. Since this would give an advantage to the predators that are farther away during the role
assignment and collision detection phase by letting them have the first preference in terms of roles and moves, one can speculate that the overall capture time might improve. However, in most cases this is already achieved using the current policy, since the leftmost predator assumes the "left" role, and so on.

\section {Conclusion}
In this work we presented how improving the policy of agents can significantly increase their performance measure. Given full observability that
makes the current state of the world common knowledge among all agents, we described how to make use of social conventions and roles in order to achieve
coordination for strictly collaborative agents. The experiments were performed in the Pursuit Domain, and the results outline the efficiency of the policies used.
 

\end {document}
