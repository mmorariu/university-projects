\documentclass [11pt] {article}

%\usepackage {anysize}
%\usepackage {fourier}
\usepackage [margin = 1in] {geometry}
\usepackage {amsmath,amssymb}
\usepackage {algorithmic}
\usepackage {algorithm}
\usepackage {verbatim}
\usepackage {graphicx}

\title {Assignment 2: Reinforcement Learning}
\author {Mathias Breuss (6408192), Mihai Morariu (10232710), Andrei Oghin\u{a} (6424171)}

\begin {document}
\maketitle
%\makeauthor

\begin {abstract}

\

In this report, we employ the use of Q-learning in the context of the Pursuit Domain paradigm. We perform experiments that combine exploration and exploitation, and outline how learning through policy evaluation and improvement works. We then extend the problem to a multi-agent setting by removing the independence assumption and learning a joint policy for two agents. Finally, we analyze, compare, evaluate and test the significance of the results, present possible future work and formulate conclusions. 
\end {abstract}

\

\section {Introduction}

\

The Pursuit Domain consists of a map (also known as world) of $N \times M$ cells, a number of predators (denoted by blue circles) and a number of preys (denoted by orange triangles), which are initially positioned randomly on the map and can move orthogonally on adjacent cells in a fully observable environment. We say that a prey is captured if a specific criterion is fulfilled, such as having one predator occupying the prey's cell. When a prey is captured, it is removed from the board. When all preys are captured, we say that one episode ends and a new one begins. This involves placing all preys and predators at random positions on the map. Collisions between predators are penalized by repositioning the involved predators at random positions. The domain reports the capture times for each episode.

\
	
\section {Independent Q-learning}
\
\subsection {Goal}

\

In the exercise, the criterion for capturing a prey is satisfied when both predators are standing next to the prey. There is one prey and two predators. The environment is fully observable, and the predators are supposed to learn their policy independently, using Q-learning.
\	
\subsection {Methods}

\

In this exercise, each predator has to learn its own policy. While the Q-learning convergence proof assumes a stationary transition model, we set to demonstrate that treating the other agent as part of the environment works well in practice. 

\

Modelling the other predator as part of the environment results in states $s \in S$, a 4-dimensional space which maps the two 2-dimensional relative positions of the prey and the other predator, respectively. As actions, we have $a \in \{"Move\ North", "Move\ South", "Move\ East", "Move\ West", \\"Stand\ Still"\}$.  

\

Assuming no prior knowledge on the transition model, the agent interacts with the environment and estimates $Q^*$ through trial-and-error. This exploration generates tuples of the form $(s, a, r, s')$, where $s$ is the current state, $a$ is the action taken at state $s$, $r = R(s, a, s')$ is the reward received, and $s'$ is the resulting state from taking action $a$.

\

We thereby define the reward function $R(s, a, s')$ as follows:
\\
\begin{equation}
R(s, a, s') =
\begin{cases}
-50;  \ \mathbf{if} \ penalizeOccured \ \mathbf{or} \ collisionOccured  \\
-1; \ \mathbf{if} \ d(prey|s) < d(prey|s')\\
0; \ \mathbf{if} \ d(prey|s) = d(prey|s') \\
1;  \ \mathbf{if} \ d(prey|s) > d(prey|s') \\
1000; \ \mathbf{if} \ preyCaptured 
\end{cases}
\end{equation}

\

The reasoning for using this reward function is that we want to penalize collisions with the other predator ($collisionOccured$) or with the prey ($penalizeOccured$), to give a big reward for capturing the prey, and to provide the predator with an incentive for getting closer to the prey. Note that these rewards, which are based on the change in the distance to the prey, are not mandatory - since, given appropriate learning rate, discount rate and reward magnitude, the reward for capturing the prey should propagate through the value functions until convergence. However, in practice, the intuition that giving instant reward to the predator for getting closer to the prey helps convergence, proves correct. 

\

The magnitudes of the values used as rewards are flexible, assuming the overall ordering is preserved. However, it is important to have a significantly larger positive incentive for capturing the prey than the absolute value of the penalty for collision with another predator or prey. This is because, otherwise, the predator becomes more interested in avoiding collisions than in capturing the prey, and thus becomes overly prudent in keeping away from the other predator and the prey. If the predator settles with such a policy, then the prey will be very hard to capture.

\

Given the reward function, the Q-values are updated after each action: 
\\
\begin{equation}
Q(s, a) \gets (1 - \lambda) Q(s, a) + \lambda [R(s, a, s') + \gamma \ max \ Q(s', a')]
\end{equation}

\

During the exploration phase, we use the epsilon-greedy policy with epsilon set at 30\%. At every step, after policy evaluation, in 70\% of the cases the best action is selected, while in the remaining 30\% of the cases a random action is selected. This allows for discovering better actions, until convergence is observed. After the number of episodes reaches a predefined threshold (for example, 3000), the algorithm switches to the greedy policy, meaning that it will always select the action with the best Q-value in a given state, as it can be seen in algorithm \ref{alg1}.

\

\begin{algorithm}
\caption{Policy evaluation. Epsilon-greedy followed by greedy policy}
\label{alg1}
\begin{algorithmic}
\STATE $maxQAction \gets argmax_a \ Q(xp, yp, xprd, yprd)$
\IF {$nEpisode > nGreedyThreshold$  \OR  $rand(1, 10) <= 7$}
	\STATE $a \gets maxQAction$
\ELSE
	\STATE $a \gets random(A)$
\ENDIF
\end{algorithmic}
\end{algorithm}

\

The rewards are given in two different ways. First, if an event is detected, such as a collision or the capturing of the prey, $Q$ is updated through a call made from the respective event handler, with the appropriate reward set as a parameter according to statement 1. In all other cases, $Q$ is updated every time a transition finalizes. Therefore, every time a new observation is made, the steps described in algorithm \ref{alg2} are followed accordingly.

\

Note that the Q-value update is performed only if the prey is within 3 cells to the predator. This is because, when the prey is further away from the predator, we use a hard-coded policy which simply dictates the predator to move closer to the prey. This is done in order to reduce the state space.

\

\begin{algorithm}[htb]
\caption{Reward and Q-values update call with no active event}
\label{alg2}
\begin{algorithmic}
\STATE $reward \gets 0$ 
\IF {$\mid xp \mid  + \mid yp \mid \ < \ \mid oldxp \mid + \mid oldyp \mid$}
	\STATE $reward \gets 1$ 
\ENDIF
\IF {$\mid xp \mid  + \mid yp \mid \ > \ \mid oldxp \mid + \mid oldyp \mid$}
	\STATE $reward \gets -1$
\ENDIF
\IF {$\mid oldxp \mid < 4 \ \AND \ \mid oldyp \mid < 4$}
	\STATE $updateQ(oldxp,\ oldyp,\  oldxprd,\  oldyprd,\ xp,\  yp,\  xprd,\  yprd,\  olda,\ reward)$
\ENDIF
\end{algorithmic}
\end{algorithm}

\

The function responsible for updating the Q-values, $updateQ$, is described in algorithm \ref{alg3}. This receives as parameters the state from which the agent just transitioned, defined by the old positions of the prey and the other predator ($oldxp, oldyp, oldxprd, oldyprd$), the taken action, $a$, the received reward, $r$, and the new state that the agent finds itself in, defined by the new positions set ($xp, yp, xprd, yprd$). 

\

\begin{algorithm}[htb]
\caption{Q-values update}
\label{alg3}
\begin{algorithmic}
\STATE $maxQPrime \gets 0$
\IF {$!penalizeOccured \ \AND \ !collisionOccured \ \AND \ !preyCaptured$}
\STATE $maxQPrime \gets max Q(xp, yp, xprd, yprd)$
\ENDIF
\STATE $Q(oldxp, oldyp, oldxprd, oldyprd, olda) = $ \\ $(1 - lambda) * Q(oldxp, oldyp, oldxprd, oldyprd, olda) + $ \\ $lambda * (reward + gamma * maxQPrime)$
\end{algorithmic}
\end{algorithm}

\

Notice that the $maxQPrime$ value, which is the maximum Q-value for the new state, is taken into consideration only when an event was not observed (like a collision or capturing the prey). In case of such an event, the predator gets repositioned randomly on the map, and it makes no sense to take the Q-values of this new random state into account. We could do that, but that would just contribute to a slower convergence, because in this case the transition is random. In other words, there is nothing to learn from these transitions, so we focus on the immediate reward the agent receives in such cases. 

\

For the purpose of our experiment, we set both the learning rate $\lambda$ ($lambda$ in algorithm \ref{alg3}) and the discount factor $\gamma$ ($gamma$ in algorithm \ref{alg3}) to be 0.3. In principle, these values are flexible, but they are important and there are situations when choosing the wrong values may result in no convergence. For the learning rate, we choose a value below 0.5 because we want the cumulative history of updates to weight more than the current value update. We use a fixed value for $\lambda$ before switching to a greedy policy, but another approach would be to start with a big value, and reduce it gradually until it becomes 0, so that the learning stops. For the discount factor, we choose a rather low value because we use a big reward for capturing the prey, and we don't want this reward to spread too easily to other states, especially since the space is crowded around a final state and undesirable states are usually in the area. Therefore, it is important to correlate the discount factor with the reward function used.

\

\section {Multiagent Reinforcement Learning}
\
\subsection {Goal}

\

In the exercise, the criterion for capturing a prey is satisfied when both predators are standing next to the prey and perform a coordinated action, such as only one predator moves into the prey's cell. There is one prey and two predators. The environment is fully observable, and the predators are supposed to learn a joint policy by extending the Q-learning algorithm implemented at exercise 1.

\newpage

\subsection {Methods}

\

The main elements that differentiate this approach to the one used in the previous fact stems from the fact that now the agents consider joint policies. While the state space remains the same, the number of possible joint actions for each state is in general 16, since each agent has 4 actions it can select from, and all combinations of actions have to be taken into account. As we will see, this increase makes convergence slower, and is similar in effect to an increase in state space size under an identical action set.

\

A consistent rewarding mechanism should be in place in order for the agents to learn joint policies. When an event happens (such as a collision or the capturing of the prey), this is ensured by the fact that both agents receive the same event handler call. When a transition doesn't trigger such an event, the consistency is achieved by generating rewards based on the group's relative position change to the prey. In this case, the new reward function $R(s, a, s')$ is defined as follows:

\

\begin{equation}
R(s, a, s') =\begin{cases}
-10;  \ \mathbf{if} \ penalizeOccured \ \mathbf{or} \ collisionOccured  \\
-1; \ \mathbf{if} \ d_1(prey|s) + d_2(prey|s) < d_1(prey|s') + d_2(prey|s')\\
0; \ \mathbf{if} \ d_1(prey|s) + d_2(prey|s) = d_1(prey|s') + d_2(prey|s') \\
1;  \ \mathbf{if} \ d_1(prey|s) + d_2(prey|s) > d_1(prey|s') + d_2(prey|s') \\
1000; \ \mathbf{if} \ preyCaptured 
\end{cases}
\end{equation}

\

It is interesting to note that in this exercise we had to reduce the penalties for colliding with the other predator or being penalized (colliding with the prey). Because learning how to capture the prey is harder (the criterion applies to two successive states), the predators first face numerous penalties before executing the first capture, and become reluctant to approach the prey because of these penalties, if they are too high, before the first capture can happen, thus spreading the high value positive reward. 

\

Policy evaluation consists in finding the joint action with the maximum Q-value, and executing the part of the joint action associated with the agent that makes the reasoning, as described in algorithm \ref{alg7}.

\

\begin{algorithm}
\caption{Joint policy evaluation and action selection}
\label{alg7}
\begin{algorithmic}
\STATE $maxQJointAction \gets argmax_a \ Q(xp, yp, xprd, yprd)$
\IF {$nEpisode > nGreedyThreshold$  \OR  $rand(1, 10) <= 7$}
	\STATE $a \gets myAction(maxQJointAction)$
\ELSE
	\STATE $a \gets random(A)$
\ENDIF
\end{algorithmic}
\end{algorithm}

\

The reward function is designed so that it encourages the predators to get closer to the prey, as a group. To compute the reward under the no event scenario, the action of the other agent doesn't have to be reasoned about. By simply relying on its observations, the agent can sum over the distances to the prey of himself and the other predator (by manipulating relative distances), and compare these sums of each successive states, as is described in algorithm \ref{alg9}.

\

\begin{algorithm}[htb]
\caption{Reward and Q-values update based on group dynamics}
\label{alg9}
\begin{algorithmic}
\STATE $reward \gets 0$ 
\IF {$\mid xp \mid  + \mid yp \mid \ + \mid xprd - xp \mid + \mid yprd - yp \mid < \ \mid oldxp \mid + \mid oldyp \mid + \mid oldxprd - oldxp \mid + \mid oldyprd - oldyp \mid$}
	\STATE $reward \gets 1$ 
\ENDIF
\IF {$\mid xp \mid  + \mid yp \mid \ + \mid xprd - xp \mid + \mid yprd - yp \mid \ > \ \mid oldxp \mid + \mid oldyp \mid + \mid oldxprd - oldxp \mid + \mid oldyprd - oldyp \mid$}
	\STATE $reward \gets -1$
\ENDIF
\IF {$\mid oldxp \mid < 4 \ \AND \ \mid oldyp \mid < 4$}
	\STATE $updateQ(oldxp,\ oldyp,\  oldxprd,\  oldyprd,\ xp,\  yp,\  xprd,\  yprd,\  olda,\ reward)$
\ENDIF
\end{algorithmic}
\end{algorithm}

Since the learning process is consistent, the other agent will always select its part of the same joint action, under the greedy policy.

\

In order to update the Q-values, the agent has to construct joint action pairs by reasoning about the action taken by the other agent. This can be achieved using the observations from the previous and current state regarding the other agent's position, and the agent's own action, as it is described in algorithm \ref{alg8}.

\

\begin{algorithm}[htb]
\caption{Q-values update for joint policies}
\label{alg8}
\begin{algorithmic}
\STATE $maxQPrime \gets 0$
\IF {$!penalizeOccured \ \AND \ !collisionOccured \ \AND \ !prayCaptured$}
\STATE $maxQPrime \gets max Q(xp, yp, xprd, yprd)$
\ENDIF
\STATE $(exprd, eyprd) \gets getExpectedPartnerPosition(oldxprd, oldyprd, olda)$
\STATE $oldoa \gets getPartnerAction(exprd, eyprd, xprd, yprd)$
\STATE $Q(oldxp, oldyp, oldxprd, oldyprd, olda, oldoa) = $ \\ $(1 - lambda) * Q(oldxp, oldyp, oldxprd, oldyprd, olda, oldoa) + $ \\ $lambda * (reward + gamma * maxQPrime)$
\end{algorithmic}
\end{algorithm}

\

Therefore, the main difference is that the action taken by the other agent, $oldoa$, is computed in order to update the joint action. This is done in two steps. First, based on the previous observations regarding the other agent's position, $oldxprd$, $oldyprd$, and the action taken by the reasoning agent, $olda$, an expected position is computed, assuming the other agent stood still, $exprd, eyprd$. Based on this position and the current observation, the other agent's action, $oldoa$, is inferred.

\

\section {Results}

\

In this section we report on the results of the two conducted experiments, given our implementation. 

\

First, we depict the learning behavior by plotting the capture times over the episodes. Because plotting the raw data is not very insightful, we use a digital filter to calculate the running average over a window of 100 episodes. 

\

Finally, we report on various statistical measures like the observed Minimum, First and Third Quartile, Median, Mean, Max and Standard Deviation, where the Mean and the Standard Deviation \( \sigma \) are most relevant to us. These values are based on measurements after the algorithm turned to greedy policy, using 1000 episodes.

\

\subsection {Independent Q-learning}

\

As depicted in figure \ref{fig:ex1}, we have high capturing times during the first 500 episodes which decrease very fast. After 500 episodes we see that the line flattens. Finally, at 3000 episodes we turn to greedy policy. Due to filtering, the drop is not precisely at 3000.

\begin{figure}[h!]
\centering
\includegraphics[scale=.7]{Exercise1.png}
\caption{Capture Time vs. Nr. of Episodes}
\label{fig:ex1}
\end{figure}

\

In table \ref{table:Independent}, the statistical measures are given after the algorithm turned to greedy policy.

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|}
%% row 1
\hline
&Min.
&1st Qu.
&Median
&\textbf{Mean}
%% row 1 col 5
&3rd Qu.
&Max.
&\(\sigma\)
\\
\hline
\hline
%% row 2
Independent
&2
&10
&12
&13.1260
&16
&42
&5.3160
\\
\hline
\end{tabular}
\caption{Independent Q-learning: Statistics after turning to greedy policy}
\label{table:Independent}
\end{table}

\subsection {Multiagent Reinforcement Learning}

\

As depicted in figure \ref{fig:ex2}, we have high capturing times during the first 300 episodes which decrease very fast. After 1000 episodes we see that the line flattens. Finally, at 3000 episodes we turn to greedy policy. Due to filtering, the drop is not precisely at 3000. An important aspect compared to figure \ref{fig:ex1} of \emph{Independent Q-learning} is the fact that we start with much higher capturing times. This is due to the fact that our search space is much bigger. \\

\begin{figure}[h!]
\centering
\includegraphics[scale=.7]{Exercise2.png}
\caption{Capture Time vs. Nr. of Episodes}
\label{fig:ex2}
\end{figure}

In table \ref{table:Multiagent}, the statistical measures are given after the algorithm turned to greedy policy. When comparing these results to table \ref{table:Independent}, we see that all values increased a little bit, still our algorithm performs very well.

\

\begin{table}[h!]
\centering
\begin{tabular}{|c|c|c|c|c|c|c|c|}
%% row 1
\hline
&Min.
&1st Qu.
&Median
&\textbf{Mean}
%% row 1 col 5
&3rd Qu.
&Max.
&\(\sigma\)
\\
\hline
\hline
%% row 2
Multiagent
&4
&13
&18
&20.4530
&25
&175
&11.7970
\\
\hline
\end{tabular}
\caption{Multiagent Reinforcement Learning: Statistics after turning to greedy policy}
\label{table:Multiagent}
\end{table}

\

As expected, the average capture time is slightly higher in this setup, compared to when using independent Q-learning. This is not because of the learning method. Learning joint actions is actually better than following the naive approach of modelling the other predator as part of the environment. However, recall that in this setup the capturing condition requires a coordinated move. The predators have to be next to the prey, and then one of them has to enter the prey's cell. Since the prey moves randomly, such criterions that span over two time steps are in general harder to fulfill than one-step capture conditions. Thus, the average capture times in such cases, under best policy, are in general higher, provided the rest of the setup is similar.

\

\section {Future Work}

\

As we discovered in figure \ref{fig:ex2}, we have very high capturing times during the beginning of the experiment (first 300 episodes) compared to the result of \emph{Independent Q-learning} in figure \ref{fig:ex1}. This is due to the bigger state space. One approach to improve capturing times in the first place is to reduce this state space. This can be done by exploiting symmetries of the given state space, so that after rearranging, the state space is actually smaller and Q-learning finds the optimal policies faster. Depending on the given configuration, i.e. number of preys and predators, such symmetries could look as follows:
\begin{itemize}
\item Mirroring, with considering the prey as center.
\begin{itemize}
\item Horizontal
\item Vertical
\item Diagonal
\end{itemize}
\item Rotation, with considering the prey as center.
\end{itemize}

While matching the current state of the world against our normalized space to determine the next action, we should always keep track of the symmetries involved, and apply them (in reversed form) to the selected action.

\

In case of multiple preys and predators, we could have more then one center, where we have a bounding box around those centers. Learning would then take place within those non overlapping centers, after predators got close enough.

\

Another aspect would be improving the way we discover convergence. As requested by this assignment, our methodology for discovering convergence, and thus switching into greedy policy, is empirical and immediate. Immediate means that we switch after a number of episodes which we discovered is sufficient and then never go back. One approach would be to reduce $ \epsilon $ step by step. By doing that, we are not making a hard switch, but rather a soft move to a final persistent greedy policy. Detection would then be done by performing a fix point iteration where, depending on the rate of change, we would have a certain level of difference which we would not accept as fix-point and thus search on. If the real measured difference falls below, we stick to greedy mode.

\newpage

\section {Conclusion}

\

In this work we presented two reinforcement learning approaches. Given full observability that makes the current state of the world common knowledge among all agents, the first approach is based on Q-learning, each agent learning an individual policy, modelling the other agent as part of the environment. The second approach was similar, but this time joint policies are learned in order to achieve coordination for strictly collaborative agents. The experiments were performed in the Pursuit Domain, and the results outline the efficiency of reinforcement learning.


\end {document}
