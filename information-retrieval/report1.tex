\documentclass [11pt] {article}

\usepackage {graphicx}

\title {Assignment 1: Running an IR Experiment}
\author {Mihai Adrian Morariu (10232710), Andrei Oghin\u{a} (6424171)}

\begin {document}

\maketitle
\makeauthor

\begin {abstract}
In this report, we are describing the setup and methods used to perform a basic IR experiment. We set to evaluate and analyze the impact of stopwords removal, stemming and blind relevance feedback on ranked retrieval results. For this, we build several indexes for a document collection and perform batch retrieval for a set of given topics. Then, we evaluate the results against human judgments, compare the metrics and discuss the advantages and disadvantages of each technique. 
\end {abstract}

\section {Introduction}
In Information Retrieval, many tasks share the common goal of developing approaches that provide small, yet significant improvements over existing systems. Given the subjectivity involved in measuring such improvements, evaluating retrieval performance is a challenging problem. Therefore, it is crucial to have a robust framework for running IR experiments and performing consistent evaluation on these results.
\\\\
Stopwords removal, stemming and blind relevance feedback are common features of IR systems, having both advantages and disadvantages. In this work, we focus on the the following questions: (i) How to index a document collection, perform batch retrieval, and evaluate ranked retrieval results? (ii) How does stopwords removal, stemming and blind relevance feedback influence the performance of an IR system? 

\section {Experimental Setup}

For the purpose of our experiment, we use the \emph {Indri} search engine, part of the \emph {Lemur Toolkit}, which is an open-source software framework for building language modeling and information retrieval software. 
\\\\
For evaluation, we use \emph {trec\_eval}, which is is a standard tool used to evaluate an ad hoc retrieval run, given the results file, and a standard set of judged results. 
\\\\
The collection used consists of 678 documents, totalling 571 MB of English language newspaper articles.

\section {Methods}

We set to perform an IR experiment consisting of three steps. First, we index two document collection. We build different types of indexes, as described below. In the second step, we preprocess a set of given topics and perform batch retrieval on the created indexes using the resulting queries. In the third step, we evaluate the results obtained at the previous step against a set of available judged results.

\subsection {Indexing}

For this step, we used the \emph {IndriBuildIndex} tool to index the documents from the document collection. We constructed four indexes, by changing the settings provided in the parameter file, such as the stemming algorithm to use and the output index path for the index. The first index is a baseline index build without stopwords removal or stemming. The second index was built with stopwords removal. The third index index was built with stemming, and the forth index was built with both stopwords removal and stemming. For stemming, we used the Porter stemmer. For stopwords removal, we used a list of 32 common English words. 

\subsection {Retrieval}
For retrieval we used the \emph {IndriRunQuery} tool, which performs ranked retrieval on a given set of queries, based on the settings specified in the parameter file, such as the index path and the maximum number of results to return. We used 25 topics, which were preprocessed to match the required format and stored in a separate file passed as an input parameter to \emph {IndriRunQuery}, along with the retrieval parameter file. The output file encodes a list comprising of ranked documents for each given query.

\subsection {Evaluation}

The evaluation part consisted in using \emph {trec\_eval} to produce retrieval metrics, such as number of relevant documents retrieved and mean average precision, based on the output of the retrieval run (the output of \emph {IndriRunQuery}) and the ground truth for the topics, given as a file that encodes human relevance judgements for all topics and documents.

\section {Results}

In this section, we present and discuss the results of our experiments.

\subsection {Indexing}

Using the \emph {dumpindex} tool, we obtained some metrics regarding each index. The differences are best outlined by comparing the number of unique terms and the index size, as it is shown in table~\ref{IndexM}.
\\
\begin{table}[htb]
\begin{tabular}{|c|c|c|c|c|}
%% row 1
\hline
 & Baseline & Stopwords & Stemming  & Stopwords+Stemming \\ \hline
Unique terms	& 335273 & 335242 & 245295 & 245293 \\ \hline
Size	& 796M & 762M & 774M & 741M \\ \hline
\end{tabular}
\caption{Indexes metrics}
\label{IndexM}
\end{table}
\\
The impact that stopwords removal and stemming have on the index is now clear. After stopwords removal, the index has a 4.27\% decrease in size comparing to baseline, while stemming produces a 2.76\% decrease. This happens despite the fact that stemming reduces the vocabulary by 26\% (89978 terms), while stopword removal reduces the vocabulary only by less than 0.01\%(recall our stopwords size is 32), because, in the case of stemming, postings lists are merged, not removed, as it is the case when using stopwords. This confirms the fact that stopwords posting lists are very long, comparing to the average posting list size, and that the vocabulary size is small in comparison with the postings lists.

\subsection {Retrieval}
\\\\
Retrieval with standard settings is performed on all four indexes. In addition, blind relevance feedback is used on the index constructed with stopwords removal and stemming. 

\subsubsection {Stopwords Removal and Stemming}
Based on the retrieval results for the given topics obtained using the standard retrieval settings on each of the four indexes, the evaluation program \emph {trec\_eval} was used to produce the evaluation metrics. Some of these metrics, namely the number of documents retrieved (num\_ret), the number fo relevant document retrieved (num\_rel\_ret), the R-precision (R-prec) and precision @ 10 (P@10) are presented in table~\ref{Ret1}. Based on the judgments, the number of relevant documents from the collection for the set of topics is 834.

\begin{table}[htb]
\begin{tabular}{|c|c|c|c|c|c|}
\hline
 					& Baseline 		& Stop	 			& Stem		  	& Stop+Stem 	& Stop+Stem w/Rel		\\ \hline
num\_ret			& 23401 		& 22591 			& 24047 		& 23239 		& 25000					\\ \hline
num\_rel\_ret		& 508 			& 508 				& 694			& 695			& 718					\\ \hline
R-prec				& 0.26 			& 0.26				& 0.32			& 0.32			& 0.32					\\ \hline
P@10				& 0.30 			& 0.30				& 0.37			& 0.37			& 0.38					\\ \hline
\end{tabular}
\caption{Evaluation metrics for retrieval runs}
\label{Ret1}
\end{table}
\\
First, we can see that stopwords removal has a minimum impact on these relevant metrics, as expected. This is the reason why search engines used to employ stopwords removal, since it can reduce index size and lookup times singnificantly, while preserving precision. While the number of retrieved documents decreases slightly after introducing stopwords removal, the additional documents retrieved for the baseline index are most likely not related to the topics. Since these documents were not retrieved after stopwords removal, we can assume they were only related to certain stopwords from some queries. This is confirmed by the fact that the metric of relevant retrieved documents remains unchanged.
\\\\
We can observe noticeable changes in the case of using stemming. The number of retrieved documents increases, and so does the number of relevant retrieved documents. Since stemming compresses the dictionary by mapping words having the same root together, recall generally improves as a result. Despite sometimes stemming can hurt precision (stemming introduces more ambiguity and the query may drift from the information need), in this setting experiments confirm that precision improves as well. 
\\\\
Next, we analyze how stopwords removal and stemming influence precision on a per query basis. For this, we compare the average precision of the results for each query on the baseline index, with the average precision of the same results obtained for the same queries, but against the index built with stopwords removal and stemming, as can be seen in figure~\ref{ptmap1}. Note the metric we used is mean average precision (MAP), which, since the values are per each query, is the same with the average precision for that query. Average precision is a stable metric that compensates for variance in the number of total relevant documents per topic. 

\begin{figure}[htp]
\centering
\includegraphics[width=0.7\textwidth]{result_pertopic.png}
\caption[Per-topic MAP difference. Baseline vs. Stop+Stem]{Per-topic MAP difference. Baseline vs. Stop+Stem}\label{ptmap1}
\end{figure}

We can notice how, for topics "C324" and "C312", the precision is better for the index built with stopwords removal and stemming, while for topic "C315", the precision is better for the baseline index. The query for "C324" is "Supermodels" and for "C312" is "Dog Attacks". In both cases, stemming seems to help increase precision, since the plural form doesn't substantially change the meaning of the singular, and, in these cases, ambiguities are not introduced by stemming ("Supermodels" has "Supermodel" as stem, while "Dog attacks" simply maps to "Dog Attack"). However, the impact of stemming and stopword removal on query "C315" is negative. The query is "Doping in Sports", which maps to "Dope Sport", with an obvious loss of meaning.

\subsubsection {Blind Relevance Feedback}
We performed the blind relevance feedback experiment on the index constructed with stopwords removal and stemming, using the top 10 results returned by the initial query to select terms for query expansion. The blind relevance feedback method consists in taking the results returned by the initial query, select top terms from these documents, and then do query expansion by adding these terms to the query and return the ranked list of documents retrieved for this new, expanded query.
\\\\
We move on to test the granular impact of blind relevance feedback on a per query basis, by performing MAP comparison with the previous retrieval run performed on the same index. The results are presented in figure~\ref{ptmap2}. 

\begin{figure}[htp]
\centering
\includegraphics[width=0.7\textwidth]{result_pertopic_relevance.png}
\caption[Per-topic MAP difference. Stop+Stem vs. Stop+Stem w/ Rel]{Per-topic MAP difference. Stop+Stem vs. Stop+Stem w/Rel}\label{ptmap2}
\end{figure}

Here, queries "C319", "Global Opium Production" and "C315", "Doping in Sports" seem to benefit the most from relevance feedback query expansion, arguably because they address more narrow information needs. On the other end, queries "C312", "Dog Attacks", and "C316", "Strikes", seem to suffer from using this technique. For "Strikes", we can definitely suspect query drift, due to the general, ambiguous nature of the initial query.

\subsubsection {Putting It All Together}
In order to compare different runs of ranked retrieval, we can plot for each of them the precision against recall, after each retrieved document. For this, we can use the interpolated recall - precision averages computed by the \emph {trec\_eval} evaluation program. 
\\\\
Each topic precision is interpolated to a set of standard recall levels, by using the maximum precision obtained for the topic for any recall level greater than the current one. Based on the metrics provided in table~\ref{Ret1}, we set to compare the standard retrieval runs for the baseline index and the Stop+Stem index, and also the relevance feedback run on the same index Stop+Stem index. We plot the results of these three runs in the precision-recall space, as can be seen in figure~\ref{prc1}.

\begin{figure}[htp]
\centering
\includegraphics[scale=0.7]{baseline-stop-stem-relevfb.png}
\caption[Precision-recall for Baseline vs. Stop+Stem vs. Stop+Stem w/Rel]{Precision-recall for Baseline vs. Stop+Stem vs. Stop+Stem w/Rel}\label{prc1}
\end{figure}

As expected, this confirms that, for the current setup, stemming helps increase precision. From table~\ref{Ret1}, we know that stopwords removal has a minimal impact on performance. However, we can notice a small improvement in precision when using blind relevance feedback. This confirms the validity of the relevance feedback premise, but the question remains if this small improvement justifies the query drift problem and the significant overhead in retrieving results.

\section {Conclusions}
In this work, we analyzed the impact of various IR techniques on key evaluation metrics for ranked retrieval runs. The experimental results prove that stopwords removal has a minumum impact on relevance metrics while significantly reducing index size, stemming improves precision most of the times, while blind relevance feedback provides small improvements in some cases, that can arguably be overshadowed by its downsides. 

\end {document}
