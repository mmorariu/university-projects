\documentclass{article}

\usepackage{amsmath}
%\usepackage{anysize}
\usepackage{fourier}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage{multirow}

\title{Statistical Structure in Language Processing \\ Assignment 1}
\author{Mihai Morariu (10232710)}

\begin{document}
\maketitle

\section{Literature Assignment}
In his paper \emph{PCFG Models of Linguistic Tree Representations}, M. Johnson analyzes, both theoretically and empirically, the effects of different tree representations and linguistic annotations on the performance of a PCFG-based parsing system. Since the estimated likelihood of a tree depends on its chosen representation in the corpus, different representations can lead to estimations that differ substantially from the tree's frequency in the training corpus. The author proposes several methods that more accurately estimate the distribution of trees.

One of the weaknesses of a PCFG model is that it makes incorrect independence assumptions, meaning that children of a node are determined based only on the label assigned to that node. This assumption sometimes leads to a poor language model and can be weakened by employing several modifications, such as different tree shapes or different labelling of the nodes. For example, a corpus with flat trees results in a model with fewer independence assumptions. 

Weakening the independence assumptions comes, however, at a cost. While the model can capture the structures in the training data more accurately (i.e. the \emph{bias} in the estimator decreases), more parameters need to be estimated and they are in general estimated less accurately (i.e. the \emph{variance} in the estimator increases). Therefore, a trade-off between bias and variance needs to be considered when choosing a representation for the trees in the corpus.

The author experiments with different tree representations and assesses the quality of the models. While the initial intuition is that Chomsky adjunction performs best, experiments on the Penn treebank show this is not always the case. By employing different transformations on the treebank and using metrics such as \emph{precision} and \emph{recall} to evaluate the trees against a test set, experiments show that \emph{flatten} and \emph{parent} transformations perform significantly better.

%His experiments show that, by adopting a proper tree representation and linguistic annotation, one can improve the performance of a parser. For example, just by adopting a simple node relabelling transformation on the Penn II treebank, one can improve the performance of a parser by 8\%, or approximately half of the performance difference between a simple PCFG model and the best today's available parsers. 
 
\section{BitPar and PCFG extraction}
\subsection{Bitpar}

{\bf Question 1}: BitPar provides the user with a compact representation of the parse forest. For each sentence in the corpus, an identifier consisting of a number is assigned to each production belonging to the tree that spans the sentence. This identifier need not be unique. In our case, the productions derived from the provided grammer are assigned the following identifiers:

\begin{center}
\begin{tabular}{rl}
NP $\rightarrow$ Mary & (\# 1) \\
V $\rightarrow$ saw & (\# 2) \\
NP $\rightarrow$ a bird & (\# 3, \# 5, \# 9) \\
PP $\rightarrow$ on a tree & (\# 4) \\
V $\rightarrow$ on a tree & (\# 6, \# 10) \\
NP $\rightarrow$ saw & (\# 7, \# 11) \\
PP $\rightarrow$ a worm & (\# 8, \# 12) 
\end{tabular}
\end{center}

In the encoding of a tree, each production is enclosed between parantheses. To keep the parse forest compact, several representations of a subtree can be incorporated within the same tree encoding. These representations are delimited by the symbol \emph{\#i} and all of them are enclosed in a pair of curly braces. For example, the parse forest of the sentence \emph{Mary saw a bird on a tree} from our corpus is encoded as

\begin{center}
\{(S \#1=(NP Mary)\{(VP \#2=(V saw) \#3=(NP a$\backslash$ bird) \#4=(PP on$\backslash$ a$\backslash$ tree))\#i(VP \#2(NP \#3 \#4))\})\#i(S(NP \#1(PP saw))(VP(V a$\backslash$ bird)(NP on$\backslash$ a$\backslash$ tree)))\}
\end{center}

Here, the parse forest contains three trees:

\begin{center}
\begin{tabular}{ccc}
\includegraphics[natwidth = 173px, natheight = 136px, scale = 0.75]{tree1.png} &
\includegraphics[natwidth = 173px, natheight = 172px, scale = 0.75]{tree2.png} &
\includegraphics[natwidth = 173px, natheight = 136px, scale = 0.75]{tree3.png}
\end{tabular}
\end{center}

Notice that the first two trees share the same two productions, namely S $\rightarrow$ NP VP and NP $\rightarrow$ Mary. The difference between them lies in the subtree whose root node is VP. This information is captured in the encoding
\begin{center}
\{(VP \#2=(V saw) \#3=(NP a$\backslash$ bird) \#4=(PP on$\backslash$ a$\backslash$ tree))\#i(VP \#2(NP \#3 \#4))\}
\end{center}
which says there are two possible subtrees with VP as the root node, delimited by \emph{\#i}. The first one is a flat representation of a verb phrase made up of a verb (V), a noun phrase (NP) and a prepositional phrase (PP). The second one, less restrictive, adds an optional PP to the noun phrase. While the first representation is more informative since it assumes a verb phrase always gives extra information about the noun involved (i.e. a bird \emph{on a tree}), the second representation is more concise, since it is closer to the syntax of the natural language (i.e. a prepositional phrase is not always included in a verb phrase).

The third representation, however, is the least accurate. In this case, words in the sentence are not assigned proper POS tags - \emph{PP} is assigned to the verb \emph{saw} and \emph{V} is assigned to the noun \emph{bird}.

\

The best parses returned by BitPar for the toy corpus are as follows:
\begin{center}
\emph{Mary saw a bird on a tree} $\rightarrow$ (S (NP Mary)(VP (V saw)(NP a\ bird)(PP on\ a\ tree))) \\
\emph{a bird on a tree saw a worm} $\rightarrow$ (S (NP (NP a\ bird)(PP on\ a\ tree))(VP (V saw)(NP a\ worm))) \\
\end{center}
which can be represented as

\begin{center}
\begin{tabular}{ccc}
\includegraphics[natwidth = 173px, natheight = 136px, scale = 0.75]{tree1.png} &
\includegraphics[natwidth = 173px, natheight = 136px, scale = 0.75]{tree6.png}
\end{tabular}
\end{center}

The Viterbi probablities returned by BitPar for the toy corpus are as follows:

\begin{center}
\emph{Mary saw a bird on a tree} $\rightarrow$ \{(S=\#i[P=0.0095693] \#1=(NP=\#i[P=0.206644] Mary)\{(VP=\#i[P=0.0463081] \#2=(V=\#i[P=0.669471] saw) \#3=(NP=\#i[P=0.206644] a$\backslash$ bird) \#4=(PP=\#i[P=0.669471] on$\backslash$ a$\backslash$ tree))\#i(VP=\#i[P=0.011577] \#2(NP=\#i[P=0.0345856] \#3 \#4))\})\#i(S=\#i[P=1.23328e-05](NP=\#i[P=0.00478113] \#1(PP=\#i[P=0.0925481] saw))(VP=\#i[P=0.00257946](V=\#i[P=0.0216779] a$\backslash$ bird)(NP=\#i[P=0.237981] on$\backslash$ a$\backslash$ tree)))\} \\
\emph{a bird on a tree saw a worm} $\rightarrow$ \{(S=\#i[P=0.00239232](NP=\#i[P=0.0345856] \#5=(NP=\#i[P=0.206644] a$\backslash$ bird)(PP=\#i[P=0.669471] on$\backslash$ a$\backslash$ tree))(VP=\#i[P=0.0691711](V=\#i[P=0.669471] saw)(NP=\#i[P=0.206644] a$\backslash$ worm)))\#i(S=\#i[P=4.9331e-05] \#5\{(VP=\#i[P=0.000238725] \#6=(V=\#i[P=0.0925481] on$\backslash$ a$\backslash$ tree) \#7=(NP=\#i[P=0.237981] saw) \#8=(PP=\#i[P=0.0216779] a$\backslash$ worm))\#i(VP=\#i[P=5.96811e-05] \#6(NP=\#i[P=0.00128973] \#7 \#8))\})\}
\end{center}

\noindent {\bf Question 2}: Obtaining the parse forest of the corpus given the toy grammar and the toy lexicon can be done by running BitPar with the command line
\begin{center}
./bitpar toy.gram toy.lex corpus -o -p
\end{center}
The first parameter, \emph{-o}, tells BitPar to print the parse forest of \emph{corpus}, given the grammar \emph{toy.gram} and the lexicon \emph{toy.lex}. The second parameters specifies that the grammar used is a PCFG.

To obtain the best \emph{N} parser with BitPar, given \emph{corpus}, the grammar \emph{toy.gram} and the lexicon \emph{toy.lex}, the following command needs to be run:
\begin{center}
./bitpar toy.gram toy.lex corpus -b N
\end{center}

Finally, the Viterbi probabilities for the same corpus, grammar and lexicon files, can be obtained by running BitPar with the command line
\begin{center}
./bitpar toy.gram toy.lex corpus -vp
\end{center}

\subsection{The Penn Treebank}

{\bf Question 3}: A PP attachment to an S (or a high-attachment) is a prepositional phrase that comments on / adds extra information about the topic of a sentence, while a PP attachment to a VP (or a low-attachment) is a prepositional phrase that comments on / adds extra information about the action described in the verb phrase. In many cases, S-PPs can be preposed, while preposed VP-PPs not only sound wrong, but can also change the meaning of the sentence. On the other hand, S-PPs always lie outside the scope of negation, while VP-PPs may or may not lie inside the scope of negation.

Using the proper attachment, however, helps disambiguating the meaning of the sentence. Consider the following example:
\begin{itemize}
\item John did not go to karaoke in Amsterdam, but went in Utrecht.
\item John did not go to karaoke in Amsterdam: he went to karaoke in Utrecht.
\end{itemize}

In the first sentence, the interpretation of the PP is sentential and there are two events which are described: although John went to karaoke only in Utrecht, he also visited Amsterdam. In the second sentence, the interpretation is that John did not visit Amsterdam at all, but he visited Utrecht, where he went to karaoke. The PPs involved are low-attachments and only one event is referred to.

\subsection{Extracting a treebank PCFG}

{\bf Question 4}: After training the parser with sections 2 to 21 of the Penn WSJ treebank corpus, parsing section 22 and evaluating the results with \emph{evalC}, the following results were obtained:
\vspace{0cm}
\begin{table}[!h]
\centering
\begin{tabular}{|c|c|c|}
\hline
& All & len $\leq 40$ \\ \hline
Number of sentence & 1700 & 1578 \\
Bracketing Recall & 66,25 & 67,25 \\
Bracketing Precision & 71,09 & 72,17 \\
Bracketing FMeasure & 68,58 & 69,62 \\
Complete match & 9,82 & 10,58 \\
Average crossing & 2,97 & 2,58 \\
No crossing & 30,82 & 32,70 \\
2 or less crossing & 56,18 & 59,70 \\
Tagging accuracy & 93,57 & 93,57 \\ \hline
\end{tabular}
\caption{Evaluating parsing performance on section 22 of the Penn WSJ corpus}
\end{table}


%For example, a sentence from the Penn WSJ corpus containing a low-attachment is
%\begin{center}
%Sixty percent of the fund will be invested in stocks, with the rest going into bonds or short-term investments.
%
%The rest of the concert was more straight jazz and mellow sounds $\underbrace{\underbrace{\mbox{written}}_{\mbox{VBN}}\underbrace{\mbox{\emph{by Charlie Parker, Ornette Coleman, Bill Douglas and Eddie Gomez}}}_{\mbox{PP}}}_{\mbox{VB}}$, with pictures for the Douglas pieces.
%\end{center}

\end{document}