#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>

using namespace std;

int main(int argc, char *argv[])
{
    string input_grammar_path, input_lexicon_path;
    string output_grammar_path, output_lexicon_path;
    string input_line;

    ifstream input_grammar, input_lexicon;
    ofstream output_grammar, output_lexicon;

    map<string, unsigned int> terminals;

    if (argc != 5)
    {
        cout << "Usage: ./treeparse [input grammar] [input lexicon] [output grammar] [output lexicon]" << endl;
        return 0;
    }
    else
    {
        input_grammar_path.append(argv[1]);
        input_lexicon_path.append(argv[2]);
        output_grammar_path.append(argv[3]);
        output_lexicon_path.append(argv[4]);

        try
        {
            cout << "Opening " << input_grammar_path
                 << "..." << endl;
            input_grammar.open(input_grammar_path.c_str());

            if (!input_grammar)
                throw "UNABLE_TO_OPEN_INPUT_GRAMMAR_EXCEPTION";

            cout << "Opening " << input_lexicon_path
                 << "..." << endl;
            input_lexicon.open(input_lexicon_path.c_str());

            if (!input_grammar)
                throw "UNABLE_TO_OPEN_INPUT_LEXICON_EXCEPTION";

            cout << "Opening " << output_grammar_path
                 << "..." << endl;
            output_grammar.open(output_grammar_path.c_str());

            if (!input_grammar)
                throw "UNABLE_TO_OPEN_OUTPUT_GRAMMAR_EXCEPTION";

            cout << "Opening " << output_lexicon_path
                 << "..." << endl;
            output_lexicon.open(output_lexicon_path.c_str());

            if (!input_grammar)
                throw "UNABLE_TO_OPEN_OUTPUT_LEXICON_EXCEPTION";
        }
        catch (string err_string)
        {
            cout << "Exception raised: " << err_string << endl;
        }
    }

    cout << endl;

    unsigned int count = 0, total_count = 0;
    string node = "", terminal = "";
    terminals.clear();

    cout << "Writing new grammar... ";

    while (getline(input_grammar, input_line))
    {
        istringstream line_stream(input_line);

        if (!input_line.empty())
        {
            line_stream >> count;
            line_stream >> node;

            if (terminals.find(node) == terminals.end())
                terminals.insert(make_pair(node, count));
            else
                terminals.at(node) += count;

            total_count += count;
            output_grammar << count << " " << node
                           << "\" [' " << node << "'";

            while (line_stream >> node)
                output_grammar << " " << node << "\"";

            output_grammar << " ]'" << endl;
        }
    }

    cout << "done." << endl
         << "Writing new lexicon... ";

    while (getline(input_lexicon, input_line))
    {
        istringstream line_stream(input_line);

        if (!input_line.empty())
        {
            line_stream >> terminal;

            if (terminals.find(terminal) == terminals.end())
                terminals.insert(make_pair(terminal, 0));

            while (line_stream >> node >> count)
            {
                output_grammar << count << " " << node << "\" [' "
                               << node << "' " << terminal << "' ]'" << endl;
                terminals.at(node) += count;
                terminals.at(terminal) += count;
                total_count += count;
            }
        }
    }

    output_lexicon << "[\t[' " << total_count << endl
                   << "]\t]' " << total_count << endl;

    for (map<string, unsigned int>::iterator iter = terminals.begin(); iter != terminals.end(); iter++)
        output_lexicon << iter->first << "\t" << iter->first << "' " << iter->second << endl;

    cout << "done." << endl;

    input_grammar.close();
    input_lexicon.close();
    output_grammar.close();
    output_lexicon.close();

    return 0;
}

