\documentclass [11pt] {article}

\usepackage{graphicx}
\usepackage{listings}
\usepackage{fullpage}
\usepackage{pbox}
\usepackage{float}
\usepackage{amsmath}
\usepackage{fancyvrb}
\restylefloat{table}

\title {Investigating the Limitations of Hierarchical Phrase-Based Translation Models}
\author {Mihai Morariu, Andrei Oghin\u{a}}

\begin {document}

\maketitle

\begin {abstract}
Recent developments in statistical translation revolve around trying to capture the recursive structure of language. However, the progress on models that use information from linguistic theory has been slow. In this work, we outline some of the trade-offs involved in these approaches, and test the margin by which a hierarchical translation model outperforms a phrase-based model. 
\end {abstract}


\section {Introduction}

\

The field of machine translation is almost as old as the modern digital computer. Warren Weaver proposed, in 1949, that statistical methods and ideas from information theory be used to tackle the problem of translating text written in a \emph{source language} into text written in a \emph{target language}, a field which he was developing along with Claude Shannon and others \cite{Weaver}. Researchers soon abandoned work in this direction, advancing several theoretical objections, but an obstacle to the development of statistical machine translation also consisted in the limited capabilities of computers at the time. \cite{Brown2}

\

Modern computers are several orders of magnitude more powerful than in the previous century, both in terms of computational speed and in terms of storage capacity, allowing for the implementation of a wide range of statistical machine translation models. Without the computational complexity being as much of an issue as it was half a century ago, the challenge that arises nowadays is the choice of a model that is able to best capture the morphological and syntactical particularities of a language. Steps have been taken into this direction and several models have emerged, as discussed in section \ref{sec:related_work}. In this work, we set to analyse two of these models, namely the hierarchical model with and without syntactical constaints, and discuss their particularities that lead to a better or a worse translation performance relative to the phrase-based model.

\

The rest of the paper is organized as follows. Section \ref{sec:related_work} gives an overview of the research that has been done in the field. Section \ref{sec:experimental_setup} discusses the setup of the experiments we set to perform in our work. Section \ref{sec:results} provides the results of these experiments. Section \ref{sec:discussion} provides a discussion on these results. Finally, section \ref{sec:conclusion} summarizes the findings and outlines future work directions.

\newpage

\section {Related Work}
\label {sec:related_work}

\

Building on the seminal work of the IBM Watson team behind Brown et al. (1993) \cite{Brown}, which revived the field of machine translation, around the turn of the century a new approach to machine translation emerged, namely phrase-based models. As the name suggests, it uses phrases instead of single words. In the framework presented by Koehn et al. (2003) \cite{koehn2003}, such a translation model is proposed in addition to a decoding algorithm. Phrases are obtained with the help of a \emph{word alignment} such as the one used by the IBM models. Parts of sentences that then point to each other in both alignments can be seen as phrase translations. 


\

While the phrase-based approach learns well certain expressions and idioms, it fails to deliver when it comes to long-distance relationships between words or phrases. To solve this, various attempts have been made to capture the recursive structure of language, by augmenting the models with linguistic information. Yamada and Knight (2001) \cite{yamada} describe a decoding algorithm which builds up an English parse tree given a sentence in a foreign language. Such models that rely on one parse tree are called \emph{string-to-tree} models. Other approaches try to learn the relationship between two grammars using \emph{tree-to-tree} models, thus relying on two parse trees. To this end, Groves et al. (2004) \cite{groves} introduce an induction algorithm for sub-structural alignments between context-free phrase structure trees. This serves as a data acquisition technique for Data-Oriented Translation (DOT), a machine translation system based on Data-Oriented Parsing (DOP) which exploits parsed, aligned bitexts to produce high quality translations. Despite these efforts, syntactic information has failed to improve the accuracy of statistical machine translation, due to the constraints it adds to the translation process.


\ 
  
A step forward was performed by Chiang (2007) \cite{Chiang}, who embeded long-distance relationships between words within the flexibility of phrase-based translations, but in a rather linguistic agnostic fashion, by introducing the concept of \emph{hierarchical phrases} - phrases that contain subphrases. The hierarchical phrases are learned from a parallel corpus without any syntactic annotations. They may capture long-range (grammatical) relations that cross phrase boundaries. Since Chiang uses hierarchical structures, we may call this a syntax-based approach to translation, even though the hierarchical structures here have nothing to do with any linguistic theory of syntax. 

\

\section {Experimental Setup}
\label {sec:experimental_setup}

\

In this work, we set to test the margin by which a hierarchical translation model outperforms a phrase-based model. To do this, we perform three experiments which involve translating a corpus of French sentences into English. The first experiment, which we call \emph{phrase-based}, performs the translation of the corpus using the phrase-based model proposed by Koehn et al. \cite{koehn2003}. The second experiment, which we call \emph{string-to-string}, performs the translation of the corpus using the hierarchical translation model proposed by Chiang \cite{Chiang}, without adding syntactical constraints on any side of the corpus. The third experiment, which we call \emph{string-to-tree}, uses the same hierarchical translation model, but with the additional constraint that the English side be assigned with syntactically annotated parse trees. All the experiments were performed on a 48-cores machine (1067 MHz each), with 250 GB RAM, running CentOS Linux.

\

The corpus we use for training the translation system and for testing represents a fraction of the data set released for the 2012 Workshop in Machine Translation. This is a sentence-aligned parallel corpus containing sentences of news analysis from project syndicate, written in both French and English. We use 10,000 sentences for training. We use this relatively small training set because, for the third experiment, we need to parse the English side, which would be very time consiming for a larger corpus. Tuning is performed on another dataset consisting of 2,051 entries, and testing on yet another parallel corpus consisting of 3,003 sentences. All sentences have been preprocessed to comply with the format imposed by the translation system. This preprocessing involves \emph{tokenisation} (spaces are inserted between words and punctuation), \emph{truecasing} (initial words in each sentence are converted to their most probable casing) and \emph{cleaning} (long, empty and misaligned sentences are removed).

\

Translation is performed using Moses, a statistical machine translation system that allows automatic training of translation models for any language pair. The parser we use for the third experiment is BitPar, which is already trained on sections 2 to 21 of the WSJ Penn treebank. Translation quality is evaluated, in all experiments, using the BLEU metric score. 

\

We use the same language models for all experiments, as it was extracted from the unannotated English-side training corpus. 

\

For all three experiments, training the translation model involves performing three steps. First, the actual training is performed, followed by testing its performance "as is". Then, we employ tuning, to adjust meta-parameters, such as the language model coefficient. Finally, we test the tuned model by translating a test data set, and computing the BLEU scores agaist human translations.

\subsection {Phrase-Based Experiment}

\

In this first experiment, we train on a set of 10,000 sentence pairs of parallel corpus consisting of news items in English and French. We follow the instructions on how to train a baseline system from the \emph{Moses} manual. After preparing the data, we first build an English language model, based solely on the English sentences. Then, we train the translation model, using \emph{Giza++} to generate the word alignments, and the \emph{train-model.perl} script to train the phrase-based model. Finally, we tune the model using the \emph{mert-moses.pl} script, based on a different, smaller English-French parallel corpora.

\subsection {String-To-String Experiment}

\

This experiment makes use of a hierarchical phrase-based model without syntax. These models use a grammar consisting of SCFG (Synchronous Context-Free Grammar) rules, thus being similar to the Hiero model described by Chiang (2007) \cite{Chiang}. While traditional phrase-based models use atomic mappings of an input phrase to an output phrase, tree-based models operate on so-called grammar rules, which include variables in the mapping rules, such as {\bf ne $X_1$ pas $\rightarrow$ not $X_1$} (French-English). This allows for handling of discontinuous, possibly long-distance relationships between words. 

\

Training the hierarchical model is similar to training the phrase-based models, and uses the same script, \emph{train-model.perl}. Some additional parameters need to be specified, such as \emph{-hierarchical} and \emph{-glue-grammar}. The command used for training is presented below.  

\begin{Verbatim}[frame=single]
nohup nice /scratch/sslp/mosesdecoder/dist/training/train-model.perl 
-root-dir train-10k -bin-dir /scratch/sslp/mosesdecoder/giza 
-corpus /scratch/andrei/corpus/news-c-10k -f fr -e en 
-alignment grow-diag-final-and 
-lm 0:3:/scratch/andrei/lm/news-commentary-v7.fr-en.blm.en:8 
-hierarchical -glue-grammar -max-phrase-length 5  >& training-10k.out &
\end{Verbatim}

\

Once the model is trained, it can be tuned using a different, smaller parallel corpus. Multiple runs are performed in order to determine the best model meta-parameters, such as the language model parameter. Tuning is performed in a similar way as for the phrase-based model. However, this time a different decoder is used (\emph{moses\_chart}), since the model is hierarchical. Also, a special, undocumented parameter has to be set in order to specify the filtering command that the tuning script should use. Using the same method to filter the model as if it would be a phrase-based model breaks the hierarchical model. Therefore, the undocumented parameter \emph{filtercmd} is used, which contains the call to the \emph{filter-model-given-input.pl}, which in turn is given the \emph{--Hierarchical} parameter, so it knows it is handling a hierarchical model. The complete command used is presented below.  

\begin{Verbatim}[frame=single]
nohup nice /scratch/sslp/mosesdecoder/scripts/training/mert-moses.pl 
-inputtype=3 /scratch/andrei/corpus/news-test2008.true.fr 
/scratch/andrei/corpus/news-test2008.true.en 
/scratch/sslp/local/bin/moses_chart 
		/scratch/andrei/project-1/working/train-10k/model/moses.ini 
--mertdir /scratch/sslp/local/bin 
-filtercmd 
  '/scratch/sslp/mosesdecoder/scripts/training/filter-model-given-input.pl 
  -Binarizer "/scratch/sslp/local/bin/CreateOnDisk 1 1 5 100 2" --Hierarchical' 
--decoder-flags="-threads 16" >& mert-10k.out &
\end{Verbatim}


\subsection {String-To-Tree Experiment}

\

This model implies adding syntactic labels to the rules on the target side. Thus, an additional step needs to be performed, consisting of parsing the English side. To do this, we use the wrapper \emph{parse-de-bitpar.perl} provided by the University of Stuttgart. While the script was initially developed to be used with the German data set, we employ some changes to it, adapting the wrapper to fit our needs. These changes involve setting the internal paths to point to the English grammar, lexicon and open class tags files extracted from the WSJ Penn treebank, instead of the old, German versions. Once the script is ready to be used, we parse the English side of the corpus with the command presented below.

\

\begin{Verbatim}[frame=single]
cat corpus/news-c-10k.en | ./parse-de-bitpar.perl &
\end{Verbatim}

There is no need to preprocess the corpus and convert it to the format required by BitPar, where each sentence is split into tokens, one token per line, and one empty line is used to delimit the end of a sentence and the beginning of the next one. This step is already performed by the wrapper, so the script only needs to be run with the English corpus provided as input parameter.

\

Once parsing is done, the output consists of a file formatted using XML markup to represent the syntactic trees associated with each sentence in the corpus, where each node \emph{X} in the tree is delimited by the tags \emph{$<$tree label="X"$>$} and \emph{$<$/tree$>$}. Each tree associated to a sentence is stored on a separate line in the output file. A sample output tree could look as follows:

\begin{Verbatim}[frame=single]
<tree label="NP"><tree label="DET">the</tree><tree label="NN">cat</tree></tree>
\end{Verbatim}

The next step in the string-to-tree experiment is to train the system, taking into account the newly processed corpus. The \emph{train-model.perl} script provided with Moses is used again to perform this step. The command is identical to the one used in the previous experiment, except that the extra parameter \emph{--source-syntax} or \emph{--target-syntax} is added to tell Moses whether we are performing tree-to-string translation or string-to-tree translation. If we add both parameters, Moses performs tree-to-tree translation. In our case, we add the extra parameter \emph{--target-syntax}. The complete command used is presented below.

\begin{Verbatim}[frame=single]
nohup nice /scratch/sslp/mosesdecoder/dist/training/train-model.perl 
-root-dir train-10k-s -bin-dir /scratch/sslp/mosesdecoder/giza 
-corpus /scratch/morariu/project/corpus/corpus-processed-10k -f fr -e en 
-alignment grow-diag-final-and --target-syntax
-lm 0:3:/scratch/morariu/project/lm/news-commentary-v7.fr-en.blm.en:8 
-hierarchical -glue-grammar -max-phrase-length 5  >& training-10k-s.out &
\end{Verbatim}

Finally, tuning is performed exactly as in the string-to-string experiment, with the proper change of paths.

\section {Results}
\label {sec:results}

\

In this section we present the evaluation results in terms of BLEU score for all the described models. We report the scores before (BLEU - RAW) and after tuning (BLEU - TUNED). The results for the three models, phrase-based (PB), hierarchical string-to-string (HSS) and hierarchical string-to-tree (HST) are presented in table \ref{table:results}.  

\

\begin{table}[H]
\begin{center}
\begin{tabular}{|l|c|c|c|c|}
%% row 1
\hline
Method 				&  BLEU - RAW	&	 BLEU - TUNED		\\ \hline	
PB					&  16.64		&	 17.46				\\ \hline
HSS					&  16.72		&	 18.33				\\ \hline
HST					&  16.04		&	 17.71				\\ \hline
\end{tabular}
\end{center}
\caption{Evaluation results}
\label{table:results}
\end{table}

We also report the computation times for each of the three experiments, in table \ref{table:times}. By computation times we refer to the amount of time it took to train the translation system, to tune the parameters and to test each system. The times reported in the "Testing" column refer to the times obtained by testing each system, after tuning is performed.

\

\begin{table}[H]
\begin{center}
\begin{tabular}{|l|c|c|c|c|}
%% row 1
\hline
Method 				& Training &  Tuning & Testing & Parsing \\ \hline	
PB					& 12 & 285 & 13 & -							\\ \hline
HSS					& 13 & 241 & 52 & -							\\ \hline
HST					& 11 & 47 & 4 & 263							\\ \hline
\end{tabular}
\end{center}
\caption{Computation time (in minutes)}
\label{table:times}
\end{table}

\

Since all the experiments use the same language model, we do not include the time required to extract it from the English side of the corpus (16 seconds) in the table. For the hierarchical translation experiment with syntax annotation, we also report the time required to parse the English side.


\section {Discussion}
\label {sec:discussion}

\ 

Results from table \ref{table:results} show that hierarchical translation performs better than phrase-based translation, receiving a higher BLEU score with or without performing the parameters tuning step. This is in line with the results obtained by Chiang \cite{Chiang}, who states in his paper that \emph{hierarchical phrase pairs, which can be learned without any syntactically-annotated training data, improve translation accuracy significantly compared with a state-of-the-art phrase-based system}. The reason for this is that phrase-based translation fails to take into account the recursive structure of the language, as opposed to the hierarchical translation model.

\

While the hierarchical string-to-string model outperforms the phrase-based model, one might expect a higher boost in performance. However, recall one of the main strengths of the hierarchical model is to model long-distance relationships between words. English and French have a relatively similar word ordering, thus making the ability of modeling such relationships less critical. Our hypothesis is that we would observe more significant differences for other language pairs. 

\

Adding syntactical constraints on the English side of the corpus does not seem to improve performance over the HSS model. This is in line with the results obtained by Koehn et al. \cite{koehn2003}, which state in their paper that \emph{requiring
constituents to be syntactically motivated does not lead to better constituent pairs, but only fewer constituent pairs, with loss of a good amount of valuable knowledge}. 

\

The problems related to syntactic approaches has also been tackled by Wellington et al. (2006) \cite{Wellington}, who zoom in and investigate why syntactic constrains fail to improve statistical translation models significantly. They start by defining the complexity of a translation equivalence as the number of gaps needed to align the two structures based on binary branching production rules. The measurement of the \emph{alignment complexity} (number of gaps) is done using a hierarchical alignment algorithm. The experiments show that, by constraining the word alignment using a parse tree on the English side, a high degree of failure rate is unavoidable (above 35\% for four out of five language pairs) under a zero-gaps assumption. However, failure rates decrease dramatically when allowing just one gap. The authors concludes that there are many cases of patterns that cannot be analyzed using binary-branching structures without gaps, and advocates for an alternative approach using discontinuous constituents. However, such approaches are estimated to prove very challenging in terms of computational costs.

\

An important thing to notice is that, while the computation times for training the translation system are roughly the same for all three experiments, they differ significantly when tuning the parameters or testing the model. HST beats the other two models by a very high margin, performing significantly faster, although producing slightly lower BLEU scores than the HSS model. A possible explanation for this could be the fact that constraining one side of the corpus with syntactically annotated parse trees limits the number of possible alignments that can be computed between a pair of sentences. Thus, for HST, the search problem becomes more informed (as opposed to the high-volume brute-force approach of PB and HSS), drastically reducing the computational complexity of the model. The HST model effectively narrows its search by eliminating most of the candidates which are not syntactically plausible. Results suggest that, while doing this, sometimes good candidates are eliminated also. There is therefore a trade-off, but this could potentially be improved when using a significantly bigger training corpus.

\


\section {Conclusions and Future Work}
\label {sec:conclusion}

\

In this work, we have performed an analysis of the phrase-based model and the hierarchical translation model and investigated their limitations. While the hierarchical translation model outperforms the phrase-based model, a drop in the translation performance is encountered when adding syntactical annotation to the side of the corpus written in the target language. However, this model achieves a significant improvement in computational performance. 

\

While we have limited ourselves to only two hierarchical models, namely string-to-string and string-to-tree, it would be interesting to also investigate the translation performance of the tree-to-string model and to put it into balance with the presented experiments. To do this, the French side of the corpus would also need to be parsed, so BitPar should be fed with a translation into French of the training data. The Europarl corpus would seem a reasonable choice, since it consists of a significant amount of data written in all languages of the European Union. For a fair evaluation, BitPar should be re-trained on the English side of the Europarl corpus for the string-to-tree approach and on the French side for the tree-to-string approach. We plan to employ this change in a future project.

\

It would be also interesting to analyze these models on a different language pair - especially one that uses significantly different word orderings. In such a scenario, the capability of the hierarchical model to learn long-distance relationships between words may play a greater role, leading to a higher performance boost, when comparing with the phrase-based approach.   

\begin {thebibliography} {100}

\bibitem{Weaver} Warren Weaver. 1949. \emph{``Translation''}. Machine Translation of Languages, MITT Press, Cambridge, MA.

\bibitem{Brown2} Peter Brown, Stephen Della Pietra, Vincent Della Pietra, Frederick Jelinek, John Lafferty Robert Mercer and Paul Rossin. 1990. \emph{``A Statistical Approach to Machine Translation''}. Computational Linguistics Volume 16, Number 2.

\bibitem{Brown} Peter Brown, Stephen Della Pietra, Vincent Della Pietra and Robert Mercer. 1993. \emph{``The Mathematics of Statistical Machine Translation: Parameter Estimation''}. Computational Linguistics 19(2).

\bibitem{koehn2003} Philipp Koehn, Franz Josef Och and Daniel Marcu. 2003. \emph{``Statistical Phrase Based Translation''}. Proceedings of HLT/NAACL 2003.

\bibitem{yamada} Kenji Yamada and Kevin Knight. 2002. \emph{``A Decoder for Syntax-based Statistical MT''}. Proceedings of the 40th anniversary meeting of the Association for Computational Linguistics, 2002.

\bibitem{groves} Declan Groves and Mary Hearne, Andy Way. 2004. \emph{``Robust Sub-Sentential Alignment of Phrase-Structure Trees''}. In Proceedings of the 20th International Conference on Computational Linguistics (COLING) 2004.

\bibitem{Chiang} David Chiang. 2007. \emph{``Hierarchical Phrase-Based Translation''}. Computational Linguistics 33(2).

\bibitem{Chiang2005} David Chiang. 2005. \emph{``A Hierarchical Phrase-Based Model for Statistical Machine Translation''}. In Proc. ACL, pages 263-270.

\bibitem{Wellington} Benjamin Wellington, Sonjia Waxmonsky, I. Dan Melamed. 2006. \emph{``Empirical lower bounds on the complexity of translational equivalence''}. In Proceedings of the 21st International Conference on Computational Linguistics and the 44th annual meeting of the Association for Computational Linguistics.

\end {thebibliography}

\end {document}
