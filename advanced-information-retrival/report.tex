\documentclass[conference]{IEEEtran}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{hyperref}
\hyphenation{op-tical net-works semi-conduc-tor}

\begin{document}
\title {Analysis of Different Approaches to Cross-Language Information Retrieval}
\author{
\IEEEauthorblockN{Jorik Duurkoop}
\IEEEauthorblockA{jorik.duurkoop@student.uva.nl}
\and
\IEEEauthorblockN{Mihai Morariu}
\IEEEauthorblockA{mihai-adrian.morariu@student.uva.nl}
\and
\IEEEauthorblockN{Alex Rietveld}
\IEEEauthorblockA{alex.rietveld@student.uva.nl}
\and
\IEEEauthorblockN{Jingting Wu}
\IEEEauthorblockA{jingting.wu@student.uva.nl}
}
\maketitle


\begin {abstract}
This work analyzes several different approaches to Dutch-English cross-language information retrieval. A key aspect of this cross-language IR research is whether to translate the set of queries or the entire document collection. We also evaluate the effect of using query expansion, whereby queries are automatically extended with additional terms. Furthermore, we investigate the impact of different translation tools on the information retrieval performance. From our experiments we conclude query translation obtains better performance than document translation and that query expansion helps; however the effect is limited. We also conclude that the translation quality can have a significant impact on IR performance.
\end {abstract}

\IEEEpeerreviewmaketitle

\section {Introduction}
\label {sec:introduction}

Cross-language information retrieval (CLIR) is the task of retrieving relevant documents from a collection written in a language that is different from the language of a user's query. Since the information that is widely available to the public comes in different languages and there are many people who speak at least two languages, CLIR systems can prove useful to users who are seeking additional results for their information need.

In this paper, we investigate several approaches for Dutch-English information retrieval and analyze their performance on the retrieval process. The paper is organized as follows: First, in section \ref{sec:work}, we discuss related work. Then, we define three research questions and provide our hypotheses for them in section \ref{sec:questions}. Next, in section \ref{sec:methodology}, we present the methodology we use in our research. In section \ref{sec:description}, we describe the experiments we performed in order to answer the research questions. In section \ref{sec:setup}, we discuss the setup of these experiments and subsequently present the results in section \ref{sec:results}. We analyze and interpret the results in section \ref{sec:analysis}. Future work is presented in section \ref{sec:future_work} and finally, we draw our conclusions in section \ref{sec:conclusions}.

\section {Related Work}
\label {sec:work}

Our work is similar to a number of existing ones. In their paper \cite{chen}, Chen and Gey analyze several approaches to monolingual, bilingual and multilingual information retrieval for European languages (English, Dutch, Finnish, French, German, Italian, Spanish and Swedish). They conclude that document translation-based multilingual retrieval is slightly better than the query translation-based one and that combining both techniques yields even better performance. However, in their experiments, the documents are translated word-for-word using either machine-translation systems or translation lexicons derived from parallel texts. We, on the other hand, translate entire documents with online translation tools.

Ballesteros and Croft \cite{ballesteros} concentrated on analyzing the impact of translation and query expansion on CLIR peformance for Spanish and English. In their paper, they conclude that: 1) proper care should be considered when translating the queries since "the effect of one poor translation can counteract any improvement gained by the correct translation of several phrases and may cause additional drops in effectiveness" and 2) query expansion techniques can "significantly reduce the error associated with dictionary translation", while "further improvements are dependent upon phrasal translation".

Akin to our work, other authors have concentrated on Dutch and English cross-language information retrieval. In her paper \cite{adriani}, Adriani analyzes several techniques used for English-Dutch CLIR which focus on query translation and query expansion. We take a different approach - we do not analyze different query translation techniques, we are only interested in determining which of the two approaches (query versus document translation) yields better retrieval performance.

\section{Research Questions}
\label{sec:questions}

For our experiments, we set the following research questions:

\

\begin{itemize}%[noitemsep]
    \item RQ1: Which of the two approaches provides better retrieval performance: translating the Dutch query into English and performing monolingual English retrieval or translating the document collection into Dutch and performing monolingual Dutch retrieval?
    \item RQ2: How does the quality of translation affect the performance of information retrieval?
    \item RQ3: How does query expansion perform in cross-lingual retrieval, given the fact that the query also needs to be translated?
\end{itemize}

\

\noindent Regarding research question RQ1, we formulated the following hypothesis:
\begin{itemize}
\item H1: Document translation will result in better retrieval performance than query translation.
\end{itemize}

\noindent We assume a higher accuracy for document translation due to the amount of additional context available. Documents contain more words and are therefore much longer than queries, meaning there is a lot more surrounding context to work with. A query usually consists of only a few words, therefore ambiguous words are harder to translate correctly. We theorize that translating a word incorrectly in a query is more severe than doing so in a document, because simply having a few words mistranslated in a document should still allow you to retrieve the document - albeit with a lower rank - but having a few words mistranslated in a query will most likely return a completely different set of documents.

\

\noindent Our hypothesis for RQ2:

\begin{itemize}
\item H2: The quality of translation will have a significant effect on the quality of information retrieval.
%Document translation will result in better retrieval performance than query translation.
%The quality of translation has a significant effect on the quality of information retrieval.
\end{itemize}

\noindent Using different translation services results in different translation performance. It is even possible for translations to differ in quality within the same service. We expect a more accurate translation to result in better IR performance.

\

\noindent The hypothesis derived from RQ3:

\begin{itemize}
\item H3: Query expansion will improve the retrieval performance.
\end{itemize}

\noindent We expect the additional terms will help retrieve relevant documents as these new terms are supposed to be the most informative terms of the initially retrieved documents.


\section{Methodology}
\label{sec:methodology}

Our experiments are accomplished in two steps: translation of queries / documents and performing information retrieval. We then use different metrics to evaluate the results of these tasks.
\subsection{Translation}

\subsubsection{Translation Models}
Current machine translation systems fall into three categories: word-based models, phrase-based models and semantics-based models. However, the models under the translators are not very important to us as for our research we use existing free translation tools instead of implementing a machine translation system ourselves. Besides, the model details of these translation tools are unknown to the public.  

\subsubsection{Translation Evaluation}
Measuring translation quality is difficult because there is not an absolute way to measure how correct a translation is. A good translation quality evaluator should be as objective as possible. Bilingual Evaluation Understudy (BLEU) \cite{papineni} is one of the most widely used algorithms for evaluating the quality of text which has been machine-translated from one natural language to another. The BLEU metric scores a translation on a scale of 0 to 1 according to the correspondence between a machine's output and that of a human. The problem is that BLEU looks to match and measure the extent to which n-grams in two documents are identical, therefore accurate translations that use synonyms may score poorly since there is no match with the human reference(s). Thus BLEU should not be used as an absolute measure of translation quality.

\subsection{Information Retrieval}

\subsubsection{Stemming and Smoothing}
Since there is no standard stemmer for Dutch and, based on our previous IR research, smoothing and excluding stop words do not really improve performance, we do not use any stemming or smoothing methods and include stop words in our queries. The performance of different smoothing methods can be seen in figure \ref{fig:1}. Notice that the graph of the "No Smoothing" method is barely visible, being almost completely overlapped by the graph of the "Dirichlet" method. However, we perform query expansion, which is expected to improve IR performance.

\begin{figure*}
  \centering
    \includegraphics[scale = 0.8]{SmoothingRP.png}
	\caption{Smoothing and stop words do not improve the retrieval performance too much}
	\label{fig:1}
\end{figure*}

\subsubsection{Query Expansion}
In our research, we use pseudo-relevance feedback, meaning that the additional terms are obtained by first performing information retrieval and then extracting the most informative terms from the retrieved documents. These are subsequently added to the original query. This process is applied to both the query and document translation approaches. Note that, for query translation, we first translate the original query and then extract the informative terms - we do not do it the other way round of first obtaining the additional terms and then translating the extended query.

\subsubsection{Information Retrieval Evaluation}

We want to compare different approaches to information retrieval, so we need to use suitable metrics to evaluate the differences in performance between them. In IR tasks, it is common practice to use Mean Average Precision (MAP) to provide a single value that rates the performance of the system. We use this metric and, on top of it, we also present the Precision-Recall for each system.

In order to determine if there is a correlation between the translation and the information retrieval performance, we also compute the Pearson coefficient \cite{rogati}\footnote{We implemented a script in Python which uses the libraries provided by SciPy in order to compute the Pearson correlation coefficient between two vectors.}. The Pearson correlation coefficient is a metric ranging from -1 to 1 which establishes the degree of correlation between two random variables. A value closer to -1 or 1 indicates a strong correlation, while 0 indicates the variables are independent.

\section{Experiment Description}
\label{sec:description}

We use a collection of English newspaper articles, a set of query topics written in Dutch (along with corresponding English translations) and a human relevance judgement file which contains information about which documents are relevant to each query. The retrieval process consists of either translating the Dutch queries into English, or the English documents into Dutch and returning documents from the collection that are relevant to the given queries. We then assess the quality of the retrieval process based on the human relevance judgement file and using the different IR measures. We also analyze the impact on the retrieval process by translating the queries with several translation tools available on the Web. Finally, we perform query expansion by pseudo-relevance feedback and analyze its effects on the cross-lingual retrieval performance.

\section{Experimental Setup}
\label{sec:setup}

For our experiment, we have at our disposal the CLEF 2003 bilingual task collection, which consists of 169,477 articles written in English from the Glasgow Herald 95 and LA Times 94 newspapers. We also have 60 topics written in Dutch (along with their corresponding English translations) and a corresponding human relevance judgement file. In order to index the collection and perform document retrieval, we use the \emph{Indri}\footnote{http://www.lemurproject.org} search engine, part of the \emph{Lemur Toolkit}, which is an open-source framework for language modeling and information retrieval software. We use the default settings for indexing / retrieving.

In order to answer the first research question, we need to translate the queries into English and the document collection into Dutch. We translate both the queries and the documents using a free online translation service based on Google Translate\footnote{http://www.onlinedoctranslator.com}. We tested three other translation tools and eventually chose Google for this task since it provides the best translation quality, as we will see in section \ref{sec:results}.
%The drawback of using this service is that it also translates the tags of the documents. This is an important issue since, for Indri to index the documents, the tags need to remain unmodified. To overcome this problem, we wrote a parser that replaces the tags in a translated document with the corresponding tags in its original version.

In order to answer the second research question, we translate the query files from Dutch to English with four different translation tools available on the Web: Bing Translator, Google Translate, MyGengo and Yahoo! Babel Fish. We use \emph{mteval} to assess the quality of translation against the English versions of the queries, which are provided to us, and \emph{trec\_eval} to assess the impact on the retrieval performance of these translations.

A balance must be found between recall and precision when expanding queries. For this we will perform a number of experiments to compare the increase in terms used for query expansion with the mean average precision. For the third research question, we perform pseudo-relevance feedback with Indri. We divide this task into two subtasks. First, we perform pseudo-relevance feedback with documents retrieved from the query translation task, translated by Google. We refer to this as "Query Expansion - Query Translation" (QEQT). In the second subtask, we consider the documents retrieved from the document translation task. We refer to this as "Query Expansion - Document Translation" (QEDT). For both subtasks, we take into account the top 16 documents for each query, which is the average number of relevant documents per query, and expand the queries with 5, 10, 25, 50, 75, 100, 500 terms.

% TODO
% Added it as part of the previous paragraph, please check if it still makes sense.
% A balance must be found between recall and precision when expanding queries. For this we did a number of experiments to compare the increase in number of terms used for query expansion with the mean average precision (we do not compare the increase in terms with recall because we expect it to increase at a similar rate - increasing the numer of informative terms, increases the recall).

%There are a couple of alternative approaches as well:
%
%\begin{itemize}
%	\item Using an online thesaurus to add synonyms for each query term
%	\item Using statistics from a large monolingual corpus (i.e. which words frequently appear together in sentences)
%\end{itemize}

\section{Results}
\label{sec:results}
\noindent In this section, we provide results for the experiments we made in order to answer the three research questions. We refer to \emph{Q} as the number of queries for which the system returns documents, \emph{Ret} as the number of documents returned by the system, \emph{Rel} as the number of relevant documents (corresponding to the given queries), \emph{Ret\_rel} as the number of relevant documents, out of {\bf Ret}, returned by the system, and \emph{MAP} as the Mean-Average-Precision for the corresponding run.

\subsection{RQ1: Query vs. Document Translation}
\noindent Below, in table \ref{table:1}, we provide the results of the experiment for research question RQ1:
\begin{table}[!h]
    \centering
	\begin{tabular}{|c|c|c|c|c|c|c|}
		\hline
		& Q & Ret & Rel & Rel\_ret & MAP \\
		\hline
		{\bf Query Translation} & 60 & 59,013 & 1006 & 909 &{\bf 0.3140} \\
		\hline
		{\bf Document Translation} & 59 & 56,533 & 1002 & 750 & 0.2992 \\
		\hline
	\end{tabular}
	\caption{Retrieval performance obtained by translating queries / documents}
	\label{table:1}
\end{table}

\begin{figure*}
	\centering
	\includegraphics[scale = 0.9]{Query-vs-Document-Translation.png}
	\caption{Precision-recall graph obtained by translating query / documents}
	\label{fig:2}
\end{figure*}

\subsection{RQ2: Translation Quality}
\noindent After running the experiment for the research question RQ2, {\bf mteval} returned the following BLEU scores for the translations of the queries:

\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|c|c|c|}
		\hline
		& Bing & Google & MyGengo & Yahoo \\
		\hline
		{\bf BLEU} &{\bf 0.3516} & 0.3535 & 0.3206 & 0.2103 \\
		\hline
	\end{tabular}
	\caption{Quality of translations provided by different tools}
	\label{table:2}
\end{table}

\noindent For the query translations provided by these systems, we performed document retrieval and obtained the following results:
\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|}
		\hline
		& Q & Ret & Rel & Rel\_ret & MAP \\
		\hline
		{\bf MyGengo} & 60 & 59,569 & 1006 & 920 &{\bf 0.3345} \\
		\hline
		{\bf Bing} & 60 & 59,569 & 1006 & 919 & 0.3302 \\
		\hline
		{\bf Google} & 60 & 59,013 & 1006 & 909 & 0.3140 \\
		\hline
		{\bf Yahoo!} & 60 & 60,000 & 1006 & 822 & 0.2649 \\
		\hline
	\end{tabular}
	\caption{Retrieval performance obtained by translating queries with different tools}
	\label{table:3}
\end{table}

\noindent Following the correlation test, we established that the Pearson correlation coefficient between the MAP values provided in table \ref{table:2} and the BLEU scores provided in table \ref{table:3} is 0.8934. This confirms our hypothesis H2 that there is indeed a significant correlation between the translation quality and the IR performance.

\begin{figure*}
	\centering
	\includegraphics[scale = 0.9]{Precision-Recall-Query-Translation2.png}
	\parbox{11cm}{\caption{Precision-recall graphs obtained by translating queries with different tools. We also show the precision-recall graph for the English monolingual retrieval baseline, which outperforms all the other retrieval tasks.}}
	\label{fig:3}
\end{figure*}

\subsection{RQ3: Query Expansion}
\noindent After running the experiment, we obtained the following results for the QEQT subtask:
\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|}
		\hline
		& Q & Ret & Rel & Rel\_ret & MAP \\
		\hline
		{\bf No Expansion} & 60 & 59,013 & 1006 & 909 & 0.3140 \\
		\hline
		{\bf 5 Terms} & 60 & 60,000 & 1006 & 909 & 0.3127 \\
		\hline
		{\bf 10 Terms} & 60 & 60,000 & 1006 & 911 & 0.3197 \\
		\hline
		{\bf 25 Terms} & 60 & 60,000 & 1006 & 915 & 0.3269 \\
		\hline
		{\bf 50 Terms} & 60 & 60,000 & 1006 & 922 & {\bf 0.3301} \\
		\hline
		{\bf 100 Terms} & 60 & 60,000 & 1006 & 922 & 0.3269 \\
		\hline
%		{\bf 250 Terms} & 60 & 60,000 & 1006 & 921 & 0.3277 \\
%		\hline
		{\bf 500 Terms} & 60 & 60,000 & 1006 & 921 & 0.3284 \\
		\hline
	\end{tabular}
	\caption{Retrieval performance obtained for the QEQT subtask}
	\label{table:4}
\end{table}

\noindent We obtained the following results for the QEDT subtask:
\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|}
		\hline
		& Q & Ret & Rel & Rel\_ret & MAP \\
		\hline
		{\bf No Expansion} & 59 & 56,533 & 1002 & 750 & 0.2992 \\
		\hline
		{\bf 5 Terms} & 59 & 59,000 & 1002 & 754 & 0.2996 \\
		\hline
		{\bf 10 Terms} & 59 & 59,000 & 1002 & 762 & 0.3206 \\
		\hline
		{\bf 25 Terms} & 59 & 59,000 & 1002 & 796 & 0.3188 \\
		\hline
   {\bf 50 Terms} & 59 & 59,000 & 1002 & 825 & 0.3182 \\
		\hline
		{\bf 100 Terms} & 59 & 59,000 & 1002 & 838 & {\bf 0.3260} \\
		\hline
%   {\bf 250 Terms} & 59 & 59,000 & 1002 & 831 & 0.3233 \\
%    \hline
   {\bf 500 Terms} & 59 & 59,000 & 1002 & 819 & 0.3220 \\
		\hline        
	\end{tabular}
	\caption{Retrieval performance obtained for the QEDT subtask}
	\label{table:5}
\end{table}

%\begin{figure}[!h]
%	\centering
%	\includegraphics [scale = 0.7]{Precision-Recall-Query-Translation.png}
%	\caption{Precision-recall graphs obtained by translating queries with different tools}
%	\label{fig:3}
%\end{figure}


\section{Discussion / Analysis}
\label{sec:analysis}

In this section, we provide an analysis on the results we obtined in section \ref{sec:results}.

\subsection{RQ1: Query vs. Document Translation}
Contrary to what we were expecting, query translation {\bf provides better retrieval performance} than document translation. The results from table \ref{table:1} show that 750 out of 1002 relevant documents are returned for the document translation task, but 909 out of 1006 for the query translation task, which results in an increase of 15.5\% in the number of relevant documents retrieved. Although query translation does not produce a significant improvement over the MAP, it does increase precision, as it can be seen in figure \ref{fig:2}. 

%This is already part of the second paragraph
%The experiments show that more documents were returned for translated queries than for translated documents.

To investigate the possible reasons, we computed the difference in Average Precision (AP) per topic.
\begin{figure*}
\centering
\includegraphics[scale = 0.8]{DifferenceAP.png}
\parbox{10cm}{\caption{Difference in Average Precision per topic between the document translation task (top) and the query translation task (bottom)}}
\label{figure:diff_ap_query_doc_trans}
\end{figure*}
We can see in figure \ref{figure:diff_ap_query_doc_trans} that even though the MAP values of the two tasks are similar, the performance on certain queries differs significantly. We investigated those queries individually and determined that the majority of those that received a lower AP for the document translation task contained compound words in Dutch. When translated, those compound words were being split into separate English words. For example, one such Dutch query was \emph{Noordzeeolie en het Milieu}, which was translated as \emph{North Sea Oil and the Environment}. In this case, relevant English documents were more easily retrieved as the English query contained the terms \emph{North}, \emph{Sea} and \emph{Oil}, and so did the relevant documents. 

It was much harder, though, to properly rebuild compound Dutch words contained in the Dutch query when translating documents from English. For example, when translating one of the relevant English documents for the aforementioned query into Dutch, the only syntagm in which the words \emph{North}, \emph{Sea} and \emph{Oil} were present, namely \emph{North Sea Oil fields}, was translated into \emph{Noordzee olievelden}. In this case, the corresponding Dutch translation of the relevant document received a lower score since the word \emph{Noordzeeolie} contained in the Dutch query was not present in the translation.

%In each of the used translation systems the assumption is that shorter sentences will receive a higher likelihood and are therefore more probable than longer sentences (short queries versus long sentences from the documents). Although we only care about the bag of words model, most of the translation is geared towards generating a proper sentence structure. Translating shorter sentences might be closer to using dictionary translation compared to properly translating longer sentences in the documents.
%
%
%
%Interestingly, when translating the documents into English and performing retrieval, no documents are returned for one query (Dienstweigering). Therefore, Indri ignores any documents related to this query, yielding a total of 1002 relevant documents instead of 1006, as is the case for query translation. The results from table \ref{table:1} show that Indri returns 750 out of the total of 1002 relevant documents when performing the document translation task, but 909 out of 1006 when performing the query translation task, which yields an increase of 15.5\% in the number of relevant documents retrieved. Although query translation does not produce a significant improvement over the MAP, it does increase precision, as it can be seen in figure \ref{fig:2}. 



%TODO
% Need some criticism for the first paragraph.

%More documents were returned for translated queries than for translated documents.
%Shorter sentences get a higher likelihood.
%We only care about the bag of words model, but most of the translation is geared towards generating the proper sentence structure (which we don't care about). Translating shorter sentences might be closer to simply using a dictionary translation than compared to the longer sentences in the documents.

\subsection{RQ2: Translation Quality}

As we hypothesized in H2 for our second research question, the quality of query translation {\bf does have a fairly significant effect on the performance of the retrieval process}. The results from table \ref{table:2} and table \ref{table:3} show that even though the rank by translation quality is not identical to the rank by IR performance, the two are nevertheless correlated to one other. The BLEU scores for Google, Bing and MyGengo are quite similar, as is their IR performance. Google has the highest BLEU score whilst MyGengo leads to best IR performance. \emph{mteval} rates Yahoo! Babel Fish significantly lower than the other three translators and it has much worse IR performance as well, which can be seen in table \ref{table:3} and figure \ref{fig:3}. This implies there is an underlying correlation between the translation quality and IR performance, a result that is also confirmed by their Pearson correlation coefficient, which is close to 1.

However, it is interesting that the two ranks are not exactly the same. One of the reasons is the bag-of-words model. The order of the words is important for the translation quality but an incorrect sequence of terms does not hurt information retrieval. 
%Besides, compound splitting might improve translation quality but hardly affect information retrieval. For example, Google translates "fast-food" in Dutch to "fast food" in English while MyGengo keeps "fast-food" as it is, but the IR performance of these two specific translations is exactly the same. 
Another reason could be that the evaluation tool is not 100 percent reliable. Given translators which have similar translation quality, it is not easy to tell which one is certainly better. For example, judged from our own perspective, we feel MyGengo translated the queries slightly better than Google, yet the automatic evaluation system gives Google a higher BLEU score. Another possible explanation is that certain translation tools may share more preferences regarding the choice of words with the human reference, resulting in a higher BLEU score. It must also be noted that we have translations from just one human reference, whereas it is common practice to have multiple references. This can affect the BLEU scores as different references may use synonyms, providing machine translations more possibilities of correctly translating words. For example, for the phrase "Soccer Riots in Dublin" by the reference, Google generates precisely that translation while MyGengo produces "Football Riots in Dublin", both of which are correct semantically. Google's translation will receive a higher BLEU score, yet MyGengo's translation results in a better IR performance on this query, which matches intuition as "football" is the more widely used term.

\subsection{RQ3: Query Expansion}

Our experiments have shown that query expansion {\bf increases retrieval performance, but not significantly}. When increasing the number of terms, the level of recall can increase at the cost of precision. Eventually all the relevant documents may be found if more and more informative terms are added to the query. But with more terms, more irrelevant documents will also be returned, assuming there is no cap on the number of documents retrieved.

When limiting the number of documents retrieved per query (1000 in our experiments, with on average 16 relevant documents per query), relevant documents within that limit can actually be displaced by irrelevant documents when you keep adding terms which are irrelevant and harmful for information retrieval. This negatively affects the precision and recall. 

%[THIS PARAGRAPH SHOULD PROBABLY GO SOMEWHERE ELSE] Therefore a balance must be found between recall and precision when expanding queries. For this we did a number of experiments to compare the increase in number of terms used for query expansion with the mean average precision (we do not compare the increase in terms with recall because we expect it to increase at a similar rate - increasing the numer of informative terms, increases the recall).


\section{Future Work}
\label{sec:future_work}

\subsection{Translation Quality}

For our research regarding the effect of translation quality, we only compared four online translation tools. Therefore, the next step is to evaluate a larger variety of translation tools, including offline ones. We plan to include Systran, Phrasal and a number of dictionary based translation tools. With a larger pool of results, we want to confirm whether there is still a correlation between the translation quality and the IR performance.   

\subsection{Dataset}

We restricted our research to Dutch-English cross-lingual information retrieval. Chen and Gey \cite{chen} used the same CLEF 2003 collection, using eight languages instead of just two, and with this bigger dataset concluded that Document Translation performs better than Query Translation. As mentioned earlier, this is in line with our first hypothesis H1, however the results turned out the other way round. Consequently, we will expand our dataset to also include the other six languages and repeat our experiments to see if we obtain the same conclusions as \cite{chen}. It would also be interesting to compare results between different CLEF editions. 

\section{Conclusions}
\label{sec:conclusions}

In this paper, we have analyzed several approaches that are used to perform Dutch-English cross-lingual information retrieval. Our experiments have shown that translating the set of queries leads to better retrieval performance than translating the entire document collection. However, proper care should be taken into consideration when choosing the translation tool, since this can significantly affect the results. We compared four online translation tools for query translation: Bing, Google Translate, MyGengo and Yahoo! Babelfish. According to the BLEU measure for translation quality, Google was ranked first, whereas MyGengo obtained the best information retrieval performance. Query expansion improves retrieval results, as expected, but not significantly. While these results give an insight on which approach should be chosen when performing Dutch-English CLIR, they are far from being optimal. Translation tools are constantly being improved and our results have shown that there is a strong correlation between the quality of translation and the IR performance. We expect the performance of CLIR to improve significantly in the upcoming years, as more accurate and reliable machine translation systems are being developed.

\begin{thebibliography}{100}
  \bibitem{adriani} M. Adriani, \emph{"English-Dutch CLIR Using Query Translation Techniques"}, Lecture Notes in Computer Science, 2002, Volume 2406/2002, 
 \bibitem{ballesteros} L. Ballesteros, W.B. Croft, \emph{"Phrasal Translation and Query
Expansion Techniques for Cross-Language Information Retrieval"}, ACM SIGIR97, 84-91, 1997
 \bibitem{chen} A. Chen, F.C. Gey, \emph{"Combining Query Translation and Document Translation in Cross-Language Retrieval"}, Working Notes for the CLEF, 2003
 \bibitem{dolamic} L. Dolamic, J. Savoy, \emph{" Retrieval Effectiveness of Machine Translated Queries"}, Journal of the American Society for Information Sciences and Technology, 2010
 \bibitem{grefensette} G. Grefensette, \emph{"(Ed) Cross-Language Information Retrieval"}, Kluwer, 1998
  \bibitem{rodgers} J. L. Rodgers, W. A. Nicewander, \emph{"Thirteen Ways to Look at the Correlation Coefficient"}, The American Statistician, 42(1):59-66, February 1988.Working Notes for the CLEF, 2003   
  \bibitem{papineni}, K. Papineni, S. Roukos, T. Ward, and J. W. Zhu, (2002) \emph{BLEU: A Method for Automatic Evaluation of Machine Translation} in ACL-2002: 40th Annual Meeting of the Association for Computational Linguistics pp. 311-318
  \bibitem{rogati} M. Rogati, Y. Yang, \emph{"CONTROL: CLEF-2003 with Open"}, Transparent Resources Off-Line, CLEF, 2003
\end{thebibliography}

\end{document}