function newImage = convertColorSpace(inputImage, newSpace)

height = size(inputImage, 1);
width = size(inputImage, 2);

inputImage = im2double(inputImage);

R = inputImage(:, :, 1);
G = inputImage(:, :, 2);
B = inputImage(:, :, 3);

newImage = zeros(height, width, 3);

switch newSpace
    case 'rgb'
        RGB = R + G + B;
        r = R./(RGB + eps);
        g = G./(RGB + eps);
        b = B./(RGB + eps);
        
        newImage(:, :, 1) = r;
        newImage(:, :, 2) = g;
        newImage(:, :, 3) = b;
        
    case 'XYZ'
        X = 0.607 * R + 0.174 * G + 0.200 * B;
        Y = 0.299 * R + 0.587 * G + 0.114 * B;
        Z = 0.000 * R + 0.066 * G + 1.116 * B;

        newImage(:, :, 1) = X;
        newImage(:, :, 2) = Y;
        newImage(:, :, 3) = Z;
        
    case 'xyz'
        X = 0.607 * R + 0.174 * G + 0.200 * B;
        Y = 0.299 * R + 0.587 * G + 0.114 * B;
        Z = 0.000 * R + 0.066 * G + 1.116 * B;
        
        x = X./(X + Y + Z + eps);
        y = Y./(X + Y + Z + eps);
        z = Z./(X + Y + Z + eps);

        newImage(:, :, 1) = x;
        newImage(:, :, 2) = y;
        newImage(:, :, 3) = z;
        
    case 'YIQ'
        Y = 0.299 * R + 0.587 * G + 0.114 * B;
        I = 0.596 * R - 0.275 * G - 0.321 * B;
        Q = 0.212 * R - 0.523 * G + 0.311 * B;

        newImage(:, :, 1) = Y;
        newImage(:, :, 2) = I;
        newImage(:, :, 3) = Q;
        
    case 'I1I2I3'
        I1 = (R + G + B) / 3;
        I2 = (R - B) / 2;
        I3 = (2*G - R - B) / 2;
        
        newImage(:, :, 1) = I1;
        newImage(:, :, 2) = I2;
        newImage(:, :, 3) = I3;
        
    case 'HSI'       
        H = atan((sqrt(3)*(G - B))./(R - G + R - B + eps));
        S = 1 - 3 * min(min(R, G), B);
        I = (R + G + B) / 3;
        
        newImage(:, :, 1) = H;
        newImage(:, :, 2) = S;
        newImage(:, :, 3) = I;
        
    case 'Opponent'       
        O1 = (R - G) / sqrt(2);
        O2 = (R + G - 2*B) / sqrt(6);
        O3 = (R + G + B) / sqrt(3);
        
        newImage(:, :, 1) = O1;
        newImage(:, :, 2) = O2;
        newImage(:, :, 3) = O3;
        
    case 'HSV'
        M = max(max(R, G), B);
        m = min(min(R, G), B);
        
        C = M - m;
        V = M;
        
        rows = size(M, 1);
        cols = size(M, 2);
        
        H = zeros(rows, cols);
        S = zeros(rows, cols);
        
        for i = 1:rows
            for j = 1:cols
                if C(i, j) == 0
                    S(i, j) = 0;
                else
                    S(i, j) = C(i, j) / V(i, j);
                end
                
                if C(i, j) == 0
                    H(i, j) = 0;
                else
                    switch M(i, j)
                        
                        case R(i, j)
                            H(i, j) = mod((G(i, j) - B(i, j)) / C(i, j), 6);
                            
                        case M(i, j)
                            H(i, j) = 2 + (B(i, j) - R(i, j)) / C(i, j);
                            
                        otherwise
                            H(i, j) = 4 + (R(i, j) - G(i, j)) / C(i, j);
                            
                    end
                end
                
                H(i, j) = H(i, j) / 6;
                
            end
        end
        
        newImage(:, :, 1) = H;
        newImage(:, :, 2) = S;
        newImage(:, :, 3) = V;
        
    case 'c1c2c3'
        c1 = atan(R./(max(G, B) + eps));
        c2 = atan(G./(max(R, B) + eps));
        c3 = atan(B./(max(R, G) + eps));
        
        newImage(:, :, 1) = c1;
        newImage(:, :, 2) = c2;
        newImage(:, :, 3) = c3;
        
    otherwise
        error('Unknown argument specified.');
end