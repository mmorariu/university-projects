function objectTracking()

clc;
clear all;
close all;

framesDir = [pwd, '\..\Data\part_1\'];
framesFiles = dir(framesDir);
nFiles = length(framesFiles);

refImg = imread('player1_2.png');
initialImg = imread([framesDir, framesFiles(3).name]);
initialHeight = size(initialImg, 1);
initialWidth = size(initialImg, 2);

figure('Name', 'Brute-Force Object Tracking', 'NumberTitle', 'off');
set(gcf, 'MenuBar', 'none', 'Resize', 'off');

image(initialImg);
text(20, 30, ['{\bf {\color{white}Getting initial position of object... }}'], 'BackgroundColor', 'k');
set(gca, 'Visible', 'off');
drawnow;

refHist = createHistogram(refImg, 0, 0);

varX = 8;
varY = 8;

refWidth = size(refImg, 2);
refHeight = size(refImg, 1);

[refX, refY] = getInitialPosition(refImg, initialImg);

for i = 4:nFiles
    currentImg = imread([framesDir, framesFiles(i).name]);
    minDist = Inf;
    
    for w = max(refX - varX, 1):3:min(refX + varX, 720)
        for h = max(refY - varY, 1):3:min(refY + varY, 576)
            candImg = currentImg(h:h + refHeight, w:w + refWidth, :);
            candHist = createHistogram(candImg, 0, 0);
            histDist = distBhattacharyya(refHist, candHist);
            % histDist = sum(sum((refHist - candHist).^2));
            
            if histDist < minDist
                candX = w;
                candY = h;
                minDist = histDist;
            end
        end
    end
    
    refX = candX;
    refY = candY;
    
    image(currentImg);
    rectangle('Position', [candX, candY, refWidth, refHeight], 'EdgeColor', 'w');
    text(20, 30, ['{\bf {\color{white}Processing...  [Frame ', num2str(i - 2), '/', num2str(nFiles - 2), '] }}'], 'BackgroundColor', 'k');
    text(20, 60, ['{\bf {\color{white}X = ', num2str(candX), '  Y = ', num2str(candY), '  }}'], 'BackgroundColor', 'k');
    set(gca, 'Visible', 'off');
    drawnow;
    
end

image(currentImg);
text(20, 30, ['{\bf {\color{white}Done. }}'], 'BackgroundColor', 'k');
set(gca, 'Visible', 'off');
drawnow;

end

function [X, Y] = getInitialPosition(refImage, inputImage)

refHeight = size(refImage, 1);
refWidth = size(refImage, 2);
inputHeight = size(inputImage, 1);
inputWidth = size(inputImage, 2);

for i = 1:inputHeight - refHeight
    for j = 1:inputWidth - refWidth
        window = inputImage(i:i + refHeight - 1, j:j + refWidth - 1, :);
        
        if isequal(window, refImage)
            X = j;
            Y = i;
            return;
        end
    end
end

end

function d = distBhattacharyya(hist1, hist2)
hist1 = hist1./sum(sum(hist1));
hist2 = hist2./sum(sum(hist2));

coeff = sum(sum(sqrt(hist1).*sqrt(hist2)));
d = sqrt(1 - coeff);
end
