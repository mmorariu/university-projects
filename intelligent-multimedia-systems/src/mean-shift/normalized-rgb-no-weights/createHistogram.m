function hist = createHistogram(inputImage, nBins, center, width, height)

if (~center)
    var_y = floor(height / 2);
    var_x = floor(width / 2);
    inputImage = inputImage(max(center(2) - var_y, 1):min(center(2) + var_y, height), max(center(1) - var_x, 1):min(center(1) + var_x, width), :);
else
    height = size(inputImage, 1);
    width = size(inputImage, 2);
end

step = 1 / nBins;
hist = zeros(nBins, nBins);

r = inputImage(:, :, 1);
g = inputImage(:, :, 2);

[cols, rows] = meshgrid(linspace(-1, 1, width), linspace(-1, 1, height));
distFromOrigin = sqrt(cols.^2 + rows.^2);
weights = EpanechnikovKernel(distFromOrigin.^2, pi, 2);

for y = 1:height
    for x = 1:width
        index_bin_r = floor(double(r(y, x)) / step) + 1;
        index_bin_g = floor(double(g(y, x)) / step) + 1;
        hist(index_bin_g, index_bin_r) = hist(index_bin_g, index_bin_r) + weights(y, x);
    end
end

hist = hist / sum(sum(hist));

end

function e = EpanechnikovKernel(dist, Cd, d)

height = size(dist, 1);
width = size(dist, 2);
e = zeros(height, width);

for y = 1:height
    for x = 1:width
        if dist(y, x) <= 1
            e(y, x) = 0.5 * 1 / Cd * (d + 2) * (1 - dist(y, x));
        end
    end
end

end