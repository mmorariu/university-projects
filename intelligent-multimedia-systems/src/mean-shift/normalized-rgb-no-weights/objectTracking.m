function [distance, centerVec] = objectTracking(imageLocation, nBins)

close all;
% outputVideo = VideoWriter('football_occ_nRGB_16.avi');
% open(outputVideo);

framesDir = [pwd, '\..\..\Data\World Cup\'];
framesFiles = dir(framesDir);
nFiles = length(framesFiles);
figure('Name', 'Mean-Shift Object Tracking', 'NumberTitle', 'off');

initialImg = imread([framesDir, framesFiles(3).name]);
initialHeight = size(initialImg, 1);
initialWidth = size(initialImg, 2);
imshow(initialImg);

if isempty(imageLocation)
    text(20, 30, '{\bf {\color{white}Waiting for the initial position of object... }}', 'BackgroundColor', 'k');
    set(gca, 'Visible', 'off');
    drawnow;
    
    coords = getrect;
    Y0 = round(coords(1:2))';
    refWidth = round(coords(3));
    refHeight = round(coords(4));
else
    text(20, 30, '{\bf {\color{white}Getting the initial position of object... }}', 'BackgroundColor', 'k');
    set(gca, 'Visible', 'off');
    drawnow;
    
    objImage = imread(imageLocation);
    [Y0(1), Y0(2)] = getInitialPosition(objImage, initialImg);
    refHeight = size(objImage, 1);
    refWidth = size(objImage, 2);
end

if mod(refHeight, 2) == 0
    refHeight = refHeight - 1;
end
if mod(refWidth, 2) == 0
    refWidth = refWidth - 1 ;
end

Y0 = [Y0(1) + floor(refWidth / 2) + 1; Y0(2) + floor(refHeight / 2) + 1];
refImg = initialImg(Y0(2) - floor(refHeight / 2):Y0(2) + floor(refHeight / 2), Y0(1) - floor(refWidth / 2):Y0(1) + floor(refWidth / 2), :);

bufferRef = im2double(refImg);
bufferRef = convertColorSpace(bufferRef, 'rgb');
refHist = createHistogram(bufferRef, nBins, [], [], []);
var_X = floor(refWidth / 2);
var_Y = floor(refHeight / 2);

distance = zeros(nFiles - 3, 1);
centerVec = zeros(nFiles - 2, 2);
centerVec(1, :) = Y0;
variation = [10; 10];
step = 1 / nBins;
% path = zeros(nFiles - 2, 2);
% path(1, :) = Y0(:);
time = 0;

for i = 4:nFiles
    clockStart = tic;
    candImg = imread([framesDir, framesFiles(i).name]);
    bufferCand = im2double(candImg);
    bufferCand = convertColorSpace(bufferCand, 'rgb');
    r = bufferCand(:, :, 1);
    g = bufferCand(:, :, 2);
    candHist = createHistogram(bufferCand, nBins, Y0, refWidth, refHeight);
    p0 = histSimilarity(refHist, candHist);
    
    Y1 = [0; 0];
    W = 0;
    
    for y = max(Y0(2) - var_Y - variation(2), 1):min(Y0(2) + var_Y + variation(2), initialHeight)
        for x = max(Y0(1) - var_X - variation(1), 1):min(Y0(1) + var_X + variation(1), initialWidth)
            index_bin_r = floor(double(r(y, x)) / step) + 1;
            index_bin_g = floor(double(g(y, x)) / step) + 1;
            w_i = sqrt(refHist(index_bin_g, index_bin_r) / candHist(index_bin_g, index_bin_r));
            
            if isnan(w_i) || isinf(w_i)
                w_i = 0;
            end
            
            Y1 = Y1 + [x; y] * w_i;
            W = W + w_i;
        end
    end
    
    Y1 = floor(Y1 / W);
    candHist = createHistogram(bufferCand, nBins, Y1, refWidth, refHeight);
    p1 = histSimilarity(refHist, candHist);
    
    while p1 < p0
        Y1 = floor(Y0 + Y1) / 2;
        candHist = createHistogram(bufferCand, nBins, Y1, refWidth, refHeight);
        p1 = histSimilarity(refHist, candHist);
        
        if norm(Y1 - Y0) < eps
            break;
        end
    end
    
    distance(i - 3) = sqrt(1 - p1);
    centerVec(i - 2, :) = Y1;    
    
    Y0 = Y1;
    
%     path(i - 2, :) = Y1(:);
%     hold on;
    imshow(candImg);
    
    if Y1(1) >= 1 && Y1(1) <= initialWidth && ...
            Y1(2) >= 1 && Y1(2) <= initialHeight
        rectangle('Position', [Y1(1) - var_X, Y1(2) - var_Y, refWidth, refHeight], 'EdgeColor', 'w');
    end
    
    text(20, 30, ['{\bf {\color{white}Processing...  [Frame ', num2str(i - 2), '/', num2str(nFiles - 2), '] }}'], 'BackgroundColor', 'k');
    text(20, 60, ['{\bf {\color{white}X = ', num2str(Y1(1)), '  Y = ', num2str(Y1(2)), '  }}'], 'BackgroundColor', 'k');
%     plot(path(:, 1), path(:, 2), '.', 'LineWidth', 1, 'Color', 'r');
    set(gca, 'Visible', 'off');
    drawnow;
%     currentFrame = getframe(gca);
%     writeVideo(outputVideo, currentFrame);

    time = time + toc(clockStart);
end

imshow(candImg);
text(20, 30, '{\bf {\color{white}Done. }}', 'BackgroundColor', 'k');
set(gca, 'Visible', 'off');
drawnow;

% save('football_occ_nRGB_16.mat', 'distance', 'centerVec');
% close(outputVideo);
time / (nFiles - 3)

end

function coeff = histSimilarity(hist1, hist2)

coeff = sum(sum(sqrt(hist1).*sqrt(hist2)));

end

function [X, Y] = getInitialPosition(objImage, initialImg)

objHeight = size(objImage, 1);
objWidth = size(objImage, 2);
initialHeight = size(initialImg, 1);
initialWidth = size(initialImg, 2);

for y = 1:initialHeight - objHeight + 1
    for x = 1:initialWidth - objWidth + 1
        patch = initialImg(y:y + objHeight - 1, x:x + objWidth - 1, :);
        
        if isequal(patch, objImage)
            X = x;
            Y = y;
            return;
        end
    end
end

end