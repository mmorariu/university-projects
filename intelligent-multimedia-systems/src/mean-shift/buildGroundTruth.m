function buildGroundTruth(height, width)

clc;
close all;
framesDir = [pwd, '\Data\World Cup\'];
framesFiles = dir(framesDir);
nFiles = length(framesFiles);
figure('Name', 'Mean-Shift Ground Truth Builder', 'NumberTitle', 'off');
centerVec = zeros(nFiles - 2, 2);

for i = 3:nFiles
    currImage = imread([framesDir, framesFiles(i).name]);
    imshow(currImage);
    doContinue = false;
    
    while ~doContinue  
        center = ginput(1);
        hold on;
        rectangle('Position', [center(1) - floor(width / 2), center(2) - floor(height / 2), width, height], 'EdgeColor', 'w');
        ch = input(['Frame ' ,num2str(i - 2), ' - Is it ok to continue? (Y/N) '], 's');
        
        if strcmp(ch, 'y') || strcmp(ch, 'Y')
            doContinue = true;
        else
            hold off;
            imshow(currImage);
            drawnow;
        end
    end
    
    centerVec(i - 2, :) = center;
end

save('groundTruth.mat', 'height', 'width', 'centerVec');
close all;

end