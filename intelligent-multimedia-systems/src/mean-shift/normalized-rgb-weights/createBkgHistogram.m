function hist = createBkgHistogram(inputImage, nBins)

height = size(inputImage, 1);
width = size(inputImage, 2);

step = 1 / nBins;
hist = zeros(nBins, nBins);
        
r = inputImage(:, :, 1);
g = inputImage(:, :, 2);
        
for y = 1:height
    for x = 1:width
        index_bin_r = floor(double(r(y, x)) / step) + 1;
        index_bin_g = floor(double(g(y, x)) / step) + 1;
        hist(index_bin_g, index_bin_r) = hist(index_bin_g, index_bin_r) + 1;
    end
end

hist = hist / sum(sum(hist));

end