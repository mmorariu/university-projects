function hist = createObjHistogram(inputImage, backWeights, nBins, center, width, height)

if (~center)
    var_y = floor(height / 2);
    var_x = floor(width / 2);
    inputImage = inputImage(max(center(2) - var_y, 1):min(center(2) + var_y, height), max(center(1) - var_x, 1):min(center(1) + var_x, width), :);
else
    height = size(inputImage, 1);
    width = size(inputImage, 2);
end

step = 256 / nBins;
hist = zeros(nBins, nBins, nBins);

R = inputImage(:, :, 1);
G = inputImage(:, :, 2);
B = inputImage(:, :, 3);

[cols, rows] = meshgrid(linspace(-1, 1, width), linspace(-1, 1, height));
distFromOrigin = sqrt(cols.^2 + rows.^2);
weights = EpanechnikovKernel(distFromOrigin.^2, pi, 2);

for y = 1:height
    for x = 1:width
        index_bin_R = floor(double(R(y, x)) / step) + 1;
        index_bin_G = floor(double(G(y, x)) / step) + 1;
        index_bin_B = floor(double(B(y, x)) / step) + 1;
        hist(index_bin_G, index_bin_R, index_bin_B) = hist(index_bin_G, index_bin_R, index_bin_B) + backWeights(index_bin_G, index_bin_R, index_bin_B) * weights(y, x);
    end
end

hist = hist / sum(sum(sum(hist)));

end

function e = EpanechnikovKernel(dist, Cd, d)

height = size(dist, 1);
width = size(dist, 2);
e = zeros(height, width);

for y = 1:height
    for x = 1:width
        if dist(y, x) <= 1
            e(y, x) = 0.5 * 1 / Cd * (d + 2) * (1 - dist(y, x));
        end
    end
end

end