function hist = createBkgHistogram(inputImage, nBins)

height = size(inputImage, 1);
width = size(inputImage, 2);

step = 256 / nBins;
hist = zeros(nBins, nBins, nBins);
        
R = inputImage(:, :, 1);
G = inputImage(:, :, 2);
B = inputImage(:, :, 3);
        
for y = 1:height
    for x = 1:width
        index_bin_R = floor(double(R(y, x)) / step) + 1;
        index_bin_G = floor(double(G(y, x)) / step) + 1;
        index_bin_B = floor(double(B(y, x)) / step) + 1;
        hist(index_bin_G, index_bin_R, index_bin_B) = hist(index_bin_G, index_bin_R, index_bin_B) + 1;
    end
end

hist = hist / sum(sum(sum(hist)));

end