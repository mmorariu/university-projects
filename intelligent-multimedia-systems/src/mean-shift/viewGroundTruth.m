load groundTruth.mat;

clc;
close all;
framesDir = [pwd, '\Data\World Cup\'];
framesFiles = dir(framesDir);
nFiles = length(framesFiles);
figure('Name', 'Mean-Shift Ground Truth Viewer', 'NumberTitle', 'off');
var_X = floor(width / 2);
var_Y = floor(height / 2);

for i = 1:nFiles - 2
    currImage = imread([framesDir, framesFiles(i + 2).name]);
    imshow(currImage);
    rectangle('Position', [centerVec(i, 1) - var_X, centerVec(i, 2) - var_Y, width, height], 'EdgeColor', 'w');
    drawnow;
end

close all;