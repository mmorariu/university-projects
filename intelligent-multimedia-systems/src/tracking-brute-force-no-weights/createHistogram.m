function hist = createHistogram(inputImage, plot, normalize)
inputImage = im2double(inputImage);
inputImage = convertColorSpace(inputImage, 'rgb');

height = size(inputImage, 1);
width = size(inputImage, 2);

hist = zeros(16, 16);
step = 1 / 16;

r = inputImage(:, :, 1);
g = inputImage(:, :, 2);

for i = 1:16
    for j = 1:16
        hist(i, j) = sum(sum(...
                            (r >= (i - 1)*step).*(r < i*step).*...
                            (g >= (j - 1)*step).*(g < j*step)...
                        )...
                     );
    end
end

if plot == 1
    figure;
    
    [r, g] = meshgrid(1:16, 1:16);
    surf(r, g, hist);
    grid on;
end

if normalize == 1
    hist = hist / sum(sum(hist));
end
end