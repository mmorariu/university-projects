clc;
close all;
clear all;

obj = imread('nemo_cropped.jpg');
img = imread('nemo4.jpg');
img_original = img;

histImg = createHistogram(img, 0, 0);
histObj = createHistogram(obj, 0, 0);
obj = convertColorSpace(obj, 'rgb');
img = convertColorSpace(img, 'rgb');
histBack = histObj./(histImg + eps);

height = size(img, 1);
width = size(img, 2);

step = 1 / 16;
probObj = zeros(height, width);

index_r = floor(img(:, :, 1) / step) + 1;
index_g = floor(img(:, :, 2) / step) + 1;

for i = 1:height
    for j = 1:width
        probObj(i, j) = histBack(index_r(i, j), index_g(i, j));
    end
end

% M = max(max(probObj));
% [I1, I2] = find(probObj == M);
% 
% imshow(img_original);
% plot(I1, I2, 'o', 'Color', 'r', 'MarkerFaceColor', 'r');
% axis([1 width 1 height]);
% hold on;
figure;
imshow(probObj);
threshold = 0.1;

% figure(1);
% imshow(img_original);
% hold on;
% plot(I1, I2, 'Color', 'r', 'LineWidth', 2);
