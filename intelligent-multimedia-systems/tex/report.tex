\documentclass [11pt] {article}

\usepackage {algorithm}
\usepackage {algpseudocode}
\usepackage {amsmath}
\usepackage {anysize}
\usepackage [justification = centering] {caption}
\usepackage {fourier}
\usepackage {graphicx}
\usepackage {html, makeidx}
\usepackage {subfigure}

\newcommand {\dsum}{\displaystyle\sum}

\title {Mean-Shift Tracking}
\author {Andrei Oghin\u{a} (6424171) \\
				 Mihai Adrian Morariu (10232710)
				}

\begin {document}

\maketitle

\

\begin {abstract}

\

In this paper, we present the Mean-Shift tracking algorithm that we use to track an object throughout a given sequence of video frames. In the first and second sections, we provide a description and an outline of the algorithm. In the third section, we describe the setup of the experiments that we carried out for our implementation. In the fourth section, we provide the results of these experiments. Finally, we evaluate the results against human judgments and suggest a way of improving the program.
\end {abstract} 

\

\section {Introduction}

\

The \emph {Mean-Shift} is an algorithm used for tracking an object throughout a given sequence of video frames. This algorithm represents an improvement of the classical \emph {Brute-Force Object Tracking}, which searches, given the position, the size and the color histogram of a \emph {target image} in a certain frame, the neighborhood around this position in the subsequent frame in the attempt to find the most "similar" image to the target. The advantage of using Mean-Shift over Brute-Force is that finding the location of the tracked object in the subsequent frames is done by maximizing a \emph {similarity function} (or minimizing a \emph {distance function}), also known as \emph {the Bhattacharyya coefficient}, which yields the new location of the target without having to perform an exhaustive search.

\

The algorithm, just as Bruce Force, relies on two assumptions regarding the video frames. The first one is that in each video frame there is a significant difference in terms of colors between the tracked object and the background. It can sometimes be difficult even for humans to track a target object if the background is of the same color as the target. The second assumption is that the location of the object does not vary largely between two consecutive frames. Therefore, searching for the object's location in a new frame reduces to considering the last known position of the object and searching for the most similar image around that location in the new frame.

\newpage

\section {Mean-Shift Algorithm}

\

In this section, we provide a description of the Mean-Shift Tracking algorithm. We discuss what the feature space consists of, why the kernel-based histograms are used and how they are built, and how the location of the tracked object is determined in a new frame. Finally, we provide an outline of the algorithm.

\

\subsection {Feature space}

\

The \emph {target model} is represented as a normalized color histogram $\left( q_u \right)_{u = \overline{1, m}}$, where $m$ denotes the number of bins and $u$ denotes the index of each bin of the histogram. \cite {comaniciu} The histogram can be seen as a matrix in which entries store information about the amount of colors present in the image and their corresponding indexes denote ranges of colors from each channel. The number of dimensions of the matrix is, therefore, dependent on the color space chosen to represent the object. Images which use the RGB color space have a corresponding 3-D histogram matrix, whereas images using the normalized RGB (nRGB) color space have a corresponding 2-D histogram matrix.

\

The \emph {candidate model} is also represented as a normalized color histogram $\left( p_u(y) \right)_{u = \overline{1, m}}$, where $y$ denotes the center of the image having the corresponding histogram $\left( p_u \right)_{u = \overline{1, m}}$. We introduce \emph {the Bhattacharyya coefficient}, a measure which reflects the "similarity" between two color histograms, given by the formula
\begin {align}
	\label {eq:rho}
	\rho \left( y \right) \equiv \rho \left( p(y), q \right) = \dsum_{u = 1}^m \sqrt{p_u(y) q_u}
\end {align}

\

Two identical normalized histograms will have the Bhattacharyya coefficient equal to one, whereas the more different they are, the more this value tends to zero. The same effect can be obtained by introducing a metric based on the previous coefficient which measures the "distance" between two color histograms and is defined as
\begin {align}
	\label {eq:dist}
	d \left( y \right) = \sqrt{1 - \rho \left( p(y), q \right)}
\end {align}
Maximizing the similarity function of the two histograms is, therefore, equivalent to minimizing the distance function.


\

\subsection {Kernel-Weighted Histograms}

\

The target image almost always provides color information regarding the object that is not relevant for our tracker. For example, when tracking a player in a sequence of frames extracted from a football video, the target image also contains color information describing the field. This is because the player cannot be "delimited" perfectly in the image and therefore green pixels describing grass are also included in the histogram. These pixels are not relevant for describing the player, so we need a way to make distinction between them and the ones which are relevant to us.

\

The way to achieve this is to build a \emph {kernel-weighted histogram}. We assume that the object is centered in the target image and we assign larger weights to pixels that are close to the center and smaller weights to pixels which are not. This is done by first creating a distance matrix (of the same size as the target image) whose entries denote distances from the center of the matrix. We then apply the Epanechnikov kernel profile, given by the formula
\begin {align}
	k(x) = 
	\begin {cases}
		\dfrac{1}{2} {C_d}^{-1} \left( d + 2 \right) \left( 1 - x \right),\ \ \ \mbox{if}\ \|x\| \leq 1 \\
		0,\ \ \ \mbox{otherwise}
	\end {cases}
\end {align}
for $C_d = \pi$ and $d = 2$, on the elements of this matrix. This creates a mask of the same size as the target image, which assigns large weights for pixels close to the center and small weights for pixels close to the edges. \cite {comaniciu}

\begin {figure}
	\centering
	\includegraphics [scale = .4] {Epanechnikov.png}
	\caption {Epanechnikov kernel}
\end {figure}

\

\subsection {Determining target location}

\

The algorithm checks, for each frame, the last known position of the object, denoted by $y_0$. The position of the object in the first frame is considered to be known (given by the user). In the new frame, the algorithm "builds" a window around that position, which is the domain of the similarity function. By determining the $y$ which maximizes the similarity function, we obtain the new location (center) of the most likely histogram to be
\begin {align}
	\label {eq:y1}
	y_1 = \dfrac{\dsum_{i = 1}^n x_i w_i}{\dsum_{i = 1}^n w_i}
\end {align}
where $x_i$ denotes the location of each pixel from the window, and $w_i$ is given by the formula
\begin {align}
	\label {eq:wi}
	w_i = \dsum_{u = 1}^m \delta \left[ b(x_i) - u \right] \sqrt{\dfrac{q_u}{p_u(y_0)}}
\end {align}
However, practical experiments showed that the Bhattacharyya coefficient computed at the location given by \eqref{eq:y1} was, in most of the cases, larger than the coefficient corresponding to $y_0$. In less than 0.1\% of the cases, an optimization was employed to increase the value of the approximated coefficient. This optimization involves reconsidering $y_1$ to be situated at half the distance between the $y_0$ and $y_1$ and checking whether the Bhattacharyya coefficient is larger for that new value. \cite {comaniciu2}

\

\subsection {Outline of the algorithm}

\

\begin {algorithm}
	\caption {Mean-Shift Tracking Algorithm}
	\begin {algorithmic}
		\State Given the distribution $\left( q_u \right)_{u = \overline{1, m}}$ of the target model and its location $y_0$ in the first frame:
		\newline
		\For {each frame in the sequence}
			\State 1. Initialize location of the target in the current frame with $y_0$
			\State 2. Compute $\rho \left( p(y_0), q \right)$ according to formula \eqref{eq:rho}			
			\State 3. Create a window around location $y_0$ in the current frame
			\newline
			\State Compute $w_i$ according to formula \eqref{eq:wi}			
			\State 4. Compute $y_1$ according to formula \eqref{eq:y1}			
			\State 5. Compute $p_u(y_1)$ and evaluate
				\begin {align}
					\nonumber
					\rho \left( p(y_1), q \right) = \dsum_{u = 1}^m \sqrt{p_u(y_1) q_u}
				\end {align}
			\While {$\rho \left( p(y_1), q \right) < \rho \left( p(y_0), q \right)$}
				\State $y_1 \leftarrow \dfrac{1}{2} \left( y_0 + y_1 \right)$
				\newline
				\If {$\| y_1 - y_0 \| < \epsilon$}
					\State STOP
				\EndIf
				\newline
			\EndWhile
			\newline
			\State $y_0 \leftarrow y_1$
		\EndFor
	\end {algorithmic}
\end {algorithm}

\

\section {Experiments}

\

We use two videos to test our tracker. The first video is an excerpt from a football match consisting of 201 frames of 720x576 pixels. We check to see that the program performs well in tracking a football player under the simplest environmental setting - there is little variation in light intensity and the target is not exposed to any type of occlusion. We call this test - the \emph {Simple Test}. In the same video, we select another player to be tracked and we check that the algorithm performs well in case of the player being occluded. We call this test - the \emph {Occlusion Test}.

\

In the second video, which is a sequence of 684 frames of 320x240 pixels, we track the face of a person and test that the program performs well in case of relatively large variations in light intensity. We call this test - the \emph {Light Intensity Test}. Each video is tested on two color spaces - RGB and nRGB, for histograms comprising 16, 32 and 64 bins for each color channel. All three tests are performed on an Intel 2.0 GHz machine running Windows 7, and the code is written in MATLAB.

\

We test the performance of our tracker by first building the ground-truth for the tracked object which consists of the sequence of centers $y_1$ of the most likely candidate histograms. Each frame from the video is, therefore, annotated by manually specifying the position of the tracked object in that frame. For this purpose, we have built a separate script file which we use to annotate the frames with.

\

The performance of the tracker is determined by computing the average of the displacements between the centers $y_1$ determined by the algorithm and the manually-specified centers $y_1^*$. A displacement between a center $y_1 = \left( x, y \right)$ determined by the tracker and a manually annotated center $y_1^* = \left( x^*, y^* \right)$ is given by the formula
\begin {align}
	d \left( y_1, y_1^* \right) = \sqrt{ \left( x - x^* \right)^2 + \left( y - y^* \right)^2 }
\end {align}
The closer the average is to zero, the closer to our expectations the tracker performs. Finally, we evaluate the average running time of the algorithm per frame. Note that the average running times reported in section \eqref {sec:results} are computed while {\bf tracking the object and generating the output video file}.

\

\section {Results and Analysis}
\label {sec:results}

\

In this section, we present the performance of the tracker for each of the three tests that we perform: the {\bf Simple Test}, the {\bf Occlusion Test} and the {\bf Light Intensity Test}. For all three tests, we search the new center $y_1$ of the target object in a region of the size of the target object plus 10 pixels to the left, right, top and bottom, centered in $y_0$, the previous known position. Thus, for a target image of 60x40 pixels, we search for the new center in a region consisting of 80x60 pixels. We choose this value since it is known that the object does not shift too much between two consecutive frames. 

\

\subsection {Simple Test}

\

The first video we use for testing is the excerpt from the football match (figure \eqref {fig:1}). As mentioned earlier, there is little variation in light intensity in this video. Therefore, using the nRGB over the RGB color space should not improve the performance of the tracker too much, as we expect it to occur in the second video, in which there are variations in light intensity.

\begin {figure}[!h]
	\centering
	\includegraphics [scale = .3] {Frame0085.png}
	\caption {First frame of the football match video that is used to perform the Simple Test and the Occlusion Test}
	\label {fig:1}	
\end {figure}

\

We choose to track the Dutch player from the top-left corner of the image since he does not get occluded throughout the sequence of frames. The tracker produces the following results for the {\bf nRGB color space}:

\begin {center}
	\begin {tabular}{|c|c|c|}
		\hline
		Bins & Average of the displacements & Average running time / frame \\ \hline
		16 & 1.9276 & 0.4354 s \\ \hline
		32 & 1.8890 & 0.4367 s \\ \hline
		64 & 2.0256 & 0.4367 s \\
		\hline
	\end {tabular}
\end {center}
and the following results for the {\bf RGB color space}:

\begin {center}
	\begin {tabular}{|c|c|c|}
		\hline
		Bins & Average of the displacements & Average running time / frame \\ \hline
		16 & 2.0493 & 0.3678 s \\ \hline
		32 & 2.4129 & 0.3731 s \\ \hline
		64 & 3.5920 & 0.4007 s \\
		\hline
	\end {tabular}
\end {center}

\

We notice that, for the {\bf nRGB color space}, the center $y_1$ produced by the Mean-Shift Tracker is displaced (on average) by a distance of approximately 2 pixels from the center $y_1^*$ determined by annotating the frames. This means that the center determined by the tracker is very close to the center determined by our approximation. 

\

The same observation holds for the {\bf RGB color space}. The center $y_1$ produced by the Mean-Shift Tracker is displaced, on average, by a distance of approximately 2-3 pixels from the center $y_1^*$ determined by annotating the frames, which is very close to our approximation.

\

\begin {figure}[!h]
	\centering
	\includegraphics [scale = .55] {distance_no_occ.png}
	\caption {Resulting target - candidate histogram distance function for the Simple Test, nRGB, 16 bins}
	\label {fig:5}
\end {figure}


\


We also notice that, when increasing the number of bins for the color histograms, the average running time per frame changes insignificantly, but the average of the displacements increases. Thus, choosing a 32-bins or 64-bins histogram for this type of videos does not justify, since a 16-bins histogram produces good results, while still maintaining high-enough peformance.

\

\subsection {Occlusion Test}

\

For this test, we use the same football match video as in the previous section. We choose to track the Dutch player from the middle of the field, above the referee, since this player gets occluded two times throught the sequence of frames. The tracker produces the following results for the {\bf nRGB color space}:

\begin {center}
	\begin {tabular}{|c|c|c|}
		\hline
		Bins & Average of the displacements & Average running time / frame \\ \hline
		16 & 2.4579 & 0.4369 s \\ \hline
		32 & 2.3094 & 0.4357 s \\ \hline
		64 & 2.3244 & 0.4358 s \\
		\hline
	\end {tabular}
\end {center}
and the following results for the {\bf RGB color space}:

\begin {center}
	\begin {tabular}{|c|c|c|}
		\hline
		Bins & Average of the displacements & Average running time / frame \\ \hline
		16 & 2.5827 & 0.3715 s \\ \hline
		32 & 3.0462 & 0.3747 s \\ \hline
		64 & 6.0122 & 0.4055 s \\
		\hline
	\end {tabular}
\end {center}

\

As in the previous section, we notice that, when choosing the {\bf nRGB color space}, the center $y_1$ determined by the tracker is displaced by a distance of approximately 2 pixels from the center $y_1^*$ determined by annotating the frames, which is very close to the human interpretation. We also notice that increasing the number of bins for the color histogram does not change the average running time significantly.

\

\begin {figure}[!h]
	\centering
	\includegraphics [scale = .4] {ShiftedPlayer.png}
	\caption {Using a 64-bins color histogram for the RGB color space increases the average of the displacements for the Occlusion Test}
	\label {fig:2}
\end {figure}


We obtain an interesting result for the {\bf RGB color space}. While the average of the displacements is about 2-3 pixels when using a 16-bins or 32-bins color histogram, which is close to the human interpretation, the average of the displacements increases to about 6 pixels when using a 64-bins histogram. This means that the rectangle that is drawn around the object throughout the sequence of frames is slightly, but visibly shifted from the position determined by annotating the frames, while still being in an acceptable range, as it can be seen in figure \eqref{fig:2}.

\

\begin {figure}[!h]
	\centering
	\includegraphics [scale = .55] {distance_occ.png}
	\caption {Resulting target - candidate histogram distance function for the Occlusion Test, nRGB, 16 bins. Notice the peaks which indicate the moments when the Dutch player is occluded / occcludes an opponent.}
	\label {fig:6}
\end {figure}

\

Again, we note that increasing the number of bins for the color histogram does not change the average running time significantly, but it increases the average of the displacements, leading to a visibly shifted rectangle around the tracked object. Thus, choosing a 16-bins color histogram for this test produces good results, while still maintaining high-enough performance.

\

\subsection {Light Intensity Test}

\

For this test, we track the face of a person that exists a tunnel. We choose to perform this test since we want to check the performance of the tracker under large variations in light intensity. In the tunnel, the light intensity is reduced and face pixels have approximately the same color as the wall pixels, as it can be seen in figure \eqref {fig:3}. When the person exists the tunnel, the colors of the face pixels change significantly, due to the color of the daylight. We check that the tracker still performs well under such a transition.

\

\begin {figure}[!h]
	\centering
	\includegraphics [scale = .9] {Andrei.png}
	\caption {Tracking the face of a person in a tunnel}
	\label {fig:3}
\end {figure}

\

The program produces the following results for the {\bf nRGB color space}:

\begin {center}
	\begin {tabular}{|c|c|c|}
		\hline
		Bins & Average of the displacements & Average running time / frame \\ \hline
		16 & 4.9572 & 0.1298 s \\ \hline
		32 & 4.5215 & 0.1297 s \\ \hline
		64 & 4.5478 & 0.1302 s \\
		\hline
	\end {tabular}
\end {center}
and the following results for the {\bf RGB color space}:

\begin {center}
	\begin {tabular}{|c|c|c|}
		\hline
		Bins & Average of the displacements & Average running time / frame \\ \hline
		16 & 17.9965 & 0.1170 s \\ \hline
		32 & 6.2778 & 0.1193 s \\ \hline
		64 & 15.9612 & 0.1415 s \\
		\hline
	\end {tabular}
\end {center}

\

We notice that the {\bf nRGB color space} performs significantly better than the {\bf RGB color space}. This is according to our intuition, since the nRGB color space is invariant to changes in light intensity. The average of the displacements for the nRGB color space is about 4-5 pixels, which implies that the rectangle surrounding the object is slightly shifted, but still in a good range. 

\

\begin {figure}[!h]
	\centering
	\includegraphics [scale = .55] {distance_person_face.png}
	\caption {Resulting target - candidate histogram distance function for the Light Intensity Test, nRGB, 16 bins. Notice the peaks which indicate the moments when the person moves the head / exists the tunnel, reducing the visibility / changing the color of the face.}
	\label {fig:7}
\end {figure}

The average of the displacements for the {\bf RGB color space} is approximately 18 and 16 for a histogram comprising 16 and 64 bins, respectively. This is a poor result which results from the fact that the tracker loses track of the face in the end of the video. The reason for this is that the colors of the pixels that make up the face change significantly when the person exists the tunnel, due to the color of the daylight. Since the RGB color space is not invariant to changes in light intensity, the new color of the face is significantly different from the original, leading to a little similarity between the candidate histogram and the target histogram. An interesting thing to note is that, when using a 32-bins histogram, the tracker does manage to keep track of the face, although the rectangle surrounding the face is slightly shifted.

\

We notice that increasing the number of bins does not change the average running time significantly. Thus, as in the previous tests, a 16-bins histogram produces good results, while still maintaining high-enough performance.

\

\section {Conclusions}

\

In this paper, we presented the Mean-Shift, an efficient algorithm for tracking an object throughout a sequence of video frames. We performed experiments with our implementation and showed that the tracker is robust against light intensity variations and occlusion when choosing the appropriate color space and parameters of the color histogram. For our tests, the nRGB color space and a 16-bins color histogram proved to produce the best results. While the algorithm performed well in most cases, it is far from working perfectly. For example, tracking a Dutch player that is very close to another Dutch player might lead in the second one being tracked at a certain moment, due to the close similarity between the color histograms. Using multiple features for describing the tracked object, such as shape or texture information, instead of a single feature, the color, could improve the performance of the tracker. We plan to employ these changes in a future project.

\

\begin {thebibliography} {100}	
	\bibitem {chen} J. Chen, G. An, S. Zhang, Z. Wu, \emph{"A Mean Shift Algorithm on Modified Parzen Window for Small Target Tracking"}, Acoustics Speech and Signal Processing (ICASSP), IEEE International Conference on IEEE, 2010
	\bibitem {comaniciu} D. Comaniciu, V. Ramesh, P. Meer, \emph{"Kernel-Based Object Tracking"}, IEEE Transactions on Pattern Analysis and Machine Intelligence, Vol. 25, No. 5, May 2003
	\bibitem {comaniciu2} D. Comaniciu, V. Ramesh, P. Meer, \emph{"Real-time Tracking of Non-Rigid Objects using Mean Shift"}, (Hilton Head Island, South Carolina, USA), pp. 42- 151, IEEE Conference on Computer Vision and Pattern Recognition (CVPR), 2000
	\bibitem {jeyakar} J. Jeyakar, R. V. Babu, K. R. Ramakrishnan, \emph{"Robust Object Tracking with Background-Weighted Local Kernels"}, Computer Vision and Image Understanding 112, pp. 296�309, 2008
\end	{thebibliography}

\end {document}