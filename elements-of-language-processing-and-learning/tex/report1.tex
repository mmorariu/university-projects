\documentclass [11pt] {article}

\usepackage {algorithm}
\usepackage {algorithmic}
\usepackage {amsmath}
\usepackage {graphicx}
\usepackage [margin = 1in] {geometry}
\newcommand {\dsum}{\displaystyle\sum}

\title {Statistical Parsing}
\author {Mihai Adrian Morariu (10232710), Andrei Oghin\u{a} (6424171)}

\begin {document}

\maketitle

\begin {abstract}
In this project, we set to build a prototype statistical parser. We extract the context-free productions from the training data trees and apply the CYK algorithm on the test sentences. We generate the parse forest of each sentence. Then, we extend CYK to implement the Viterbi algorithm and find the most likely derivations, testing multiple treatments for unknown words. Finally, we evaluate the parser and present our conclusions.
\end {abstract}

\section {Introduction}

\

In order to automatically operate on natural language, it is paramount to have a deep understanding on the underlying structure of a sentence. Humans find it easy to manipulate language and rarely, if ever, think about these underlying structures. However, while constructing sentences that make sense requires extensive knowledge about the outside world, in order for a sentence to at least sound right, there are complex dependencies between words that need to be satisfied. 

\subsection {Grammar}

\

A formal grammar is a set of \emph {formation rules} that describe how to form strings from a language that are valid according to the language's syntax. In that sense, a grammar is a \emph {language generator}, which can be used to compose valid strings, with disregard of their meaning. Formally, a grammar is a tuple $G = \left( \mathcal{N}, \Sigma, \mathcal{R}, \mathcal{S} \right)$, where:

\begin {itemize}
	\item $\mathcal{N}$ is a finite set of nonterminals
	\item $\Sigma$ is a finite set of terminal symbols, symbols disjoint from $\mathcal{N}$, called \emph {the alphabet}
	\item $\mathcal{R}$ is a finite set of productions of the form $\alpha \longrightarrow \beta$, where $\exists x\ \left( x \in \alpha \wedge x \in \mathcal{N} \right)$
	\item $\mathcal{S}$ is a unique member of $N$ called \emph {the start symbol}
\end {itemize}

\

Grammars describe languages in terms of their building blocks and enable recursive generation of sentences from smaller phrases, and eventually words. The main component in the specification of a formal grammar is a finite set of productions, $R$, which specify rewrite rules in the form of symbol substitutions that can be recursively performed to generate new sequences.

\subsection {Context-Free Grammar}

\

The first formalization of context-free grammars (CFG) was developed in 1956 by Noam Chomsky, who classified them as a special type of formal grammar. The restriction that applies in the case of CFG is that the left-hand side of each production rule consists of only a single nonterminal symbol. This makes the context of the non-terminals irrelevant. Although less powerful than unrestricted grammars, which can express any language, these types of restricted grammars are most often used because parsers for them can be efficiently implemented.

\subsection {Chomsky Normal Form}

\

A context-free grammar is said to be in the \emph {Chomsky Normal Form} if each production rule is of the form $A \to BC$, $A \to \alpha$, $S \to \epsilon$, where $A$, $B$ and $C$ are nonterminals, $\alpha$ is a terminal, $S$ is the start symbol and $\epsilon$ is the empty string. This adds additional constrains to a CFG, but makes it easier to apply certain algorithms on the grammar. For instance, in order to apply the CYK algorithm to produce parse-forests, it is required that the CFG be in Chomsky Normal Form. Algorithms exist for transforming any CFG in its equivalent Chomsky normal form grammar.

\subsection {Probabilistic Context-Free Grammar}

\

A probabilistic context-free grammar (PCFG) is a context-free grammar in which each production is given a probability. A PCFG is a CFG with an additional probability mass function $P : \mathcal{R} \to \left( 0, 1 \right]$, so that $\forall \alpha \in \mathcal{N}$, we have: 

\begin{equation}
\dsum_{\alpha \to \beta \in \mathcal{R}} P \left( \beta | \alpha \right) = 1
\end{equation}

Such an augmented grammar captures information regarding the various interpretations of ambiguous words. For instance, it can deduce that "point" is more likely to be a noun, than it is to be a verb, albeit both interpretations are possible.

\subsection {Parsing Models}

\

The parse of a sentence $S$ can be represented as a tree $T$, where each parent denotes the left hand side (LHS) of a production and its immediate children denote the right-hand side (RHS) of that production. The probability of a parse is then the product of the probabilities of the productions used in that parse. Thus, in a PCFG, for a tree $T$ derived for sentence $S$ from productions of the form  $LHS_i \to RHS_i$, $1 \le i \le n$, it stands:

\begin{equation}
P(T, S) = \prod_{i = 1..n} P(RHS_i|LHS_i)
\label{eqts}
\end{equation}

Therefore, some derivations are more consistent with the PCFG than others. Probabilistic CFGs are dealing with the complexities of natural languages. They address the combinatorial explosion problem of applying CFGs by using the frequency of various productions to order them, resulting in a most likely interpretation.

\

A statistical parser uses an algorithm to search for the best tree, $T^*$, that maximizes the conditional probability $P(T | S)$. Such an algorithm is Viterbi, a variant of the CYK algorithm which finds the most likely parse of a sequence, for a given PCFG. The Viterbi algorithm is described in detail in section 3.3.


\section {Experimental Setup}

\

We train our parses on the Penn Treebank, a structurally annotated corpus which consists of a sequence of sentences taken from the Wall Street Journal. In total, there are 39,832 phrases in the data set. The data is available in binarized form, having extra nodes labeled with the at sign ("@"), that add new tree levels. The unaries are pulled out, with unary productions in the form $X \to Y$ being presented as a single node denoted $X\%\%\%\%\%Y$. There are two exceptions: unaries can be present above the last non-terminals, and there is also a unary at the TOP of the tree.

\

We test our implementation on a set of test sentences. There are 2,416 test sentences, from which we extracted 603 sentences with length less or equal to 15 words. For all sentences, gold standard trees are available, for evaluation purposes.

\

We use PHP to perform the implementation. Consequently, we take advantage of the fact that an array in PHP is actually an ordered map. A map is a type that associates values to keys. This type is optimized for several different uses, and it can be treated as an array, list (vector), hash table (an implementation of a map), dictionary, collection, stack or queue. As array values can be other arrays, trees and multidimensional arrays are also possible.

\section {Methods}

\

In this section, we describe the methods used to construct the grammar, to build the parser that implements the CYK algorithm and then to generate the parse-forest of a sentence. Then, we discuss how CYK can be augmented with probabilities in order to compute the most likely parse of a sentence, using the Viterbi algorithm.

\subsection {Extracting the PCFG}

\

This phase consists in extracting all depth one sub-trees from the treebank. Based on these sub-trees, the PCFG is constructed and its parameters are estimated by Maximum Likelihood Estimation. The probabilities are computed by counting the occurrences of each production, and then computing conditional probabilities based on the LHS of the productions.

\

The trees in the Penn Treebank use a standard representation based on complete balanced bracketing. Each sentence has been annotated for part-of-speech labels and phrase-structures. For example, an encoding of the type \emph{(S (A orange) (B apple))} represents a tree having a root node S, where S has two children, A and B, and each of these having exactly one child - \emph{orange} for A and \emph{apple} for B. The task is to extract all productions of the type S $\rightarrow$ A B. This is done for each sentence, and then merged into the grammar data structure.

\

The data structure used to represent the grammar is an ordered map (PHP array), which is treated as a hash array. This allows for fast lookup with $O(1)$ complexity for a given production, when searching by the LHS. The entries are indexed by their LHS, while the values are sub-arrays, indexed by the RHS of the production, where non-terminals are separated using the pipe character (''$\mid$''). The values represent the probabilities. For example, a short sub-part of the the hash has the following structure:

\begin {center}
\begin {tabular}{lll}
array ( & & \\
  & ... & \\
  & 'S' $\to$ & \\
  & array ( & \\
  & & 'NP $\mid$ VP' $\to 0.34393530160504$, \\
  & & 'RB $\mid$ VP' $\to 0.0015988326969048$, \\
  & &  \\
  & ) & \\
  &  & \\
) & & 
\end {tabular}
\end {center}

The first step consists in normalizing the representation. This is done by removing the extra white spaces and adding the parenthesis needed to consistently represent the last tree level just like the other levels. Therefore, the special case where for the terminals a space is used instead of parenthesis, is removed. For example, after applying these transformations on \emph{(S (A orange) (B apple))}, the sequence becomes \emph{(S(A(orange))(B(apple)))}.

\

The second step consists in extracting the productions from a sentence. This is done by traversing the tree representation once, from left to right. Therefore, the complexity of this algorithm is $O(n)$, where $n$ is the number of characters in the tree representation, which is optimal. Each character read is tested against the set of two special character, the open round bracket (''('') and the closed round bracket ('')''). A stack is used to trace the level of the tree. Every open bracket leads to a stack push, while every closed bracket lead to a stack pop. The stack consists in tokens, which are the labels extracted from the tree representation, each having an associated unique identifier. This allows for correct parent-child associations when popping sequential children from the stack. In the end, the unique identifiers are removed, and the productions are grouped by their LHS.

\

The third step consists in merging the productions extracted from a sentence with the grammar. This is done by appending in the appropriate positions in the grammar structure described above the newly generated productions, with their corresponding frequencies. In case the entry already exists, the frequency is added to the existing entry.

\

The last step consists in computing probabilities. Each LHS is considered in turn, and all the possible production frequencies for that LHS are summed up to N. Then, each production receives a probability equal to the division of their frequency to the total number N.

\subsection {CYK Algorithm}

\

Cocke-Younger-Kasami (CYK) is a bottom-up parsing algorithm for context-free grammars that uses dynamic programming. The algorithm receives a sentence as input, extracts its words, and considers every possible subsequence of words. It constructs a dense forest in a three-dimensional structure $S$, such that $S[i, j, k]$ is true if the subsequence of words starting from $i$ to $j$ can be generated from $R_k$, as described in in algorithm \ref{alg1}. 

\

Since the algorithm performs a large number of grammar lookups by RHS (answering the question, "What LHS could have generated this RHS?"), an additional structure is pre-computed. This structure mirrors the hash structure described in section 3.1, only this time the index keys are the RHS of the productions, in order to enable fast lookups in $O(1)$.

\

The subsequences are considered in increasing order of length. Hence, the subsequences of length 1, which represent the words, are considered first. Then, subsequences of length 2 are considered, therefore all possible partitions of two consecutive parts, checking for the existence of productions $X_{\alpha} \to X_{\beta}X_{\gamma}$ such that $X_{\beta}$ matches the first part and $X_{\gamma}$ matches the second part. For all $X_{\alpha}$ found, it records $X_{\alpha}$ as matching the subsequence. A sentence is recognized by the grammar if the subsequence containing the entire sentence is matched by the TOP symbol.

%\newpage
\begin {algorithm}
\caption{CYK Algorithm}
\label{alg1}
\begin {algorithmic}[1] 
	\STATE $G \leftarrow \left( \mathcal{N}, \Sigma, \mathcal{R}, \mathcal{S} \right)$
	\STATE $\Sigma \cup \mathcal{N} \leftarrow \left\{ X_1, \dots, X_r \right\}$ \\
	\STATE $w = w_1 w_2 \dots w_n$ \\
	Initialize the 3D array S[1..n, 1..n, 1..r] to {\bf FALSE} 
	
	\FOR {$i = 1$ to $n$}
		\FOR {$\left( X_j \to x \right) \in \mathcal{R}$}
			\IF {$x = w_i$} \STATE $S[i, i, j] \gets \mathbf{TRUE}$ \ENDIF
		\ENDFOR
	\ENDFOR
	
	\FOR {$i = 2$ to $n$}
		\FOR {$L = 1 \to n - i + 1$}
			\STATE {$R \gets L + i - 1$}
			\FOR {$M = L + 1 \to R$}
				\FOR {$\left( X_{\alpha} \to X_{\beta}X_{\gamma} \right) \in \mathcal{R}$}
					\IF {$S[L, M-1, \beta]$ and $S[M, R, \gamma]$}
						\STATE $S[L, R, \alpha] \gets \mathbf{TRUE}$
					\ENDIF
				\ENDFOR
			\ENDFOR
		\ENDFOR
	\ENDFOR
	
	\FOR {$i = 1$ to $r$}
		\IF {$S[1, n, i]$}
			\STATE return {\bf true}
		\ENDIF
	\ENDFOR
	
	\STATE return {\bf false}
\end {algorithmic}
\end {algorithm}

As mentioned before, unary productions sometimes appear. Unary productions can only be found at the top of a parse tree \emph{(TOP $\to$ S $\to$ A B)}, or at the bottom \emph{(C $\to$ D $\to$ word)}. The former case is handled when checking to see if the entire sentence (LHS found at $<1, n>$ positions) can be produced by the TOP symbol. For the latter, each time subsequences of length 1 are processed (words), the program also checks to see if there are non-terminals that can generate each of the POS-tags. If such a non-terminals exists, they are added to the structure.

\subsection {Viterbi Algorithm}

\

The Viterbi algorithm is used to find the most likely parse of a sentence. Considering a grammar $G$ which defines the set $(\Sigma \times \Pi)$, where all sentence-parse pairs $<T, S> \in (\Sigma \times \Pi)$, each with probability $P(T, S)$, the task is to select $T^* \in \Pi$, for any sentence $S$, such that: 

\begin{equation}
\begin{align}
T^* & = argmax_{T \in \Pi} P(T | S) & = argmax_{T \in \Pi} \frac{P(T, S)}{P(S)} \\
  & = argmax_{T \in \Pi} P(T, S) & = argmax_{T \in G(U)} P(T)
\end{align}
\end{equation}

\

This implies using a generative model based on the observation that maximizing $P(T|S)$ is equivalent to maximizing $P(T, S)$, which is in turn expressed in equation~\ref{eqts}. 

\

This can be accomplished by extending the CYK algorithm with probabilities. To this end, while building the parse forest using algorithm \ref{alg1} (CYK), for all the productions that are added, the actual production probabilities are computed and stored. For the first level, the probabilities are directly available in the PCGF. For the other levels, each production entry gets its probability by multiplying the production probability (from the PCFG) with the probabilities of the subtrees that it is set to produce. Lines 16-18 from algorithm 1 are subject to the following expansion:

\begin {algorithm}
\caption{Viterbi extension to CYK}
\label{alg2}
\begin {algorithmic}[1] 
\IF {$S[L, M-1, \beta]$ and $S[M, R, \gamma]$}
	\STATE prob \gets $S[L, M-1, \beta] \times S[M, R, \gamma] \times P(R|L) $
	\IF {$prob > $S[L, R, \alpha]$}
		\STATE {$S[L, R, \alpha] \gets prob$}
		\STATE {$BP[L, R, \alpha] \gets (M, \beta, \gamma)}
	\ENDIF
\ENDIF
\end {algorithmic}
\end {algorithm}


For the probability of a subtree, the highest production probability is selected. This effectively computes the maximum probability of all subtrees. In the end, in order to select the most likely tree, it suffices to select the entry on the $<1, n>$ position that has the highest probability. 

\subsection {Tree Reconstruction}

\

In order to reconstruct the tree with the highest probability, at each step, back-pointers are used to store the highest probability subtree for each LHS. This structure, labeled $BP$ (see algorithm extension~\ref{alg2}), is defined in the same three-dimensional space as the structure $P$ described in the previous section. It consists in tuples of the form $(M, \beta, \gamma)$, where $M$ represents the split point for the subtree with highest probability bellow the current level, and $\beta$ and $\gamma$ represent the non-terminals of the current production for the subtree with highest probability. 

\

Given the structure $BP$, a simple recursive traversal following the tree specified using the $(S, B, C)$ tuples is used in order to reconstruct the most likely tree.  

\subsection {Binarization Reversing and Unaries Treatment}

\

Recall the trees in the training data were binarized, by adding extra nodes labeled with the at sign, where necessary. This is helpful for manipulation during the CYK algorithm, but, in order to generate unconstrained trees, this process has to be reversed during tree reconstruction. Useful for generating realistic parses, this becomes a necessity at evaluation time, since the set of golden trees is not binarized. 

\

The binarization reversing process consists in testing, at each recursive call performed on the $BP$ structure, if the LHS tag contains the at sign. Since these are artificial nodes added to obtain a binary form, they can be simply ignored. Therefore, the RHS of such productions will be merged together with the RHS of the production above them, effectively flattening the tree and allowing for more than two children per node.

\

A similar approach is used to handle unaries. Recall most unaries are pulled out in the training data, by constructing artificial non-terminals containing the percentage character. These non-terminals can contain more than two levels of merged unaries, such as $VP\%\%\%\%\%S\%\%\%\%\%VP$, which should be expanded to $VP \to S \to VP$. This is achived within the recursive calls that reconstruct the trees. If the percentage character is detected, the non-terminal is split based on the $\%\%\%\%\%$ sequence, and multiple (all) tree levels are added at that recursive call level. 

\subsection {Treatment of Unknown Words}

\

During the testing and evaluation phase, the parser naturally encounters unknown words, that is, words that are not present in the grammar. This phenomena leads to the failure of the CYK algorithm to build up the parse forest that would eventually allow for computing the most likely parse.

\

The initial approach in handling unknown words is to simply generate flat trees with bogus POS tags for such sentences, for the purpose of evaluation. These trees however are not generated by the grammar at hand, and are mere placeholders that, as expected, perform badly upon evaluation against the gold trees. The question arises, how to make use of the grammar and compute a parse forest, even when some of the words in the sentence are unknown. In this work, three methods are used for this purpose.

\

The first method employs the use of a simple heuristic: for all unknown words encountered at parse time, ad-hoc POS-tag association is performed. The rule used is to associate on the fly the cardinal number POS tag (CD) if the word has a numeric form, and the singular proper noun tag (NNP) in all other cases. This follows the simple intuition that new words (that were not encountered in the training data) are most likely to be proper nouns.

\

The second, more robust and formal method is to employ the use of smoothing. The first step consists in morphing the PCFG by agglomerating rare words into feature-based artificial terminals representations that statistically model their behavior. This is done by pooling all rare words (we consider words that appear only once in the training data). Then, a transformation is applied to these words to translate them into feature space, after prefixing with the distinctive \emph{XXXUNKNOWN} sequence. To this end, we use five rules, loosely based on the Porter Stemmer rules. These are: \emph{word ends in "ing"}, \emph{word ends in "ly"}, \emph{word ends in "ed"}, \emph{word has first letter capitalized}, and \emph{word is numeric}. For instance, the word "Furiously" would translate in $"XXXUNKNOWN \mid 0\mid1 \mid 0 \mid 1 \mid 0"$. These rules try to capture distinctive trademarks of words, that may signal the distribution over possible POS tags. 

\

After mapping, these new words are introduced in the grammar (or their probabilities consolidated), and the original word is discarded, to preserve the overall probability mass. Then, unknown words found in the test set can be subject to the same feature-based transformation, matched against the points in feature space and attributed their POS tag initializations, with their respective probabilities. This feature-based method with replacement is labeled \emph{FB-R}.

\

The third method is a variant of method 2, where the rare words used to populate the feature space are not discarded from the grammar. Although this approach may violate the formal constraint of having all probabilities summing up to 1, they preserve useful information in the grammar. We consider noteworthy to investigate the difference in performance between these two models. This feature-based method without replacement is labeled \emph{FB-WR}.   


\section {Results and Analysis}

In this section, we present and discuss the experimental results. We mention grammar statistics, and then focus on parser evaluation.

\subsection {Grammar statistics}

Our grammar doesn't explicitly disambiguate between terminals and non terminals. The grammar structure contains 173 unique LHS nodes, which we can deduce that represent non-terminals. The total number of nodes is 46671. Therefore, there are 46498 non-terminals in the grammar. 

\

Interestingly, close to half (20623) of all words represent words that appear only once in the corpus. This highlights the long tail of rare words that appear in any large corpus, and confirms the spirit of Zipf's Law, which states that  the frequency of any word in a corpus is inversely proportional (therefore, decreases rapidly) to its rank in the frequency table.

\

As expected, the grammar contains syntactically ambiguous words, for which the probabilities of various associated POS tags can be computed from the frequencies observed in the corpus. To this end, in table~\ref{ambT} we provide four examples of such words, with the associated probabilities. 

\newpage
\begin{table}[h]
\begin {center}
\begin {tabular}{|l|c|} \hline
	Production & Probability (approx.) \\
	\hline
	\hline
	NN $\to$ point 	& 0.0018 \\ \hline
	VBP $\to$ point & 0.0008 \\ \hline
	VB $\to$ point 	& 0.0003 \\ \hline
	JJ $\to$ point 	& 0 \\
	\hline
\end {tabular}
\caption{Ambiguous word}
\label{ambT}
\end {center}
\end{table}

\

Here, we can observe how, for instance, the word "point" is most likely to be a noun, but, as expected, it can also be a verb, with a relatively high probability. 

\

To further outline overall key metrics of the grammar, we present in table ~\ref{prodT} the four most likely productions $S \to \alpha$, with their probabilities.

\

\begin{table}[h]
\begin {center}
\begin {tabular}{|l|c|} \hline
	Production & Probability (approx.) \\
	\hline
	S $\to$ NP $\mid$ S@ & 0.354 \\ \hline
	S $\to$ NP $\mid$ VP & 0.343 \\ \hline
	S $\to$ PP $\mid$ S@ & 0.071 \\ \hline
	S $\to$ S $\mid$ S@ & 0.061  \\ \hline
\end {tabular}
\caption{Likely productions for S}
\label{prodT}
\end {center}
\end{table}

\

From here, we can verify the intuition that sentences usually begin with a noun phrase (NP).

\

For the purpose of our experiments, besides the primary grammar, we built two grammars for each of the feature-based methods for treating unknown words described in section 3.6.

\subsection {CYK}

\

Following the CYK algorithm, our implementation determines the parse-forest for each sentence from the test set. We noticed that, even for short sentences, the forest contains a large set of parse trees. This was according to our intuition, since the grammar contained a large set of production rules, therefore increasing the chances of each non-terminal being derived from some other node. 

\subsection {Parser evaluation}

\

In order to evaluate the performance of the parser, we ran four sets of experiments. All experiments were performed on the test data consisting in 603 sentences with length less or equal to 15 words, for which the gold standard trees were available. The experiments consisted in generating the Viterbi parse for each of the sentences. Then, for each run, the results were evaluated against the gold trees. The evaluation results can be investigated in table~\ref{evalT}.  

\begin{table}[htb]
\begin {center}
\begin {tabular}{|c|c|c|c|c|} 
	\hline
	 					& Baseline  &  Heuristic 	& 		FB-R  	& 		FB-WR 	\\ \hline
	Recall 				& 	61.89	& 		74.03	& 		73.42	& 		74.51	\\ \hline
	Precision			& 	74.02	& 		73.47	& 		73.60	&		74.03	\\ \hline
	FMeasure			& 	67.41	& 		73.75	& 		73.51	&		74.27	\\ \hline
	Complete match		& 	12.60	& 		16.92	& 		16.92	&		17.98	\\ \hline
	Tagging accuracy	& 	75.18	& 		92.97	& 		93.05	&		93.31	\\ \hline
\end {tabular}
\caption{Parser evaluation metrics}
\label{evalT}
\end {center}
\end{table}

\

For the first \emph{Baseline} experiment, we used the feature-free grammar, where rare words were left "as is" and thus terminals consist in the actual words. For sentences without a parse, a flat representation with bogus POS tags was used for evaluation purposes.  

\

The second \emph{Heuristic} experiment produces significantly better evaluation metrics. Here, we also use the feature-free grammar, and simply associate either the cardinal number POS tag (CD) or the singular proper noun tag (NNP) to unknown words. While this is a rather crude assumption, it perform rather well, since it allows CYK to build a proper parse forest and select a tree, which is much better than the flat bogus representation used as baseline. 

\

The third model, \emph{FB-R}, provides a modest improvement on some evaluation metrics over the heuristic approach. Built on a modified grammar, this approach uses the distribution of rare words to construct a feature space, which in turn is used to map unknown words. Hence, words from the test set having similar profiles with low frequency words from the training set inherit their behavior. While more formal, this approach disappoints by not improving significantly over the much simpler heuristic method. We speculate this could potentially be improved with more careful feature engineering, but leave this as a future research goal.  

\

The \emph{FB-R} experiment, while theoretically sound in modeling rare words behavior, loses significant information by removing all rare words from the grammar. This is necessary to preserve probability mass. However, this information loss could also be one of the reasons why \emph{FB-R} doesn't improve much over the \emph{Heuristic} approach. To this end, we set up the \emph{FB-WR} experiment. Here, the modified grammar introduces the feature representations, but doesn't prune the rare words. The results confirm our expectations, in that the evaluation metrics are indeed better than for \emph{FB-R}. However, the increase is modest, and hardly justifies the overhead over the heuristic approach.

\section {Conclusions}

\

In this project, we built a prototype statistical parser. We started by constructing a PCFG from a set of training sentence trees. We then used the CYK algorithm to produce parse forests for a set of sentences. Further, we extended CYK with probabilities and derived the Viterbi parse of each sentence. Based on evaluation metrics, we investigated the performance of various smoothing techniques. Since more advanced treatments don't seem to improve the evaluation metrics significantly, we conclude that heuristic approaches are preferable, due to their simplicity, robustness, and good performance. Formal smoothing could potentially be improved with more careful feature engineering. We leave this topic open for future work.   

\

\begin {thebibliography} {100}
	\bibitem {aho} A. V. Aho, J. D. Ullman, \emph{``The Theory of Parsing, Translation and Compiling''}, Prentice - Hall, 1972, ISBN 0-13-914556-7
	\bibitem {manning-scutze1} C. D. Manning, \emph{``Seven Lectures on Statistical Parsing''}, 2007
	\bibitem {manning-scutze2} C. D. Manning, H. Scu\"{u}tze, \emph{``Foundations of Statistical Natural Language Processing''}, The MIT Press, 1999, ISBN 0-262-13360-l
	\bibitem {peled} S. Har-Peled, M. Parthasarathy, \emph{``CYK Parsing Algorithm''}, 2009
	\bibitem {collins} M. Collins, \emph{``Three Generative, Lexicalized Models of Statistical Parsing''}, Proceedings of ACL'97, 1997	
\end	{thebibliography}

\end {document}
