#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <sstream>
#include <time.h>

#include <vector>
#include <map>

using namespace std;

typedef map<string, unsigned int> string_unsigned_int;

map<string, string_unsigned_int> G;
string_unsigned_int sum;

void addEntry(string &node, string &children)
{
	auto iterNode = G.find(node);

	if (iterNode == G.end())
	{
		string_unsigned_int pairChildren;
		pairChildren.insert(make_pair(children, 1));
		G.insert(make_pair(node, pairChildren));
	}
	else
	{
		auto iterChildren = iterNode->second.find(children);

		if (iterChildren == iterNode->second.end())
			iterNode->second.insert(make_pair(children, 1));
		else
			iterChildren->second++;
	}
	
	auto iterSum = sum.find(node);

	if (iterSum == sum.end())
		sum.insert(make_pair(node, 1));
	else
		iterSum->second++;
}

void buildGrammar(string &line)
{
	unsigned int lineLen = line.length(), stackLen, bracks = 0;

	vector<string> nodes;
	vector<int> parent;

	string str(""), node(""), children("");
	char character;

	for (unsigned int j = 0; j < lineLen; j++)
		switch (character = line.at(j))
	{
		case '(': 
			if (!str.empty())
			{
				nodes.push_back(str);
				parent.push_back(bracks);
				str.clear();
			}
			bracks++;

			break;
		case ')':
				stackLen = nodes.size();

				if (!str.empty())
				{
					children = str;
					node = nodes.at(stackLen - 1);
					addEntry(node, children);
					str.clear();
				}
				bracks--; break;
		case ' ':
			{
				nodes.push_back(str);
				parent.push_back(bracks);
				str.clear();
			}; break;
		default:
			str.push_back(character);
	}

	stackLen = parent.size();

	for (unsigned int i = 0; i < stackLen - 1; i++)
	{
		node = nodes.at(i);
		children.clear();

		for (unsigned int j = i + 1; j < stackLen && parent.at(j) > parent.at(i); j++)
			if (parent.at(j) == parent.at(i) + 1)
				children.append(nodes.at(j) + "|");

		if (!children.empty())
		{
			size_t posBar = children.find_last_of("|");
			children.replace(posBar, 1, "");
			addEntry(node, children);
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	clock_t clockInitial, clockFinal;

	clockInitial = clock();

	unsigned int i = 0, j = 0;

	ifstream inputFile;
	ofstream outputFile;

	if (argc == 3)
	{
		cout << endl << "Opening training file..." << endl;
		inputFile.open(argv[1], ifstream::in);
		string line;

		if (inputFile.is_open())
		{
			cout << "Done." << endl << endl;
			outputFile.open(argv[2]);
			
			if (!outputFile.is_open())
			{
				cout << "Error opening output file !" << endl;
				return -1;
			}

			cout << "Building productions..." << endl;

			while (!inputFile.eof())
			{
				getline(inputFile, line);

				if (!line.empty())
				{
					while ((i = line.find(" (")) != string::npos) line.replace(i, 2, "(");
					while ((i = line.find(" )")) != string::npos) line.replace(i, 2, ")");
	
					buildGrammar(line);
				}
			}

			for each (pair<string, string_unsigned_int> node in G)
				for each (pair<string, unsigned int> children in node.second)
					outputFile << node.first << " " << children.first << " " << (float) children.second / sum[node.first] << endl;
		}
		else
		{
			cout << "Error opening training file !" << endl;
			return -1;
		}

		cout << "Done." << endl << endl;
		cout << "Output file written." << endl;

		inputFile.close();
		outputFile.close();

		clockFinal = clock() - clockInitial;
		cout << endl << "Time elapsed: " << (double) clockFinal / (double)CLOCKS_PER_SEC << " seconds." << endl << endl;
	}
	else
		cout << endl << "Usage: treebank.exe [input_training_file] [output_grammar_file]" << endl;

	return 0;
}