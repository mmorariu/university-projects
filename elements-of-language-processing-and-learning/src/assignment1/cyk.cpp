#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <sstream>
#include <time.h>

#include <vector>
#include <map>

using namespace std;

typedef multimap<string, string> mm_ss;
mm_ss G;

ifstream grammarFile, sentenceFile;
ofstream outputFile;

bool isNumber(string str)
{
	if (atoi(str.c_str()))
		return true;

	return false;
}

void addEntryToGrammar(mm_ss &G, string &child, string &node)
{
	pair<mm_ss::iterator, mm_ss::iterator> iterChild = G.equal_range(child);
	
	for (mm_ss::iterator iter = iterChild.first; iter != iterChild.second; iter++)
		if (!iter->first.compare(child) && !iter->second.compare(node)) return;

	G.insert(make_pair(child, node));
}

void addEntryToTable(vector<string> (&table)[50][50], unsigned int begin, unsigned int end, string rule)
{
	bool isRule = false;
	unsigned int nRules = table[begin - 1][end - 1].size();

	for (unsigned int ruleIndex = 0; ruleIndex < nRules && !isRule; ruleIndex++)
		if (!rule.compare(table[begin - 1][end - 1].at(ruleIndex)))
			isRule = true;

	if (!isRule)
	{
		vector<string> temp = table[begin - 1][end - 1];
		temp.push_back(rule);
		table[begin - 1][end - 1] = temp;
	}
}

void printOutput(ofstream& outputFile, vector<string> (&table)[50][50], vector<string> &tokens)
{
	unsigned int nWords = tokens.size();
	vector<string> finalChildren;

	if (!table[0][nWords - 1].empty())
	{
		for each (string production in table[0][nWords - 1])
		{
			pair<mm_ss::iterator, mm_ss::iterator> iterChild = G.equal_range(production);
	
			for (mm_ss::iterator iter = iterChild.first; iter != iterChild.second; iter++)
				if (!iter->second.compare("TOP"))
				{
					finalChildren.push_back(production);
					break;
				}
		}

		if (!finalChildren.empty())
		{
			sort(finalChildren.begin(), finalChildren.end());
			unsigned int nWords = finalChildren.size();

			for (vector<string>::size_type wIndex = 0; wIndex < nWords - 1; wIndex++)
				outputFile << "TOP -> " << finalChildren.at(wIndex) << ", ";

			outputFile << "TOP -> " << finalChildren.at(nWords - 1) << endl;
		}
		else
			outputFile << "No productions that cover the whole sentence were found." << endl;
	}
	else
		outputFile << "No productions that cover the whole sentence were found." << endl;
}

void buildTable(vector<string> (&table)[50][50], vector<string> tokens)
{
	unsigned int nWords = tokens.size(), length = 0, span = 0, part = 0;

	// for (vector<string>::size_type wIndex = 0; wIndex < nWords; wIndex++)
	for (unsigned int wIndex = 0; wIndex < nWords; wIndex++)
	{
		string token = tokens.at(wIndex);
		pair<mm_ss::iterator, mm_ss::iterator> iterChild = G.equal_range(token);

		if (iterChild.first != iterChild.second)
			for (auto iter = iterChild.first; iter != iterChild.second; iter++)
			{
				string parent(iter->second);
				pair<mm_ss::iterator, mm_ss::iterator> iterUnaries = G.equal_range(parent);

				if (iterUnaries.first != iterUnaries.second)
					for (auto iter = iterUnaries.first; iter != iterUnaries.second; iter++)
					{
						string parentUnaries(iter->second);
						addEntryToTable(table, wIndex + 1, wIndex + 1, parentUnaries);
					}
					
				addEntryToTable(table, (unsigned int)wIndex + 1, (unsigned int)wIndex + 1, parent);
			}
		else
			if (isNumber(token))
				addEntryToTable(table, wIndex + 1, wIndex + 1, "CD");
			else
				addEntryToTable(table, wIndex + 1, wIndex + 1, "NNP");
	}

	for (length = 2; length <= nWords; length++)
			for (span = 1; span <= nWords - length + 1; span++)
				for (part = span + 1; part <= span + length - 1; part++)
					if (!table[span - 1][part - 2].empty() && !table[part - 1][span + length - 2].empty())
						for each (string beta in table[span - 1][part - 2])
							for each (string gamma in table[part - 1][span + length - 2])
							{
								string alpha(beta + "|" + gamma);
								pair<mm_ss::iterator, mm_ss::iterator> iterAlpha = G.equal_range(alpha);

								for (auto iter = iterAlpha.first; iter != iterAlpha.second; iter++)
								{
									string parent(iter->second);
									addEntryToTable(table, span, span + length - 1, parent);
								}
							}
}

void tokenizeString(vector<string> &tokens, string line)
{
	istringstream lineStream(line);
	
	do
	{
		string token;
		lineStream >> token;
	
		if (!token.empty())
			tokens.push_back(token);
	}
	while (lineStream);
}

void loadGrammar()
{
	string node, children;
	double count;

	while (!grammarFile.eof())
	{
		grammarFile >> node >> children >> count;

		if (!node.empty() && !children.empty())
			addEntryToGrammar(G, children, node);
	}
}

void processSentences()
{
	string line;

	while (!sentenceFile.eof())
	{
		getline(sentenceFile, line);

		if (!line.empty())
		{
			vector<string> tokens;
			tokenizeString(tokens, line);

			vector<string> table[50][50];
			buildTable(table, tokens);

			printOutput(outputFile, table, tokens);
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	clock_t clockInitial, clockFinal;

	clockInitial = clock();

	if (argc == 4)
	{
		cout << endl << "Loading grammar file..." << endl;
		grammarFile.open(argv[1], ifstream::in);

		if (grammarFile.is_open())
		{
			loadGrammar();
			grammarFile.close();
			
			cout << "Done." << endl << endl;
			cout << "Opening sentence file..." << endl;
			sentenceFile.open(argv[2], ifstream::in);


			if (sentenceFile.is_open())
			{
				outputFile.open(argv[3]);
				
				if (!outputFile.is_open())
				{
					cout << "Error opening output file !" << endl;
					return -1;
				}
				else
				{
					cout << "Done." << endl << endl;
					cout << "Processing sentence file..." << endl;
					processSentences();	
				}
			}
			else
			{
				cout << "Error opening sentence file !" << endl;
				return -1;
			}
		}
		else
		{
			cout << "Error opening grammar file !" << endl;
			return -1;
		}

		cout << "Done." << endl << endl;
		cout << "Output file written." << endl;
		clockFinal = clock() - clockInitial;
		cout << endl << "Time elapsed: " << (double) clockFinal / (double)CLOCKS_PER_SEC << " seconds." << endl << endl;
	}
	else
		cout << endl << "Usage: cyk.exe [grammar_file] [sentence_file] [output_file]" << endl;

	sentenceFile.close();
	outputFile.close();

	return 0;
}
