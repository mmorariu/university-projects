function varargout = cerc_osc(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cerc_osc_OpeningFcn, ...
                   'gui_OutputFcn',  @cerc_osc_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% -------------------------------------------------------------------------

function cerc_osc_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;
set(0, 'DefaultTextInterpreter', 'TeX');

guidata(hObject, handles);

% -------------------------------------------------------------------------

function varargout = cerc_osc_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;

% -------------------------------------------------------------------------

function txtX_Callback(hObject, eventdata, handles)
function txtX_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtY_Callback(hObject, eventdata, handles)
function txtY_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtT_Callback(hObject, eventdata, handles)

input = str2num(get(hObject, 'String'));

if (isempty(input))
    set(hObject,'String','0');
end

guidata(hObject,handles);

% -------------------------------------------------------------------------

function txtT_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function btnOk_Callback(hObject, eventdata, handles)

syms t real;

global LX LY;

X = sym(get(handles.txtX, 'String'));
Y = sym(get(handles.txtY, 'String'));
LMIN = str2num(get(handles.txtA, 'String'));
LMAX = str2num(get(handles.txtB, 'String'));
T = str2num(get(handles.txtT, 'String'));

if (isempty(X) || isempty(Y) || isempty(T) || isempty(LMIN) || isempty(LMAX))
    msgbox('Please fill in all the required fields!', 'Eroare', 'warn', 'modal');
else    
    if (LMIN >= LMAX)
        msgbox('Please choose two values for Min and Max so that Min < Max!', 'Error', 'warn', 'modal');
    else
        if (T < LMIN || T > LMAX)
            msgbox('Please choose a value for T so that Min <= T <= Max!', 'Error', 'warn', 'modal');
        else
            X1 = diff(X);
            X2 = diff(X1);
            Y1 = diff(Y);
            Y2 = diff(Y1);
    
            A = X + Y1*(X1^2+Y1^2) / (X2*Y1-X1*Y2);
            B = Y - X1*(X1^2+Y1^2) / (X2*Y1-X1*Y2);
        
            K = (X1*Y2-X2*Y1) / (sqrt(X1^2+Y1^2)^3);
            R = 1 / abs(K);
    
            Rv = subs(R,t,T);
            Av = subs(A,t,T);
            Bv = subs(B,t,T);
    
            tv = linspace(LMIN, LMAX, 1000);
            tc = linspace(0, 2*pi, 3000);
            
            set(gcf, 'CurrentAxes', handles.axesPlot);
    
            cla reset;
            plot(subs(Av + Rv*cos(t), t, tc).*ones(size(tc)), subs(Bv + Rv*sin(t), t, tc).*ones(size(tc)), 'Color', 'r', 'LineWidth', 1.5);
            hold on;
            plot(subs(X, t, tv).*ones(size(tv)), subs(Y, t, tv).*ones(size(tv)), 'Color', 'k', 'LineWidth', 1.5);
           
            axis equal;
            
            LX = get(gca, 'XLim');
            LY = get(gca, 'YLim');
    
            set(handles.axesPlot, 'XGrid', 'on');
            set(handles.axesPlot, 'YGrid', 'on');
            set(handles.axesPlot, 'ZGrid', 'on');
    
            set(handles.axesPlot, 'XMinorGrid', 'on');
            set(handles.axesPlot, 'YMinorGrid', 'on');
            set(handles.axesPlot, 'ZMinorGrid', 'on');
    
            set(handles.axesPlot, 'XMinorTick', 'on');
            set(handles.axesPlot, 'YMinorTick', 'on');
            set(handles.axesPlot, 'ZMinorTick', 'on');
    
            set(handles.slider, 'Enable', 'on');
            set(handles.slider, 'Value', 1);
            set(handles.lblZoom, 'String', 'x 1.0');
    
            pan on;
       
            set(gcf, 'CurrentAxes', handles.axesRezultat);
            cla;
            text(.075, .95, 'Center of the circle', 'FontName', 'Tahoma', 'FontSize', 9, 'BackgroundColor', [.7 .9 .7]);
            text(.075, .85, ['A = ', sprintf('%3.1f', Av)], 'FontName', 'Tahoma', 'FontSize', 9, 'BackgroundColor', [.9412 .9412 .9412]);
            text(.075, .78, ['B = ', sprintf('%3.1f', Bv)], 'FontName', 'Tahoma', 'FontSize', 9, 'BackgroundColor', [.9412 .9412 .9412]);    

            text(.075, .63, 'Radius of the circle', 'FontName', 'Tahoma', 'FontSize', 9, 'BackgroundColor', [.7 .9 .7]);
            text(.075, .53, ['R = ', sprintf('%3.1f', Rv)], 'FontName', 'Tahoma', 'FontSize', 9, 'BackgroundColor', [.9412 .9412 .9412]);
    
            text(.075, .38, 'Equation of the circle', 'FontName', 'Tahoma', 'FontSize', 9, 'BackgroundColor', [.7 .9 .7]);
            text(.075, .28, ['(X-', sprintf('%3.1f', Av), ')', '^2 + (Y-', sprintf('%3.1f', Bv), ')', '^2 = ', sprintf('%3.1f', Rv), '^2'], 'FontName', 'Tahoma', 'FontSize', 9, 'BackgroundColor', [.9412 .9412 .9412]);    
        end
    end
end

% -------------------------------------------------------------------------

function slider_Callback(hObject, eventdata, handles)

global LX LY;

value = get(handles.slider, 'Value');
set(handles.lblZoom, 'String', ['x ', sprintf('%1.1f', value)]);
set(handles.axesPlot, 'XLim', [LX(1)/value, LX(2)/value]);
set(handles.axesPlot, 'YLim', [LY(1)/value, LY(2)/value]);

% -------------------------------------------------------------------------

function slider_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% -------------------------------------------------------------------------

function txtA_Callback(hObject, eventdata, handles)

a = str2num(get(hObject, 'String'));

if (isempty(a))
    set(hObject, 'String', '0');
end

% -------------------------------------------------------------------------

function txtA_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------


function txtB_Callback(hObject, eventdata, handles)

b = str2num(get(hObject, 'String'));

if (isempty(b))
    set(hObject, 'String', '1');
end

% -------------------------------------------------------------------------

function txtB_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

