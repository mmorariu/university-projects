function varargout = tried_frenet(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @tried_frenet_OpeningFcn, ...
                   'gui_OutputFcn',  @tried_frenet_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% -------------------------------------------------------------------------

function tried_frenet_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;
set(gcf, 'CurrentAxes', handles.axesPlot);
hold on;

guidata(hObject, handles);

% -------------------------------------------------------------------------

function varargout = tried_frenet_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;

% -------------------------------------------------------------------------

function txtX_Callback(hObject, eventdata, handles)
function txtX_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtY_Callback(hObject, eventdata, handles)
function txtY_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtZ_Callback(hObject, eventdata, handles)
function txtZ_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtP_Callback(hObject, eventdata, handles)

p = str2num(get(hObject, 'String'));

if (isempty(p))
    set(hObject, 'String', '0');
end

guidata(hObject, handles);

% -------------------------------------------------------------------------

function txtP_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function btnOk_Callback(hObject, eventdata, handles)

cla reset;
syms t X Y Z real;
global P T N B c LMax LMin;

X = sym(get(handles.txtX, 'String'));
Y = sym(get(handles.txtY, 'String'));
Z = sym(get(handles.txtZ, 'String'));
P = double(sym(get(handles.txtP, 'String')));

c = [X; Y; Z];

if (isempty(X) | isempty(Y) | isempty(Z) | isempty(P))
    msgbox('Please fill in all the required fields!', 'Error', 'error', 'modal');
else   
        dc = diff(c);
        
        if (isequal(subs(dc, t, P), [0; 0; 0]))
            msgbox(['P = ', sprintf('%d', P), ' is not a regular point. The Frenet frame cannot be constructed here.'],  'Error', 'error', 'modal');
        else            
            n_dc = simple(sqrt(dot(dc, dc)));     
            T = simple(dc / n_dc);
        
            dT = diff(T);
            n_N = simple(sqrt(dot(dT, dT)));
            N = simple(dT / n_N); 
        
            B = cross(T, N);
          
            TP = double(subs(T, t, P));
            NP = double(subs(N, t, P));
            BP = double(subs(B, t, P));
        
            p = P-5:0.05:P+5;
            cP = double(subs(c, t, P));
            
            Min = [min(subs(c(1), t, p)); min(subs(c(2), t, p)); min(subs(c(3), t, p))];
            Max = [max(subs(c(1), t, p)); max(subs(c(2), t, p)); max(subs(c(3), t, p))];
    
            if (diff(Z) == 0)        
            
                plot(subs(c(1), t, p), subs(c(2), t, p));
       
                line([cP(1), cP(1) + TP(1)], [cP(2), cP(2) + TP(2)], 'Color', [0.83, 0.06, 0.10], 'LineWidth', 3);
                line([cP(1), cP(1) + NP(1)], [cP(2), cP(2) + NP(2)], 'Color', [0, 0.66, 0.66], 'LineWidth', 3);
    
                set(handles.lblTRez, 'String', sprintf('[ %-3.2f, %-3.2f ]', TP(1), TP(2)));
                set(handles.lblNRez, 'String', sprintf('[ %-3.2f, %-3.2f ]', NP(1), NP(2)))
                set(handles.lblBRez, 'String', 'NaN');

                pan on;
                rotate3d off;
            else
       
                plot3(subs(c(1), t, p), subs(c(2), t, p), subs(c(3), t, p));
    
                line([cP(1), cP(1) + TP(1)], [cP(2); cP(2) + TP(2)], [cP(3); cP(3) + TP(3)], 'Color', [0.83, 0.06, 0.10], 'LineWidth', 2);
                line([cP(1); cP(1) + NP(1)], [cP(2); cP(2) + NP(2)], [cP(3); cP(3) + NP(3)], 'Color', [0, 0.66, 0.66], 'LineWidth', 2);
                line([cP(1); cP(1) + BP(1)], [cP(2); cP(2) + BP(2)], [cP(3); cP(3) + BP(3)], 'Color', [0.73, 0.69, 0], 'LineWidth', 2);
    
                set(handles.lblTRez, 'String', sprintf('[ %-3.2f, %-3.2f, %-3.2f ]', TP(1), TP(2), TP(3)));
                set(handles.lblNRez, 'String', sprintf('[ %-3.2f, %-3.2f, %-3.2f ]', NP(1), NP(2), NP(3)));
                set(handles.lblBRez, 'String', sprintf('[ %-3.2f, %-3.2f, %-3.2f ]', BP(1), BP(2), BP(3)));
            
                rotate3d on;
            end
        end
    
        LMax = max(Max);
        LMin = min(Min);

        if (LMin == -Inf) LMin = -LMax; end
        if (LMax == Inf) LMax = -LMin; end

        set(gca, 'XLim', [LMin LMax], 'YLim', [LMin LMax], 'ZLim', [LMin LMax]);
        
        xlabel('X');
        ylabel('Y');
        zlabel('Z');

        grid on;
    
        set(handles.txtX, 'Enable', 'off');
        set(handles.txtY, 'Enable', 'off');
        set(handles.txtZ, 'Enable', 'off');
        set(handles.txtP, 'Enable', 'off');
    
        set(handles.btnAnimeaza, 'Visible', 'on');
        set(handles.btnOk, 'Visible', 'off');
        set(handles.slider, 'Value', 1);    
        set(handles.slider, 'Enable', 'on');
end

% -------------------------------------------------------------------------

function btnReset_Callback(hObject, eventdata, handles)

global playing LMin LMax;

playing = false;

set(handles.txtX, 'String', '');
set(handles.txtY, 'String', '');
set(handles.txtZ, 'String', '');
set(handles.txtP, 'String', '');

cla reset;

set(handles.lblTRez, 'String', '[ 0.0, 0.0, 0.0 ]');
set(handles.lblNRez, 'String', '[ 0.0, 0.0, 0.0 ]');
set(handles.lblBRez, 'String', '[ 0.0, 0.0, 0.0 ]');

set(gca, 'XMinorGrid', 'on');
set(gca, 'YMinorGrid', 'on');
set(gca, 'ZMinorGrid', 'on');

set(gca, 'XMinorTick', 'on');
set(gca, 'YMinorTick', 'on');
set(gca, 'ZMinorTick', 'on');

set(gca, 'XLim', [LMin LMax], 'YLim', [LMin LMax], 'ZLim', [LMin LMax]);

set(handles.txtX, 'Enable', 'on');
set(handles.txtY, 'Enable', 'on');
set(handles.txtZ, 'Enable', 'on');
set(handles.txtP, 'Enable', 'on');

set(handles.btnAnimeaza, 'Visible', 'off');
set(handles.btnOk, 'Visible', 'on');
set(handles.slider, 'Value', 1);
set(handles.slider, 'Enable', 'off');

pan off;
rotate3d off;

uicontrol(handles.txtX);

% -------------------------------------------------------------------------

function btnAnimeaza_Callback(hObject, eventdata, handles)

global P T N B c LMax LMin playing;
syms t;

hold off;

value = get(handles.slider, 'Value');

set(handles.slider, 'Enable', 'off');

p = P-5:0.05:P+5;
r = P-2:0.05:P+2;

playing = true;

for i = 1:size(r, 2)
    
    if (playing == false) 
        return;
    end
    
    Tr = double(subs(T, t, r(i)));
    Nr = double(subs(N, t, r(i)));
    Br = double(subs(B, t, r(i)));
    
    cr = double(subs(c, t, r(i)));
    
    if (diff(c(3)) == 0)
        plot(subs(c(1), t, p), subs(c(2), t, p));

        line([cr(1), cr(1) + Tr(1)], [cr(2), cr(2) + Tr(2)], 'Color', [0.83, 0.06, 0.10], 'LineWidth', 3);
        line([cr(1), cr(1) + Nr(1)], [cr(2), cr(2) + Nr(2)], 'Color', [0, 0.66, 0.66], 'LineWidth', 3);
        
        set(handles.lblTRez, 'String', sprintf('[ %-3.2f, %-3.2f ]', Tr(1), Tr(2)));
        set(handles.lblNRez, 'String', sprintf('[ %-3.2f, %-3.2f ]', Nr(1), Nr(2)))
        set(handles.lblBRez, 'String', 'NaN');        
        
        grid on;
        set(gca, 'XLim', [LMin/value LMax/value], 'YLim', [LMin/value LMax/value], 'ZLim', [LMin/value LMax/value]);

        getframe;
        
        rotate3d off;
    else
        plot3(subs(c(1), t, p), subs(c(2), t, p), subs(c(3), t, p));
        
        line([cr(1), cr(1) + Tr(1)], [cr(2), cr(2) + Tr(2)], [cr(3), cr(3) + Tr(3)], 'Color', [0.83, 0.06, 0.10], 'LineWidth', 2);
        line([cr(1), cr(1) + Nr(1)], [cr(2), cr(2) + Nr(2)], [cr(3), cr(3) + Nr(3)], 'Color', [0, 0.66, 0.66], 'LineWidth', 2);
        line([cr(1), cr(1) + Br(1)], [cr(2), cr(2) + Br(2)], [cr(3), cr(3) + Br(3)], 'Color', [0.73, 0.69, 0], 'LineWidth', 2);

        set(handles.lblTRez, 'String', sprintf('[ %-3.2f, %-3.2f, %-3.2f ]', Tr(1), Tr(2), Tr(3)));
        set(handles.lblNRez, 'String', sprintf('[ %-3.2f, %-3.2f, %-3.2f ]', Nr(1), Nr(2), Nr(3)));
        set(handles.lblBRez, 'String', sprintf('[ %-3.2f, %-3.2f, %-3.2f ]', Br(1), Br(2), Br(3)));
        
        grid on;
        set(gca, 'XLim', [LMin/value LMax/value], 'YLim', [LMin/value LMax/value], 'ZLim', [LMin/value LMax/value]);
   
        getframe;
        
        rotate3d on;
    end    
end

set(handles.btnAnimeaza, 'Visible', 'off');
set(handles.btnOk, 'Visible', 'on');

set(handles.txtX, 'Enable', 'on');
set(handles.txtY, 'Enable', 'on');
set(handles.txtZ, 'Enable', 'on');
set(handles.txtP, 'Enable', 'on');
set(handles.slider, 'Enable', 'on');

% -------------------------------------------------------------------------

function slider_Callback(hObject, eventdata, handles)

global LMin LMax;

value = get(handles.slider, 'Value');
set(gca, 'XLim', [LMin/value LMax/value], 'YLim', [LMin/value LMax/value], 'ZLim', [LMin/value LMax/value]);

% -------------------------------------------------------------------------

function slider_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% t, cos(t)*sin(t), 0
% t, cos(t), t - 0