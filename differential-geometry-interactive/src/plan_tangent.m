function varargout = plan_tangent(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @plan_tangent_OpeningFcn, ...
                   'gui_OutputFcn',  @plan_tangent_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% -------------------------------------------------------------------------

function plan_tangent_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;

colormap bone;
set(gcf, 'CurrentAxes', handles.axesPlot);
colorbar;

% -------------------------------------------------------------------------

function varargout = plan_tangent_OutputFcn(hObject, eventdata, handles) 

% -------------------------------------------------------------------------

function txtX_Callback(hObject, eventdata, handles)
function txtX_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtY_Callback(hObject, eventdata, handles)
function txtY_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtZ_Callback(hObject, eventdata, handles)
function txtZ_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtU_Callback(hObject, eventdata, handles)

U = str2num(get(hObject, 'String'));

if (isempty(U))
    set(hObject, 'String', '0');
end

guidata(hObject, handles);

% -------------------------------------------------------------------------

function txtU_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtV_Callback(hObject, eventdata, handles)

V = str2num(get(hObject, 'String'));

if (isempty(V))
    set(hObject, 'String', '0');
end

guidata(hObject, handles);

function txtV_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function btnOk_Callback(hObject, eventdata, handles)

syms u v x y z real;
rotate3d off;
cla;
set(handles.lblEcuatie, 'String', 'NaN');

X = sym(get(handles.txtX, 'String'));
Y = sym(get(handles.txtY, 'String'));
Z = sym(get(handles.txtZ, 'String'));

U = str2num(get(handles.txtU, 'String'));
V = str2num(get(handles.txtV, 'String'));

if (isempty(X) | isempty(Y) | isempty(Z) | isempty(U) | isempty(V))
    msgbox('Please fill in all the required fields!', 'Error', 'error', 'modal');
else
    S = [X; Y; Z];
    Ru = jacobian(S, u);
    Rv = jacobian(S, v);
    J = [Ru Rv];
    
    if (rank(J) == 2)
        ABC = subs(simple(cross(Ru, Rv)), {u, v}, {U, V});
    
        if (isequal(ABC, [0; 0; 0]) | isnan(ABC(1)) | isnan(ABC(2)) | isnan(ABC(3)))
            msgbox('The chosen point is not regular. Cannot build the tangent plane.', 'Error', 'error', 'modal');
        else
            set(handles.lblEcuatie, 'String', 'Processing data... [0%]');
            drawnow update;

            iu = U-5:0.2:U+5;
            iv = V-5:0.2:V+5;
            [vu, vv] = meshgrid(iu, iv);
        
            max_XYZ = [max(subs(X, {u, v}, {iu, iv})); max(subs(Y, {u, v}, {iu, iv})); max(subs(Z, {u, v}, {iu, iv}))];
            min_XYZ = [min(subs(X, {u, v}, {iu, iv})); min(subs(Y, {u, v}, {iu, iv})); min(subs(Z, {u, v}, {iu, iv}))];
                    
            global LMin LMax;
            
            LMin = min(min_XYZ);
            LMax = max(max_XYZ);
                   
            set(handles.lblEcuatie, 'String', 'Processing data... [25%]');
            drawnow update;
                          
            surf(subs(X, {u, v}, {vu, vv}), subs(Y, {u, v}, {vu, vv}), subs(Z, {u, v}, {vu, vv}), 'EdgeAlpha', .2);
            colorbar;

            set(handles.lblEcuatie, 'String', 'Processing data... [50%]');
            drawnow update;

            rotate3d on;    
            hold on;   
                
            XP = subs(X, {u, v}, {U, V});
            YP = subs(Y, {u, v}, {U, V});
            ZP = subs(Z, {u, v}, {U, V});
            plot3(XP, YP, ZP, 'x', 'Color', 'r', 'LineWidth', 8);

            set(handles.lblEcuatie, 'String', 'Processing data... [75%]');    
            drawnow update;
            
            if (ABC(3) ~= 0)
                [vx, vy] = meshgrid(XP-1:0.2:XP+1, YP-1:0.2:YP+1);        
                vz = -1/ABC(3)*(ABC(1)*(vx-XP)+ABC(2)*(vy-YP)-ABC(3)*ZP);
            elseif (ABC(2) ~= 0)
                [vx, vz] = meshgrid(XP-1:0.2:XP+1, ZP-1:0.2:ZP+1);
                vy = -1/ABC(2)*(ABC(1)*(vx-XP)+ABC(3)*(vz-ZP)-ABC(2)*YP);
            else
                [vy, vz] = meshgrid(YP-1:0.2:YP+1, ZP-1:0.2:ZP+1);
                vx = -1/ABC(1)*(ABC(2)*(vy-YP)+ABC(3)*(vz-ZP)-ABC(1)*XP);
            end
        
            surf(vx, vy, vz, 'FaceColor', [1 0 0], 'FaceAlpha', .5, 'EdgeAlpha', .2);   
            set(handles.lblEcuatie, 'String', 'Processing data... [100%]');
            drawnow update;
                       
            semn = ['-'; '-'; '-'; '-'; '-'];
            
            if (XP < 0) semn(1) = '+'; XP = -XP; end
            if (YP < 0) semn(2) = '+'; YP = -YP; end
            if (ZP < 0) semn(3) = '+'; ZP = -ZP; end
            if (ABC(2) < 0) semn(4) = '+'; ABC(2) = -ABC(2); end
            if (ABC(3) < 0) semn(5) = '+'; ABC(3) = -ABC(3); end
        
            set(handles.lblEcuatie, 'String', sprintf('%5.1f * (X %c %5.1f) %c \n%5.1f * (Y %c %5.1f) %c \n%5.1f * (Z %c %5.1f)\n = 0', ABC(1), semn(1), XP, semn(4), ABC(2), semn(2), YP, semn(5), ABC(3), semn(3), ZP));
                
            set(handles.axesPlot, 'XLim', [LMin, LMax]);
            set(handles.axesPlot, 'YLim', [LMin, LMax]);  
            set(handles.axesPlot, 'ZLim', [LMin, LMax]);
            set(handles.chkIncadrare, 'Enable', 'on');
        
            xlabel('X');
            ylabel('Y');
            zlabel('Z');
        end
    else
        msgbox('The chosen parametrization does not describe a surface (rank(J) must be equal to 2).', 'Error', 'error', 'modal');
    end
end

function btnReset_Callback(hObject, eventdata, handles)

set(handles.txtX, 'String', '');
set(handles.txtY, 'String', '');
set(handles.txtZ, 'String', '');

set(handles.txtU, 'String', '');
set(handles.txtV, 'String', '');

rotate3d off;
cla;
view(2);

set(handles.lblEcuatie, 'String', 'NaN');
set(handles.chkIncadrare, 'Enable', 'off');
set(handles.chkIncadrare, 'Value', 0);

uicontrol(handles.txtX);

function lblEcuatie_Callback(hObject, eventdata, handles)
% hObject    handle to lblEcuatie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lblEcuatie as text
%        str2double(get(hObject,'String')) returns contents of lblEcuatie as a double


% --- Executes during object creation, after setting all properties.
function lblEcuatie_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lblEcuatie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chkIncadrare.
function chkIncadrare_Callback(hObject, eventdata, handles)

global LMin LMax;

val = get(hObject, 'Value');

if (val == 1)
    set(handles.axesPlot, 'XLimMode', 'auto');
    set(handles.axesPlot, 'YLimMode', 'auto');
    set(handles.axesPlot, 'ZLimMode', 'auto');
else
    set(handles.axesPlot, 'XLim', [LMin, LMax]);
    set(handles.axesPlot, 'YLim', [LMin, LMax]);
    set(handles.axesPlot, 'ZLim', [LMin, LMax]);
end
