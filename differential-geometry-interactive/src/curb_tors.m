function varargout = curb_tors(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @curb_tors_OpeningFcn, ...
                   'gui_OutputFcn',  @curb_tors_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% -------------------------------------------------------------------------

function curb_tors_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;
guidata(hObject, handles);

% -------------------------------------------------------------------------

function varargout = curb_tors_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;

% -------------------------------------------------------------------------

function txtX_Callback(hObject, eventdata, handles)
function txtX_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtY_Callback(hObject, eventdata, handles)
function txtY_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtZ_Callback(hObject, eventdata, handles)
function txtZ_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtT_Callback(hObject, eventdata, handles)

t = str2num(get(hObject, 'String'));

if (isempty(t))
    set(hObject, 'String', '0');
end

guidata(hObject, handles);

% -------------------------------------------------------------------------

function txtT_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function btnOk_Callback(hObject, eventdata, handles)

syms t X Y Z;

X = sym(get(handles.txtX, 'String')); 
Y = sym(get(handles.txtY, 'String'));
Z = sym(get(handles.txtZ, 'String'));
T = str2num(get(handles.txtT, 'String'));

if (isempty(X) | isempty(Y) | isempty(Z) | isempty(T))
    h = msgbox('Please fill in all the required fields!', 'Error', 'error', 'modal');
else
    set(handles.lblLoading, 'String', 'Processing data... [0%]');
    drawnow update;
    
    c = [X; Y; Z];
    c1 = jacobian(c, t);
    c2 = jacobian(c1, t);
    c3 = jacobian(c2, t);
    
    set(handles.lblLoading, 'String', 'Processing data... [16%]');
    drawnow update;    
    
    A = det([diff(Y) diff(Z); diff(Y, 2) diff(Z, 2)]);
    B = -det([diff(X) diff(Z); diff(X, 2) diff(Z, 2)]);
    C = det([diff(X) diff(Y); diff(X, 2) diff(Y, 2)]);
    
    set(handles.lblLoading, 'String', 'Processing data... [25%]');
    drawnow update;

    K1 = sqrt(A^2 + B^2 + C^2);
    K2 = sqrt(diff(X)^2 + diff(Y)^2 + diff(Z)^2) ^ 3;
    KF = simple(K1 / K2);
    K = subs(K1, t, T) / subs(K2, t, T);
    
    set(handles.lblLoading, 'String', 'Processing data... [32%]');
    drawnow update;        

    TAU1 = det([c1 c2 c3]);
    TAU2 = A^2 + B^2 + C^2;
    TAUF = simple(TAU1 / TAU2);
    TAU = subs(TAU1, t, T) / subs(TAU2, t, T);
    
    set(handles.lblLoading, 'String', 'Processing data... [50%]');
    drawnow update;    
    
    set(gcf, 'CurrentAxes' , handles.axesRezultat);
    cla;
    text(.02, .90, 'Curvature ', 'FontName', 'Tahoma', 'FontSize', 9, 'BackgroundColor', [.7 .9 .7], 'Interpreter', 'tex');
    text(.02, .75, ['$\mathtt{K(t) = }', latex(KF), '$'], 'FontName', 'Tahoma', 'FontSize', 12, 'BackgroundColor', [.9412 .9412 .9412], 'Interpreter', 'latex');
    text(.02, .60, ['$\mathtt{K(P) = }$', sprintf('%3.1f', K)], 'FontName', 'Tahoma', 'FontSize', 12, 'BackgroundColor', [.9412 .9412 .9412], 'Interpreter', 'latex');
    
    set(handles.lblLoading, 'String', 'Processing data... [66%]');
    drawnow update;

    text(.02, .45, 'Torsion ', 'FontName', 'Tahoma', 'FontSize', 9, 'BackgroundColor', [.7 .9 .7], 'Interpreter', 'tex');
    text(.02, .30, ['$\mathtt{\tau(t) = \ }', latex(TAUF), '$'], 'FontName', 'Tahoma', 'FontSize', 12, 'BackgroundColor', [.9412 .9412 .9412], 'Interpreter', 'latex');
    text(.02, .15, ['$\mathtt{\tau(P) =\ }$', sprintf('%3.1f', TAU)], 'FontName', 'Tahoma', 'FontSize', 12, 'BackgroundColor', [.9412 .9412 .9412], 'Interpreter', 'latex');
    
    set(handles.lblLoading, 'String', 'Processing data... [75%]');
    drawnow update;
    
    set(gcf, 'CurrentAxes', handles.axesPlot);
    x = linspace(T-5, T+5, 1000);
    XP = subs(X, t, x).*ones(size(x));
    YP = subs(Y, t, x).*ones(size(x));
    ZP = subs(Z, t, x).*ones(size(x));
    
    set(handles.lblLoading, 'String', 'Processing data... [80%]');
    drawnow update;   

   if (diff(Z) == 0)
       plot(XP, YP, 'LineWidth', 2, 'Color', 'k');
   else
       plot3(XP, YP, ZP, 'LineWidth', 1.5, 'Color', 'k');
   end
   
    set(handles.lblLoading, 'String', 'Processing data... [100%]');
    drawnow update;  
    grid on;
    set(handles.lblLoading, 'String', 'Enter a parametrization and press OK to continue.');   
    drawnow update;
end

% -------------------------------------------------------------------------

function btnInstructiuni_Callback(hObject, eventdata, handles)

msgbox(sprintf('The program computes the formula for the curvature and torsion of a given curve at any point, as well as for a particular P point.\n\nThree functions X(t), Y(t) and Z(t) and a value for P have to be entered in the appropriate fields.\n\nThe formulas for curvature and torsion will be displayed, then their values at P, as well as the graph of the chosen curve.'), 'Instructions', 'help', 'modal');
