function varargout = suprafrot(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @suprafrot_OpeningFcn, ...
                   'gui_OutputFcn',  @suprafrot_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% -------------------------------------------------------------------------

function suprafrot_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;
guidata(hObject, handles);

% -------------------------------------------------------------------------

function varargout = suprafrot_OutputFcn(hObject, eventdata, handles) 

varargout{1} = handles.output;

% -------------------------------------------------------------------------

function txtX_Callback(hObject, eventdata, handles)
function txtX_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtY_Callback(hObject, eventdata, handles)
function txtY_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function btnOk_Callback(hObject, eventdata, handles)

syms t real;

X = sym(get(handles.txtX, 'String'));
Y = sym(get(handles.txtY, 'String'));

A = double(sym(get(handles.txtA, 'String')));
B = double(sym(get(handles.txtB, 'String')));
N = double(sym(get(handles.txtN, 'String')));

if (isempty(X) || isempty(Y) || isempty(A) ||isempty(B))
    msgbox('Please fill in all the required fields!', 'Error', 'warn', 'modal');
else
    if (N < 5)
        msgbox('Please choose a value for N so that N >= 5!', 'Error', 'warn', 'modal');
    else
        if (A >= B)
            msgbox('Please choose two values for A and B so that A < B!', 'Error', 'warn', 'modal');
        else
            AX = get(handles.popupAxes, 'Value');

            axesC = handles.axesCurba;
            axesS = handles.axesSuprafata;

            T = linspace(A, B, N);
            plot(axesC, subs(X, t, T).*ones(size(T)), subs(Y, t, T).*ones(size(T)), 'Color', 'k', 'LineWidth', 1.2);
    
            setGrids(axesC);
            setLabels(axesC);
            rotate3d(axesC, 'off');
            axis(axesC, 'equal');

            [T, U] = meshgrid(linspace(A, B, N), linspace(0, 2*pi, N));

            switch AX
                case 1
                    X1 = subs(Y, t, T).*cos(U);
                    Y1 = subs(Y, t, T).*sin(U);
                    Z1 = subs(X, t, T).*ones(size(X1));

                    surf(axesS, Z1, Y1, X1, 'EdgeAlpha', .2);
                case 2
                    X1 = subs(X, t, T).*cos(U);
                    Y1 = subs(X, t, T).*sin(U);
                    Z1 = subs(Y, t, T).*ones(size(X1));
            
                    surf(axesS, X1, Y1, Z1, 'EdgeAlpha', .2);
            end
    
            setGrids(axesS);
            rotate3d(axesS, 'on');
            axis(axesS, 'equal');
        end
    end
end

% -------------------------------------------------------------------------

function txtA_Callback(hObject, eventdata, handles)

a = str2num(get(hObject, 'String'));

if (isempty(a))
    set(hObject, 'String', '0');
end

% -------------------------------------------------------------------------

function txtA_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtN_Callback(hObject, eventdata, handles)

n = str2num(get(hObject, 'String'));

if (isempty(n))
    set(hObject, 'String', '5');
end

% -------------------------------------------------------------------------

function txtN_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function txtB_Callback(hObject, eventdata, handles)

b = str2num(get(hObject, 'String'));

if (isempty(b))
    set(hObject, 'String', '1');
end

% -------------------------------------------------------------------------

function txtB_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function popupAxes_Callback(hObject, eventdata, handles)
function popupAxes_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% -------------------------------------------------------------------------

function setGrids(axesObject)
    set(axesObject, 'XGrid', 'on');
    set(axesObject, 'YGrid', 'on');
    set(axesObject, 'ZGrid', 'on');
    
% -------------------------------------------------------------------------    
   
function setLabels(axesObject)
    xlabel(axesObject, 'X');
    ylabel(axesObject, 'Y');
    zlabel(axesObject, 'Z');
    
    % (t>0) - (t>.5) + t*(t>1), t, 0-5, OX
    % (t>-1) - (t>6), t/15, 0-10, OX
    % t, t^2*log(t), 0-2, OY