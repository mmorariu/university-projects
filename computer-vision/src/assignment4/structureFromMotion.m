% STRUCTUREFROMMOTION Reconstruct the 3D structure of an object based on a
% set of tracked points
%
%   [M S] = structureFromMotion(Points, show_output, color)
%
%   Parameters are:
%     Points        =   The set of tracked points
%     show_output   =   Whether or not to show the result
%     color         =   The color used to display the output, which
%                       consists in a set of 3D points
%
%   Returns:
%     M             =   Motion matrix
%     S             =   Shape matrix
function [M S] = structureFromMotion(Points, show_output, color)

if nargin < 2
    show_output = false;
    color = 'b';
elseif nargin < 3
    color = 'b';
end

D = zeros(2*size(Points, 3), size(Points, 2));

% Create the D matrix

for i = 1:size(Points, 3)
    D(2*i - 1, :) = Points(1, :, i) - mean(Points(1, :, i));
    D(2*i, :) = Points(2, :, i) - mean(Points(2, :, i));
end

% Factorize D	

[U, W, V] = svd(D);

% Select the right rows and columns from U, W and V, as explained in the
% lecture

U3 = U(:, 1:3);
W3 = W(1:3, 1:3);
V3 = V(:, 1:3);

% Compute M and S

M = U3;
S = W3*V3';

if show_output    
    plot3(S(1, :), S(2, :), S(3, :), [color, '.']);
    xlabel('X');
    ylabel('Y');
    zlabel('Z');
    
    grid on;
    
    % Reverse the Z axis so that the 3D structure becomes easier to
    % manipulate
    
    set(gca, 'zdir', 'reverse');
end

end
