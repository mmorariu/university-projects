% LUCASKANADE Calculate optical flow for points of interest
%
% V_out = lucasKanade(I1,I2, x_in, y_in, show_output)
%   Calculate the velocities for points in (y_in,x_in) based on 
%   the optical flow method Lucas-Kanade. Uses 8x8 regions.
%
%   Parameters are:
%     I1            =   The previous frame (image)
%     I2            =   The current frame (image)
%     x_in          =   The x coordinates of the points
%     y_in          =   The y coordinates of the points
%     show_output   =   Whether or not to show the result
%
%   Returns:
%     V_out         =   Velocities per point (as indexed in y_in)
function V_out = lucasKanade(I1, I2, x_in, y_in, show_output)

if nargin < 5
    show_output = false;
end

I = I1;

% Convert image to grayscale, if necessary

if size(I1, 3) > 1
    I1 = rgb2gray(I1);
end

if size(I2, 3) > 1
    I2 = rgb2gray(I2);
end

I1 = im2double(I1);
I2 = im2double(I2);

% Compute image derivatives - we found this technique of averaging the
% derivatives Ix and Iy on the Internet (I1 and I2 are very similar
% to each other) and we noticed it yields better results compared to
% the one where we were convolving the image with a derivative of Gaussian
% kernel. For this reason, we decided to use it in all our experiments.

[Ix1 Iy1] = gradient(I1);
[Ix2 Iy2] = gradient(I2);

Ix = (Ix1 + Ix2) / 2;
Iy = (Iy1 + Iy2) / 2;

% Compute temporal derivative

It = I2 - I1;

no_points = numel(x_in);
V_out = zeros(no_points, 2);
[height, width] = size(I1);

for i = 1:numel(y_in)
    
    % Crop a 15x15 region from the image
    
    Ix_region = Ix(max(y_in(i)-7, 0):min(y_in(i)+7, height), max(x_in(i)-7, 0):min(x_in(i)+7, width))';
    Iy_region = Iy(max(y_in(i)-7, 0):min(y_in(i)+7, height), max(x_in(i)-7, 0):min(x_in(i)+7, width))';
    It_region = It(max(y_in(i)-7, 0):min(y_in(i)+7, height), max(x_in(i)-7, 0):min(x_in(i)+7, width))';
    
    % Compute A, A' and b for the region of interest
    
    A = [Ix_region(:) Iy_region(:)];
    b = -It_region(:);
    
    % Solve the system and store the returned v vector
    
    v = pinv(A'*A)*A'*b;
    V_out(i, :) = v';
    
end

% Plot the optical flow

if show_output    
    imshow(I, []);
    hold on;
    quiver(x_in, y_in, V_out(:, 1)', V_out(:, 2)');
end

end
