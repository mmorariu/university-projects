close all;

I1 = imread('sphere1.ppm');
I2 = imread('sphere2.ppm');
[height, width, dim] = size(I1);
[x, y] = meshgrid(8:15:width, 8:15:height);

figure;
subplot(1, 2, 1);
imshow(I1);
title('First image of sphere');
subplot(1, 2, 2);
imshow(I2);
title('Second image of sphere');

figure;
V = lucasKanade(I1, I2, x(:)', y(:)', true);
title('Estimated optical flow of sphere image');

I1 = imread('texture1.pgm');
I2 = imread('texture2.pgm');
[height, width, dim] = size(I1);
[x, y] = meshgrid(8:15:width, 8:15:height);

figure;
subplot(1, 2, 1);
imshow(I1);
title('First image of texture');
subplot(1, 2, 2);
imshow(I2);
title('Second image of texture');

figure;
V = lucasKanade(I1, I2, x(:)', y(:)', true);
title('Estimated optical flow of texture image');