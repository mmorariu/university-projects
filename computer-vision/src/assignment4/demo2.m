M = load('model house/measurement_matrix.txt');

display('Warning! To create the video, you need to have a version of MATLAB installed that supports the VideoWriter class.');
key = input('Press any key to continue...');

display('Creating video file...');
[err points] = trackPoints('model house', M, 'house.avi');
display('Done');