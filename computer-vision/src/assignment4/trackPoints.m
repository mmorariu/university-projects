% TRACKPOINTS track points based on optical flow
%
% FrameError = trackPoints(FramesDir, GroundTruth)
%   Track points given in the first frame of GroundTruth over
%   all jpg images in FramesDir.
%
%   Parameters are:
%     FramesDir   =   Path to images
%     GroundTruth =   Matrix containing ground truth
%
%   Returns:
%     FrameError  =   Vector of average error values per frame
%
% FrameError = trackPoints(FramesDir, GroundTruth, Movie)
%   Track points given in the first frame of GroundTruth over
%   all jpg images in FramesDir. Record the tracking and
%   store it in an avi movie with the filename Movie.
%
%   Parameters are:
%     FramesDir   =   Path to images
%     GroundTruth =   Matrix containing ground truth
%     Movie       =   Filename of the movie
%
%   Returns:
%     FrameError  =   Vector of average error values per frame
%
% See also:
% LUCASKANADE
function [FrameError Points] = trackPoints(FramesDir, GroundTruth, Movie)
  if nargin == 2
    Movie = [];
  end
  
  display('Tracking points...');

  tic;
  [M, N] = size(GroundTruth);
  Points = zeros(2,N,M/2);
  
  % Read in files in FramesDir
  Files = struct2cell(dir(FramesDir));

  % Filter out jpg files
  Frames = {};
  for f = 1:length(Files)
    file = Files{1,f};
    if length(file) > 4 && strcmp(file(end-3:end), '.jpg')
      Frames{end+1} = fullfile(FramesDir, file);
    end
  end

  % Container for per-frame errors
  FrameError = zeros(M/2,1);

  % Extract N points from the 1st frame
  points = GroundTruth(1:2,:);
  Points(:,:,1) = points;
  
  %Movie = moviein(M/2);
  if ~isempty(Movie)
    mov = VideoWriter(Movie);
    open(mov);
  end

  % Track these points with Lucas-Kanade method for optical flow estimation.
  for m = 4:2:M
    frame1 = imread(Frames{(m-2)/2});
    frame2 = imread(Frames{m/2});
    
    % Find optical flow in this frame compared to the previous frame
    % for now, assume this returns V(X, Y, dir)
    V = lucasKanade(frame1, frame2, round(points(1,:)'), round(points(2,:)'), 0);

    points(1:2, :) = points(1:2, :) + V(:, 1:2)';
    Points(1:2, :, m/2) = points(1:2, :);
    
    % Compute the distance between the predicted points and the ground
    % truth
    dist = sum(sqrt((GroundTruth(m-1, :) - points(1, :)).^2 + (GroundTruth(m, :) - points(2, :)).^2));

    if ~isempty(Movie)
      imshow(frame2);
      hold on;
      h1 = plot(GroundTruth(m-1,:), GroundTruth(m,:),'b.');
      P = reshape(Points(:,:,1:(m/2)), 2, (m/2)*N);
      plot(P(1,:), P(2,:),'y.');
      h3 = plot(points(1,:), points(2,:),'r.');
      legend([h1 h3], 'Ground truth', 'Predicted points');
      Frame = getframe;
      writeVideo(mov, Frame);
      hold off;
    end

    % Frame error
    FrameError(m/2) = dist/N;
  end

  if ~isempty(Movie)
    close(mov);
  end
  
  display('Done');
  toc;
end
