close all;
G = load('model house/measurement_matrix.txt');

[err points] = trackPoints('model house', G);
figure; 
[M S] = structureFromMotion(points, true, 'r');
title('Reconstruction based on the predicted points');

R = zeros(2, 215, 101);

for i = 1:101
    R(1, 1:215, i) = G(2*(i-1) + 1, :);
    R(2, 1:215, i) = G(2*i, :);
end

figure;
[M S] = structureFromMotion(R, true, 'b');
title('Reconstruction based on the ground-truth points');