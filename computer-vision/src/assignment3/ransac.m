% [best_no_inliers, best_inliers, best_tform] = RANSAC(I1, I2, show_transformation, show_intermediate_transf)
% 
% Finds the best affine transformation of the first image compared to the second image.
%
% Algorithm:
%  * N times:
%   * pick P random matches
%   * find the transformation
%   * count how many inliers there are
%  * Keep the transformation with the highest inlier count
%
% Parameters are:
%   I1                        =   First image
%   I2                        =   Second image
%   show_transformation       =   Whether or not to show the transformed I1 image
%   show_intermediate_transf  =   Whether or not to show the transformation of each iteration
% 
% Returns:
%   best_no_inliers           =   The number of inliers when using the best transformation found
%   best_inliers              =   The inliers when using the best transformation found
%   best_tform                =   The best transformation found, to be used in the function imtransform
%
% See also:
% IMTRANSFORM
function [best_no_inliers best_inliers best_tform] = ransac(I1, I2, show_transformation, show_intermediate_transf)

if nargin < 3
    show_transformation = false;
    show_intermediate_transf = false;
elseif nargin < 4
    show_intermediate_transf = false;
end

if size(I1, 3) > 1
    I1 = rgb2gray(I1);
end

if size(I2, 3) > 1
    I2 = rgb2gray(I2);
end

I1 = single(I1);
I2 = single(I2);

display('Computing SIFT descriptors...');

[keypoints1 descriptors1] = vl_sift(I1);
[keypoints2 descriptors2] = vl_sift(I2);

display('Done');
display('Matching SIFT descriptors...');

[matches] = vl_ubcmatch(descriptors1, descriptors2, 3.5);

display('Done');

T = size(matches, 2);
N = 100;
P = 3;
A = zeros(2*P, 6);
b = zeros(2*P, 1);

best_no_inliers = 0;
best_inliers = [];
best_M = [];
best_t = [];

display('Applying RANSAC to compute transformation parameters...');

%  N times
%   pick P random matches
%   find the transformation
%   count how many inliers there are
%  Keep the transformation with the highest inlier count
for i = 1:N
    P_idx = randperm(T);
    
    for j = 0:P-1
        x = keypoints1(1, matches(1, P_idx(j + 1)));
        y = keypoints1(2, matches(1, P_idx(j + 1)));
        
        x_prime = keypoints2(1, matches(2, P_idx(j + 1)));
        y_prime = keypoints2(2, matches(2, P_idx(j + 1)));
        
        A(j*2 + 1, :) = [x y 0 0 1 0];
        A(j*2 + 2, :) = [0 0 x y 0 1];
        
        b(j*2 + 1) = x_prime;
        b(j*2 + 2) = y_prime;
    end
    
    params = pinv(A)*b;
    M = [params(1) params(2); params(3) params(4)];
    t = [params(5); params(6)];
    
    transf_keypoints = M * keypoints1(1:2, :);
    transf_keypoints(1, :) = transf_keypoints(1, :) + t(1);
    transf_keypoints(2, :) = transf_keypoints(2, :) + t(2);
    
    if show_intermediate_transf
        plotTransformation(I1, I2, keypoints1, transf_keypoints, matches);
        waitforbuttonpress;
    end
    
    [no_inliers inliers] = countInliers(transf_keypoints, keypoints2, matches);
    
    if no_inliers >= best_no_inliers
        best_no_inliers = no_inliers;
        best_inliers = inliers;
        best_M = M;
        best_t = t;
    end
end

% Store the best transformation
best_tform = maketform('affine', [best_M' [0; 0]; best_t' 1]);
display('Done');

if show_transformation
    figure;
    subplottight(1, 3, 1);
    imshow(I1, []);
    title('Initial image');
    subplottight(1, 3, 2);
    imshow(I2, []);
    title('Target image');
    subplottight(1, 3, 3);
    imshow(imtransform(I1, best_tform), []);
    title('Transformed image');
end
