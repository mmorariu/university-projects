function h = subplottight(n, m, i)
    [c, r] = ind2sub([m n], i);
    h = subplot('Position', [0.02 + (c-1)/m, 0.04 + 1-(r)/n, 1/m - 0.04, 1/n - 0.02])
end