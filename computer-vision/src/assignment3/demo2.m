fprintf('In this demo, image stitching using affine transformations will be performed.\nBefore continuing, please make sure that VLFeat (http://www.vlfeat.org/) is installed and that MATLAB can access its functions.\n');
key = input('Press any key to continue...');

left_img = imread('left.jpg');
right_img = imread('right.jpg');

o = stitchImages(left_img, right_img, 'affine');