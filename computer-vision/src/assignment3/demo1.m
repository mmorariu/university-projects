fprintf('In this demo, image alignment using the RANSAC algorithm will be performed.\nBefore continuing, please make sure that VLFeat (http://www.vlfeat.org/) is installed and that MATLAB can access its functions.\n');
key = input('Press any key to continue...');

left_img = imread('boat/img1.pgm');
right_img = imread('boat/img2.pgm');

[bni bi bt] = ransac(left_img, right_img, true);

left_img = imread('boat/img1.pgm');
right_img = imread('boat/img3.pgm');

[bni bi bt] = ransac(left_img, right_img, true);

left_img = imread('boat/img1.pgm');
right_img = imread('boat/img4.pgm');

[bni bi bt] = ransac(left_img, right_img, true);