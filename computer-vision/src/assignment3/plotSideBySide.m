function plotSideBySide(I1, I2)

[h1, w1] = size(I1);
[h2, w2] = size(I2);

h = max(h1, h2);
w = w1 + w2;

I = zeros(h, w);
I(1:h1, 1:w1) = I1(:, :);
I(1:h2, w1+1:w) = I2(:, :);

imshow(I, []);

end