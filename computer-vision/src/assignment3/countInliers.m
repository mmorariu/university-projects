% [no_inliers, inliers] = COUNTINLIERS(transf_keypoints, keypoints2, matches)
%
% Count matched keypoints that end up within 10 pixels of their 
% transformed counterparts after being transformed themselves.
%
% Parameters are:
%   transf_keypoints  =   Transformed keypoints of the first image 
%   keypoints2        =   Keypoints of the second image
%   matches           =   Matches as returned by vl_ubcmatch
%
% Returns:
%   no_inliers        =   Number of inliers found
%   inliers           =   Inliers found
%
% See also:
% VL_UBCMATCH
function [no_inliers inliers] = countInliers(transf_keypoints, keypoints2, matches)

diff = [transf_keypoints(1, matches(1, :)) - keypoints2(1, matches(2, :)); ...
        transf_keypoints(2, matches(1, :)) - keypoints2(2, matches(2, :))];
distance = sqrt(diff(1, :).^2 + diff(2, :).^2);

inliers = diff;
inliers(1, :) = inliers(1, :) + keypoints2(1, matches(2, :));
inliers(2, :) = inliers(2, :) + keypoints2(2, matches(2, :));
no_inliers = sum(distance <= 10);

end
