% out_img = STITCHIMAGES(left_img, right_img, type)
%
% Parameters are:
%   left_img    =   Left image that needs to be stiched
%   right_img   =   Right image that needs to be stiched
%   type        =   Type of expected transformation: `affine' or `projective'
%
% Returns:
%   out_img     =   Stitched image
%
% See also:
% RANSAC, PROJECTIVE
function out_img = stitchImages(left_img, right_img, type)

if nargin < 3
    error('Please specify the type of transformation (affine/projective) that will be performed.');
else
    if ~isequal(upper(type), 'PROJECTIVE') && ~isequal(upper(type), 'AFFINE')
        error('Unknown transformation type.');
    end
end

% Find transformation between left and right image
% and get the transformed right image
if isequal(upper(type), 'AFFINE')
    [bni, bi, tform] = ransac(right_img, left_img);
    [b, x_data, y_data] = imtransform(right_img, tform);
else
    tform = projective(right_img, left_img);
    [b, x_data, y_data] = imtransform(right_img, tform);
end

% Calculate the corner positions of the new stitched image
corners_x = [min(1, x_data(1)) max(size(left_img, 2), x_data(2))];
corners_y = [min(1, y_data(1)) max(size(left_img, 1), y_data(2))];

% Transform the right image back and put it in the stitched image scale
right_img_transf = imtransform(right_img, tform, 'xdata', corners_x, 'ydata', corners_y);
% Put the left image in the stitched image scale, without transforming it.
left_img_transf = imtransform(left_img, maketform('affine', eye(3)), 'xdata', corners_x, 'ydata', corners_y);

% Combine both images, by selecting at each position either the pixel from the right image or the left image
out_img = max(right_img_transf, left_img_transf);

% Display the result
figure;
imshow(out_img);

end
