% tfrom = PROJECTIVE(I1, I2)
%
% Find the projective transformation of the first image compared to the second image.
%
% Parameters are:
%   I1      =   The first image
%   I2      =   The second image
%
% Returns:
%   tform   =   The found projective transformation to be used with the function imtransform
%
% See also:
% IMTRANSFORM
function tform = projective(I1, I2)

if size(I1, 3) > 1
    I1 = rgb2gray(I1);
end

if size(I2, 3) > 1
    I2 = rgb2gray(I2);
end

I1 = single(I1);
I2 = single(I2);

display('Computing SIFT descriptors...');

[keypoints1 descriptors1] = vl_sift(I1);
[keypoints2 descriptors2] = vl_sift(I2);

display('Done');
display('Matching SIFT descriptors...');
[matches] = vl_ubcmatch(descriptors1, descriptors2, 10);

display('Done');

matches1 = keypoints1(1:2, matches(1, :));
matches2 = keypoints2(1:2, matches(2, :));

display('Computing projective transformation...');

tform = cp2tform(matches1', matches2', 'projective');

display('Done');

end
