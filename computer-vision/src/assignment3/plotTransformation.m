function plotTransformation(I1, I2, keypoints1, transf_keypoints, matches)

plotSideBySide(I1, I2);
hold all;

for i = 1:size(matches, 2)
    plot([keypoints1(1, matches(1, i)) transf_keypoints(1, matches(1, i)) + size(I1, 2)], ...
         [keypoints1(2, matches(1, i)) transf_keypoints(2, matches(1, i))], ...
        'o-', 'color', [rand(1) rand(1) rand(1)], 'markerfacecolor', 'r');
end

end