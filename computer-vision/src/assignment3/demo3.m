fprintf('In this demo, image stitching using projective transformations will be performed.\nBefore continuing, please make sure that VLFeat (http://www.vlfeat.org/) is installed and that MATLAB can access its functions.\n');
key = input('Press any key to continue...');

left_img = imread('bigsur1.jpg');
right_img = imread('bigsur2.jpg');

o = stitchImages(left_img, right_img, 'projective');

left_img = imread('mountains1.jpg');
right_img = imread('mountains2.jpg');

o = stitchImages(left_img, right_img, 'projective');