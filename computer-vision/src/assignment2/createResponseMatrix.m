% R = CREATERESPONSEMATRIX(I, wsize, k)
%
% Creates the cornerness response matrix of an image
% using a window of size (2*wsize+1)*(2*wsize+1) and constant k
%
% The cornerness response at point (i,j) is given by:
%   det(M)-k*(trace(M))^2
% where matrix M contains derivatives of image I in
% a specific window around point (i,j).
%
% Parameters are:
%   I     =   The image
%   wsize =   Size of the window is given by (2*wsize+1)^2
%   k     =   Empirical constant (usally between 0.04 and 0.06)
%
% Returns:
%   R     =   Cornerness response matrix for image I
function R = createResponseMatrix(I, wsize, k)
G = gaussian(1);
dG = gaussianDer(G, 1);

Ix = conv2(I, dG, 'same');
Iy = conv2(I, dG', 'same');
Ix2 = Ix.^2;
Iy2 = Iy.^2;
IxIy = Ix.*Iy;

[height, width] = size(Ix2);
R = zeros(height, width);

for i = wsize + 1:height - wsize - 1  
    for j = wsize + 1:width - wsize - 1
        A = 0;
        B = 0;
        C = 0;
        
        for m = i - wsize:i + wsize
            for n = j - wsize:j + wsize
                A = A + Ix2(m, n);
                B = B + IxIy(m, n);
                C = C + Iy2(m, n);
            end
        end
        
        R(i, j) = (A*C-B^2) - k*4*B^2;
    end
end

end
