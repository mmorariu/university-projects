% [row, col, scale] = HARRISDOG(I, N, thresh, show_output)
%
% Finds corners in an image using the harris corner detector,
% extended with the difference of Gaussian (DOG) approach.
%
% Parameters are:
%   I               =   The image you want to find corners in
%   N               =   Determines the patch size as 2*N+1
%   thresh          =   Cornerness threshold
%   show_output     =   Whether or not to show the annotated image
%
% Returns:
%   row             =   The rows where the corners were found
%   col             =   The columns where the corners were found
%   scale           =   The scales where the corners were found
%
% See also:
% CREATERESPONSEMATRIX, HARRIS
function [row col scale] = harrisDOG(I, N, thresh, show_output)

if size(I, 3) == 3
    I = rgb2gray(I);
end

I = im2double(I);
[h w] = size(I);
R = zeros(size(I, 1), size(I, 2), 4);

display('Computing Harris corners...');
k = 1.6;

for sigma = 1:7
    L = sigma^2 * fspecial('log', 6*floor(sigma) + 1, sigma);
    G1 = fspecial('gaussian', 6*floor(sigma) + 1, k*sigma);
    G2 = fspecial('gaussian', 6*floor(sigma) + 1, sigma);
    DOG = 1/(k - 1) * (G1 - G2);
    response = conv2(I, DOG, 'same');
    R(:, :, sigma) = createResponseMatrix(response, 2, 0.0005);
end

row = [];
col = [];
scale = [];

for i = N + 1:2*N + 1:h - N - 1
    for j = N + 1:2*N + 1:w - N - 1
        [max_val max_scale] = max(max(max(R(i - N:i + N, j - N:j + N, :))));
        
        if max_val > thresh
            [max_y max_x] = find(R(:, :, max_scale) == max_val);
            row(end + 1) = max_y;
            col(end + 1) = max_x;
            scale(end + 1) = max_scale;
        end
    end
end

display('Done');

if show_output
    figure;
    imshow(I);
    hold on;
    plot(col, row, 'r.');
end

end
