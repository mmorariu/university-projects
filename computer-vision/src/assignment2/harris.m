% [row, col, scale] = HARRIS(I, N, thresh, show_output)
%
% Finds corners in an image using the harris corner detector.
%
% Parameters are:
%   I                         =   The image you want to find corners in
%   N                         =   Determines the patch size as 2*N+1
%   thresh                    =   Cornerness threshold
%   iterate                   =   Whether or not to iterate until convergence
%   show_output               =   Whether or not to show the annotated image
%   show_intermediate_output  =   Whether or not to show the annotated image
%
% Returns:
%   row                       =   The rows where the corners were found
%   col                       =   The columns where the corners were found
%   scale                     =   The scales where the corners were found
%
% See also:
% CREATERESPONSEMATRIX, HARRISDOG
function [row col scale] = harris(I, N, thresh, iterate, show_output, show_intermediate_output)

if nargin == 3
    iterate = false;
    show_output = false;
    show_intermediate_output = false;
elseif nargin == 4
    show_output = false;
    show_intermediate_output = false;
elseif nargin == 5
    show_intermediate_output = false;
elseif nargin < 3 || nargin > 6
    error('Invalid parameter list.');
end

display(sprintf('Computing harris corners with threshold: %s', [thresh]));

% Make sure I is a gray image
if size(I, 3) == 3
    I = rgb2gray(I);
end

I = im2double(I);
[h w] = size(I);

% Innitiate R matrix for the size of the image in 7 sigma dimensions
R = zeros(h, w, 8);

for sigma = 1:8
    display(sprintf(' * Creating image on sigma scale %d', [sigma]));
    if sigma > 0
      L = sigma^2 * fspecial('log', 6*floor(sigma) + 1, sigma);
      response = conv2(I, L, 'same');
    else
      response = I;
    end
    display(' * Computing response matrix with wsize set to 2 and k set to 0.05...');
    R(:, :, sigma) = createResponseMatrix(response, 2, 0.05);
end

row = [];
col = [];
scale = [];

display(' * Finding corners...');

% Slide NxN window over the R matrix
% and find the maximum corner in the window in all dimensions 
for i = N + 1:2*N + 1:h - N - 1
    for j = N + 1:2*N + 1:w - N - 1
        % find the maximum corner response and on which scale
        [max_val max_scale] = max(max(max(abs(R(i - N:i + N, j - N:j + N, :)))));
        
        % If the corner response is high enough
        if max_val > thresh
            % find the coordinates of the corner within the image of that scale
            [max_y max_x] = find(R(:, :, max_scale) == max_val);
            % store corner
            row(end + 1) = max_y;
            col(end + 1) = max_x;
            scale(end + 1) = max_scale;
        end
    end
end
display(sprintf('    [Found %d corners]',[length(row)]));

if show_intermediate_output
    figure;
    subplot(1,2,1);
    imshow(I);
    hold on;
    title(sprintf('Initially found %d corners',[length(row)]));
    plot(col, row, 'r+');
end

if iterate
  iter = 1;
  % Iterate untill convergence
  display(' * Start iterative optimization untill convergence');
  while 1
      display(sprintf('   - Iteration %d',[iter]));
      row2 = [];
      col2 = [];
      scale2 = [];
      for i = 1:length(row)
          if row(i) > N && col(i) > N && row(i) < h-N+1 && col(i) < w-N+1
              % find the maximum corner response and on which scale
              [max_val max_scale] = max(max(max(abs(R(row(i) - N:row(i) + N, col(i) - N:col(i) + N, :)))));
              
              % find the coordinates of the corner within the image of that scale
              [max_y max_x] = find(R(:, :, max_scale) == max_val);
              
              % Check if row,col and scale combination is new
              if sum(scale2(find(col2(find(row2==max_y))==max_x))==max_scale) == 0
                % store corner
                row2(end + 1) = max_y;
                col2(end + 1) = max_x;
                scale2(end + 1) = max_scale;
              end
          end
      end
      if ( ...
        length(row2) == length(row) && ...
        length(col2) == length(col) && ...
        length(scale2) == length(scale) && ...
        sum(row2~=row) == 0 && ...
        sum(col2~=col) == 0 && ...
        sum(scale2~=scale) == 0 ...
      )
          display('    Converged!');
          break
      end
      row = row2;
      col = col2;
      scale = scale2;
      iter = iter + 1;
      display(sprintf('    [Found %d corners]',[length(row)]));
  end
end
display('Done');

if show_output
    if show_intermediate_output
      subplot(1,2,2);
    end
    imshow(I);
    hold on;
    if show_intermediate_output    
      title(sprintf('Iteration %d found %d corners',[iter,length(row)]));
    else
      title(sprintf('Window: %d X %d, threshold: %f, corners: %d',[2*N+1, 2*N+1, thresh,length(row)]));
    end
    plot(col, row, 'r+');
end

end
