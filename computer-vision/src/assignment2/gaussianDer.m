% Gd = GAUSSIANDER(G, sigma)
% 
% Calculates the derivative of gaussian kernel G with variance sigma.
%
% Paremeters are:
%   G     =   A gaussian kernel as returned from gaussian(sigma)
%   sigma =   The variance of the gaussian kernel
%
% Returns:
%   Gd    =   Gaussian derivative kernel
%
% See also:
% GAUSSIAN
function Gd = gaussianDer(G, sigma)

% Discretize the interval [-3 sigma...3 sigma] and compute the Gaussian
% derivative for each value x from the discretized interval

x = linspace(-3*sigma, 3*sigma, 6*floor(sigma) + 1);
Gd = -x/(sigma^2).*G;
end
