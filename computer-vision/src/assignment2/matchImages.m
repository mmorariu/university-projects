% M = MATCHIMAGES(I1,I2, match_thresh, show_keypoints, show_descriptors)
%
% Matches keypoints in images I1 and I2 using the SIFT descriptors.
% Uses vl_sift and others from the vl_feat library (http://www.vlfeat.org).
% Corners are detected using the harris corner detector.
%
% Parameters are:
%   I1                  =   First image to match
%   I2                  =   Second image to match
%   match_thresh        =   Threshold indicating a match, as used in vl_ubcmatch
%   show_keypoints      =   Whether or not to display the keypoints
%   show_descriptors    =   Whether or not to display the SIFT descriptors
%
% Returns:
%   M                   =   The matched keypoints.
%
% See also:
% VL_SIFT, VL_UBCMATCH, HARRIS
function M = matchImages(I1, I2, match_thresh, show_keypoints, show_descriptors)

if nargin == 2
    match_thresh = 1.5;
    show_keypoints = false;
    show_descriptors = false;
elseif nargin == 3
    show_keypoints = false;
    show_descriptors = false;
elseif nargin == 4
    show_descriptors = false;
elseif nargin < 2 || nargin > 5
    error('Invalid parameter list.');
end

if size(I1, 3) > 1
    I1 = rgb2gray(I1);
end

if size(I2, 3) > 1
    I2 = rgb2gray(I2);
end

[rows1 cols1 scales1] = harris(I1, 1, 0.001);
[rows2 cols2 scales2] = harris(I2, 1, 0.001);

[height1, width1] = size(I1);
[height2, width2] = size(I2);

I3 = zeros(max(height1, height2), width1 + width2);
I3(1:height1, 1:width1) = I1(:, :);
I3(1:height2, width1 + 1:width1 + width2) = I2(:, :);

I1 = single(I1);
I2 = single(I2);

display('Computing SIFT descriptors...');
[keypoints1 descriptors1] = vl_sift(I1, 'frames', [cols1; rows1; scales1; zeros(size(rows1))], 'orientations');
[keypoints2 descriptors2] = vl_sift(I2, 'frames', [cols2; rows2; scales2; zeros(size(rows2))], 'orientations');
keypoints2(1, :) = keypoints2(1, :) + width1;
display('Done');

display('Matching images...');
matches = vl_ubcmatch(descriptors1, descriptors2, match_thresh);
display('Done');

close all;
imshow(I3, []);
hold all;

if show_keypoints
    no_keypoints1 = size(keypoints1, 2);
    no_keypoints2 = size(keypoints2, 2);
    
    h11 = vl_plotframe(keypoints1(:, 1:no_keypoints1));
    h12 = vl_plotframe(keypoints1(:, 1:no_keypoints1));
    set(h11, 'color', 'k', 'linewidth', 3);
    set(h12, 'color', 'y', 'linewidth', 2);
    
    h21 = vl_plotframe(keypoints2(:, 1:no_keypoints2));
    h22 = vl_plotframe(keypoints2(:, 1:no_keypoints2));
    set(h21, 'color', 'k', 'linewidth', 3);
    set(h22, 'color', 'y', 'linewidth', 2);
end

if show_descriptors
    h13 = vl_plotsiftdescriptor(descriptors1(:, 1:no_keypoints1), keypoints1(:, 1:no_keypoints1));
    set(h13,'color','g');
    
    h23 = vl_plotsiftdescriptor(descriptors2(:, 1:no_keypoints2), keypoints2(:, 1:no_keypoints2));
    set(h23,'color','g');    
end

M = [];
for i = 1:size(matches, 2);
    plot([keypoints1(1, matches(1, i)) keypoints2(1, matches(2, i))], [keypoints1(2, matches(1, i)), keypoints2(2, matches(2, i))], 'color', [rand(1),rand(1),rand(1)]);
    M(1,i,:) = [keypoints1(1, matches(1, i)) keypoints2(2, matches(1, i))];
    M(2,i,:) = [keypoints1(1, matches(2, i)) keypoints2(2, matches(2, i))];
end

end
