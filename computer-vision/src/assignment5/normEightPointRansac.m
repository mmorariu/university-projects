% NORMEIGHTPOINTRANSAC Estimate the fundamental matrix between two images
%                      by applying the normalized eight-point algorithm
%                      with RANSAC
%
%   [F_denorm res] = normEightPointRansac(image1, image2, show_matches,
%                    show_selected_keypoints)
%
%   Parameters are:
%     image1        =   Path to the first image
%     image2        =   Path to the second image
%     show_matches  =   Whether or not to show the matches selected by the
%                       RANSAC algorithm corresponding to the best
%                       estimation of F
%     show_selected_keypoints  =  Whether or not to show the keypoints
%                       obtained after removing the background interest
%                       points
%
%   Returns:
%     F_denorm      =   Denormalized F
%     res           =   Vector containing the values x'T F_denorm x, where
%                       x and x' are matching keypoints from the two images

function [F_denorm res] = normEightPointRansac(image1, image2, show_matches, show_selected_keypoints)

if nargin < 3
    show_matches = false;
    show_selected_keypoints = false;
elseif nargin < 4
    show_selected_keypoints = false;
end

close all;

I1 = imread(image1);
I2 = imread(image2);

% Extract features from the two images: locations of keypoints (x and y),
% ellipse parameters (a, b, c) and descriptor vectors (desc)

[x1 y1 a1 b1 c1 desc1] = readFeatures(image1);
[x2 y2 a2 b2 c2 desc2] = readFeatures(image2);

% Filter out keypoints that belong to background. We assume that most of
% the found keypoints will be located on the object of interest and that
% its size is approximately one third of the image size. We compute the
% mean of the found keypoints to get the object's center and filter out
% those keypoints for which the distance to the center is larger than one
% third of the image size.

N1 = max(size(I1, 1), size(I1, 2)) / 3;
N2 = max(size(I2, 1), size(I2, 2)) / 3;

m1 = mean([x1 y1]);
d1 = sqrt((x1 - m1(1)).^2 + (y1 - m1(2)).^2);
x1 = x1(d1 < N1);
y1 = y1(d1 < N1);
desc1 = desc1(d1 < N1, :);

m2 = mean([x2 y2]);
d2 = sqrt((x2 - m2(1)).^2 + (y2 - m2(2)).^2);
x2 = x2(d2 < N2);
y2 = y2(d2 < N2);
desc2 = desc2(d2 < N2, :);

% Plot the keypoints obtained after filtering

if show_selected_keypoints
    figure;
    imshow(I1);
    hold on;
    plot(x1, y1, 'ro', 'markerfacecolor', 'r');
    title(['Selected keypoints for image ', image1]);
    
    figure;
    imshow(I2);
    hold on;
    plot(x2, y2, 'ro', 'markerfacecolor', 'r');
    title(['Selected keypoints for image ', image2]);
end

% Match descriptors

display('Matching images...');
[matches, scores] = vl_ubcmatch(desc1', desc2', 1.5);
display('Done');

% Apply RANSAC to get an estimation of the fundamental matrix. Return the
% maximum number of inliers, the best tranformation matrix, the best point
% correspondences that were used to compute the matrix and the T matrices
% obtained from the normalization step.

[bni bF bm1 bm2 T1 T2] = ransac([x1 y1], [x2 y2], matches, 0.00001);

x1 = x1(matches(1, :));
y1 = y1(matches(1, :));
x2 = x2(matches(2, :));
y2 = y2(matches(2, :));
res = zeros(numel(x1), 1);

% Denormalize the fundamental matrix in order to test it on the matching
% keypoints. An alternative to this would be to normalize the matches and
% use the matrix returned from RANSAC to check if the equation x'^T F x = 0
% holds.

F_denorm = T2' * bF * T1;

for i = 1:numel(x1)
    res(i) = [x2(i) y2(i) 1] * F_denorm * [x1(i) y1(i) 1]';
end

% Plot the best matching keypoints that were selected by the RANSAC
% algorithm for estimating the fundamental matrix

if show_matches
    figure;
    imshow(I1);
    hold on;
    plot(bm1(:, 1), bm1(:, 2), 'ro', 'markerfacecolor', 'r');
    title(['Selected correspondences for image ', image1]);
    
    figure;
    imshow(I2);
    hold on;
    plot(bm2(:, 1), bm2(:, 2), 'ro', 'markerfacecolor', 'r');
    title(['Selected correspondences for image ', image2]);
end

end