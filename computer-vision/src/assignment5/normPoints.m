% NORMPOINTS Normalize matching keypoints
%
%   [norm_points1, norm_points2, T1, T2] = normPoints(points1, points2)
%
%   Parameters are:
%     points1       =   Keypoints from the first image
%     points2       =   Corresponding keypoints from the second image
%
%   Returns:
%     norm_points1  =   Normalized keypoints from the first image
%     norm_points2  =   Normalized keypoints from the second image
%     T1            =   Transformation matrix used to obtain the normalized
%                       keypoints in the first image
%     T2            =   Transformation matrix used to obtain the normalized
%                       keypoints in the second image

function [norm_points1, norm_points2, T1, T2] = normPoints(points1, points2)

points1 = [points1 ones(size(points1, 1), 1)];
mean1 = mean(points1);
d1 = mean(sqrt((points1(:, 1) - mean1(1)).^2 + (points1(:, 2) - mean1(2)).^2));
T1 = [sqrt(2)/d1 0 -mean1(1)*sqrt(2)/d1; 0 sqrt(2)/d1 -mean1(2)*sqrt(2)/d1; 0 0 1];

norm_points1 = (T1*points1')';
norm_points1 = norm_points1(:, 1:2);

points2 = [points2 ones(size(points2, 1), 1)];
mean2 = mean(points2);
d2 = mean(sqrt((points2(:, 1) - mean2(1)).^2 + (points2(:, 2) - mean2(2)).^2));
T2 = [sqrt(2)/d2 0 -mean2(1)*sqrt(2)/d2; 0 sqrt(2)/d2 -mean2(2)*sqrt(2)/d2; 0 0 1];

norm_points2 = (T2*points2')';
norm_points2 = norm_points2(:, 1:2);

end