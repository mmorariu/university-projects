key = input('Warning! To run this program, you need to have VLFeat installed and recognized by MATLAB, as well as \nthe Harris/Hessian Affine implementation!');

image1 = 'left.png';
image2 = 'right.png';

[F r] = normEightPointRansac(image1, image2, true);
display(F);