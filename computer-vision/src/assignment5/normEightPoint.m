% NORMEIGHTPOINT Estimate the fundamental matrix between two images by
%                applying the normalized eight-point algorithm
%
%   F = normEightPoint(image1, image2, show_selected_keypoints)
%
%   Parameters are:
%     image1        =   Path to the first image
%     image2        =   Path to the second image
%     show_selected_keypoints  =  Whether or not to show the keypoints
%                       obtained after removing the background interest
%                       points
%
%   Returns:
%     F             =   Fundamental matrix

function F = normEightPoint(image1, image2, show_selected_keypoints)

if nargin < 3
    show_selected_keypoints = false;
end

% Extract features from the two images: locations of keypoints (x and y),
% ellipse parameters (a, b, c) and descriptor vectors (desc)

[x1 y1 a1 b1 c1 desc1] = readFeatures(image1);
[x2 y2 a2 b2 c2 desc2] = readFeatures(image2);

I1 = imread(image1);
I2 = imread(image2);

% Filter out keypoints that belong to background. We assume that most of
% the found keypoints will be located on the object of interest and that
% its size is approximately one third of the image size. We compute the
% mean of the found keypoints to get the object's center and filter out
% those keypoints for which the distance to the center is larger than one
% third of the image size.

N1 = max(size(I1, 1), size(I1, 2)) / 3;
N2 = max(size(I2, 1), size(I2, 2)) / 3;

m1 = mean([x1 y1]);
d1 = sqrt((x1 - m1(1)).^2 + (y1 - m1(2)).^2);
x1 = x1(d1 < N1);
y1 = y1(d1 < N1);
desc1 = desc1(d1 < N1, :);

m2 = mean([x2 y2]);
d2 = sqrt((x2 - m2(1)).^2 + (y2 - m2(2)).^2);
x2 = x2(d2 < N2);
y2 = y2(d2 < N2);
desc2 = desc2(d2 < N2, :);

% Plot the keypoints obtained after filtering

if show_selected_keypoints
    figure;
    imshow(I1);
    hold on;
    plot(x1, y1, 'ro', 'markerfacecolor', 'r');
    title(['Selected keypoints for image ', image1]);
    
    figure;
    imshow(I2);
    hold on;
    plot(x2, y2, 'ro', 'markerfacecolor', 'r');
    title(['Selected keypoints for image ', image2]);
end

% Match descriptors 

[matches, scores] = vl_ubcmatch(desc1', desc2', 1.2);

matches1 = [x1(matches(1, :)) y1(matches(1, :))];
matches2 = [x2(matches(2, :)) y2(matches(2, :))];

% Normalize the point matches

[norm_points1 norm_points2 T1 T2] = normPoints(matches1, matches2);

r = randperm(size(matches, 2));

ax1 = norm_points1(:, 1);
y1 = norm_points1(:, 2);
x2 = norm_points2(:, 1);
y2 = norm_points2(:, 2);

% Select 20 random correspondences that will be used to compute A

A = [ x1(r(1:20)).*x2(r(1:20)) x1(r(1:20)).*y2(r(1:20)) x1(r(1:20)) y1(r(1:20)).*x2(r(1:20)) y1(r(1:20)).*y2(r(1:20)) y1(r(1:20)) x2(r(1:20)) y2(r(1:20)) ones(20,1)];

% Compute the singular value decomposition

[U D V] = svd(A);
F = reshape(V(:, end), 3, 3);

% Compute the fundamental matrix

[Uf Df Vf] = svd(F);
F = Uf * diag([Df(1, 1) Df(2, 2) 0]) * Vf';
F = T2' * F * T1;

end