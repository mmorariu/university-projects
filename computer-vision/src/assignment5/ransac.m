function [best_no_inliers best_F best_matches1 best_matches2 T1 T2] = ransac(points1, points2, matches, thresh)

% Number of matching keypoints

T = size(matches, 2);

% Number of iterations

N = 5000;

best_no_inliers = 0;
best_inliers = [];
best_inliers_corresp = [];
best_A = [];

display('Applying RANSAC to estimate fundamental matrix...');

matches1 = points1(matches(1, :), :);
matches2 = points2(matches(2, :), :);

% Normalize keypoints

[norm_matches1 norm_matches2 T1 T2] = normPoints(matches1, matches2);

for i = 1:N
    
    % Select 8 random matching keypoints
    
    P_idx = randperm(T);
    
    % Compute A
    
    A = [ norm_matches1(P_idx(1:8), 1).*norm_matches2(P_idx(1:8), 1) ...
        norm_matches1(P_idx(1:8), 1).*norm_matches2(P_idx(1:8), 2) ...
        norm_matches1(P_idx(1:8), 1) ...
        norm_matches1(P_idx(1:8), 2).*norm_matches2(P_idx(1:8), 1) ...
        norm_matches1(P_idx(1:8), 2).*norm_matches2(P_idx(1:8), 2) ...
        norm_matches1(P_idx(1:8), 2) ...
        norm_matches2(P_idx(1:8), 1) ...
        norm_matches2(P_idx(1:8), 2) ...
        ones(8, 1)
        ];
    
    % Decompose A...
    
    [U D V] = svd(A);
    F = reshape(V(:, 9), 3, 3)';
    
    % ... and estimate the fundamental matrix F
    
    [Uf Df Vf] = svd(F);
    F = Uf * diag([Df(1, 1) Df(2, 2) 0]) * Vf';
    
    % Return the inliers (keypoints from the first image), their
    % corresponding matches in the second image and their number
    
    [no_inliers inliers inliers_corresp] = countInliers(norm_matches1, norm_matches2, P_idx, F, thresh);
    
    % Did we get a better estimation of the fundamental matrix?
    
    if no_inliers > best_no_inliers
        best_no_inliers = no_inliers;
        best_F = F;
        best_inliers = inliers;
        best_inliers_corresp = inliers_corresp;
        best_A = A;
        best_matches1 = matches1(P_idx(1:8), :);
        best_matches2 = matches2(P_idx(1:8), :);
        
        display([num2str(i), ' ', num2str(best_no_inliers)]);
    end
end

% Re-estimate F once more, this time on the set of all inliers

if size(best_inliers, 1) > 0
    best_A = [ best_A;
        best_inliers(:, 1).*best_inliers_corresp(:, 1) ...
        best_inliers(:, 1).*best_inliers_corresp(:, 2) ...
        best_inliers(:, 1) ...
        best_inliers(:, 2).*best_inliers_corresp(:, 1) ...
        best_inliers(:, 2).*best_inliers_corresp(:, 2) ...
        best_inliers(:, 2) ...
        best_inliers_corresp(:, 1) ...
        best_inliers_corresp(:, 2) ...
        ones(size(best_inliers, 1), 1)
        ];
    
    [U D V] = svd(best_A);
    best_F = reshape(V(:, 9), 3, 3)';
    
    [Uf Df Vf] = svd(best_F);
    best_F = Uf * diag([Df(1, 1) Df(2, 2) 0]) * Vf';
end

display('Done');

end