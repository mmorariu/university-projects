function [x, y, a, b, c, desc] = readFeatures(file)

% Are we running Windows or Linux?

if ispc
    system(['extract_features.exe -haraff -i ', file, ' -sift -o1 ', file, '.haraff.sift']);
elseif isunix
    system(['./extract_features.ln -haraff -i ', file, ' -sift -o1 ', file, '.haraff.sift']);
else
    error('Unsupported OS. Please use Windows or Linux.');
end

M = importdata([file, '.haraff.sift'], ' ', 2);

x = M.data(:, 1);
y = M.data(:, 2);
a = M.data(:, 3);
b = M.data(:, 4);
c = M.data(:, 5);

desc = M.data(:, 6:end);

% Perform cleanup

system(['rm ', file, '.haraff.sift']);

end
