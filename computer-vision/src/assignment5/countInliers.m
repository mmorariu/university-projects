function [no_inliers inliers inliers_corresp] = countInliers(norm_matches1, norm_matches2, P_idx, F, thresh)

T = size(P_idx, 2);

norm_matches1 = norm_matches1(P_idx(9:T), :);
norm_matches2 = norm_matches2(P_idx(9:T), :);

norm_matches1 = [norm_matches1 ones(size(norm_matches1, 1), 1)];
norm_matches2 = [norm_matches2 ones(size(norm_matches2, 1), 1)];

d1 = dot(norm_matches2', F * norm_matches1').^2;
Fp = F * norm_matches1';
Fp_prime = F' * norm_matches2';
d2 = Fp(1, :).^2 + Fp(2, :).^2 + Fp_prime(1, :).^2 + Fp_prime(2, :).^2;

d = d1./d2;
inliers = norm_matches1(d < thresh, 1:2);
inliers_corresp = norm_matches2(d < thresh, 1:2);
no_inliers = size(inliers, 1);

end