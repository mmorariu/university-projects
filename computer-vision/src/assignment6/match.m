%% MATCH Find point-correspondences between two images
%
% 1) Finds descriptors of key points in both images using extractDescriptors
% 2) Matches these descriptors using the VLFeat library.
% 3) Finds the biggest set of matches to keep using RANSAC
% 4) Removes inconsistent matches
%
% [matched_points1 matched_points2] = match(image1, image2)
%   Parameters:
%     image1            -  Filename of first image 
%     image2            -  Filename of second image
%
%   Returns:  
%     matched_points1   -  Points in the image1 that matched in image2
%     matched_points2   -  Points in the image2 that matched in image1
%
% See also:
% EXTRACTDESCRIPTORS, VL_UBCMATCH, RANSAC, REMOVEINCONSISTENTMATCHES
function [matched_points1 matched_points2] = match(image1, image2)

display('Extracting descriptors...');
tic
[points1 desc1] = extractDescriptors(image1);
[points2 desc2] = extractDescriptors(image2);
display('Done');
toc

display('Matching descriptors...');
tic
[matches scores] = vl_ubcmatch(desc1', desc2', 2);
display('Done');
toc

matched_points1 = points1(matches(1, :), :);
matched_points2 = points2(matches(2, :), :);
display('Estimating the fundamental matrix using RANSAC and the normalized eight-point algorithm...');
tic
[F inliers] = ransac(matched_points1', matched_points2', 1000, 0.5);
display('Done');
toc

inliers = removeInconsistentMatches(matched_points1, matched_points2, inliers);
matched_points1 = matched_points1(inliers, :);
matched_points2 = matched_points2(inliers, :);

end
