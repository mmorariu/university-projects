function [Xn T] = normalizePoints(Xh)

m = mean(Xh(1:2, :), 2);
d = mean(sqrt((Xh(1, :) - m(1)).^2 + (Xh(2, :) - m(2)).^2));
s = sqrt(2) / sqrt(d);

T = [s 0 -s * m(1);
     0 s -s * m(2);
     0 0 1];

Xn = T * Xh;

end
