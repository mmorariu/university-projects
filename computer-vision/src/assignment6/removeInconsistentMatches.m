%% REMOVEINCONSISTENTMATCHES Remove inconsistent matches
%
% good_inliers = removeInconsistentMatches(matched_points1, matched_points2, inliers)
%   Parameters:
%     matched_points1   -   Matched points in image 1
%     matched_points2   -   Matched points in image 2
%     inliers           -   Inliers after RANSAC procedure
%   Returns:
%     good_inliers      -   Resulting inliers
% 
function good_inliers = removeInconsistentMatches(matched_points1, matched_points2, inliers)

good_inliers = [];

for i = 1:numel(inliers)
    val1 = matched_points1(inliers(i), :);
    val2 = matched_points2(inliers(i), :);
    
    if sum(ismember(matched_points1, val1, 'rows')) > 1 || sum(ismember(matched_points2, val2, 'rows')) > 1
        continue;
    else
        good_inliers(end + 1) = inliers(i);
    end
end

end
