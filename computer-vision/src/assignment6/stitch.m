%%STITCH Stitches images from a directory
%
% stitch(dir_name, elim_aff_ambiguity)
%   Parameters:
%     dir_name             -    Directory containing images to be stitched
%     elim_aff_ambiguity   -    Whether or not to eliminate affine ambiguity
%
function stitch(dir_name, elim_aff_ambiguity)

if nargin < 1
    elim_aff_ambiguity = false;
end

images = dir(dir_name);

% It is useful to know how long it takes to generate the 3D model...

tic

% Create the full file path for each image

for i = 1:numel(images)
    images(i).name = fullfile(dir_name, images(i).name);
end

images_count = numel(images);

% This function finds matching points between consecutive images and 
% applies the normalized 8-point RANSAC algorithm to find the best matches.
% These matches are in turn used to construct the point-view matrix. The
% rows correspond to image IDs, the columns correspond to IDs of points
% that are unique over multiple views and the elements pv_matrix(i, j) are cells 
% which store the coordinates of point j in image i. For example, if the
% point from location (100, 100) in image 1 is matched with point from
% location (200, 200) in image 2 and both are assigned the index 3, then
% pv_matrix(1, 3) = (100, 100) and pv_matrix(2, 3) = (200, 200)

pv_matrix = chain(images);

% This cell array stores the recovered 3D points, such that
% recovered_3D_points{i} stores the point cloud obtained by applying the
% Affine Structure from Motion algorithm on images i and i + 1. For example,
% recovered_3D_points{1} stores the 3D points obtained from images 1 and 2,
% recovered_3D_points{2} stores the 3D points obtained from images 2 and 3
% etc. The last element in this cell array stores the point cloud obtained
% from the last and the first image.

recovered_3D_points = cell(1, 0);

% This matrix stores the 3D points coordinates of the final model (that
% will be displayed on the screen).

final_model = [ ];

% This matrix stores the projection matrices resulted after applying the
% Affine Structure from Motion algorithm.

proj_matrices = zeros(2*(images_count - 2), 3);

% In this "for" loop we apply the Affine Structure from Motion algorithm to
% recover 3D points from consecutive images.

for i = 3:images_count
    
    % Take pairs of images: (1 2), (2 3), ..., (N-1 N), (N 1)
    
    if i < images_count
        first_img_idx = i;
        second_img_idx = i + 1;
    else
        first_img_idx = i;
        second_img_idx = 3;
    end
    
    % What are the indexes of the points that appear in both the first and
    % the second image?
    
    matched_pts_in_imgs_idx = find(cellfun(@(x)~isempty(x), pv_matrix(first_img_idx - 2, :)) == 1 & ...
                                   cellfun(@(x)~isempty(x), pv_matrix(second_img_idx - 2, :)) == 1);
                               
    % Once these indexes are found, get the points' coordinates
                               
    matched_pts_first_img = cell2mat(cellfun(@(x)x', pv_matrix(first_img_idx - 2, matched_pts_in_imgs_idx), 'uniformoutput', false));
    matched_pts_second_img = cell2mat(cellfun(@(x)x', pv_matrix(second_img_idx - 2, matched_pts_in_imgs_idx), 'uniformoutput', false));
    
    % Fill in the D matrix that is passed to the Affine Structure from
    % Motion algorithm.
    
    D = [ ];
    D(:, :, 1) = matched_pts_first_img;
    D(:, :, 2) = matched_pts_second_img;
    
    % Apply the ASfM algorithm
    
    [M S] = structureFromMotion(D);
    
    % Store the projection matrices; these will be used later for
    % eliminating the affine ambiguity
    
    proj_matrices(2*(first_img_idx - 2) - 1:2*(first_img_idx - 2), :) = M(1:2, :);
    
    % Store the obtained point cloud, we will use this later to perform
    % stitching
    
    recovered_3D_points{first_img_idx - 2} = S;
end

% In the following "for" loop we stitch point clouds together, generating
% the final 3D model

for i = 3:images_count - 2
    
    % We take sets of three images: (1 2 3), (2 3 4), ..., (N-2 N-1 N), 
    % (N-1 N 1) (N 1 2)
    
    if i < images_count
        first_img_idx = i;
        second_img_idx = i + 1;
    else
        first_img_idx = i;
        second_img_idx = 3;
    end
    
    if second_img_idx == images_count
        third_img_idx = 3;
    else
        third_img_idx = second_img_idx + 1;
    end
    
    % Let's say we take images i, i+1 and i+2. For images i and i+1 we have
    % the point cloud recovered_3D_points{i}, while for i+1 and i+2 we have
    % recovered_3D_points{i+1}. We now need to stitch them together. To do
    % this, we first find the points that are common in all three images.
    % These are matched_pts_in_imgs_idx.

    matched_pts_in_imgs_idx = find(cellfun(@(x)~isempty(x), pv_matrix(first_img_idx - 2, :)) == 1 & ...
                                   cellfun(@(x)~isempty(x), pv_matrix(second_img_idx - 2, :)) == 1 & ...
                                   cellfun(@(x)~isempty(x), pv_matrix(third_img_idx - 2, :)) == 1);
                               
    % No common points found, then cannot align anything
     
    if sum(matched_pts_in_imgs_idx) == 0
        continue;
    end
    
    % Next - which points from recovered_3D_points{i} and
    % recovered_3D_points{i+1} correspond to the previously found ones?
    % We can find their indices using the point-view matrix. The recovered
    % indices are stored in common_pts1_3D_idx and common_pts2_3D_idx.

    matched_pts1_idx = find(cellfun(@(x)~isempty(x), pv_matrix(first_img_idx - 2, :)) == 1 & cellfun(@(x)~isempty(x), pv_matrix(second_img_idx - 2, :)) == 1);
    matched_pts2_idx = find(cellfun(@(x)~isempty(x), pv_matrix(second_img_idx - 2, :)) == 1 & cellfun(@(x)~isempty(x), pv_matrix(third_img_idx - 2, :)) == 1);
    common_pts1_3D_idx = find(ismember(matched_pts1_idx, matched_pts_in_imgs_idx) == 1);
    common_pts2_3D_idx = find(ismember(matched_pts2_idx, matched_pts_in_imgs_idx) == 1);
    
    % Perform Procrustes Analysis and align the common points from
    % recovered_3D_points{i} to the common points from
    % recovered_3D_points{i+1}. To put it simply, we stitch points from
    % image i to points from image i+1. We get as output the Procrustes
    % distance d (which tells us how well the point clouds were stitched to
    % each other), the scale component s, the rotation matrix Q and the
    % translation vector t.
    
    [d s Q t] = procrustesAnalysis(recovered_3D_points{second_img_idx - 2}(:, common_pts2_3D_idx)', recovered_3D_points{first_img_idx - 2}(:, common_pts1_3D_idx)');
    
    % We discard the common points from recovered_3D_points{i+1} (we keep
    % the ones from recovered_3D_points{i}, after they are transformed). We
    % attach the remaining points from recovered_3D_points{i+1} to the
    % partial 3D model.
  
    rest_of_pts_in_3D_idx = find(ismember(1:size(recovered_3D_points{second_img_idx - 2}, 2), common_pts2_3D_idx) == 0);
    rest_of_pts_in_3D = recovered_3D_points{second_img_idx - 2}(:, rest_of_pts_in_3D_idx)';
    
    % Transform the remaining 3D points and attach them to the model
    
    if isempty(final_model)
        final_model = [recovered_3D_points{first_img_idx - 2}'; rest_of_pts_in_3D];
    else
        final_model = s * final_model * Q;
        final_model(:, 1) = final_model(:, 1) + t(1);
        final_model(:, 2) = final_model(:, 2) + t(2);
        final_model(:, 3) = final_model(:, 3) + t(3);
        final_model = [final_model; rest_of_pts_in_3D];
    end
end

% If set to true, eliminate the affine ambiguity. Finally, plot the
% generated 3D model.

if elim_aff_ambiguity
    [proj_matrices final_model] = eliminateAffineAmbiguity(proj_matrices, final_model);
    plot3(final_model(1, :), final_model(2, :), final_model(3, :), 'b.');
else
    plot3(final_model(:, 1), final_model(:, 2), final_model(:, 3), 'b.');
end

display('Done');
toc

end
