function d = computeDistance(X1h, X2h, F)

pFp = dot(X2h, F * X1h).^2;
Fp1 = F * X1h;
Fp2 = F' * X2h;
d = pFp./(Fp1(1, :).^2 + Fp1(2, :).^2 + Fp2(1, :).^2 + Fp2(2, :).^2);

end
