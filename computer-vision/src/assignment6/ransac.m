function [best_F best_inliers] = ransac(X1, X2, N, t)

num_points = size(X1, 2);

if num_points >= 8
    X1h = convertToHomogeneousCoords(X1);
    X2h = convertToHomogeneousCoords(X2);
    best_inliers = [];
    
    for i = 1:N
        perm = randperm(num_points);
        idx = perm(1:8);
        F = normEightPoint(X1h(:, idx), X2h(:, idx));
        d = computeDistance(X1h, X2h, F);
        
        inliers_idx = find(d <= t);
        
        if numel(inliers_idx) > numel(best_inliers)
            best_inliers = inliers_idx;
        end
    end
    
    if numel(best_inliers) > 0
        best_F = normEightPoint(X1h(:, best_inliers), X2h(:, best_inliers));
    end
else
    error('Not enough points available.');
end

end
