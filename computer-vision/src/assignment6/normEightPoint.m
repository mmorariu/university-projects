function F = normEightPoint(X1h, X2h)

[X1n T1] = normalizePoints(X1h);
[X2n T2] = normalizePoints(X2h);

A = [ X1n(1, :)'.*X2n(1, :)', X1n(2, :)'.*X2n(1, :)', X2n(1, :)', ...
      X1n(1, :)'.*X2n(2, :)', X1n(2, :)'.*X2n(2, :)', X2n(2, :)', ...
      X1n(1, :)', X1n(2, :)', ones(size(X1h, 2), 1) ];
 
[U D V] = svd(A, 0);
F = reshape(V(:, end), 3, 3)';

[Uf Df Vf] = svd(F);
F = Uf * diag([Df(1, 1) Df(2, 2) 0]) * Vf';
F = T2' * F * T1;

end
