function Xh = convertToHomogeneousCoords(X)

if size(X, 1) == 2
    X(3, :) = ones(1, size(X, 2));
end

Xh = X;

end
