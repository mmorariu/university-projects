function [PVMatrix, Points] = chaining(Images)
  % Store number of frames
  framelen = length(Images);

  % Initialize PVMatrix:
  % `Point-View-Matrix' matrix that contains frames on each row
  % and point indices on each column, where each index
  % corresponds to a point in the frame of that row, which
  % can be seen in all other non-zero cells of that column
  PVMatrix = zeros(framelen,0);

  % Initialize Points:
  % Point matrix that contains frames on each row and
  % 2D point coordinates in that frame that correspond to 
  % the point index in PVMatrix on the same position
  Points = zeros(framelen,0,0);

  % For all frames
  for i = 1:framelen
    if i == 1
      % Find matches between both frames
      [x1,x2,matches] = match(Images{1},Images{2});
      % Store the matches in PVMatrix
      PVMatrix(1,size(matches,1)) = matches(:,1)';
      PVMatrix(2,size(matches,1)) = matches(:,2)';

      % Store the matched points in Points
      Points(1,:,:) = x1(matches(:,1),:);
      Points(2,:,:) = x2(matches(:,2),:);
    else
      % Calculate index of next frame.
      % This is necessary for matching the last frame with the first
      i2 = mod(i,framelen)+1;

      % Find matched points between frames i and i2
      [x1,x2,matches] = match(Images{i},Images{i2});

      % Find matched points between frames i and i2 that
      % also existed between frames i and i-1
      [C, IA, IB] = intersect(matches(:,1)', PVMatrix(i,:));
      % Store the matches of frames i2 and i that also matched
      % in frame i-1 while preserving the indices
      PVMatrix(i2,IB) = matches(IA,2)';
      
      % Store the known matched points in Points
      Points(i2,IA,:) = x2(IA,:);

      % Find matched points between frames i and i that
      % did not exist between frames i and i-1
      [D, IA] = setdiff(matches(:,1)', PVMatrix(i,:));
      % Find offset for matches that will be appended
      offset = size(PVMatrix, 2)+1;
      % Enlarge PVMatrix to fit new matches
      PVMatrix = [PVMatrix zeros(framelen, size(D,2))];
      % Add matched points under frames i and i2 that
      % did not yet exist between frames i and i-1
      PVMatrix(i, offset:end) = D;
      PVMatrix(i2, offset:end) = matches(IA,2);
      
      % Enlarge Points to fit new matches
      Points = [Points zeros(framelen, size(D,2))];
      % Store the new matched points in Points
      Points(i,offset:end,:) = x1(IA,:);
      Points(i2,offset:end,:) = x2(IA,:);
    end
  end
end
