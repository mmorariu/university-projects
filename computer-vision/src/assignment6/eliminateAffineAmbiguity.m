function [M2 S2] = eliminateAffineAmbiguity(M1, S1)

% Get the number of frames; M1 is of size 2F x 3

F = size(M1, 1) / 2;

% Compute all the odd rows of M1

A = M1(1:2:end-1, :);

% Compute all the even rows of M1

B = M1(2:2:end, :);

% Compute the G matrix

G = [ A(:, 1).*A(:, 1)     A(:, 1).*A(:, 2) + A(:, 2).*A(:, 1)     A(:, 1).*A(:, 3) + A(:, 3).*A(:, 1) ...
      A(:, 2).*A(:, 2)     A(:, 2).*A(:, 3) + A(:, 3).*A(:, 2)     A(:, 3).*A(:, 3); ...
      B(:, 1).*B(:, 1)     B(:, 1).*B(:, 2) + B(:, 2).*B(:, 1)     B(:, 1).*B(:, 3) + B(:, 3).*B(:, 1) ...
      B(:, 2).*B(:, 2)     B(:, 2).*B(:, 3) + B(:, 3).*B(:, 2)     B(:, 3).*B(:, 3); ...
      A(:, 1).*B(:, 1)     A(:, 1).*B(:, 2) + A(:, 2).*B(:, 1)     A(:, 1).*B(:, 3) + A(:, 3).*B(:, 1) ...
      A(:, 2).*B(:, 2)     A(:, 2).*B(:, 3) + A(:, 3).*B(:, 2)     A(:, 3).*B(:, 3) ];

c = [ones(2*F, 1); zeros(F, 1)];

% Compute the vector l, which determines the symmetric matrix L

l = (G'*G) \ (G'*c);
L = [ l(1) l(2) l(3); l(2) l(4) l(5); l(3) l(5) l(6) ];

% Recover C from L by Cholesky decomposition: L = CC';

C = chol(L)';
M2 = M1 * C;
S2 = C \ S1';

end
