%%PROCRUSTESANALYSIS
%
% [d s Q t] = procrustesAnalysis(X, Y)
%   Parameters: 
%     X   -   Set of landmarks
%     Y   -   Set of landmarks
%   Results:
%     d   -   Procrustes distance
%     s   -   Scale component
%     Q   -   Optimal rotation matrix
%     t   -   translation vector
function [d s Q t] = procrustesAnalysis(X, Y)

% Get the number of landmarks and their dimensionality for both shapes

[num_rows_X num_cols_X] = size(X);
[num_rows_Y num_cols_Y] = size(Y);

% Do we have the same number of points for both shapes?

if num_rows_Y ~= num_rows_X
    error('X and Y must have the same number of rows !');
end

% Compute the means of the shapes

mean_X = mean(X, 1);
mean_Y = mean(Y, 1);

if size(unique(X, 'rows'), 1) ~= 1 && size(unique(Y, 'rows'), 1) ~= 1
    
    % Using the means of the shapes, center them at the origin
    
    Xc = X - repmat(mean_X, num_rows_X, 1);
    Yc = Y - repmat(mean_Y, num_rows_Y, 1);
    
    % Compute the root mean square distance from the points to the
    % translated origin and use that to remove the scale component.
    
    rmsdX = sqrt(sum(sum(Xc.^2, 1)));
    rmsdY = sqrt(sum(sum(Yc.^2, 1)));
    Xn = Xc / rmsdX;
    Yn = Yc / rmsdY;
    
    % Using the technique presented in the paper "Procrustes analysis" by
    % Amy Ross, compute the optimal rotation matrix Q.
    
    A = Xn' * Yn;
    [U S V] = svd(A);
    Q = V * U';
    
    % Compute the Procrustes distance, the scale component and the
    % translation vector.
    
    T = sum(diag(S));
    d = 1 - T.^2;
    s = T * rmsdX / rmsdY;
    t = mean_X - s * mean_Y * Q;
    
% If X and Y are actually comprised of a single point...    
    
elseif size(unique(X, 'rows'), 1) == 1
    d = 0;
    s = 0;
    Q = eye(num_cols_Y, num_cols_X);
    t = mean_X;
else
    d = 1;
    s = 0;
    Q = eye(num_cols_Y, num_cols_X);
    t = mean_X;
end

end
