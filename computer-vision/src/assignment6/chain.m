function pv_matrix = chain(images)

images_count = numel(images) - 2;

% Create a cell array that will store the point-view matrix. The rows 
% correspond to image IDs, the columns correspond to IDs of points that are
% unique over multiple views and the elements pv_matrix(i, j) are cells which
% store the coordinates of point j in image i. For example, if the point
% from location (100, 100) in image 1 is matched with point from
% location (200, 200) in image 2 and both are assigned the index 3, then
% pv_matrix(1, 3) = (100, 100) and pv_matrix(2, 3) = (200, 200)

pv_matrix = cell(images_count, 0);

for i = 1:images_count
    
    % Take pairs of consecutive images: (1 2), (2 3), ..., (N-1 N), (N 1)
    
    if i < images_count
        first_img_idx = i + 2;
        second_img_idx = i + 3;
    else
        first_img_idx = i + 2;
        second_img_idx = 3;
    end
    
    % Apply the normalized eight-point RANSAC algorithm and get the best
    % matches
    
    [points1 points2] = match(images(first_img_idx).name, images(second_img_idx).name);
    num_matches = size(points1, 1);
    
    if num_matches == 0
        error('No matches found between images %s and %s !', images(first_img_idx).name, images(second_img_idx).name);
    end
    
    for j = 1:num_matches
        
        % We have a new match: (x_i, y_i) from image i with (x_j, y_j)
        % from image j. Did (x_i, y_i) appear before? Look for it in the
        % point-view matrix. Is there any (x_i, y_i) stored? If yes, is it
        % stored on row i (i.e. does it correspond to image i)?
        
        p = find(cellfun(@(x)isequal(x, points1(j, :)), pv_matrix));
        [r c] = ind2sub(size(pv_matrix), p);
        
        if ~isequal(r, first_img_idx - 2)
            
            % We haven't found any point (x_i, y_i) or we have found it,
            % but it was present in another image. Store the new match in
            % the point-view matrix.
            
            pv_matrix{first_img_idx - 2, end + 1} = points1(j, :);
            pv_matrix{second_img_idx - 2, end} = points2(j, :);
        else
            
            % The point (x_i, y_i) has been seen before in image i and its
            % index was c. Thus, store (x_j, y_j) on the j-th row and c-th
            % column of the point-view matrix.
            
            pv_matrix{second_img_idx - 2, c} = points2(j, :);
        end
    end
end

end
