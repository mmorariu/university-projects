%% READFEATURES Wrapper function for extract_features
%
% Calls extract_features.exe or extract_features.ln depending on the OS.
% See http://www.robots.ox.ac.uk/~vgg/research/affine/detectors.html
%
% [x, y, a, b, c, desc ] = readFeatures(file, type)
%   Parameters:
%     file  -   Filename of the image / prefix of the tmp file
%     type  -   Type of feature
%
%   Returns:
%     x     -  X coordinates of key points 
%     y     -  Y coordinates of key points
%     a     -  Affine parameter A
%     b     -  Affine parameter B
%     c     -  Affine parameter C
%     desc  -  Descriptions
%     
function [x, y, a, b, c, desc] = readFeatures(file, type)

% Are we running Windows or Unix?

if ispc
    system(['extract_features.exe -', type, ' -i ', file, ' -sift -o1 ', file, '.', type, '.sift']);
elseif isunix
    system(['./extract_features.ln -', type, ' -i ', file, ' -sift -o1 ', file, '.', type, '.sift']);
else
    error('Unsupported OS. Please use a Windows or Unix based system.');
end

M = importdata([file, '.', type, '.sift'], ' ', 2);

x = M.data(:, 1);
y = M.data(:, 2);
a = M.data(:, 3);
b = M.data(:, 4);
c = M.data(:, 5);

desc = M.data(:, 6:end);

% Perform cleanup

if ispc
    system(['del ', file, '.', type, '.sift']);
else
    system(['rm ', file, '.', type, '.sift']);
end

end
