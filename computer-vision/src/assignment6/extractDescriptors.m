%% EXTRACTDESCRIPTORS Wrapper for readFeatures
%
% - Uses readFeatures to extract haraff and hesaff features and descriptors.
% - Combines them into one set of feature points and one set of descriptors.
%
% [points desc] = extractDescriptors(image)
%   Parameters:
%     image   -  Filename of the image
%   
%   Returns:
%     points  -  Concatenated haraff and hesaff key points
%     desc    -  Concatenated haraff and hesaff descriptors
%
% See also:
% READFEATURES
function [points desc] = extractDescriptors(image)

[x11 y11 a11 b11 c11 desc11] = readFeatures(image, 'haraff');
[x12 y12 a12 b12 c12 desc12] = readFeatures(image, 'hesaff');
x1 = [x11; x12];
y1 = [y11; y12];

points = [x1 y1];
desc = [desc11; desc12];

end
