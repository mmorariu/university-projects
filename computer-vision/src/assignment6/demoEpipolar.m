image1 = 'obj02_001.png';
image2 = 'obj02_002.png';

I1 = imread(image1);
I2 = imread(image2);

[x1 y1 a1 b1 c1 desc1] = readFeatures(image1, 'haraff');
[x2 y2 a2 b2 c2 desc2] = readFeatures(image2, 'haraff');

display('Matching images...');
[matches, scores] = vl_ubcmatch(desc1', desc2', 4);
display('Done');

points1 = [x1 y1];
points2 = [x2 y2];
matched_points1 = points1(matches(1, :), :);
matched_points2 = points2(matches(2, :), :);

[fLMedS inliers] = ransac(matched_points1', matched_points2', 10000, 0.01);

figure;
subplot(121); imshow(I1); title('Inliers and Epipolar Lines in First Image'); hold on;
plot(matched_points1(inliers,1), matched_points1(inliers,2), 'go')
epiLines = epipolarLine(fLMedS', matched_points2(inliers, :));
pts = lineToBorderPoints(epiLines, size(I1));
line(pts(:, [1,3])', pts(:, [2,4])');

subplot(122); imshow(I2); title('Inliers and Epipole Lines in Second Image'); hold on;
plot(matched_points2(inliers,1), matched_points2(inliers,2), 'go')
epiLines = epipolarLine(fLMedS, matched_points1(inliers, :));
pts = lineToBorderPoints(epiLines, size(I2));
line(pts(:, [1,3])', pts(:, [2,4])');

truesize;