I = im2double(rgb2gray(imread('zebra.png')));
figure(1);

thresh = 0.08;

for sigma = 1:2:11
    [mag, orient] = gradmad(I, sigma);
    mag(mag < thresh) = 0;
    mag(mag >= thresh) = 255;    
    subplot(2, 3, (sigma+1)/2);
    imshow(mag);
    title(['\fontsize{15}\sigma = ', num2str(sigma)]);
end
