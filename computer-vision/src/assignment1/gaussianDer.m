function Gd = gaussianDer(G, sigma)

% Discretize the interval [-3 sigma...3 sigma] and compute the Gaussian
% derivative for each value x from the discretized interval

x = linspace(-3*sigma, 3*sigma, 6*floor(sigma) + 1);
Gd = -x/(sigma^2).*G;
end