% imOut = GAUSSIANCONV(image_path, sigma_x, sigma_y)
%
% Convolves an image with a gaussian in the x and y direction.
% Uses the GAUSSIAN function to the create the gaussians and uses
% the CONV2 function to convolve the image with the 'same' parameter.
%
% The image is converted to a gray image.
%
% Parameters are:
%   image_path  =   Path to the image that should be convolved
%   sigma_x     =   The sigma parameter for the gaussian that will be used to
%                   convolve the image in the x direction
%   sigma_y     =   The sigma parameter for the gaussian that will be used to
%                   convolve the image in the y direction
%
% Returns:
%   imOut       =   The convolved image
%
% See also:
% GAUSSIAN, CONV2
function imOut = gaussianConv(image_path, sigma_x, sigma_y)

% Create the Gaussian kernels corresponding to the X and Y axes

G_cols = gaussian(sigma_x);
G_rows = gaussian(sigma_y);

%Convolve the loaded image along the X and Y directions with G_x and G_y,
%respectively

I = im2double(rgb2gray(imread(image_path)));
imOut = conv2(G_rows, G_cols, I, 'same');

end
