function F = ImageDerivatives(img, sigma, type)
G = gaussian(sigma);
x = linspace(-3*sigma, 3*sigma, 6*floor(sigma) + 1);

Gd1 = gaussianDer(G, sigma);
Gd2 = (-sigma^2 + x.^2)/(sigma^4).*G;
id = [zeros(1, 3*floor(sigma)) 1 zeros(1, 3*floor(sigma))];

switch type
    case 'x'
        F = conv2(id, Gd1, img, 'same');
    case 'y'
        F = conv2(Gd1, id, img, 'same');
    case 'xy'
        F = conv2(id, Gd1, img, 'same');
        F = conv2(Gd1, id, F, 'same');
    case 'yx'
        F = conv2(Gd1, id, img, 'same');
        F = conv2(id, Gd1, F, 'same');
    case 'xx'
        F = conv2(id, Gd2, img, 'same');
    case 'yy'
        F = conv2(Gd2, id, img, 'same');
end

end