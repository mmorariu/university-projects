function checkGaussian(image_path, sigma)

% Convolve the image using our implementation

subplottight(1, 2, 1);
J = gaussianConv(image_path, sigma, sigma);
imshow(J);

% Convolve the image using MATLAB's implementation

subplottight(1, 2, 2);
I = im2double(rgb2gray(imread(image_path)));
G = fspecial('gaussian', 6*floor(sigma)+1, sigma);
K = conv2(I, G, 'same');
imshow(K);

end