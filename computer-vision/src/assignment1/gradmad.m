function [magnitude, orientation] = gradmad(img, sigma)

% Create the Gaussian derivatives kernels

G_x = gaussianDer(gaussian(sigma), sigma);
G_y = G_x';

% We convolve the image 

id = [zeros(1, 3*floor(sigma)) 1 zeros(1, 3*floor(sigma))];

% Convolve the input image with these kernels; compute the magnitude and
% the orientation of the gradient

I_x = conv2(id, G_x, img, 'same');
I_y = conv2(G_y, id', img, 'same');
magnitude = sqrt(I_x.^2 + I_y.^2);
orientation = atan2(I_y, I_x);

end