close all;
I = im2double(rgb2gray(imread('zebra.png')));

figure(1);

for sigma = 2:5:27;
    [mag, orient] = gradmad(I, sigma);
    subplot(2, 3, (sigma+3)/5);
    imshow(mag);

% Uncomment the following code to visualize the histograms of gradient
% magnitude
    
%     hist(mag(:), 512);
%     grid on;

    title(['\fontsize{15}\sigma = ', num2str(sigma)]);
end

figure(2);
id = 1:20:512;
[x, y] = meshgrid(id, id);

colorbar;

for sigma = 2:5:27;
    [mag, orient] = gradmad(I, sigma);
    
    subplot(2, 3, (sigma+3)/5);
    
% Uncomment the following code to visualize the histograms of gradient
% orientation
    
%     hist(orient(:), 360);
%     grid on;
%     axis([-pi pi 0 3000]);
    
    orient = orient(id, id);
    mag = mag(id, id);
    
    imshow(I);
    hold on;
    quiver(x, y, cos(orient).*mag, sin(orient).*mag, 'Color', 'b');
    title(['\fontsize{15}\sigma = ', num2str(sigma)]);
end