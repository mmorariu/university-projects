% G = GAUSSIAN(sigma)
%
% Returns a gaussian kernel for a specific sigma.
% The kernel size is determined by 6*floor(sigma)+1 and the gaussian is
% applied to the values from -3*sigma to 3*sigma by linspace.
% 
% Parameters are:
%   sigma   =   The sigma parameter
%
% Returns:
%   G       =   The gaussian kernel
%
% See also:
% LINSPACE
function G = gaussian(sigma)

% Discretize the interval -3 sigma...3 sigma and compute G_sigma for each
% value x from the discretized interval

x = linspace(-3*sigma, 3*sigma, 6*floor(sigma) + 1);
G = 1 / (sigma*sqrt(2*pi)) * exp(-(x.^2)/(2*sigma^2));

end
