size = 150;
I = zeros(size, size);
I(ceil(size/2), ceil(size/2)) = 255;

sigma = 11;
G = fspecial('gaussian', 6*floor(sigma)+1, sigma);
I0 = conv2(I, G, 'same');
I1 = ImageDerivatives(I, sigma, 'xy');
I2 = ImageDerivatives(I, sigma, 'xx');
I2 = ImageDerivatives(I2, sigma, 'yy');

subplot(1, 3, 1);
imshow(I0, []);
title('0th Order Derivative');
subplot(1, 3, 2);
imshow(I1, []);
title('1st Order Derivative');
subplot(1, 3, 3);
imshow(I2, []);
title('2nd Order Derivative');