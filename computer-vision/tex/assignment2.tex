\documentclass[11pt]{article}
\usepackage{anysize}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{subcaption}
\usepackage[vmargin=3cm, hmargin=2cm]{geometry}
\usepackage[numbered, framed]{mcode}

\title{\textsc{Computer Vision} \\ \textbf{Assignment 2} \\ \textit{Harris Corner Detector and SIFT Descriptor}}
\author{Sander Latour \\ 5743044 \and Mihai Morariu \\ 10232710}
\date{\today}

\begin{document}

\maketitle

\section{Harris Corner Detector}
\subsection{Cornerness}
%In this exercise, we implement the Harris corner detector algorithm. In order to do this, the first step is to compute the elements of the structure tensor matrix $M$ and, consequently, the response matrix $R$. The \emph{createResponseMatrix} function takes an image $I$, the patch size $wsize$ and the constant $k$ as input parameters and produces as output the response matrix $R$ corresponding to $I$. In our experiments, the $k$ constant is set to 0.05 (during the course, a value in the range 0.04-0.06 was recommended), which empirically shows to provide good results, and a $5 \times 5$ pixels patch around each image pixel. 

In order to detect corners we need a way to describe how much something looks like a corner. In the Harris corner detection algorithm this is represented by the measure of corner response R. The structure tensor, or second moment matrix, M, can be used to calculate the corner response efficiently and is denoted by equation \ref{eq:tensor}. The structure tensor M is calculated for a window around the point of interest by applying the window function $w(x,y)$. The implementation presented in this report uses a box window function as denoted by equation \ref{eq:window}, where $\delta$ denotes the number of extra pixels in all directions taken into account by the window and $i$ and $j$ denote the coordinates of the point of interest. The window function effectively creates a window of size $(2\delta+1)$x$(2\delta+1)$ for each point of interest.

\begin{equation}
\label{eq:tensor}
M_{i,j} = \sum\limits_{x,y} w_{i,j}(x,y) \left[ \begin{array}{cc} I_x^2 & I_x I_y \\ I_x I_y & I_y^2 \end{array} \right]
\end{equation}

\begin{equation}
\label{eq:window}
%w_{i,j}(x,y) = \left\{ \begin{array}{ll} 1, & \left[ \begin{array}{l} i \\ j \end{array} \right] - \delta \leq \left[ \begin{array}{l} x \\ y \end{array} \right] \leq \left[ \begin{array}{l} i \\ j \end{array} \right] + \delta \\ 0, & other \end{array} \right.
w_{i,j}(x,y) = \left\{ \begin{array}{ll} 1, & \left[ \begin{array}{l} x \\ y \end{array} \right] \in \left[ i - \delta, i + \delta \right] \times \left[ j - \delta, j + \delta \right] \\ 0, & other \end{array} \right.
\end{equation}

A good corner has high intensity changes in all directions. To capture this into a single value one can use the measure corner response as denoted by equation \ref{eq:r}, where $\lambda_x$ and $\lambda_y$ denote the eigenvalues of structure tensor M and $k$ denotes an empirical constant. However, since the determinant of M is equal to $\lambda_x \lambda_y$ and the trace of M is equal to $\lambda_x + \lambda_y$ the equation can be rewritten into equation \ref{eq:r2}. Our harris corner detector implementation uses equation \ref{eq:r2} to determine the cornerness of a specific point based on the intensity changes within a window of 5x5 pixels and the empirical constant k set to 0.05\footnote{This value is typically between 0.04 and 0.06}.

\begin{equation}
\label{eq:r}
R(i,j) = \lambda_x \lambda_y - k(\lambda_x + \lambda_y)^2
\end{equation}

\begin{equation}
\label{eq:r2}
R(i,j) = det(M_{i,j}) - k(trace(M_{i,j}))^2
\end{equation}
\clearpage
In our implementation the function \textsc{createResponseMatrix} generates corner response matrix $\mathcal{R}$ that contains the cornerness of each pixel, as returned by equation \ref{eq:r2}, of an image. Matrix $\mathcal{R}$, as shown by equation \ref{eq:R}, is however zero for the pixels in a border of width $\delta$, because the window will not fit at those locations. In other applications where this problem occurs, such as in applying convolution close to the border, the solution is typically searched in enlarging the image by adding zeros, mirroring the image or copying the nearest neighbour. However, this might introduce artifacts when calculating the derivative afterwards. We therefore decided to go with an inward border, based on the assumption that important corners will not likely be close to the border.

\begin{equation}
\label{eq:R}
\mathcal{R} = \left[
    \begin{array}{ccccccc} 
        0 & 0 & 0 & \hdots & 0 & 0 & 0 \\
        0 & 0 & 0 & \hdots & 0 & 0 & 0 \\
        0 & 0 & R(\delta+1,\delta+1) & \hdots & R(\delta+1,w-\delta-1) & 0 & 0 \\
        \vdots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots \\
        0 & 0 & R(h-\delta-1,\delta+1) & \hdots & R(h-\delta-1,w-\delta-1) & 0 & 0 \\
        0 & 0 & 0 & \hdots & 0 & 0 & 0 \\
        0 & 0 & 0 & \hdots & 0 & 0 & 0 \\
    \end{array}
\right]
\end{equation}

\subsection{Finding corners}
\label{sec:finding_corners}
Corners are obtained by selecting local extrema from the $\mathcal{R}$ matrix which are above a predefined threshold. Local extrema are found by sliding a window over the R matrix and determining on each window position where the maximum corner response lies. If the local extremum corner response is above a given threshold, the corner is stored. The window is moved by the size of the window. In our implementation, the threshold was set to 0.001, a value we empirically found to work well with the images we have tested the implementation on.

The method described permits extremum corner responses to be found everywhere in the window. That means that the found extrema were not necessarily in the center of window when it was determined that they were bigger than the other points. In order to check that the found point is indeed the best corner in its area, a window should be placed on the found location and the criterea checked. We have implemented an additional iterative approach that loops through all found corners and checks their neighborhoods for possible better corners. After each iteration the newly found list of corners is compared with the list of corners of the previous iteration, the method continues until convergence. This iterative method is optional in our implementation. Figure \ref{fig:convergence1} shows the corners found in the \emph{cocacola.jpg} image after the initial round of harris and after the iterative method converged. Figure \ref{fig:convergence2} shows the same setup for a larger window size when finding the local extrema. 

\begin{figure}[!h]
\centering
  \begin{subfigure}[b]{\linewidth}
    \includegraphics[scale=.45]{convergence_cocacola_2_001}
    \caption{Threshold set to 0.001 and a window of 5x5. After the first round 383 corners were found (left). After 11 iterations the result converged to 135 corners (right).}
    \label{fig:convergence1}
  \end{subfigure}
  \begin{subfigure}[b]{\linewidth}
    \includegraphics[scale=.45]{convergence_cocacola_4_001}
    \caption{Threshold set to 0.001 and a window of 9x9. After the first round 191 corners were found (left). After 6 iterations the result converged to 75 corners (right).}
    \label{fig:convergence2}
  \end{subfigure}
    \caption{Comparison of detected corners after the first round of the harris corner detector and after the iterative method converged.}
\end{figure}

\subsection{Scale invariance}
The harris corner detector described so far is effective in finding corners that all exist on the same scale. In order to make the harris corner detector scale invariant, the method is extended with a Gaussian pyramid to take multiple scales into account. The corner response matrix $\mathcal{R}$ is computed for every scale in the Gaussian pyramid. The harris corner detector implementation is altered to look for the extremum corner response within the window on all scales. In our implementation we search for eight scales. 

%In order to make the detector scale-invariant, we compute the response matrices $R_i$ at different scales $\sigma_i \in \left\{ 1, 2, ..., N_s \right\}$  of the image $I$, where $N_s$ is the number of scales we take into consideration (in our case, this is set to 8). Next, an $N \times N$ patch is slid across the image. For each patch location, we search for the scale $s$ and the pixel location $\left( x, y \right)$ at which a local extremum response is found within the patch bounds. If the value of the response is above the predefined threshold, then the detected pixel is considered to be "most cornerish" at scale $s$. 

The function \textsc{harris} implements the scale-invariant detector and takes as input parameters the image $I$, the patch size $N$, the response threshold $t_r$, a boolean parameter on whether or not to do the additional iterative optimization and two boolean parameters on whether or not to display the visual results after the first round and after the iterations. The output consists in the rows and the columns of the detected keypoints, as well as the scales at which their response is maximum. 

\subsection{Choosing the threshold and window size}

\begin{figure}[!h]
\centering
  \begin{subfigure}[b]{0.49\linewidth}
    \includegraphics[scale=.45]{corners_cocacola_2_001.png}
    \caption{Threshold: 0.001, window: 5x5, corners: 135.}
    \label{fig:thresh_2_001}
  \end{subfigure}
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[scale=.45]{corners_cocacola_4_001.png}
    \caption{Threshold: 0.001, window: 9x9, corners: 75.}
    \label{fig:thresh_4_001}
  \end{subfigure}
  \begin{subfigure}[b]{0.49\linewidth}
    \includegraphics[scale=.45]{corners_cocacola_2_002.png}
    \caption{Threshold: 0.002, window: 5x5, corners: 60.}
    \label{fig:thresh_2_002}
  \end{subfigure}
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[scale=.45]{corners_cocacola_4_002.png}
    \caption{Threshold: 0.002, window: 9x9, corners: 55.}
    \label{fig:thresh_4_002}
  \end{subfigure}
  \begin{subfigure}[b]{0.49\linewidth}
    \includegraphics[scale=.45]{corners_cocacola_2_003.png}
    \caption{Threshold: 0.003, window: 5x5, corners: 33.}
    \label{fig:thresh_2_003}
  \end{subfigure}
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[scale=.45]{corners_cocacola_4_003.png}
    \caption{Threshold: 0.003, window: 9x9, corners: 35.}
    \label{fig:thresh_4_003}
  \end{subfigure}
  \caption{Overview of found corners for various thresholds and window sizes}
  \label{fig:thresh}
\end{figure}

\begin{figure}[!h]
\centering
  \begin{subfigure}[b]{0.49\linewidth}
    \includegraphics[scale=.4]{corners_landscape-b_2_001.png}
    \caption{Threshold: 0.001, window: 5x5, corners: 931.}
    \label{fig:thresh2_2_001}
  \end{subfigure}
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[scale=.4]{corners_landscape-b_4_001.png}
    \caption{Threshold: 0.001, window: 9x9, corners: 502.}
    \label{fig:thresh2_4_001}
  \end{subfigure}
  \begin{subfigure}[b]{0.49\linewidth}
    \includegraphics[scale=.4]{corners_landscape-b_2_003.png}
    \caption{Threshold: 0.003, window: 5x5, corners: 278.}
    \label{fig:thresh2_2_003}
  \end{subfigure}
  \begin{subfigure}[b]{0.5\linewidth}
    \includegraphics[scale=.4]{corners_landscape-b_4_003.png}
    \caption{Threshold: 0.003, window: 9x9, corners: 220.}
    \label{fig:thresh2_4_003}
  \end{subfigure}
  \caption{Overview of found corners for various thresholds and window sizes}
  \label{fig:thresh2}
\end{figure}

Figures \ref{fig:thresh} and \ref{fig:thresh2} show the effect of changing the parameter values of the threshold and window size, and several observations can be made. First, the threshold determines the minimum strength of the corner response needed in order to be a corner and therefore, non surprisingly, lowers the number of found corners when its value is increased. Second, the window size determines the size of the area in which each pixel needs to be dominant and therefore, also not surprisingly, lowers the number of found corners as well when its value is increased. However, the influence of the window size parameter is reduced when the threshold is higher, this is because with a higher threshold the resulting corners are typically farther apart. We conclude from this that when you need to use a low threshold, in order to keep specific crucial corners with a lower response, you would probably need to increase the window size. In general there is a trade-off between having only few strong corners and having many weak corners. This trade-off becomes important when matching images, since if too few keypoints are detected, there might be too few or even no descriptors to be matched. On the other hand, a large number of keypoints not only leads to an increase in computational complexity, since more descriptors need to be matched in that case, but might also lead to erroneous matches.

\section{Image Matching with SIFT}
\subsection{Implementation}
The Harris corner keypoints can be used together with the SIFT descriptor in image matching.In order to do this, we use the open source library {\bf VLFeat}\footnote{http://www.vlfeat.org/} to create and match descriptors. We implemented the \textsc{matchImages} function, which takes as input parameters the images $I_1$ and $I_2$ to be matched and produces as output a new figure with $I_1$ and $I_2$ aligned next to each other and patches corresponding to similar descriptors connected by a line. The {\bf vl\_sift} class of VLFeat is used to create the SIFT descriptors, using as input parameters the rows and the columns of the Harris keypoints and the sizes of the surrounding patches, which in our implementation are set to be equal to the characteristic scale of each corner. The orientation of the patches is computed automatically. The function returns the rows and the columns of the feature keypoints, as well as the SIFT descriptors, stored as 128-elements vectors, one per keypoint.

\begin{lstlisting}
[keypoints descriptors] = vl_sift(I, 'frames', [cols; rows; scales; zeros(size(rows))],
                                     'orientations');
\end{lstlisting}
Once the descriptors are computed for both images, the next step is to match them. This is done using the {\bf vl\_ubcmatch} function of VLFeat, which takes as input parameters the two descriptor vectors, but also a user specified threshold $t_d$. This threshold is used to reject ambiguous matches, such that a descriptor $D$ corresponding to the first image is matched to a descriptor $D_i$ corresponding to the second image only if
\[ d \left( D, D_i \right) t_d \leq d \left( D, D_j \right),\ \ j \neq i \]
where $D_j$ denotes a descriptor of the second image and $d$ the distance between a pair of descriptors. 
\begin{lstlisting}
matches = vl_ubcmatch(descriptors1, descriptors2, match_thresh);
\end{lstlisting}

An example of two matched images matched with SIFT is shown in figure \ref{fig:4}.
\begin{figure}[!h]
\hspace{-1cm}
\includegraphics[sclae=0.7]{LandscapeMatching_1_5}
\caption{Matched images}
\label{fig:4}
\end{figure}

\subsection{Comparing effect of iterations}
To fully understand the implications of the added iterations in the harris corner detector, as described in section \ref{sec:finding_corners}, we have conducted a little experiment to visualise its effect. Figure \ref{imagematch_iteration_compare} shows the matches between two images, with and without using the iterative optimization. First, it is noticable that running the version with iterations takes considerably longer (287 seconds) than the version without (100 seconds). Second, not only are there less keypoints to be matched, which was already discussed, but also the number of good matches can be lower than when not using the iterations. This is even better visible in figure \ref{fig:imagematch_zoom_iteration_compare}, which shows a small part of the image zoomed in. It is clear that there is a loss in number of matches, but it is not completely clear whether these matches were supposed to be kept. In figure \ref{fig:imagematch_zoom_iteration_compare1} it is clear that several keypoints are too close to other keypoints and should not be there. However, in some of these locations had absolutely no corner left in figure \ref{fig:imagematch_zoom_iteration_compare2} and the optimized corners were not always matches. Because of this we decided that it was not absolutely certain that the iterative optimization would have no unwanted side-effects. We therefore made the iterations optional, as was stated before.
\begin{figure}[ht]
    \begin{subfigure}[b]{\linewidth}
        \includegraphics[scale=0.5]{imagematch_2_colors_noniterative.png}
        \caption{Matching corners found without iterations. Duration: 100 seconds}
        \label{fig:imagematch_iteration_compare1}
    \end{subfigure}
    \begin{subfigure}[b]{\linewidth}
        \includegraphics[scale=0.5]{imagematch_2_colors_iterative.png}
        \caption{Matching corners found with iterations. Duration: 287 seconds}
        \label{fig:imagematch_iteration_compare2}
    \end{subfigure}
    \caption{Comparison of matches between images with and without iterations. In both images the threshold for matches was set to 2 and the threshold for the cornerness response was set to 0.001}
    \label{fig:imagematch_iteration_compare}
\end{figure}

\begin{figure}[ht]
    \begin{subfigure}[b]{\linewidth}
        \includegraphics[scale=0.5]{imagematch_2_zoom_colors_noniterative.png}
        \caption{Matching corners found without iterations.}
        \label{fig:imagematch_zoom_iteration_compare1}
    \end{subfigure}
    \begin{subfigure}[b]{\linewidth}
        \includegraphics[scale=0.5]{imagematch_2_zoom_colors_iterative.png}
        \caption{Matching corners found with iterations.}
        \label{fig:imagematch_zoom_iteration_compare2}
    \end{subfigure}
    \caption{Comparison of matches between a zoomed area in both images with and without iterations. In both images the threshold for matches was set to 2 and the threshold for the cornerness response was set to 0.001}
    \label{fig:imagematch_zoom_iteration_compare}
\end{figure}

\subsection{Comparing effect of match treshold}
%% Say something about the matches in terms of threshold
Figure \ref{fig:imagematch_alt_thresh} shows the results of image matching when the threshold for matching is set to 1.5 instead of 2. Not unexpectedly the number of matches go up considerably compared to the results in figure \ref{fig:imagematch_iteration_compare}. Of course it is not easy to automatically know how good these matches are without some ground truth. In order to have some idea of the performance we tried to match an image to itself. An example of the result can be seen in figure \ref{fig:imagematch_self}. However, there was no difference in results when varying the theshold in this setup. Also the addition of iterations did not result in changes between 1.5 and 2.0. For future work we recommend to repeat the experiment, but then compare an image to a transformed version of itself.
\begin{figure}[ht]
    \begin{subfigure}[b]{\linewidth}
        \includegraphics[scale=0.5]{imagematch_1_5_colors_noniterative.png}
        \caption{Matching corners without iterations.}
        \label{fig:imagematch_alt_thresh1}
    \end{subfigure}
    \begin{subfigure}[b]{\linewidth}
        \includegraphics[scale=0.5]{imagematch_1_5_colors_iterative.png}
        \caption{Matching corners found with iterations.}
        \label{fig:imagematch_alt_thresh2}
    \end{subfigure}
    \caption{Comparison of matches between images with and without iterations. In both images the threshold for matches was set to 1.5 and the threshold for the cornerness response was set to 0.001}
    \label{fig:imagematch_alt_thresh}
\end{figure}

\begin{figure}[ht]
\includegraphics[scale=0.5]{imagematch_self_1_5_colors_iterative.png}
\caption{Matching corners with iterations between an image and itself using matching threshold 1.5.}
\label{fig:imagematch_self}
\end{figure}        
\end{document}
