\documentclass[11pt]{article}
\usepackage{anysize}
\usepackage{graphicx}

\title{Computer Vision AI - Assignment 1 \\ Linear Filters: Gaussians and Derivatives}
\author{Sander Latour \\ Mihai Morariu (10232710)}
\date{}
\newcommand{\dfrac}{\displaystyle\frac}

\begin{document}

\maketitle

\section{Gaussian Filters}
\

\subsection{1D Gaussian Filter}
\label{sec:gaussian_filter}
In order to create a 1D Gaussian filter, we discretize the interval $\langle -3\sigma, 3\sigma \rangle$ into $6\sigma + 1$ discrete values $x_i$, where $\{ 1 \le i \le 6 \lfloor \sigma \rfloor +1 | i \in \mathbb{N} \}$, and compute $G_\sigma$ at each value $x_i$. We chose $-3\sigma$ and $3\sigma$ as the lower and upper boundaries of the interval since at these limits the Gaussian function closely reaches the X axis and (practially) no new weights are introduced outside this range.

\subsection{Convolving an Image with a 2D Gaussian}

We use the function created at the previous step to create two 1D Gaussian kernels, $G_{cols}$ and $G_{rows}$, where the parameter $\sigma_x$ corresponds to $G_{cols}$ and the parameter $\sigma_y$ corresponds to $G_{rows}$. We use the MATLAB function \emph{conv2} to convolve the image $I$ with $G_{rows}$ along the Y axis and with $G_{cols}$ along the X axis.

\subsection{Comparing with MATLAB's Gaussian Filter}

%As shown in figure \ref{fig:1}, in our implementation a black border is introduced in the image, whereas this does not occur when using MATLAB's \emph{conv2}. This is because we do not add an extra border, using one of the methods presented during the course (clip filter, wrap around, copy edge etc), which prevents the filter window from falling off the edge of the image.

Figure \ref{fig:1} shows the result of two implementations of the gaussian filter, our implementation from section \ref{sec:gaussian_filter} and the native implementation provided by fspecial. It is clear to see that both implementations have artifacts around the edges, in the sense that a black border was introduced. This happens because around the edges the convolution function conv2, for any kernel larger than 1x1, requires pixel values of neighbors that are not there. To overcome this problem the conv2 function introduces a black border around the image and uses the newly introduced pixels in the convolution. This has the result as seen in figure \ref{fig:1}. Possible solutions for this problem involve different strategies in solving the missing pixel problems around the edges, alternatives are: filling the border with the mirrored image ; filling the border with the pixels on the opposite side of the image ; filling the border with the nearest neigbor.

\begin{figure}[!h]
\centering
\includegraphics{CheckImplementation5}
\caption{Our implementation of 2D Gaussian convolution (left) and MATLAB's implementation (right) for an input image, using $\sigma=5$}
\label{fig:1}
\end{figure}

\subsection{Gaussian Derivative}

In order to compute the Gaussian first order derivative, we again discretize the interval $\left[ -3\sigma, 3\sigma \right]$ and compute $\dfrac{d}{dx}G_{\sigma} = -\dfrac{x}{\sigma^2} G_{\sigma}$ at every $x_i \in \left[-3\sigma, 3\sigma\right]$, $i = \overline{1, 6\sigma+1}$.

\subsection{Gradient Magnitude and Orientation}

To compute the gradient of an input image, we first require the derivatives $I_x$ and $I_y$ of the image along the X and Y axes. As mentioned in the course, these can be obtained by convolving the image with the derivative of a Gaussian kernel along the X and Y axes, respectively. The magnitude and the orientation of the gradient at each pixel $(u, v)$ in the input image are obtained using $\| \nabla I(u, v) \| = \sqrt{{I_x(u, v)}^2 + {I_y(u, v)}^2}$ and $\angle \nabla I(u, v) = \arctan \dfrac{I_y(u, v)}{I_x(u, v)}$. The results are shown in figure \ref{fig:2}.

\subsubsection{Gradient Magnitude and Orientation Results}

\begin{figure}[!h]
\centering
\includegraphics[scale=.47]{Magnitude3}
\includegraphics[scale=.53]{Orientation3}
\caption{Magnitude of gradient (left) and its orientation map (right) for an input image, using $\sigma=3$}
\label{fig:2}\begin{flushleft}\end{flushleft}
\end{figure}

\subsubsection{Varying $\sigma$}

In figure \ref{fig:3} we present the magnitude images obtained for $\sigma \in \left\{ 2, 7, 12, 17, 22, 27 \right\}$. As it can be seen, increasing the value of $\sigma$ results in edges becoming more difficult to be noticed, up to the point where they are barely visible or not visible at all. This is explained by the fact that an image gets increasingly blurred as the value of $\sigma$ goes up, due to the averaging effect induced by the Gaussian kernel, causing neighboring pixels to get similar intensity values.

\begin{figure}[!h]
\hspace{-1cm}
\includegraphics[scale=.61]{VarySigmaMagnitude}
\caption{Magnitude images corresponding to increasing $\sigma$ values}
\label{fig:3}
\end{figure}

\begin{figure}[!h]
\hspace{-2cm}
\includegraphics[scale=.6]{VarySigmaMagnitudeHist}
\caption{Histograms of gradient magnitudes corresponding to different values of $\sigma$}
\label{fig:4}
\end{figure}

As intensity values of neighboring pixels get closer to each other, derivative values approach zero, which in turn results in gradient magnitudes approaching zero. This is also confirmed by figure \ref{fig:4}, where we plotted the histograms of gradient magnitudes corresponding to the aforementioned $\sigma$ values. Here, we can see two effects of increasing the value of $\sigma$. The first one is the tendency to remove peaks (when $\sigma=2$, the frequency of gradient magnitude values equal to zero is close to 15000, whereas when $\sigma=27$, this drops below 25), which is intuitive, since the purpose of a Gaussian kernel is to smooth out a signal. The second one, which explains our results, is that the maximum gradient magnitude value decreases towards zero as $\sigma$ increases. In the first image, we see that the maximum value is slightly above 0.2, whereas in the last image it gets slightly above 0.015.

\

We also computed the histograms of gradient orientations corresponding to the different values of $\sigma$. We notice that all images display the same patterns: peaks at $\pm\pi$, $\pm\dfrac{\pi}{2}$ and 0, which suggests a large number of pixels at which the horizontal and vertical derivatives of the image are zero. However, as we increase $\sigma$, we notice an increasingly larger peak at 0, which could suggest a large number of pixels at which the vertical derivative of the image is zero, since the orientation is given by the formula $\angle \nabla I(u, v) = \arctan \dfrac{I_y(u, v)}{I_x(u, v)}$. But this could also suggest an increasingly number of pixels at which the horizontal and vertical edges are equal to zero, since in MATLAB $\arctan(0, 0)$ is defined as 0.

\

\begin{figure}[!h]
\hspace{-1.5cm}
\includegraphics[scale=.65]{VarySigmaOrientation}
\caption{Orientation images corresponding to increasing $\sigma$ values}
\label{fig:5}
\end{figure}

\begin{figure}[!h]
\hspace{-2cm}
\includegraphics[scale=.6]{VarySigmaOrientationHist}
\caption{Histograms of gradient orientations corresponding to different values of $\sigma$}
\label{fig:6}
\end{figure}

\subsubsection{Gradient Magnitude Threshold}

For this exercise, we used $\sigma \in \left\{ 1, 3, 5, 7, 9, 11 \right\}$ and threshold values of $t \in \left\{ 0.02, 0.04, 0.06 \right\}$

\begin{figure}[!h]
\centering
\includegraphics[scale=.5]{Threshold002}
\caption{Thresholded images using $t = 0.02$}	
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=.5]{Threshold004}
\caption{Thresholded images using $t = 0.04$}	
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=.5]{Threshold006}
\caption{Thresholded images using $t = 0.06$}
\end{figure}

\subsubsection{Gaussian Derivatives}

In order to compute the Gaussian derivatives, we again discretized the interval $\left[ -3 \sigma, 3 \sigma \right]$ and computed the derivative values at each value $x$ from the discretized interval.

\subsubsection{Impulse Image}

Below we present the Gaussian derivatives of the impulse image using $\sigma \in \left\{ 3, 7, 11 \right\}$. On the left side we display the 0th order derivative, in the middle we display the 1st order derivative and on the right side the 2nd order derivative.

\

\begin{figure}[!h]
\centering
\includegraphics{Impulse3}
\caption{Gaussian derivatives of the impulse image using $\sigma = 3$}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics{Impulse7}
\caption{Gaussian derivatives of the impulse image using $\sigma = 7$}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics{Impulse11}
\caption{Gaussian derivatives of the impulse image using $\sigma = 11$}
\end{figure}

\end{document}
