\documentclass[conference]{IEEEtran}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{hyperref}
\hyphenation{op-tical net-works semi-conduc-tor}

\begin{document}
\title{A Rule-Based System for Controlling Agent Behaviour in the Domination Game}
\author{\IEEEauthorblockN{Koen Bonenkamp}
\IEEEauthorblockA{koen.bonenkamp@gmail.com}
\and
\IEEEauthorblockN{Vasileios Maragkos}
\IEEEauthorblockA{vasileios.maragkos@gmail.com}
\and
\IEEEauthorblockN{Mihai Morariu}
\IEEEauthorblockA{mihaimorariu@gmail.com}
\and
\IEEEauthorblockN{Nikolas Tzimoulis}
\IEEEauthorblockA{nikolaos@science.uva.nl}}

\maketitle

\begin{abstract}
%Designing a competitive multi-agent system that is able to take optimal actions in a partially observable environment is a challenging task. It requires learning a model of the environement and / or the opponent that can be used to plan a winning strategy for the competitive agent. These types of algorithms are usually known as "learning algorithms" and have proven successful in many real-world applications. Another type of algorithms that are commonly used in multi-agent systems are known as "rule-based algorithms". In the latter case, agents are pre-programmed to follow built-in strategies for different states they end up in. While they are easier to implement and typically run faster, they are not always guaranteed to yield an effective solution. In this paper, we present the solution  we developed for the final tournament of the \emph{Domination} game.
Designing a competitive squad-based multi-agent system that is capable of acting with reasonable competence in a partially observable environment is a challenging task. There are generally two schools of thought regarding how to tackle a problem like this. One approach is to codify the available knowledge of the domain into an algorithm. We call this approach rule-based, as a set of rules is one of the most straightforward and intuitive ways to achieve this. The other approach is to develop a system, dubbed a learning system, capable of extracting knowledge from the problem domain and then applying this knowledge during play. The \emph{Domination Game} is a simple team-based multiplayer game that can be used as a testing ground for both the aforementioned approaches. In this paper, we present a rule-based solution to the \emph{Domination Game} accompanied with experimental data about its performance. We also conduct an extensive discussion on the merits and shortfalls of both the rule-based and learning approach.
\end{abstract}

\IEEEpeerreviewmaketitle

\section{Introduction}
\label{sec:introduction}

The \emph{Domination Game} \cite{domination} is a finite-horizon multi-agent game in which two teams of agents compete for the possession of three control points. A control point is captured the moment after a team has more agents standing on it than the other one and remains in in that team's possession unless captured by the other team. Each agents has a limited range of visibility, making the world partially observable, but is allowed to communicate with their teammates. The map contains packs of ammo which an agent can collect and use to shoot at enemies. Whenever an agent is shot, it remains out of play for a specific duration and then respawns at a designated area, losing any ammo gathered. The map is procedurally generated at the beginning of each match. It is always symmetric with respect to the teams, but other than that the control points, packs of ammo and walls that need to be navigated around are placed randomly, 

The teams start with an equal number of points. At each time step, a team's score is incremented or decremented based on the difference between the number of control points each team possesses. The game ends when either the time runs out, or one of the teams runs out of points. The team with the highest score is declared the winner.

%In this paper we describe the approach that we used for the \emph{Domination} game. We implemented both a rule-based agent and a learning agent, but used the rule-based one for the final tournament of the game. We present both of them and describe the issues we encountered with the learning agent and the reasons for which we decided to not submit the latter one.
%since, from our perspective, a learning algorithm would be time-consuming for this game and possibly computationally expensive, depending on how the state space is chosen. We also implemented a learning agent and we describe 
%From our experience from the previous Multi Agent Systems course, it takes a relatively large amount of time (~3000 episodes for a 7x7 grid) in order to learn an optimal policy by applying, for example, the Q-Learning algorithm.
%By the nature of the game, agents have a short amount of time in which they are required to take an action, otherwise they miss their turn. Therefore, an agent must be able to take decisions 

\subsection*{}

%The rest of the paper is organized as follows. Section \ref{sec:related_work} reviews the literature for multi-agent POMDPS and describes algorithms that are used to determine optimal policies for this type of problems. Section \ref{sec:reinforcement_learning} describes our learning agent and motivates our choice of submitting the rule-based agent for the final tournament. Section \ref{sec:methodology} describes the methodology we used for the tournament. Section \ref{sec:experimental_results} provides the results for the tests we have performed in order to assess the performance of our agent. Finally, section \ref{sec:conclusion} summarizes our findings and provides our ideas for further improvement.

The rest of the paper is organized as follows. Section \ref{sec:related_work} reviews the state of the practice of the game industry regarding the dilemma of using learning versus rule-based systems. Section \ref{sec:methodology} describes, in detail, our rule-based approach for solving the \emph{Domination Game}. Section \ref{sec:experimental_results} provides the results for the tests we have performed in order to assess the performance of our system. Then, in Section \ref{sec:reinforcement_learning} we discuss some theory and ideas for a learning system, as well as provide some motivation for our focus on the rule-based system. Finally, Section \ref{sec:conclusion} summarizes our findings and provides our ideas for further improvement.

\section{Related Work}
\label{sec:related_work}

In the game industry, the question of whether AI systems need to learn by themselves or be told in advance how to act seems to be mostly settled in favour of the latter approach. The state of the practice in the industry is heavily geared towards planning, with a strong emphasis on efficiency \cite{gameAIworks}. This is justified, in the sense that a lot of planning techniques are of high time and space complexity and therefore do not fit well with the real-time constraints of the medium. 

With that in mind, it is no wonder that techniques like Behaviour Trees \cite{behaviourtrees}, which are little more than a pre-programmed variation of decision trees, have been particularly popular in the recent past. On the other hand, even the word ``leaning'' is completely absent in the public discussions of industry participants, for example in the Game AI Conference in 2009 \cite{gameAIconf}. 

However, learning appears to introduce itself to the industry, mostly thanks to academic endeavours. For instance, the AIIDE 2010 Starcraft Competition \cite{starcraftcomp} was won by an agent developed in Berkeley \cite{overmind}. As part of its planning, the agent utilised potential fields whose parameters were tuned using reinforcement learning. 

Even though learning is not and probably cannot be used to solve comprehensive AI problems in the game industry for the time being, there is still a niche of sub-problems that can be effectively tackled using learning-based techniques, as long as the learning phase is part of the development process and does not happen in-game in real time.

\section{Methodology}
\label{sec:methodology}
In this section, we provide the methodology we used in our rule-based agent. We give an overview of the main challenges an agent faces throughout the game. We then analyze each of them separately and provide our solutions for tackling the respective problems. 

From our perspective, in order to win a game against the enemy, an agent must
\begin{itemize}
\item Collect ammopacks and prevent the enemy from getting into their possession 
\item Capture the control points and prevent the enemy from re-taking control over them
\end{itemize}
With these tasks in mind, a problem that we face is finding a way to share goals among agents. For this purpose, we split the agents into two subteams: \emph{scouts} and \emph{troopers}. Scouts denote agents that are assigned the task to explore the map in order to find the locations of the ammopacks and to collect them. Troopers denote agents that are assigned the task of taking over the control points and re-capturing them whenever they have knowledge of an enemy approaching that control point. 

We now describe the two mentioned tasks that an agent must perform throughout the game and explain how our agents deal with them.
 
\subsection{Collecting ammopacks}
\label{subsec:collecting_ammopacks}
Collecting ammopacks is essential in order to dominate the other team and win the match. But it is also important that the available ammopacks be split among teammates in order to increase the defense against the opponents. 

In our system, the agents that are in charge of collecting them are the scouts. Once a scout discovers and collects an ammopack, it is assigned the role of a trooper and another agent who has no ammo is designated to become a scout. At each time step, the number of agents that are assigned the role of becoming scouts is inversely proportional to the number of control points that are {\bf not} possessed by our team. We made this selection since we want our agents to focus on taking over the control points whenever they are in the possession of few of them and to focus on collecting ammopacks (or forcing the enemy into the respawn area) whenever they have control over the majority of control points (or all of them). Occasionally, a trooper may also collect an ammopack from his proximity if no other agent is closer to that ammopack. 

The behaviour of a scout is defined by the {\bf scoutBehaviour} function. A scout is provided with a list of the locations on the map where ammopacks were located up to that time step. Given this list, it decides towards which ammopack it should move based the distance between the current location and the locations of the previously seen ammopacks. If not all the ammopacks have been discovered, a scout begins to explore the map by moving towards the nodes on the mesh that were not previously visited. 

\begin{algorithm}
\caption{Code for the scoutBehaviour function}
\begin{algorithmic}
\vspace{0.2cm}
\STATE Given:
\begin{itemize}
\item The list of currently visible ammopack locations
\item The list of currently possessed / not possessed control point locations
\end{itemize}
\vspace{0.2cm}
\STATE Do the following:
\vspace{0.2cm}
\IF {agent does not have any goal \AND agent knows locations of ammopacks}
\STATE Filter ammopack locations based on distance
\STATE Get best ammopack location for the current agent
\STATE Set goal to this ammopack location
\ENDIF

\IF {not all ammo locations were found}
\STATE Set goal to the closest unexplored node
\ENDIF

\IF {everything else fails}
\STATE Stop being a scout
\STATE Become a trooper
\ENDIF
\vspace{0.2cm}
\end{algorithmic}
\end{algorithm}

\subsection{Capturing control points}
\label{subsec:capturing_control_points}
Capturing the control points and remaining in the possession of them is the goal of a trooper. Since the opponents are also going to follow the same goal, it is important that our troopers be equipped with ammo and shoot at enemies whenever these appear in their visibility range. The reason is that there is always the possibility that the opponents also be equipped with ammo and shoot at our agent, causing it to respawn and lose all the gathered ammopacks. For this reason, agents alternate between the scout / trooper roles whenever they run out of ammo / are equipped with ammo.

The behaviour of a trooper is defined by the {\bf trooperBehaviour} function. A trooper tries to capture the closest control point from his current location and, if they spot an enemy approaching another control point, goes back to re-capture it. If the team is in the possession of all control points, troopers go \emph{spawn camping}, meaning that they engage in an assault meant to force the enemy back into his respawn area and prevent him from escaping and going towards the control points or collecting amopacks.

\begin{algorithm}
\caption{Code for the trooperBehaviour function}
\begin{algorithmic}
\vspace{0.2cm}
\STATE Given:
\begin{itemize}
\item The list of currently visible ammopack locations
\item The list of currently possessed / not possessed control point locations
\end{itemize}
\vspace{0.2cm}
\STATE Do the following:
\vspace{0.2cm}
\IF {enemy is nearest to CP we own}
\STATE Start going back to defend / re-capture
\ENDIF

\IF {agent still does not have any goal}
\IF {there is at least one unpossessed control point}
\STATE Set goal to the closest unpossessed control point
\ELSE \IF {all control points are possessed \AND agent has ammo}
\STATE Go spawn camping
\ELSE
\STATE Occupy a random control point
\ENDIF
\ENDIF
\ENDIF

\IF {everything else fails}
\STATE Stop being a scout
\STATE Become a trooper
\ENDIF
\vspace{0.2cm}
\end{algorithmic}
\end{algorithm}

A problem that we faced in our experiments was that troopers were shooting at own teammates whenever the latter ones were being positioned between the former and enemies entering the visibility range. To overcome this, we implemented a feature that allows agents to shoot whenever there is no obstacle standing between them and the enemies (such as a wall), as well as a teammate. Another problem we faced was that agents from the same were occasionally colliding with each other and getting stuck when they were going for a goal. To overcome this, we implemented a feature that enables the agent to change position if it has not moved for a pre-defined number of stages.

\section{Experimental Setup}
\label{sec:experimental_setup}
In this section, we describe the experiments we perform in order to assess the performance of our agents. Each experiment consists in running a number of 100 games with default game settings between our agent and fixed opponents. To this point, we divide our experiments into two main categories: \emph{agent progress} and \emph{feature progress}. 

\subsection{Agent Progress}
\label{sub:agent_progress}
For the first category, we assessed the overall performance of our agent throughout the tournament in terms of average won games in order to see whether the features we included at each stage proved effective or not. We tested each of the three versions (for each stage of the tournament) we submitted against the default agent, as well as against the corresponding versions of the agents submitted by the two teams (team 1 and team 5) that ended up right ahead us in the final tournament.
\subsection{Feature Progress}
\label{sub:feature_progress}
For the second category, we assessed the performance of individual features that we included in the agent. For this purpose, we performed two experiments. In the first experiment, we tested the impact of varying the maximum number of scouts that are sent to collect ammopacks on the performance of the agent. We refer to this as \emph{Scouts Experiment}. In our agent, the number of scouts is a function of the number of control points that are not possessed, given by the formula
\begin{equation*}
\mbox{max\_scouts} = \dfrac{\mbox{team\_size}}{\mbox{nr\_of\_not\_poss\_cps} + 1}
\end{equation*}

This means that whenever there is a larger number of control points that are not possessed, a smaller number of agents become scouts and, therefore, a larger number of agents are sent to take over the control points. In this experiment, we also tested the performance in terms of average number of won mathces against the opponents when:
\begin{itemize}
\item The number of scouts is equal to the number of troopers, namely
\begin{equation*}
\mbox{max\_scouts} = \dfrac{\mbox{team\_size}}{2}
\end{equation*}
\item The number of scouts depends of the number of control points that are possessed, namely 
\begin{equation*}
\mbox{max\_scouts} = \dfrac{\mbox{team\_size}}{\mbox{nr\_of\_poss\_cps} + 1}
\end{equation*}
\end{itemize}

In the third and final experiment, we tested the agents' ability of correctly estimating the availability of ammopacks. We refer to this as \emph{Ammopacks Experiment}. Since agents do not have full observability of the world, they cannot tell at each stage whether ammopacks are available at certain locations. For this reason, they need to estimate the respawn time and go for an ammopack only the moment they consider something is to be found at that specific location.

\section{Experimental Results}
\label{sec:experimental_results}
In this section, we provide the results of our experiments.

\subsection{Agent Progress}
While our agent constantly improved over the default agent, the features we implemented were not sufficient to make the agent always win against other opponents. Table \ref{table:1} shows the number of won matches (out of 100) against the default agent and the two teams that ended up right ahead us, for each version we submitted.
\begin{table}[!h]
\centering
\begin{tabular}{|c|c|c|c|}
\hline
& Default agent & Team 1 & Team 5 \\
\hline
Tournament 1 & 73 & 23 & 2 \\
\hline
Tournament 2 & 99 & 94 & 20 \\
\hline
Tournament 3 & 100 & 82 & 19 \\
\hline
\end{tabular}
\label{table:1}
\caption{Number of wins of our agent against different opponents}
\end{table}

\begin{figure}[!h]
\centering
\includegraphics[scale = 0.7]{progress.png}
\end{figure}

In tournament 2, there were some improvements regarding the behaviour of different classes of agents and goal selection, along with development of better cooperation and decision making methods.

First of all there was a difference between the divisions of agents in the two classes, as described in the \emph{Experimental Setup} section. The "Greedy Goal" was introduced that integrated the ammopack hunting, disregarding the class if needed. Extra features were added in the behaviour of the two agent classes; the trooper would now return to a control point to defend it if there was an enemy heading that way, while a scout would stop being a scout if everything failed, meaning if there were no ammopacks that could be hunt down and nothing else to explore.

Regarding the cooperation among the agents, there was an ammopack updating method available, which involved a list being shared between the agents. According to that list of "updated" ammopacks, the agents would avoid trying to visit the same ammopack in the same round. This saved a lot of unnecessary moves. Concerning the various decision making methods, one good example would be that the enemy that would be shot at, was selected after some more strict criteria. Main conditions also applied, like being in the right distance with no walls or fellows inside the line of fire, but there was a list generated with all those available to shoot at. From that list the best candidate would now be selected by the result of checking for whom the agent needed the least angle degrees for the turn action. Together with this minimum turn, there were many other functions computing the best distance from a target, used throughout the code.

In tournament 3, collision detection was employed. A list containing a fixed number of self previous locations on the map was retained by each agent. An agent would detect whenever its location on the map had not changed for a couple of rounds and change its orientation in order to escape and reach the current goal. This feature did not improve performance significantly, but it allowed agents from the same team to escape whenever they were getting stuck into each other while going for the goals. 

Another feature that was added was goal sharing. Since multiple agents could be assigned the same goal throughout the game, we limited the maximum number of agents that could go for the same goal, to a pre-defined constant. The rest of the agents were assigned the task of capturing the control points. This feature did not perform well in the final tournament, as the same version of the agent but without goal sharing proved to be more effective against the opponents.

\subsection{Feature Progress}
\subsubsection{Scouts Experiment}

Based on the match results versus the three opponent teams, employing less scouts to collect ammopacks whenever more control points were being in the possession of the agent, did not make any substantial difference. With the number of scouts being half of the team, no further improvements could be observed either. A probably better strategy would have been to first send the agents collect all the available ammopacks and then assign scout roles to agent whenever they would run out of ammo.

\begin{table}[!h]
\centering
\begin{tabular}{|c|c|c|c|}
\hline
& Default agent & Team 1 & Team 5 \\
\hline
Not Possessed & 99 & 100 & 16 \\
\hline
Possessed & 98 & 92 & 27 \\
\hline
Equal & 100 & 93 & 19 \\
\hline
\end{tabular}
\label{table:2}
\caption{Number of wins for the Scouts Experiment}
\end{table}


\subsubsection{Ammopacks Experiment}
\begin{table}[!h]
\centering
\begin{tabular}{|c|c|c|c|}
\hline
& Default agent & Team 1 & Team 5 \\
\hline
Not Possessed & 0 & 0 & 0 \\
\hline
Possessed & 0 & 0 & 0 \\
\hline
Equal & 0 & 0 & 0 \\
\hline
\end{tabular}
\label{table:2}
\caption{Number of wins for the Ammopacks Experiment}
\end{table}

\section{Reinforcement Learning}
\label{sec:reinforcement_learning}

Extensive research has been done over the last decades in order to come up with algorithms that are able to determine optimal solutions for decision-making problems. Some of the first algorithms were developed for single-agent MDP problems for which the agent has full knowledge of the current state and transitions. These include value iteration, policy iteration, dynamic programming and variants of them \cite{boutilier}.

In case of decision-making problems where the world is partially observable (POMDPs), it is much harder to determine an optimal solution. At every stage, the agent does not have full knowledge of the current state of the world. Problems with partial observability introduce a so-called \emph{belief space}, which represents a probability distribution over the state space that allows the agent to estimate the current state of the world. Therefore, finding optimal policies for POMDPs is a much more complex task than finding optimal policies for MDPs in the single-agent case. In the case of multi-agent problems, this complexity is even further increased. 

For this reason, algorithms that approximate the solutions of POMDPs are preferred, although they come at the cost of a less accurate solution. In case of multi-agents, the methods that are used to learn a policy can be divided into two categories: \emph{supervised learning} and \emph{reinforcement learning}. In the former one, the learning algorithm uses a set of \emph{training examples} in order to learn an optimal policy. In the latter one, the agent needs to explore the environment in order to determine the model of the world, which is previously unknown. Some of the algorithms that determine an optimal policy through reinforcement learning include Q-Learning, Dyna, TD($\lambda$) and variants of them \cite{kaelbling}.

In case of multi-agent systems with multiple learners, the environment is non-stationary, meaning that it is not sufficient to just train the agent before beginning the task (offline learning) and then apply the rules described by the optimal policy. Instead, the agent needs to adapt its policy at each stage during the game. This makes it more difficult to converge to a good strategy than it is in the case of single-agent systems. For this reason, we chose to implement a rule-based system in \emph{Domination}, since time is an important issue in the game and online adaption might exceed the maximum time alloted per agent at each stage. 

Apart from programming the agents in a rule-based manner, we experimented with reinforcement learning. In reinforcement learning the actions that an agent takes in a particular situation (state) are learned instead of coded by hand. The agents learns, by linking state-action pairs to (future) rewards, for every state the agent should pick the action with the highest reward. During learning, agents should not always pick the action with the highest reward (greedy policy), instead with some probability they should explore. This way agents will converge to a globally optimal policy.

To apply reiforcement learning techniques to the domination game we would like the following to hold:
\begin{itemize}
\item We want to incorporate all that (environmental) information that is known to the agents into the state description (including an opponent model)
\item We want the agents to do the action selection in a centralised / coordinated way, and selection based on joint rewards
\end{itemize}

We are convinced that if we can satisfy these two conditions, a team that efficiently learns will outperform rule-based agents.
If you are familiar with the domination game and the field of reinforcement learning, you will have the intuition this is an extremely hard problem. Modeling all this information and state-action pairs will easily become computationally intractable. 
Condition 1 would make the state-action space continuous (or at its best enormous).
Condition 2 would require an extended version of the Dec-POMDP framework. 
Although the number of agents, control points, ammopacks and actions are rather small, a space that combines them all becomes enormous due to exponential factors.
In the papers by van Hasselt \cite{continuous} and Oliehoek \cite{decpomdp} both continuous state-action spaces and Dec-POMDPs are discussed. Both of them result in a computational complexity that is proven to be intractable, when computing a globally optimal policy.

As both conditions can't be fully met, some concessions have to made. By making these concessions, we lose guarantees of arriving at a global optimum. The question that arises is if the local optimum that can be found results in a performance that can compete with rule-based agents. Later on we will discuss this question further.

Right from the beginning we noticed three things, the domination game is about:
\begin{itemize}
\item The trade off between going for control points or ammopacks
\item The trade off between exploiting known ammolocations and searching for more ammolocations
\item A team is 'guaranteed' to win if it has ammo and it locks the enemy agents in at their spawn area
\end{itemize}
The model should contain enough information to learn solutions to these three subproblems.

\subsection{State space}
\label{subsec:state_space}
We started by discretizing the state space. The state space for every agent consisted of:
\begin{itemize}
\item log2 of distance to 'best' controlpoint (distances larger than 5 where mapped to the value 5)
\item log2 of distance to 'best' ammopack (distances larger than 5 where mapped to the value 5)
\item log2 of distance to spawn area of enemies (distances larger than 5 where mapped to the value 5)
\item number of controlpoints that are are held by the team
\item number of ammopacks that were discovered (use symmetry in levels)
\item number of ammopacks the agent has in its possession (upperbound of 5)
\end{itemize}
Note that by taking the logarithm we obtain more detailed information when we are closer to the locations and less detailed information when we are further away.

Every agent learned on its own (they should all learn the same policy so we allowed them to share the learned knowledge, see section about Q-learning).
This is already an enourmous concession because the agent does not capture other agents in it's state space, and there is no communication between the agents, advanced coordination can never be achieved by our implementation of reinforcement learning.

How did we select the 'best' points of interest (controlpoint and ammopack).
We used selection techniques similar to our implementation of the rule-based agent, like distance to the POI, distance of other agents to the POI. For ammopack it also calculates the likelihood that ammo is available etc.
The shooting action was also hard-coded and based on the rule-based agent techniques (such as determining if a teammate is inbetween you and the enemy before you shoot). If an agent can shoot an enemy player, it always picks the action shoot. Direction in which it is shooting is hard-coded aswell.

\subsection{Action space}
\label{subsec:action_space}
The action space consisted of:
\begin{itemize}
\item Move towards best control point
\item Move towards best ammopack
\item Move towards spawnarea of enemy
\item Move to unvisited area of playfield (determined similar as in rule-based agent)
\end{itemize}

\subsection{Reward signal}
\label{subsec:reward_signal}
It turned out to be quite complicated to think of a good reward signal.
There are basically two options:
\begin{itemize}
\item reward only at the end of the game (e.g. 1 for winning, 0 for draw, -1 for losing)
\item reward at every timestep
\end{itemize}
To fasten up learning one should pick option two. A very interesting advantage of reinforcement learning techniques, is that you can implement them without any bias. We, humans, are always biased when we learn, we have some initial thoughts about how to win this game (therefore we 'converge' very efficiently to a local optimum). But it is hard for humans to find a global optimum when this requires some out of the box thinking. By giving rewards at every timestep we interfer with the 'tabula rasa' way reinforcement learning is capable of. For example if we give a small reward for shooting an enemy, we give the agent a bias for shooting and picking up ammo. This could sound like a good strategy but it actually is nothing more than a biased thought.
As we expected a faster convergence we focussed on rewards at every timestep based on the joint score (number of controlpoints).

\subsection{Opponent}
\label{subsec:opponent}
In order for agents to learn, their enemy should not play too easy or too hard.
Ideally the enemy gets more difficult to beat during the learning process (armsrace).
In selfplay the agent plays against itself. What we were wondering is if convergence to an optimal policy can be obtained if both the agent and opponent update the same Q-value table. This way they would have exactly the same policy for the whole learning process, the question is if this would result in a natural armsrace?
Another option, and the option we chose, was to let the agent play against the default agent in the beginning and later against our rule-based agent.

To summarize our reinforcement learning model, we did basically three things:
-let every agent learn/plan on it's own (decentralized)
-create a compact discrete state and action space
-implement some basic rule-based algorithms for shooting and ammopack/cp selection

\subsection{Limitations of the model}
\label{subsec:limitations}
\begin{itemize}
\item Shooting policy cannot be updated
\item Oponent modelling is not possible
\item Coordination is limited (only indirectly by means of best cp/ap, thus not updated by learning)
\item Reward signals at every timestep (potentially a wrong bias)
\item History is not modelled
\item The default agent could be a too strong opponent to start with
\end{itemize}

\subsection{Q-learning}
\label{subsec:qlearning}
We implemented Q-learning on the model and experimented with different exploration and learning rates. We created one Q-value table for a team of agents, the six agents all updated the same table, this way we expected convergence to happen six times faster.
Due to the rule-based shooting and controlpoint-ammopack selection the agents were forced to always pick a target that made some sense. But what was happening is that agents switched every timestep between actions and therefore almost never reached a target. We implemented commitment sequences \cite{gameth} to minimize this behaviour in order to speed up learning. A commitment sequence is a list of time slots for which an agent is committed to selecting always the same action

\subsection{Conclusion}
\label{subsec:conclusion}
After hours of compuation time for learning we didn't notice significant improvement. We analysed potential problems. A first intuition is that number of state-action pairs (82.944) is too large. We computed the average running time of a game, that is based on computation time per agent, this turned out to be approximately 15 seconds. The absolute lowerbound of games required to try all state-action pairs with games of 600 timesteps is 139. This lowerbound will in fact never be reached as this requires every subsequent state-action pair to be unique for a sequence of 139 games. For future rewards to propagate to earlier state-action pairs many more games are required. Multiplying this large number of games with computation time clarified the fact that we didn't oberserve convergence. Although there are other factors that could be of influence as well; opponent is too hard, reward signal is not representative, state space is too elementary to allow intelligent reasoning etc.
We reconsidered the trade-off of defining a compact state space on the one hand and a very descriptive state space, to allow for more intelligent reasoning, on the other hand. As described in the section limitations the state description had already many limitations, making it even smaller would make it even less likely to allow for intelligent reasoning.
Therefore we concluded that reinforcement learning techniques have interesting advantages and a lot of potential, but with our hardware and time constraints reinforcement learning did not have the potential to outperform ingenious developed rule-based agents.
Focussing on smaller subproblems to be solved by reinforcement learning could be a feasible option although we question their utility over completely rule-based agents.\footnote{What would maybe be interesting for next year's course is to have more competition focus on reinforcement learning techniques. E.g. all teams get a rule-based agent that leaves some subproblems to be solved by reinforcement learning techniques. For the competition all teams should be constrained to using this 'template agent' and reinforcement learning techniques.}

\section{Conclusion}
\label{sec:conclusion}
In this paper, we presented our approaches for the \emph{Domination} game. While a rule-based system was easier to implement and usually less computationally expensive, it proved difficult to find strategies that work effectively against all of the opponents. Our experiments have shown that tunning parameters that describe the behaviour of the agent does not significantly increase the overall performance. Instead, strategically better rules should be seeked in order to make a difference.

On the other hand, a learning system, despite its potential, proved to be an infeasible solution due to hardware and time constraints. Probably a better performance would be obtained by defining a set of rules that describe the behaviour of the agent and learning which one should be used in each situation, based on the previous experience. We plan to employ these changes in a future project.



\begin{thebibliography}{99}
\bibitem{domination} T. van den Berg and T. Doolan, \emph{Domination Game}, 2012, \url{https://github.com/noio/Domination-Game/}
\bibitem{boutilier} C. Boutilier, T. Dean, S. Hanks, \emph{Decision-Theoretic Planning: Structural Assumptions and Computational Leverage}, Journal of Artificial Intelligence Research, Vol. 11, 1-94, 1999.
\bibitem{kaelbling} L. P. Kaelbling, M. L. Littman, A. W. Moore, \emph{Reinforcement Learning: A Survey}, Journal of Artificial Intelligence Research, Vol. 4, 237-285, 1996
\bibitem{continuous} H. Hasselt, \emph{Reinforcement Learning in Continuous State and Action Spaces}, Reinforcement Learning: State of the Art, Chapter 7, Unpublished manuscript, 2012.
\bibitem{decpomdp} F.A. Oliehoek, \emph{Decentralized POMDPs}, Reinforcement Learning: State of the Art, Chapter 15, Unpublished manuscript, 2012.
\bibitem{gameth} A. Nowe�, P Vrancx, Y. De Hauwere, \emph{Game Theory and Multi-agent Reinforcement Learning}, Reinforcement Learning: State of the Art, Chapter 14, Unpublished manuscript, 2012.
\bibitem{gameAIworks} B. Hardwidge, \emph{How AI in Games Works}, bitGamer, 2009, \url{http://www.bit-tech.net/gaming/2009/03/05/how-ai-in-games-works/}.
\bibitem{behaviourtrees} R. Pillosu, \emph{Coordinating Agents with Behavior Trees}, Paris Game AI Conference, 2009, \url{http://files.aigamedev.com/coverage/GAIC09_CoordinatingAgents_RicardoPillosu.pdf}.
\bibitem{gameAIconf} A. J. Champandard, \emph{Paris Game AI Conference �09: Highlights, Photos \& Slides}, 2009, \url{http://aigamedev.com/open/coverage/paris09-report/}.
\bibitem{starcraftcomp} Expressive Intelligence Studio at University of California, \emph{AIIDE 2010 Starcraft Competition}, 2010, \url{http://eis.ucsc.edu/StarCraftAICompetition}.
\bibitem{overmind} Dan Klein et al., \emph{The Berkeley Overmind Project}, 2010, \url{http://overmind.cs.berkeley.edu/}.
\end{thebibliography}

\end{document}


